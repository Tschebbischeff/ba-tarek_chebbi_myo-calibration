This folder is a snapshot of the complete files in a git repo as they were when the thesis was printed.
It includes gitignored files and only the .git folder was removed.
This applies only if this file and the following folders were found on a DVD in the back of the thesis)


This folder contains the following main folders:

/source/MyoCalibration

 Contains the implementation of the calibration method. Including project files for use with IntelliJ IDEA.
 The folder MyoCaLib contains the library, which can be compiled as an .aar for use with other
 Android Apps. The folder ExampleApp contains an undocumented example application, that uses the
 library to connect to Myos and record their raw, as well as their calibrated data.
 It is necessary to either compile an .aar or import the MyoCaLib folder as a module in another
 project. Please see the MyoCaLib/build.gradle for the necessary build parameters.
 
/stuff

 Contains various assets used while writing the thesis.
 "3d prints" contains some 3d printable files planned to be used in the analysis of the gyroscope errors.
 evaluation contains the MATLAB files containing the code that simulated the neural networks for the
 evaluation as well as the data that was procured for the evaluation in their raw and processed forms
 (folder evaluation/data). The python script generates a confusion matrix svg.
 "matlab_analysis" contains highly unsorted data and files used for various analyzing graphs of the
 accelerometer, gyroscope and EMG, while creating the calibration method.
 
/thesis

 Contains all latex files and figures from which the thesis can be compiled.