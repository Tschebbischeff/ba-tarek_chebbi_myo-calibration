\select@language {english}
\contentsline {paragraph}{Kurzfassung}{v}{section*.1}
\contentsline {paragraph}{Abstract}{v}{section*.2}
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}
\contentsline {chapter}{\numberline {2}Background}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}Inertial Measurement Unit}{3}{section.2.1}
\contentsline {section}{\numberline {2.2}Electromyography}{6}{section.2.2}
\contentsline {chapter}{\numberline {3}Design}{9}{chapter.3}
\contentsline {section}{\numberline {3.1}Inertial Measurement Unit}{10}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Accelerometer Errors}{10}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}Gyroscope Errors}{14}{subsection.3.1.2}
\contentsline {subsection}{\numberline {3.1.3}IMU Calibration}{17}{subsection.3.1.3}
\contentsline {section}{\numberline {3.2}Surface Electromyography Sensors}{18}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Noise}{19}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Rotation}{20}{subsection.3.2.2}
\contentsline {subsection}{\numberline {3.2.3}Normalization}{23}{subsection.3.2.3}
\contentsline {subsection}{\numberline {3.2.4}Translation}{26}{subsection.3.2.4}
\contentsline {chapter}{\numberline {4}Implementation}{29}{chapter.4}
\contentsline {section}{\numberline {4.1}User Interface}{29}{section.4.1}
\contentsline {section}{\numberline {4.2}API}{30}{section.4.2}
\contentsline {section}{\numberline {4.3}Back End}{32}{section.4.3}
\contentsline {section}{\numberline {4.4}Expandability}{33}{section.4.4}
\contentsline {chapter}{\numberline {5}Evaluation}{35}{chapter.5}
\contentsline {section}{\numberline {5.1}Usability}{35}{section.5.1}
\contentsline {section}{\numberline {5.2}Gesture Recognition}{36}{section.5.2}
\contentsline {chapter}{\numberline {6}Discussion}{39}{chapter.6}
\contentsline {section}{\numberline {6.1}Usability}{39}{section.6.1}
\contentsline {section}{\numberline {6.2}Gesture Recognition}{39}{section.6.2}
\contentsline {section}{\numberline {6.3}General Discussion}{40}{section.6.3}
\contentsline {chapter}{\numberline {7}Conclusion}{41}{chapter.7}
\contentsline {section}{\numberline {7.1}Future Work}{41}{section.7.1}
\vspace *{\baselineskip }
\contentsline {chapter}{Bibliography}{43}{section*.20}
\contentsline {chapter}{List of Figures}{46}{section*.20}
\contentsline {chapter}{List of Tables}{48}{section*.20}
\contentsline {chapter}{\numberline {A}Appendix}{51}{appendix.A}
\contentsline {section}{\numberline {A.1}List of Abbreviations}{51}{section.A.1}
