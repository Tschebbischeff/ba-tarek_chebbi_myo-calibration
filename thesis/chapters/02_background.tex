\chapter{Background}
Calibration of \aclp{IMU} as well as signal processing and feature extraction of \acl{sEMG} data are fields of ongoing research. State of the art techniques from both areas can only be applied to the Myo in limited form, due to its unique layout and fixed properties as a ready-made product. Furthermore, it is not always documented, whether and which signal processing approaches may already be implemented on the device, as this includes proprietary information.


\section{Inertial Measurement Unit}
Inertial measurement units are devices capable of measuring translative accelerations and angular speeds on and around the three spatial axes and are well-known as a common component of any system with the need for measuring movement (\EG{} smartphones, aircrafts, spacecrafts).
They are commercially available in different precision degrees. High performance sensors, as used in industrial, aerospace and defense markets, can however reach prices of multiple thousand dollars, which is simply too costly for integration into a wearable device, designed for consumers. Low-cost \acp{IMU}, as the ones found in mobile phones for example, are more appropriate for such devices, at the drawback of being less accurate.
Modern \acp{IMU} typically consist of a three-axis accelerometer and a three-axis gyroscope, and are therefore called \acp{IMU} with six degrees of freedom. Sometimes a three-axis magnetometer is included in the \ac{IMU}, raising the \acp{DOF} from six to nine.

A three-axis accelerometer can consist of either a single \ac{MEMS} measuring the linear acceleration on all three spatial axes, or three separate \acp{MEMS}, that are assembled on orthogonal planes and measure the linear acceleration on a single spatial axis each.
The second configuration can increase a sensor error called axis-misalignment significantly, furthering the need for a calibration method that counteracts this error in such cases.
The three-axis gyroscope measures the angular speeds around the three spatial axes. The type of gyroscope used depends on the field of application, since the choice significantly influences the quality of the measurements. Gyroscopes range from high-tech fiber-optic gyroscopes and ring laser gyroscopes to \acp{MEMS}-based devices, usually utilizing vibrating parts to measure the angular speeds \cite{acar_mems_2008}.
Magnetometers are devices capable of determining the direction and the strength of the magnetic field surrounding it. In low-cost \acp{IMU} the magnetometers are often \acp{MEMS}-based devices, because they are cheap in mass production and are small in size.%Don't really like this

Accelerometers, gyroscopes and magnetometers can suffer from different sensor errors, which are especially important when the measurement needs to be integrated once or even twice (\EG{} for determining the current position from accelerometer measurements). Existing literature documents the different kinds of errors visible in accelerometer, gyroscope and magnetometer readings very well \cite{tedaldi_robust_2014,cheuk_automatic_2012,flenniken_characterization_2005,novatel_2016}.

The main sensor errors discussed in current literature and the factors influencing them include:
\begin{description}
	\item[\textbf{Axis-misalignment}] results from the non-orthogonality of the three measuring axes. The magnitude of this error is highly dependent on the design of the sensor. A second type of axis-misalignment is the misalignment between two different sensors in an \ac{IMU}. Calibration methods for \acp{IMU} attempt to align all sensor's measuring axes to the corresponding axis in the \ac{IMU}-frame, eliminating both types of axis-misalignment in the process.
	\item[\textbf{Scaling factor}] describes how the magnitude of the sensor's real output relates to the input.
	For example in case of an accelerometer the measurement influenced by scaling errors could be $1.02g$
	while the \ac{IMU} is completely stationary. This would correspond to a scaling error of $2\%$, since the input should be $1g$. This linear scaling factor can be eliminated by rescaling the measurement vector. However, in most cases, the error increases with increasing magnitude of the measurement.
	\item[\textbf{Bias}] is the offset of the sensor's measurement from the expected output. The bias is measured at an expected output of zero, to compensate for scaling errors, that might influence the measurement. The compensation of the bias is important especially for dead reckoning position estimation, as the acceleration is integrated twice to obtain a position estimate. Large bias therefore introduces a very large error in the estimated position, which grows with time and is called sensor drift.
	\item[\textbf{Random noise}] is inevitably present in each electronic measurement device and can be filtered under certain conditions. Noise is assumed to be normally distributed and to have zero-mean. Bandpass filters are well suited to filter low- and high-frequency noise from the sensor's measurements.
	\item[\textbf{Stability}] or in-run stability of a sensor error is a measure of how constant that error remains over a given timespan of consecutive measuring. In contrast the turn-on to turn-on stability describes the change in the error, when the sensor is shut down and powered up at a later point in time.
	The stability influences some errors more than others, for example the axis-misalignment should not change significantly over time at all, whereas the bias can change slowly during a session of consecutive measurements, owing to physical factors. 
	\item[\textbf{Environment-sensitivity}] describes how sensitive the measurements are to the physical factors influencing the stability.
	The main environmental influences are changes in temperature and acceleration. For example, high sensitivity to acceleration essentially introduces nonlinearity in the scaling error of the sensors, while temperature changes may impact the bias.
\end{description}

\begin{figure}[tbp]
	\begin{center}
		\includegraphics[width = 0.8\textwidth]{figures/sensor_errors.eps}
		\caption[IMU sensor errors]{\label{sensorerrors}A graph visualizing bias, scaling factor and noise in the context of a general sensor. The perfect sensor reaction is depicted as the green line.
		\\(Adapted from NovAtel \cite{novatel_2016})}
	\end{center}
\end{figure}

While the technical specification of the Myo armband lists a 9\acp{DOF} \ac{IMU}, the data from the magnetometer is inaccessible, which reduces the \ac{IMU} to 6\acp{DOF} at the point of implementation of the developed calibration method.
%In \ac{IMU} calibration and dead-reckoning systems the magnetometer is used mainly to stabilize the output of \acp{IMU} over long periods of time though \citeme, which is of less relevance for the task at hand, since the Myo armband has an average battery life of \textcolor{red}{two hours [check me!]} of continued use (\IE{} sending the \ac{IMU} and \ac{EMG} data to a connected device).
The Myo specification also does not list the types of the accelerometer and gyroscope embedded in the \ac{IMU}. The existence and magnitude of the aforementioned errors must therefore be tested to determine which errors the calibration method has to process.

Calibration of 6\acp{DOF} and 9\acp{DOF} \acp{IMU} is a topic discussed in various existing research papers and theses.
While the comparison of sensor outputs with data generated by specifically designed calibration instruments is a well known and working method to counteract common sensor errors, the method is highly dependent on the accuracy of the reference data provided, which in turn is mainly based on the quality of the used instruments.
The general approach is described in detail by Titterton and Weston \cite{titterton_strapdown_2004}, while specific uses are researched for example by Kim and Golnaraghi, who use an optical tracking system to obtain the calibration data \cite{kim_initial_2004}, or Grewal \ETAL{}, who utilize Kalman Filtering to calibrate a 6\acp{DOF} \ac{IMU} in their work \cite{grewal_application_1991}.

The goal of the calibration method developed in this thesis is to be executed by a user in-field, though, and thus no specific external equipment apart from the user's smartphone can be used in the process.
Such a calibration method, utilizing only the stable gravity vector and positioning of the \ac{IMU} by the user without any external equipment, is presented and discussed by Fong \ETAL{} \cite{fong_methods_2008} and Tedaldi \ETAL{} \cite{tedaldi_robust_2014}.

\section{Electromyography}

%\Acl{EMG}
%TODO?
Electromyography is a technique used in medicine to study muscles or the motor neurons controlling their contraction. Beginning in 1960, \ac{EMG} was used for diagnosis and treatment of different diseases and defects by a group of scientists in Russia \cite{sherman_russian_1964}.
\ac{EMG} measures the activity of a muscle by measuring the electric potential originating in the action potentials of the neurons innervating the targeted muscle. These innervating motor neurons and the innervated muscle are collectively called motor unit, while the sum of the motor neurons' action potentials is called a \acl{MUAP} accordingly. Consecutive \acp{MUAP}, recorded within a short time interval are called \acp{MUAPT}.

The usage of electrodes to measure the \ac{MUAP} or \acp{MUAPT} is well explained in literature on the topic \cite{basmajian1985muscles}. Two types of electrodes are used: Non-invasive \textit{surface electrodes} placed relatively tight on the skin, and  more invasive \textit{needle electrodes} or \textit{wire electrodes}, which are inserted directly into the muscle of the subject. Surface electrodes are better suited for recording the activity of the whole muscle the electrode is placed on, while needle and wire electrodes are used to measure signals of specific motor units.

\begin{figure}[tbp]
	\subfigure[Surface electrodes]{\includegraphics[width = 0.31\textwidth]{figures/emg_surface_electrodes_myo.eps}} \hfill
	\subfigure[Wire electrodes]{\includegraphics[width = 0.31\textwidth]{figures/emg_wire_electrodes.eps}} \hfill
	\subfigure[Needle electrodes]{\includegraphics[width = 0.31\textwidth]{figures/emg_needle_electrodes.eps}}
	\caption[EMG electrode types]{\label{emgelectrodetypes} Typical examples of the three electrode types.
		\\(a) Surface electrodes used by the Myo armband.
		\\(b) Wire electrodes, which are inserted into the muscle with the needle and remain inside after the needle has been removed. (Image courtesy of Microprobes for Life Science \cite{emg_wire_electr_pic})
		\\(c) The basic schematic of multiple types of needle electrodes, black parts indicate insulation. (Adapted from Henneberg \cite{henneberg1995biomedical})}
\end{figure}

Recording \ac{EMG} data with either electrode type is traditionally done in either a monopolar or a bipolar configuration.
Apart from a reference electrode, which is used in both configurations and usually placed in a bony area or electrically unrelated tissue, the monopolar configuration uses an electrode with a single electronic contact surface at the analyzed muscle, while the bipolar configuration uses an electrode with two contact surfaces situated in close proximity.
The monopolar setting amplifies the signal from the electrode with respect to the reference electrode. The bipolar setting amplifies the difference of the two signals. The resulting advantage of the bipolar electrode configuration is, that noise originating far away from the electrode (\EG{} power line noise) is automatically filtered out, because, for each point in time, the noisy part of the measured signal is the same at the two detection surfaces, while the \ac{EMG} signal reaches one detection surface before the other and therefore is still part of the differential signal.

\begin{figure}[tbp]
	\subfigure[]{\includegraphics[width = 0.48\textwidth]{figures/monopolar_emg_configuration.eps}}\hfill
	\subfigure[]{\includegraphics[width = 0.48\textwidth]{figures/bipolar_emg_configuration.eps}}
	\caption[EMG electrode configurations]{\label{emgmonopolarbipolar}Monopolar (a) and bipolar (b) electrode configurations for measuring \ac{EMG} signals in a muscle. Note that the detection electrode can also be a surface electrode.
		\\(Adapted from Basmajian and De Luca \cite{basmajian1985muscles})}
\end{figure}

As a wearable device, the Myo armband uses non-invasive surface \ac{EMG} electrodes to acquire the \ac{EMG} signals from the multiple muscles in the user's forearm.
%The contact surfaces of each electrode are presumably placed in a double differential configuration, as seen in \reffig{emgelectrodetypes} (a).
The exact way the Myo calculates the \ac{EMG} data is proprietary information. However, the setting of the three contact surfaces of each pod, as seen in \reffig{emgelectrodetypes} (a), resembles the double differential configuration, where the amplified differentials of the outer contact surfaces with the middle surface are fed into a differential amplifier again.
In contrast to the simple bipolar configuration, which is also called single differential technique, this setting substantially reduces crosstalk, which is the contamination of an \ac{EMG} signal with signals produced by other muscles than the one directly beneath the electrode \cite{koh_evaluation_1993}.
The signals measured by the eight \ac{EMG} sensors are robust against electrical noise originating from outside the body and device as well, since the double differential setting includes the simple bipolar configuration.
Usage of signal processing techniques to filter out noise from the \ac{EMG} data is implicitly declared by Thalmic in their release of the Bluetooth protocol, where the specification lists a mode in which the \ac{EMG} data can also be received without filtering of electrical noise \cite{thalmic_myo_bluetooth_spec}.
The signal is, however, contaminated by noise originating from the device itself and additionally highly dependent on properties inherent to the user's body as well as the placement of the armband on the user's body.
The most important sources that influence the signal picked up by the eight \ac{EMG} sensors of the Myo armband are:

\begin{description}
	\item [\textbf{Random noise}] As every electronics device the Myo is generating some baseline noise by itself. Since this noise factor can be different for each contact surface of the \ac{EMG} electrode, it is not filtered out in the differential. Additional noise is generated by the body-electrode interface. Minimal, inevitable movements of the contact surfaces on the user's skin can also introduce small movement artifacts, that result in quasi-random noise.
	\item[\textbf{Body fat}] The thickness of the subcutaneous fat layer at the point the \ac{sEMG} electrode is positioned, is directly correlated to the amplitude of the \ac{EMG} signal, as reported by Nordander \ETAL{} \cite{nordander_influence_2003}. Normalization by means of a \ac{MVC} or \ac{RVC} are commonly used to align signals of different users.
	\item[\textbf{Muscle fatigue}] While the muscle is fatiguing, different effects can be observed in the \ac{EMG} amplitude and in the Fourier transform of the \ac{EMG} signal. Shifts of the center frequencies in  Fourier power spectra are reported by Petrofsky \ETAL{}, while Moritani \ETAL{} describe amplitude changes in detail and found the amplitude of the signal decreasing over time when sustaining a \ac{MVC}, in contrast to increasing when sustaining sub maximal voluntary contractions \cite{petrofsky_evaluation_1982, moritani_intramuscular_1986}.
	\item[\textbf{Skin temperature}] Changes in electrode and skin temperature directly affect the \ac{EMG} amplitude. Therefore, it is desired to establish a stable temperature before working with the armband, to avoid changes in the data during relevant recordings \cite{winkel_significance_1991}.
	\item[\textbf{Armband rotation}] Rotating the armband around the arm results in different \ac{EMG} signals being picked up by an individual \ac{sEMG} sensor, as the muscles directly beneath it and in close vicinity change.
	This is especially important if crosstalk is reduced due to the Myo employing the double differential technique, which results in a signal local to the area directly beneath the sensor.
	\item[\textbf{Armband translation}] Due to the anatomy of the forearm and the layout of the multiple superficial muscles, translation of the armband can introduce differences in the recorded signals as well.
	Depending on pro- and supination of the arm the superficial forearm muscles are either mostly parallel from elbow to wrist or spiral around the ulna and radius. Translation of the armband results in different superficial muscles being monitored by the same electrodes. The muscle is also thicker in the upper region of the forearm, additionally affecting the recorded \ac{EMG} signal.
\end{description}

The signal picked up by the Myo's \ac{sEMG} sensors is scaled to values fitting a signed byte (\IE{} values from $-127$ to $128$) and sent to a receiving device via Bluetooth with a frequency of $100 Hz$. However, two \ac{EMG} data packets are arriving in that frequency, with one's data corresponding to a measurement $5 ms$ earlier. The effective streaming rate of the \ac{EMG} data is therefore $200 Hz$.
%The \ac{EMG} signal is erratic in nature and needs further processing to be of any use. The standards for reporting \ac{EMG} data contain specifications of digitally processing the data \cite{merletti_standards_1999}.
%To extract information about the amplitude and envelope of the signal, it is rectified either by only considering positive values (half-wave rectification) or by taking the absolute value (full-wave rectification).
%The rectified signal is then smoothed with either a moving average or a low-pass filter over a given time interval. Depending on the implementation, this can introduce a time-shift in the signal.
%Common used features of the processed signal include, but are not limited to, the \ac{ARV}, \ac{MAV} or the \ac{RMS}. Some features like the \ac{ZCR} require the signal to be unprocessed, however. When the signal is normalized, it is either expressed as percentage of a \acl{MVC} or \acl{RVC}.