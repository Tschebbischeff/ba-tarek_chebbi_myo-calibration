\chapter{Evaluation}

The presented Android library was used in a recording application to evaluate the performance of the calibration method. Calibrated data and raw data supplied by the library were recorded simultaneously and stored for later analysis.
The application used the default configuration of the library, apart from the calibration of the \ac{IMU} biases. To show the participants the full sequence of steps required to calibrate a Myo, the bias calibration was enabled.
% which was enabled to show the participants the full sequence of steps required to calibrate a Myo.
A total of fifteen subjects gave informed consent, took part in a usability study and executed multiple gestures for a performance analysis of the presented method.
The subjects ($N=15$, $6$ female, $9$ male) are from technical as well as non-technical fields.

\section{Usability}

The subjects were given the task to connect to the Myo and calibrate it.
Thereafter, they were only shown the first screen of the example application, which hinted at clicking a button that subsequently took them to the main screen of the calibration library.
All subjects successfully completed the task and filled out a questionnaire with statements regarding the usability of the library from a user's point of view. The questionnaire is based on the \ac{SUS}, developed by John Brooke, and as such each statement was rated on a scale from "Strongly disagree" to "Strongly agree". It was available in English as well as German, depending on the participants preferred language. For clarification, the statements of the questionnaire were adapted to refer to "the application" instead of "the system".

The full results of the usability questionnaire are compiled in \reftbl{tbl_usabilityscores}.

\begin{table}
	\begin{center}
		\begin{tabular}{ c | c c c c c c c c c c | c }
			Statement Number: & $1$ & $2$ & $3$ & $4$ & $5$ & $6$ & $7$ & $8$ & $9$ & $10$ & $2.5 * \sum$ \Bstrut\\
			\hline
			Subject 1 & $3$ & $4$ & $4$ & $4$ & $4$ & $4$ & $4$ & $4$ & $3$ & $4$ & ~$95$ \Tstrut\\
			Subject 2 & $3$ & $4$ & $4$ & $4$ & $4$ & $4$ & $3$ & $4$ & $4$ & $4$ & $95$ \\
			Subject 3 & $3$ & $3$ & $3$ & $4$ & $3$ & $3$ & $3$ & $4$ & $3$ & $3$ & $80$ \\
			Subject 4 & $4$ & $4$ & $4$ & $4$ & $4$ & $4$ & $4$ & $4$ & $4$ & $4$ & $100$ \\
			Subject 5 & $4$ & $4$ & $4$ & $4$ & $4$ & $4$ & $4$ & $4$ & $4$ & $4$ & $100$ \\
			Subject 6 & $4$ & $4$ & $3$ & $4$ & $3$ & $4$ & $4$ & $4$ & $3$ & $4$ & $92.5$ \\
			Subject 7 & $3$ & $4$ & $4$ & $4$ & $4$ & $4$ & $3$ & $4$ & $3$ & $4$ & $92.5$ \\
			Subject 8 & $4$ & $4$ & $4$ & $4$ & $4$ & $4$ & $3$ & $4$ & $4$ & $4$ & $97.5$ \\
			Subject 9 & $3$ & $3$ & $3$ & $4$ & $3$ & $4$ & $3$ & $4$ & $3$ & $4$ & $85$ \\
			Subject 10 & $4$ & $4$ & $4$ & $4$ & $4$ & $4$ & $3$ & $4$ & $0$ & $4$ & $87.5$ \\
			Subject 11 & $3$ & $4$ & $3$ & $3$ & $4$ & $3$ & $3$ & $4$ & $2$ & $3$ & $80$ \\
			Subject 12 & $2$ & $4$ & $4$ & $3$ & $3$ & $4$ & $4$ & $3$ & $2$ & $4$ & $82.5$ \\
			Subject 13 & $4$ & $4$ & $4$ & $4$ & $3$ & $4$ & $4$ & $4$ & $3$ & $4$ & $95$ \\
			Subject 14 & $4$ & $3$ & $4$ & $4$ & $4$ & $3$ & $4$ & $4$ & $2$ & $4$ & $90$ \\
			Subject 15 & $4$ & $3$ & $3$ & $3$ & $4$ & $3$ & $4$ & $4$ & $2$ & $4$ & ~$85$ \Bstrut\\
			\hline
			Sum &   $52$ & $56$ & $55$ & $57$ & $55$ & $56$ & $53$ & $59$ & $42$ & $58$ & ~$1357.5$ \TBstrut\\
			\hline
			Mean & $3.5$ & $3.7$ & $3.7$ & $3.8$ & $3.7$ & $3.7$ & $3.5$ & $3.9$ & $2.8$ & $3.9$ & ~$90.5$ \Tstrut
		\end{tabular}
	\end{center}
	\caption[Usability scores]{\label{tbl_usabilityscores}The individual scores from the usability questionnaire and their sums as well as their means.}
\end{table}

With a value of $90.5$, the total average score of the usability study is located in the top $25$ percent of scores and the usability is best described as "best imaginable" \cite{SUS_Bangor_2009}. However, it is notable that the score of the ninth statement of the questionnaire, which was "I felt very confident using the application.", was rated $25\%$ less than the average of the remaining statements ($55.67$). The statement with the next higher total points is the first statement, which was rated $5\%$ less than the average of the remaining statements ($54.56$).

\section{Gesture Recognition}

After evaluating the usability of the library, the subjects were asked to perform gestures under supervision.
For all but one participant three datasets were recorded, each containing a total of at least ten executions of a single gesture. Each gesture execution is approximately five seconds long and all executions are seperated by pauses of approximately five seconds. The data of the last participant was corrupted and not usable.
Each dataset contains the \ac{IMU}'s accelerometer, gyroscope as well as orientation data and the \ac{EMG} data collected from the eight pods. In case of the calibrated interpolated signal, the function was sampled in $ 45 $ degree steps beginning at $ 0 $ degrees to obtain eight values.
Each user executed the same three gestures, which were making a fist, extending the wrist and flexing the wrist, all while the arm was left hanging.

For comparison of the gesture recognition rates on raw and calibrated data, each dataset's \ac{IMU} values were first merged with its \ac{EMG} values and each of the resulting records was assigned a class of "No gesture", "Fist", "Extension" or "Flexion".

Merging was accomplished by interpolating the \ac{IMU}'s values linearly to match the frequency of the \ac{EMG} data. Full-wave rectification and a centered moving average filter of window size 101 were applied to the mean of the eight \ac{EMG} channels thereafter.
The class was then assigned based on a threshold, which was chosen differently for each dataset, such that the classes were clustered on the gesture execution.
A visualization of the process is shown in \reffig{classassignment}, where the full-wave rectified mean of the \ac{EMG} data and the averaged data as well as the class assignment is shown.

\begin{figure}[tbp]
	\includegraphics[width = \textwidth]{figures/ClassAssignmentExample.eps}
	\caption[Semi-automatic class assignment]{\label{classassignment}The assignment of classes to a dataset, in this case the calibrated data from the second subject performing the fist gesture ten times. The yellow line shows where the "Fist" gesture was assigned (high) and where "No gesture" was assigned (low)}
\end{figure}

After assigning the corresponding classes to each dataset, two main datasets were created for each participant. One containing the calibrated data of all three gesture executions and one containing the raw data. No further feature extraction was performed on the recorded data.
A leave-one-out cross-validation was conducted by training artificial neural networks on $13$ participants at a time and evaluating their performances on the fourteenth subject.
The neural networks were created using MATLAB's \javakeyword{patternnet}, with one hidden layer and a maximum number of $500$ iterations.
The evaluation was tested with hidden layer sizes of $5$, $11$ and $20$ neurons.
A size of $5$ neurons was chosen, as it returned the best results for the raw data as well as for the calibrated data.

After training a total of $50$ networks on the same data, the results of testing the networks on the data of the left out subject were averaged and evaluated by calculating the correctly and incorrectly classified gestures as well as the average \ac{RMSE}. The full results are compiled in \reftbl{tbl_gesturerecognitionresultsraw}, while the confusion matrices are shown in \reffig{gesturerecognitionresultsconfusion}.

\begin{figure}[tbp]
	\centering
	\begin{tabular}{c c}
		& \textbf{Raw Data - Confusion Matrix:} \\
		\rotatebox{90}{~~~~~~~~~~~~~~~~~~~~~~~~~~Real gesture}\quad &
		\subfigure{\includegraphics[width = 0.80\textwidth]{figures/ConfMatrix_RAW_Worse.eps}} \\
		& Recognized gesture \\
		&\\
		&\\
		& \textbf{Calibrated Data - Confusion Matrix:} \\
		\rotatebox{90}{~~~~~~~~~~~~~~~~~~~~~~~~~~Real gesture}\quad &
		\subfigure{\includegraphics[width = 0.80\textwidth]{figures/ConfMatrix_CAL_Worse.eps}} \\
		& Recognized gesture
	\end{tabular}
	%\centering
%	\textbf{Raw data confusion matrix:}\\
	
	%\\
	%Real gesture\\
	%\vspace {0.5cm}
	%\textbf{Calibrated data confusion matrix:}\\
	%\rotatebox[origin=c]{90}{Recognized gesture}
	
	%\\Real gesture\\
	\caption[Evaluation confusion matrices]{\label{gesturerecognitionresultsconfusion}The confusion matrices from the application of neural networks on the calibrated and raw data. The confusion matrices were created using the sum data from the leave-one-out cross-validation performed with each subject.}
\end{figure}

The confusion matrices show that the gesture recognition rate on all gestures is higher for the calibrated data than it is for the raw data.
However, the difference between the respective rates is highest on the fist and extension gestures.