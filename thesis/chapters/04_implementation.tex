\chapter{Implementation}

The implementation of the calibration method is realized in form of an Android library that can be used in different Android applications. This approach is suitable, because the armband is a wearable device and a mobile phone has the advantage of being able to stay in the vicinity of the armband.
The implementation is split into internal classes, which include the back end and the \ac{UI}, and \ac{API} classes.
Only the \ac{API} is meant to be accessed by an application using the library and therefore its classes are combined in the package \javapackage{api}, while classes of the back end and \ac{UI} are collected in the package \javapackage{internal}.
Any reference from \ac{API} classes to classes from the \javapackage{internal} package is hidden by appropriate access modifiers.

\section{User Interface}

As the \ac{UI} is the part of the library that is shown directly to the user, it should be constructed as simple and efficient as possible to allow for easy understanding of the ongoing processes.
It consists of a single Android activity, which can be started with an \ac{API} call, and combines the connection to one or multiple Myo's and their calibration.
Screenshots of the user interface in different stages are collected in \reffig{userinterface}. The connection's and calibration's layouts are split to increase maintainability and are managed by a \javaclass{PageList} construct, which allows switching from and to objects of the \javaclass{Page} class' subclasses. The subclasses are notified about changes in their state (\EG{} whether they are about to be shown or about to vanish) and can set up or finish necessary functionality. The layout of the pages is automatically inflated when the respective page is shown.

To scan the vicinity for active Myo devices using Bluetooth and to store data on the mobile phone, Android applications and libraries need specific permissions.
(a) On the first run, the application explains and asks for the necessary permissions and waits for the user to provide them.
When the permissions are given, the \ac{UI} always automatically switches to (b,c,d,f) a view showing all Myo's that are found via Bluetooth scans and, if connected, (d,f) whether they are calibrated or not.
When a connected Myo, which is not calibrated yet, is clicked, (e) the calibration screen is shown, in which the user is guided through the process step by step.

These steps are realized as modular layouts, which are assembled in a list. The cards each explain what the user needs to do and are shown only until the user provides specific input or until the \ac{EMG} or \ac{IMU} data required by the step is collected.
The modular layouts are referred to as cards and are managed by Android's \javaclass{RecyclerView} construct.
The user interface can be closed by clicking the checkmark button in the top-right corner at any point.
Myos will stay connected and if the calibration was successfully completed, the data is transformed accordingly before the application receives it.

\begin{figure}[tbp]
	\subfigure[Welcome screen]{\includegraphics[width = 0.32\textwidth]{figures/UI_Screen_1.eps}} \hfill
	\subfigure[Connection screen]{\includegraphics[width = 0.32\textwidth]{figures/UI_Screen_2.eps}} \hfill
	\subfigure[Connection screen]{\includegraphics[width = 0.32\textwidth]{figures/UI_Screen_3.eps}}
	\subfigure[Connection screen]{\includegraphics[width = 0.32\textwidth]{figures/UI_Screen_5.eps}} \hfill
	\subfigure[Calibration screen]{\includegraphics[width = 0.32\textwidth]{figures/UI_Screen_6.eps}} \hfill
	\subfigure[Connection screen]{\includegraphics[width = 0.32\textwidth]{figures/UI_Screen_9.eps}}
	\caption[Implementation user interface]{\label{userinterface} The \ac{UI} provided by the library. Shown in the order in which the user usually sees them.
		\\(a) Welcome screen, when the user has not yet given the necessary permissions.
		\\(b,c,d,f) Connection screen in different states, the color coding of the Myo's name refers to whether the Myo is calibrated or not.
		\\(e) Calibration screen, shown when a Myo is clicked.}
\end{figure}

\section{API}

The \ac{API} of the library includes the main \javaclass{MyoCaLib} class, from which everything related to the \ac{API} is reachable. This class is designed as an Android service, so that the same instance can be accessed from multiple places in the implementing application. While general methods, for example to get a list of calibrated Myos, are implemented directly in the class, the settings are encapsulated in the \javaclass{CalibrationSettings} class, which in turn encapsulates the EMG and IMU related settings in two respective classes.

Before the user interface can be shown, the settings must be made immutable by calling the \javamethod{done} method of the \javaclass{CalibrationSettings} object available through the \ac{API}. This guarantees, that no changes can be made when the user is calibrating a Myo. Furthermore, each connected Myo is assigned a calibration profile, which is also part of the \ac{API}. It contains the parameters calculated during the calibration as attributes, which are read-only through the respective getters in the \javaclass{CalibrationProfile} class.
When the user calibrates a Myo the attributes are set by the back end automatically. Additionally, methods to add listeners receiving raw as well as calibrated data on a per Myo basis are available in the calibration profile.

Application classes that wish to receive data must implement either the \javainterface{IDataOutputListener} or the \javainterface{IRawDataOutputListener} interface to receive calibrated or raw data respectively. Additionally, the \javainterface{IMyoStateListener} interface can be implemented and registered in the \javaclass{MyoCaLib} object provided by the Android service, to receive events related to connection and calibration.

The \ac{API} is constructed to only allow access to specific parts, while completely hiding any references to the back end by the appropriate access modifiers. This, however, posed a problem for accessing the writable instance of the calibration profile. As the reference to this instance is nested in one of the \javaclass{MyoCaLib} object's attributes, which should not be accessible from outside the package, the back end can not write to the attributes either.

To solve this issue, an intermediate \javaclass{InternalApiInterface} class is present in the \javapackage{api} package. This is the only class in the package that is not intended to be used by an implementing application, which is enforced through authenticating the class that is creating an instance of the \javaclass{InternalApiInterface} class.
The constructor requires a non-null object of a class nested inside the class that should be able to create an object. By restricting access to the nested class' constructor via the \javakeyword{private} access modifier, only the outer class can create an object, and hence authenticate itself to the \javaclass{InternalApiInterface} class.

\section{Back End}

The first step in the development of the library was to connect to the Myo and receive data reliably. Although such functionality is provided by the official Myo SDK, it is currently not possible to acquire the raw \ac{EMG} data with the provided \ac{API}. Another approach was to communicate with the Myo directly over the published Bluetooth protocol, which is efficiently handled by Matthias Urhahn's implementation called myolib \cite{urhahn_myolib}.

Some minor changes were made to his implementation to improve stability and add new functionality.
The Bluetooth service discovery, for example, might not finish on slower mobile phones before issuing the first command, resulting in an exception being thrown. A check to verify that the services are discovered before handling any commands was implemented as a countermeasure.
Extensions to the functionality are placed inside the package \javapackage{extension} and include classes and interfaces that implement myolib's provided \javamethod{onCommandDone} callback, which is invoked whenever a Bluetooth command was executed.
As the callback does not include information about which command was executed, the interfaces declare their own callback, which is called by the respective class whenever the \javamethod{onCommandDone} callback was called for their respective type of command.

The \javaclass{ConnectionHandler} class implements the connection-related callbacks provided by myolib, and adds a preparation stage to the connection, in which the device name and battery level are read from the device. As these require an active Bluetooth connection to the Myo, myolib's callbacks are invoked, but the connection is clearly distinguished from being connected due to the user's request in the library.
Appropriate callbacks in the library's \ac{API} are only invoked when the user explicitly requests the connect or disconnect, but not when the Myo is connected to read the initial information. Additional callbacks include methods, which are called when the device is successfully calibrated or the calibration is no longer valid.
This places the responsibility of connecting to Myos fully in the library and the provided \ac{UI} and simplifies the \ac{API} for implementing applications.

After connecting to a Myo the user is required to calibrate the Myo according to the methods established in the design. Which calibration steps are necessary is determined by the settings set by the application. The necessary steps are internally listed and then shown to the user one by one in form of layout cards listed in a \javaclass{RecyclerView}.
Each of these steps only records the needed data and stores it for later analysis and automatically vanishes, when the necessary data was recorded.

The last step shows a progress bar and calculates all required values, such as the normalization factor, the rotation and the translation parameters. The library then decides whether these values are good enough based on the application defined settings and either completes the calibration or shows the user what is required to complete it.
For example if the application defines, that the user is required to rotate the armband into a position that corresponds to an angle no higher than ten degrees, but the calculated angle is 45 degrees, the user is advised to rotate the armband and the execution of the forward \ac{RVC} must be completed again.

The maximum required steps to complete the calibration are the calculation of the accelerometer biases, defining on which arm the armband is worn, the execution of the \ac{RVC} while the arm is left hanging and the execution while the arm is stretched forward. The biases are stored in a file and loaded in subsequent calibrations of the same Myo to decrease the workload on the user.

As soon as the calibration is completed, the library starts applying the parameters to the data sent by myolib and forwards it to any listener registered in the calibration profile associated with the Myo.

\section{Expandability}

The main aim with the design of the implementation was to make it easily expandable for future work.
The interpolation of the \ac{EMG} values takes place in subclasses of the \javaclass{CyclicInterpolationFunction} class. This parent class ensures that sampling takes place only on the interval defined by its subclasses. For the present application of interpolating \ac{EMG} values depending on an angle, the subclasses \javaclass{NearestNeighborInterpolation}, \javaclass{LinearInterpolation} and \javaclass{CubicHermiteSplineInterpolation} define an interval from $0$ to $2\pi$ to model the range of possible radians. Any attempt made by an application to sample the function at higher or lower values is changed to a method call on the defined interval, by applying the modulus.
The \javaclass{CyclicInterpolationFunction} class additionally ensures, that subclasses can use the \javamethod{getLowerSupportPoint} and \javamethod{getHigherSupportPoint} methods to get the correct point at which the interpolation is backed by a support, depending on an input point.
If no support below or above the input point exists the last or first support is returned respectively to model the cyclic nature of the function.

In the \javaclass{EmgCalibrationSettings} class, an inner \javakeyword{enum} is defined, including the \javamethod{createNewFunction} method called by the back end when a new object of the interpolation function selected by an application is needed. Future work may include a different model for the muscle activity between two pods, which can then be realized by adding a new interpolation mode to the \javakeyword{enum}.
%, which is shown in \reflst{code_interpolationmodeenum}.
The implementation of such a model would be a subclass of \javaclass{CyclicInterpolationFunction}, of which a new object has to be returned in the \javakeyword{enum}'s \javamethod{createNewFunction} method.

%\begin{lstlisting}[caption={Caption goes here.},label=code_interpolationmodeenum]
%public enum EmgInterpolationMode {
%
%	NEAREST_NEIGHBOR,
%	LINEAR,
%	CUBIC_HERMITE_SPLINE;
%
%	protected CyclicInterpolationFunction createNewFunction() {
%		switch(this) {
%			case NEAREST_NEIGHBOR:
%				return new NearestNeighborInterpolation();
%			case LINEAR:
%				return new LinearInterpolation();
%			case CUBIC_HERMITE_SPLINE:
%				return new CubicHermiteSplineInterpolation();
%			default:
%				return new NearestNeighborInterpolation();
%		}
%	}
%}
%\end{lstlisting}

The principle of managing multiple layout cards, each responsible for one step of the calibration, allows for an extension by one or multiple new calibration steps as well. The cards are shown in a \javaclass{RecyclerView} and are therefore subclasses of the \javaclass{PageCalibrationActionListViewHolder}, which in turn is a subclass of the android-provided class \javaclass{RecyclerView.ViewHolder}.
Each \javaclass{ViewHolder} instance is responsible for managing the layout elements of a card shown in a \javaclass{RecyclerView}. The \javaclass{PageCalibrationActionListViewHolder} class manages the references to parent constructs, like the \javaclass{Activity} of the library or the \javaclass{PageCalibration} class, which is responsible for showing and managing the \javaclass{RecyclerView} and providing access to the calibration settings and calibration profiles via the \javaclass{InternalApiInterface}.

New calibration steps can be implemented by creating a subclass of \javaclass{PageCalibrationActionListViewHolder} and implementing the necessary methods. The step's position in the list of steps and conditions for its visibility can be set in the \javaclass{ViewType} \javakeyword{enum}, which is nested inside the \javaclass{PageCalibrationActionListAdapter} class. A new \javakeyword{enum} constant describing the new step has to be added which automatically defines its position relative to the other steps.
Inside the \javamethod{getUniqueViewHolder} method a new case inside the present switch must be implemented, which creates and returns an object of the new \javaclass{PageCalibrationActionListViewHolder} descendant, when the \javaclass{RecyclerView} is bound by the back end.
Finally, the visibility conditions can be set in the \javamethod{enabled} method by returning either \javakeyword{true} or \javakeyword{false} based on custom conditions (\EG{} the settings set by an implementing application).