import numpy as np
import matplotlib.pyplot as plt

#conf_arr_raw = np.array([[196959, 19954, 14803, 15887],
#    [3096, 14199, 20308, 8027],
#    [3489, 27636, 25680, 18637],
#    [22837, 20688, 20699, 41475]])
conf_arr_raw = np.array([[196959, 3096, 3489, 22837],
    [19954, 14199, 27636, 20688],
    [14803, 20308, 25680, 20699],
    [15887, 8027, 18637, 41475]])
#conf_arr_cal = np.array([[195359, 14720, 15165, 11035],
#    [3380, 37478, 10029, 27910],
#    [4710, 8767, 52134, 1732],
#    [16430, 24932, 7237, 43354]])
conf_arr_cal = np.array([[195359, 3380, 4710, 16430],
    [14720, 37478, 8767, 24932],
    [15165, 10029, 52134, 7237],
    [11035, 27910, 1732, 43354]])

norm_conf = []
for i in conf_arr_cal:
    a = 0
    tmp_arr = []
    a = sum(i, 0)
    for j in i:
        tmp_arr.append(float(j)/float(a))
    norm_conf.append(tmp_arr)

fig = plt.figure()
plt.clf()
ax = fig.add_subplot(111)
ax.set_aspect(1)
res = ax.imshow(np.array(norm_conf), cmap=plt.cm.Blues, 
                interpolation='nearest')

norm_conf = np.array(norm_conf)
width, height = norm_conf.shape

for x in range(width):
    for y in range(height):
        ax.annotate("{:0.2f}".format(norm_conf[x][y]), xy=(y, x), 
                    horizontalalignment='center',
                    verticalalignment='center')

cb = fig.colorbar(res)
plt.xticks( range(width), ('No Gesture', 'Fist', 'Extension', 'Flexion') )
plt.yticks( range(height), ('No Gesture', 'Fist', 'Extension', 'Flexion') )
#plt.xticks(range(width), alphabet[:width])
#plt.yticks(range(height), alphabet[:height])
plt.savefig('WorseMatrix_CAL.svg')