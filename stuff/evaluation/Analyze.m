%% Initialize
%delete(findall(0,'Type','figure'))
clc;
%clf;
clear;

baseFolder = 'data/processed/';
participantNum = 14;
sets = {'CAL','RAW'};

nnHiddenLayerSize = 5;
nnTrainRatio = 90/100;
nnMaxEpochs = 500;
nnNum = 50;

%% Work
%delete(findall(0,'Type','figure'))
for s=1:size(sets,2)
    % Load each participants data
    participantData = cell(participantNum,0);
    for i=1:participantNum
        participantData{i} = csvread([baseFolder,num2str(i),'/',sets{s},'.csv'],1,0);
    end
    % Leave one out cross-validation for each participant
    concatTargets = zeros(4,0);
    concatOutputs = zeros(4,0);
    concatTruePos = zeros(participantNum,4);
    concatFalsePos = zeros(participantNum,4);
    concatRmse = zeros(participantNum,4);
    for i=1:participantNum
        disp(['Currently leaving participant ',num2str(i),' out...']);
        inputs = zeros(0,18);
        targets = zeros(4,0);
        testInputs = transpose(participantData{i}(:,2:19));
        testTargets = zeros(4,size(participantData{i},1));
        testTargets(1,participantData{i}(:,20) == 0) = 1;
        testTargets(2,participantData{i}(:,20) == 1) = 1;
        testTargets(3,participantData{i}(:,20) == 2) = 1;
        testTargets(4,participantData{i}(:,20) == 3) = 1;
        for j=1:participantNum
            if (j == i)
                continue;
            end
            participantInputs = participantData{j}(:,2:19);
            participantTargets = zeros(4,size(participantData{j},1));
            participantTargets(1,participantData{j}(:,20) == 0) = 1;
            participantTargets(2,participantData{j}(:,20) == 1) = 1;
            participantTargets(3,participantData{j}(:,20) == 2) = 1;
            participantTargets(4,participantData{j}(:,20) == 3) = 1;
            inputs((size(inputs,1)+1):(size(inputs,1)+size(participantInputs,1)), :) = participantInputs(:,:);
            targets(:, (size(targets,2)+1):(size(targets,2)+size(participantTargets,2))) = participantTargets(:,:);
        end
        inputs = transpose(inputs);
        %Inputs and Targets compiled, calculate neural network X times in
        %parallel
        individualOutputs = cell(nnNum,1);
        disp('Beginning parallelization!');
        parfor k=1:nnNum
            net = patternnet(nnHiddenLayerSize);
            net.divideParam.trainRatio = nnTrainRatio;
            net.divideParam.valRatio = 1-nnTrainRatio;
            net.divideParam.testRatio = 0;
            net.trainParam.epochs = nnMaxEpochs;
            [net,tr] = train(net,inputs,targets);%,'useGPU','yes');
            individualOutputs{k} = net(testInputs);
        end
        disp('Parallelization finished!');
        %Combine results
        testOutputs = zeros(4,0);
        for k=1:nnNum
            if (size(testOutputs,2) == 0)
                testOutputs = individualOutputs{k};
            else
                testOutputs = testOutputs + individualOutputs{k};
            end
        end
        testOutputs = testOutputs ./ nnNum;
        %Done
        testErrors = gsubtract(testTargets,testOutputs);
        [c,cm,ind,per] = confusion(testTargets,testOutputs);
        for j=1:4
            concatTruePos(i,j) = cm(j,j)/sum(cm(j,:));
            concatFalsePos(i,j) = 1-(cm(j,j)/sum(cm(:,j)));
        end
        rmse = transpose(mean(testErrors.^2,2).^(0.5));
        concatRmse(i,:) = rmse(1,:);
        concatTargets(:, (size(concatTargets,2)+1):(size(concatTargets,2)+size(testTargets,2))) = testTargets(:,:);
        concatOutputs(:, (size(concatOutputs,2)+1):(size(concatOutputs,2)+size(testOutputs,2))) = testOutputs(:,:);
    end
    %Analysis of set finished
    figure
    plotconfusion(concatTargets,concatOutputs,sets{s})
    disp('True pos:');
    disp(concatTruePos);
    disp('False pos:');
    disp(concatFalsePos);
    disp('RMSE:');
    disp(concatRmse);
    xlabel('Real Gesture')
    ylabel('Recognized Gesture')
    set(gca,'xticklabel',{'No Gesture' 'Fist' 'Extension' 'Flexion' ''})
    set(gca,'yticklabel',{'No Gesture' 'Fist' 'Extension' 'Flexion' ''});
end

%view(net)

%testPerformance = perform(net,testTargets,testOutputs);
% Plots
% Uncomment these lines to enable various plots.
% figure, plotperform(tr)
% figure, plottrainstate(tr)
% figure, plotconfusion(targets,outputs)
% figure, ploterrhist(errors)

%% Finalize
%clc;
%clf;
%clear;