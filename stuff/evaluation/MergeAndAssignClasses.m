clc;
clf;
clear;

level0 = {'CAL','RAW'};
level1 = {'data/01','data/02','data/03','data/04','data/05','data/06','data/07','data/08','data/09','data/10','data/11','data/12','data/13','data/14'};
level2 = {'fist','extension','flexion'};
classificationThresholds = {...
    ... %CAL:
    {{9, 15, 10, 19, 18, 13, 12, 12, 14, 23, 23, 6.2, 10, 10}, ... %FIST
    {15, 15, 10, 15, 10, 10, 12, 10, 10, 15, 15, 10, 10, 10}, ... %EXTENSION
    {20, 15, 10, 15, 10, 10.7, 30, 15, 15, 15, 30, 8, 10, 8}}, ... %FLEXION
    ... %RAW:
    {{9, 10, 8, 9, 5, 3.5, 8, 5, 8, 8, 8, 3.943, 6, 5}, ... %FIST
    {10, 8, 10, 10, 8, 2.5, 7, 9, 5, 5, 8, 5, 5, 5}, ... %EXTENSION
    {10, 9, 5, 5, 5, 2.54, 5, 5, 5, 5, 5, 3, 5, 5}}... %FLEXION
};
outputFolder = 'data/processed';
outputHeader = {'Time','AcclX','AcclY','AcclZ','GyroX','GyroY','GyroZ','OrientationW','OrientationI','OrientationJ','OrientationK','Emg0','Emg1','Emg2','Emg3','Emg4','Emg5','Emg6','Emg7','Gesture'};

fig = figure('Color','white','DefaultAxesFontSize',24,'Name','ClassAssignment','NumberTitle','off');

for e = 1:size(level0,2)
    level1mat = zeros(0,20);
    for f = 1:size(level1,2)
        usermat = zeros(0,20);
        for g = 1:size(level2,2)
            prefix = [level1{f},'/',level2{g},'/E59EE051C954_',level0{e},'_'];
            %Load data
            emg = readData(fullfile(cd, [prefix,'EMG.csv']));
            accl = readData(fullfile(cd, [prefix,'ACCL.csv']));
            gyro = readData(fullfile(cd, [prefix,'GYRO.csv']));
            ornt = readData(fullfile(cd, [prefix,'ORNT.csv']));
            %Join IMU data
            imumat = zeros(size(accl,1),11);
            for i = 1:min(size(accl,1),min(size(gyro,1),size(ornt,1)))
                imumat(i,1) = accl(i,1);
                imumat(i,2) = accl(i,2);
                imumat(i,3) = accl(i,3);
                imumat(i,4) = accl(i,4);
                imumat(i,5) = gyro(i,1);
                imumat(i,6) = gyro(i,2);
                imumat(i,7) = gyro(i,3);
                imumat(i,8) = ornt(i,1);
                imumat(i,9) = ornt(i,2);
                imumat(i,10) = ornt(i,3);
                imumat(i,11) = ornt(i,4);
            end
            %Interpolate IMU to match EMG timestamps
            completemat = zeros(size(emg,1),20);
            for i=1:size(emg,1)
                completemat(i,1) = emg(i,1);
                completemat(i,2:11) = interpolateImuValue(emg(i,1),imumat,emg(1,1),emg(size(emg,1),1));
                completemat(i,12:19) = emg(i,2:9);
                completemat(i,20) = mean(abs(emg(i,2:9)));
            end
            %Fix timestamp
            completemat(:,1) = completemat(:,1)-completemat(1,1);
            %Cut off beginning and end 2 seconds
            completemat = completemat(completemat(:,1)>2000,:);
            completemat = completemat(completemat(:,1)<(completemat(size(completemat,1),1)-2000),:);
            %Fix timestamp again
            completemat(:,1) = completemat(:,1)-completemat(1,1);
            %Assign classes
            clf;
            plot(completemat(:,1), completemat(:,20));
            hold on;
            completemat(:,20) = movingAverage(completemat(:,20), 50);
            plot(completemat(:,1), completemat(:,20), 'linewidth', 2);
            completemat(:,20) = thresholdAssign(completemat(:,20), classificationThresholds{e}{g}{f}, 0, g);
            plot(completemat(:,1), completemat(:,20).*(100/f), 'linewidth', 2);
            [~,hObj]=legend('F.W.Rectified Mean','Moving Average','Class Assignment');           % return the handles array
            hL=findobj(hObj,'type','line');  % get the lines, not text
            set(hL,'linewidth',5)            % set their width property
            xlabel('Time [ms]') % x-axis label
            ylabel('EMG value') % y-axis label
            hold off;
            %concatenate with user matrix
            if (size(usermat,1)>0)
                completemat(:,1) = completemat(:,1) + (usermat(size(usermat,1),1) + 1);
            end
            usermat((size(usermat,1)+1):(size(usermat,1)+size(completemat,1)), :) = completemat(:,:);
        end
        %output user matrix
        clf;
        plot(usermat(:,1), mean(usermat(:,12:19),2));
        hold on;
        plot(usermat(:,1), usermat(:,20).*100);
        hold off;
        mkdir(fullfile(cd, outputFolder),num2str(f));
        writeData([outputFolder,'/',num2str(f),'/',level0{e},'.csv'], outputHeader, usermat);
    end
end
hold off;

'done';
clc;
clf;
clear;

function y = interpolateImuValue(timestamp, imumat, lowestTimestamp, highestTimestamp)
    imumat = [lowestTimestamp-1, zeros(1,10); imumat; highestTimestamp, zeros(1,10)];
    for i=1:size(imumat,1)
        if (imumat(i,1) < timestamp)
            continue;
        end
        weight = (timestamp - imumat(i-1,1)) / (imumat(i,1) - imumat(i-1,1));
        y = imumat(i-1,2:11) .* weight + imumat(i,2:11) .* (1-weight);
        break;
    end
end

function y = movingAverage(colVec, halfWindowSize)
    y = zeros(size(colVec,1), 1);
    for i=1:size(colVec,1)
        loIndex = max(1,i-halfWindowSize);
        hiIndex = min(size(colVec,1),i+halfWindowSize);
        y(i) = sum(colVec(loIndex:hiIndex),1) / (hiIndex-loIndex+1);
    end
end

function y = thresholdAssign(colVec, threshold, loAssign, hiAssign)
    y = zeros(size(colVec,1),1);
    for i=1:size(colVec,1)
        if (colVec(i)<threshold)
            y(i) = loAssign;
        else
            y(i) = hiAssign;
        end
    end
end

function y = readData(filename)
    data = csvread(filename,1,0);
    y = sortrows(data,1);
end

function writeData(filename, header, data)
    textHeader = strjoin(header, ',');
    fid = fopen(filename,'w'); 
    fprintf(fid,'%s\n',textHeader);
    fclose(fid);
    dlmwrite(filename,data,'-append','precision',17);
end