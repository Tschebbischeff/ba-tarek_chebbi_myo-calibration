clc;
clear;

raw = {'Raw Data - ', [...
    196959, 19954, 14803, 15887
    3096, 14199, 20308, 8027
    3489, 27636, 25680, 18637
    22837, 20688, 20699, 41475
]};
cal = {'Calibrated Data - ', [...
    195359, 14720, 15165, 11035
    3380, 37478, 10029, 27910
    4710, 8767, 52134, 1732
    16430, 24932, 7237, 43354
]};

data = raw;

sum = 0;
for i=1:size(data{2},1)
    for j=1:size(data{2},2)
        sum = sum + data{2}(i,j);
    end
end
outputs = zeros(4, sum);
targets = zeros(4, sum);

index = 0;
for i=1:size(data{2},1)
    for j=1:size(data{2},2)
        for k=1:data{2}(i,j)
            outputs(i,index+k) = 1;
            targets(j,index+k) = 1;
        end
        index = index + data{2}(i,j);
    end
end

plotconfusion(targets, outputs, data{1});
xlabel('Real Gesture')
ylabel('Recognized Gesture')
set(gca,'xticklabel',{'No Gesture' 'Fist' 'Extension' 'Flexion' ''})
set(gca,'yticklabel',{'No Gesture' 'Fist' 'Extension' 'Flexion' ''});
set(findobj(gca,'type','text'),'fontsize',20);
set(gca,'fontsize',22);