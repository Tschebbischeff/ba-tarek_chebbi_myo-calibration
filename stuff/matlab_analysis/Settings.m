global myoLeft myoLeftMac myoLeftLabel myoLeftUsbWrist myoRight myoRightMac myoRightLabel myoRightUsbWrist;
global loadLowerLimit loadUpperLimit loadSetLowerLimitZero loadAngles;
global prepHalfWaveRectification prepFullWaveRectification prepMvAvgHalfWinSize prepNormalization prepNormalizationSeperate prepNormalizationSourceChannel prepNormalizationThreshold prepNormalizationHalfWinSize prepNormalizationTarget;
global plotChannels plotAngles plotMean plotChannelMads plotTotalMean plotMeanMax plotNormalizationFactor plotTotalMeanThresholdSourceChannel plotTotalMeanThreshold plotTotalMeanThresholdByMean plotFontSize plotDisplayLegend plotUseGridLegend plotColorOrder;
global plot2dAxisLimits;
global prepChannelDeviationFromMean prepChannelSpread prepChannelSpreadThreshold prepChannelDeviationThreshold plotChannelDeviationAverage prepZeroCrossingChannels prepZeroCrossingHalfWinSize prepZeroCrossingScale;
global plot3dEnabled plot3dAngularOffset plotChannelSpreadAverage plot3dAxisLimits;
global interpolationEnabled interpolationResolution interpolationLowerBound interpolationUpperBound;
global fftEnabled fftChannels fftSampleFrequency fftAxisLimits fftMvAvgHalfWinSize;
%Myo configuration
myoLeft = true;                                                             %Whether the user was wearing a myo on the left arm
myoLeftMac = 'C5.AD.C4.5E.46.A0';                                           %The MAC address of the myo worn on the left arm
myoLeftLabel = 'Left Myo';                                                  %The Label shown over the graph of the left myo
myoLeftUsbWrist = true;                                                     %Whether the USB port of the left myo was facing the wrist of the user
myoRight = true;                                                            %Whether the user was wearing a myo on the right arm
myoRightMac = 'E5.9E.E0.51.C9.54';                                          %The MAC address of the myo worn on the right arm
myoRightLabel = 'Right Myo';                                                %The Label shown over the graph of the right myo
myoRightUsbWrist = true;                                                    %Whether the USB port of the right myo was facing the wrist of the user
%Dataloading
loadLowerLimit = -1;                                                        %Values of zero and higher limit the data extracted from the files to this second and higher
loadUpperLimit = -1;                                                        %Values of zero and higher limit the data extracted from the files to this second and lower
loadSetLowerLimitZero = true;                                               %When the lower limit is set, set this value to true to make the lower limit equal zero seconds
loadAngles = true;                                                          %If true, data is expected to be in angle_000.00 folders describing the rotation around the users arm (counter clockwise around shoulder-to-wrist axis)
%Preprocessing
prepHalfWaveRectification = false;                                          %When true, the data is half wave rectified: data = max(data,0) (full wave rectification takes precedence)
prepFullWaveRectification = true;                                           %When true, the data is full wave rectified: data = abs(data) (takes precedence before half wave rectification)
prepMvAvgHalfWinSize = 50;                                                  %The data is averaged over (index-this value) to (index+this value), set to 0 to disable moving average
prepNormalization = false;                                                  %When true, the data is normalized according to the thresholded mean value (threshold is chosen as total mean of untresholded data)
prepNormalizationSeperate = false;                                          %When true each of the eight channel is seperately normalized with its own normalization factor
prepNormalizationSourceChannel = 9;                                         %The source channel for the normalization, 9 equals the mean of all eight channels (no effect, if seperate normalization is true)
prepNormalizationThreshold = 5;                                             %The hard cap threshold for eliminating near zero data
prepNormalizationHalfWinSize = 50;                                          %The window size used on the mean for the thresholding
prepNormalizationTarget = 32;                                               %The value representing 100% of the scaling. The data will be normalized such that the total thresholded mean will be equal to this value (assuming the threshold is scaled aswell)
prepChannelDeviationFromMean = 0;                                           %Channel (only one) for which to calculate the deviation from the channel mean (0 = no deviation plotting)
prepChannelSpread = [];                                                     %Channels of which to calculate the total spread (the spread is the maximum of the given channels - the minimum of the given channels)
prepChannelSpreadThreshold = prepNormalizationThreshold * 2;                %Only calculates the spread for x values, where the mean is higher than this value
prepChannelDeviationThreshold = prepNormalizationThreshold;                 %The deviation is only calculated where the channel value is greater than this threshold value
prepZeroCrossingChannels = [];                                              %Channels for which to calculate the zero crossing rate (Only one rate is calculated, average of multiple rates is taken, if more than one channel is given)
prepZeroCrossingHalfWinSize = 25;                                           %The window size on which to calculate the zero crossing rate
prepZeroCrossingScale = 200;                                                %Crossing rate is scaled to this number of samples
%Plotting (2D & 3D)
plotChannels = 1:8;                                                         %Array with channels that should be plotted
plotAngles = 0:22.5:360;                                                    %Array with angles that can be found as angle_000.00 folders inside the data folder (only applies, when loadAngles is true)
plotMean = true;                                                            %When true, the mean of all channels is plotted (which channels are plotted does not matter)
plotChannelMads = [];                                                       %Array with channels on which the Mean Absolute Deviation around the mean is calculated
plotTotalMean = false;                                                      %When true, the total mean is calculated and shown in the legend (plot will be invisible)
plotMeanMax = false;                                                        %When true, the maximum of the mean is displayed in the legend
plotNormalizationFactor = true;                                             %When true and if normalization was done in preprocessing, the normalization factor is shown in the legend
plotTotalMeanThresholdSourceChannel = 9;                                    %The source channel for the shown threshold, 9 equals the mean of all eight channels
plotTotalMeanThreshold = 0;                                                 %All values below this threshold are cut away from the mean
plotTotalMeanThresholdByMean = false;                                       %When true, sets the threshold to the total mean calculated on the unthresholded data
plotChannelDeviationAverage = false;                                        %Whether to show the value for the average deviation
plotChannelSpreadAverage = false;                                           %Whether to show the average of the calculated channel spread
plotFontSize = 24;                                                          %The font size of the legend and the axis units and the title
plotDisplayLegend = true;                                                   %When true, a legend containing the plots descriptions is drawn
plotUseGridLegend = true;                                                   %When true, gridlegend will be used for 2d plots
plotColorOrder = [get(gca, 'colororder');
    0.61568627450980392156862745098039 0.31372549019607843137254901960784 0.31372549019607843137254901960784;
    1 0.5 1];                                                               %The color order for the plots (Channels, Mean, Thresholded Mean)
%Plotting (2D)
plot2dAxisLimits = [-inf inf 0 75];                                         %The axis limits for 2 dimensional plots
%Plotting (3D)
plot3dEnabled = true;                                                       %When true, the area under each channel is plotted and rotated according to the angle and angular offset (to display position of pod around arm)
plot3dAngularOffset = 180;                                                  %Position of pod 1 on arm (rotational axis is around shoulder-to-wrist axis)
plot3dAxisLimits = [-inf inf -64 64 -64 64];                                %The axis limits for 3 dimensional plots
%Plotting (Interpolation (3D settings apply))
interpolationEnabled = false;                                               %When true the channel data is used as supports for a pChip interpolation and plotted in dependency of the angle (basically, the three dimensional view is extended to interpolations between the channels)
interpolationResolution = 4;                                                %1 is equivalent to the real values only, higher values increase resolution, but also increases calculation time dramatically
interpolationLowerBound = 10;                                               %time in seconds from which to start the interpolation
interpolationUpperBound = 20;                                               %time in seconds at which to end the interpolation
%Fourier Transform
fftEnabled = false;                                                         %If true, the fourier transform is plotted (no other visualizations are possible in this mode, normalization may be applied however)
fftChannels = 1:8;                                                          %The channels which to transform into frequency domain
fftSampleFrequency = 200;                                                   %Sample frequency necessary for fast fourier transform
fftAxisLimits = [-inf inf -inf inf];                                        %Axis limits when FFT is displayed
fftMvAvgHalfWinSize = 50;                                                   %The window size for the moving average applied to the fft signal (0 = disabled)