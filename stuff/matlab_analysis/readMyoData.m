function y = readMyoData(filename, loLim, hiLim, zeroAfterLim)
    filename
    data = csvread(filename,1,0);
    %reorder data by timestamp
    data = sortrows(data,1);
    %subtract first timestamp and convert to seconds
    data(:,1) = (data(:,1) - data(1,1)) / 1000;
    %limit data
    index = 1;
    finData = zeros(1,size(data,2));
    for i = 1:size(data,1)
        if (loLim >= 0 && data(i,1)<loLim)
            continue;
        end
        if (hiLim >= 0 && data(i,1)>hiLim)
            break;
        end
        finData(index,:) = data(i,:);
        index = index+1;
    end
    %subtract first timestamp again
    if (zeroAfterLim && loLim > 0)
        finData(:,1) = (finData(:,1) - finData(1,1));
    end
    %out
    y = finData;
end

