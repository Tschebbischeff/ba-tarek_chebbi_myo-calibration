clear;
clf;
clc;
Settings;

datasetpath='imu/gyroscaling_new/';

%overwriting settings
myo1 = 'E5.9E.E0.51.C9.54';
myo2 = 'EE.13.19.77.B2.78';
myo3 = 'EE.BE.DA.22.FE.CA';
myo4 = 'EF.5A.83.1E.F3.1A';
global CurrentlyA;
global CurrentlyG;
global CurrentlyO;
global CurrentlyO2;
global vecNorm;
vecNorm = 3;
AcclActive = false;
GyroActive = true;
OrntActive = false;
AnglActive = false;
mvAvgA = 50;
outlierThresholdA = 0;
mvAvgG = 50;
%outlierThresholdG = 0.25;
outlierThresholdG = 0;
%mvAvgO = 50;
mvAvgO = 25;
outlierThresholdO = 0;
mvAvgO2 = 50;
outlierThresholdO2 = 0;
plotFontSize = 22;
loadLowerLimit = 10;

%close unrelated, still opened figure windows
allfigs = findall(0,'type','figure');
for f = 1:size(allfigs)
    fig = allfigs(f);
    %if size(find(strcmp(loadDataset, get(fig,'Name'))),2) == 0
        close(fig); %close all figure windows
    %end
end

%Read data
if (AcclActive)
    CurrentlyA = true;
    CurrentlyG = false;
    CurrentlyO = false;
    CurrentlyO2 = false;
    m1AcclX = readMyoData([datasetpath,'x/',myo1,'_accl_data.csv'], loadLowerLimit, loadUpperLimit, loadSetLowerLimitZero);
    m2AcclX = readMyoData([datasetpath,'x/',myo2,'_accl_data.csv'], loadLowerLimit, loadUpperLimit, loadSetLowerLimitZero);
    m3AcclX = readMyoData([datasetpath,'x/',myo3,'_accl_data.csv'], loadLowerLimit, loadUpperLimit, loadSetLowerLimitZero);
    m4AcclX = readMyoData([datasetpath,'x/',myo4,'_accl_data.csv'], loadLowerLimit, loadUpperLimit, loadSetLowerLimitZero);
    m1AcclY = readMyoData([datasetpath,'y/',myo1,'_accl_data.csv'], loadLowerLimit, loadUpperLimit, loadSetLowerLimitZero);
    m2AcclY = readMyoData([datasetpath,'y/',myo2,'_accl_data.csv'], loadLowerLimit, loadUpperLimit, loadSetLowerLimitZero);
    m3AcclY = readMyoData([datasetpath,'y/',myo3,'_accl_data.csv'], loadLowerLimit, loadUpperLimit, loadSetLowerLimitZero);
    m4AcclY = readMyoData([datasetpath,'y/',myo4,'_accl_data.csv'], loadLowerLimit, loadUpperLimit, loadSetLowerLimitZero);
    m1AcclZ = readMyoData([datasetpath,'z/',myo1,'_accl_data.csv'], loadLowerLimit, loadUpperLimit, loadSetLowerLimitZero);
    m2AcclZ = readMyoData([datasetpath,'z/',myo2,'_accl_data.csv'], loadLowerLimit, loadUpperLimit, loadSetLowerLimitZero);
    m3AcclZ = readMyoData([datasetpath,'z/',myo3,'_accl_data.csv'], loadLowerLimit, loadUpperLimit, loadSetLowerLimitZero);
    m4AcclZ = readMyoData([datasetpath,'z/',myo4,'_accl_data.csv'], loadLowerLimit, loadUpperLimit, loadSetLowerLimitZero);
end
if (GyroActive)
    CurrentlyA = false;
    CurrentlyG = true;
    CurrentlyO = false;
    CurrentlyO2 = false;
    m1GyroX = readMyoData([datasetpath,'x/',myo1,'_gyro_data.csv'], loadLowerLimit, loadUpperLimit, loadSetLowerLimitZero);
    m2GyroX = readMyoData([datasetpath,'x/',myo2,'_gyro_data.csv'], loadLowerLimit, loadUpperLimit, loadSetLowerLimitZero);
    m3GyroX = readMyoData([datasetpath,'x/',myo3,'_gyro_data.csv'], loadLowerLimit, loadUpperLimit, loadSetLowerLimitZero);
    m4GyroX = readMyoData([datasetpath,'x/',myo4,'_gyro_data.csv'], loadLowerLimit, loadUpperLimit, loadSetLowerLimitZero);
    m1GyroY = readMyoData([datasetpath,'y/',myo1,'_gyro_data.csv'], loadLowerLimit, loadUpperLimit, loadSetLowerLimitZero);
    m2GyroY = readMyoData([datasetpath,'y/',myo2,'_gyro_data.csv'], loadLowerLimit, loadUpperLimit, loadSetLowerLimitZero);
    m3GyroY = readMyoData([datasetpath,'y/',myo3,'_gyro_data.csv'], loadLowerLimit, loadUpperLimit, loadSetLowerLimitZero);
    m4GyroY = readMyoData([datasetpath,'y/',myo4,'_gyro_data.csv'], loadLowerLimit, loadUpperLimit, loadSetLowerLimitZero);
    m1GyroZ = readMyoData([datasetpath,'z/',myo1,'_gyro_data.csv'], loadLowerLimit, loadUpperLimit, loadSetLowerLimitZero);
    m2GyroZ = readMyoData([datasetpath,'z/',myo2,'_gyro_data.csv'], loadLowerLimit, loadUpperLimit, loadSetLowerLimitZero);
    m3GyroZ = readMyoData([datasetpath,'z/',myo3,'_gyro_data.csv'], loadLowerLimit, loadUpperLimit, loadSetLowerLimitZero);
    m4GyroZ = readMyoData([datasetpath,'z/',myo4,'_gyro_data.csv'], loadLowerLimit, loadUpperLimit, loadSetLowerLimitZero);
end
if (OrntActive || AnglActive)
    CurrentlyA = false;
    CurrentlyG = false;
    if (OrntActive)
        CurrentlyO = true;
        CurrentlyO2 = false;
    else
        CurrentlyO = false;
        CurrentlyO2 = true;
    end
    m1OrntX = readMyoData([datasetpath,'x/',myo1,'_orient_data.csv'], loadLowerLimit, loadUpperLimit, loadSetLowerLimitZero);
    m2OrntX = readMyoData([datasetpath,'x/',myo2,'_orient_data.csv'], loadLowerLimit, loadUpperLimit, loadSetLowerLimitZero);
    m3OrntX = readMyoData([datasetpath,'x/',myo3,'_orient_data.csv'], loadLowerLimit, loadUpperLimit, loadSetLowerLimitZero);
    m4OrntX = readMyoData([datasetpath,'x/',myo4,'_orient_data.csv'], loadLowerLimit, loadUpperLimit, loadSetLowerLimitZero);
    m1OrntY = readMyoData([datasetpath,'y/',myo1,'_orient_data.csv'], loadLowerLimit, loadUpperLimit, loadSetLowerLimitZero);
    m2OrntY = readMyoData([datasetpath,'y/',myo2,'_orient_data.csv'], loadLowerLimit, loadUpperLimit, loadSetLowerLimitZero);
    m3OrntY = readMyoData([datasetpath,'y/',myo3,'_orient_data.csv'], loadLowerLimit, loadUpperLimit, loadSetLowerLimitZero);
    m4OrntY = readMyoData([datasetpath,'y/',myo4,'_orient_data.csv'], loadLowerLimit, loadUpperLimit, loadSetLowerLimitZero);
    m1OrntZ = readMyoData([datasetpath,'z/',myo1,'_orient_data.csv'], loadLowerLimit, loadUpperLimit, loadSetLowerLimitZero);
    m2OrntZ = readMyoData([datasetpath,'z/',myo2,'_orient_data.csv'], loadLowerLimit, loadUpperLimit, loadSetLowerLimitZero);
    m3OrntZ = readMyoData([datasetpath,'z/',myo3,'_orient_data.csv'], loadLowerLimit, loadUpperLimit, loadSetLowerLimitZero);
    m4OrntZ = readMyoData([datasetpath,'z/',myo4,'_orient_data.csv'], loadLowerLimit, loadUpperLimit, loadSetLowerLimitZero);
end

%Preprocess data
if (AcclActive)
    CurrentlyA = true;
    CurrentlyG = false;
    CurrentlyO = false;
    CurrentlyO2 = false;
    m1AcclX = preprocessData(m1AcclX, mvAvgA, outlierThresholdA);
    m2AcclX = preprocessData(m2AcclX, mvAvgA, outlierThresholdA);
    m3AcclX = preprocessData(m3AcclX, mvAvgA, outlierThresholdA);
    m4AcclX = preprocessData(m4AcclX, mvAvgA, outlierThresholdA);
    m1AcclY = preprocessData(m1AcclY, mvAvgA, outlierThresholdA);
    m2AcclY = preprocessData(m2AcclY, mvAvgA, outlierThresholdA);
    m3AcclY = preprocessData(m3AcclY, mvAvgA, outlierThresholdA);
    m4AcclY = preprocessData(m4AcclY, mvAvgA, outlierThresholdA);
    m1AcclZ = preprocessData(m1AcclZ, mvAvgA, outlierThresholdA);
    m2AcclZ = preprocessData(m2AcclZ, mvAvgA, outlierThresholdA);
    m3AcclZ = preprocessData(m3AcclZ, mvAvgA, outlierThresholdA);
    m4AcclZ = preprocessData(m4AcclZ, mvAvgA, outlierThresholdA);
end
if (GyroActive)
    CurrentlyA = false;
    CurrentlyG = true;
    CurrentlyO = false;
    CurrentlyO2 = false;
    m1GyroX = preprocessData(m1GyroX, mvAvgG, outlierThresholdG);
    m2GyroX = preprocessData(m2GyroX, mvAvgG, outlierThresholdG);
    m3GyroX = preprocessData(m3GyroX, mvAvgG, outlierThresholdG);
    m4GyroX = preprocessData(m4GyroX, mvAvgG, outlierThresholdG);
    m1GyroY = preprocessData(m1GyroY, mvAvgG, outlierThresholdG);
    m2GyroY = preprocessData(m2GyroY, mvAvgG, outlierThresholdG);
    m3GyroY = preprocessData(m3GyroY, mvAvgG, outlierThresholdG);
    m4GyroY = preprocessData(m4GyroY, mvAvgG, outlierThresholdG);
    m1GyroZ = preprocessData(m1GyroZ, mvAvgG, outlierThresholdG);
    m2GyroZ = preprocessData(m2GyroZ, mvAvgG, outlierThresholdG);
    m3GyroZ = preprocessData(m3GyroZ, mvAvgG, outlierThresholdG);
    m4GyroZ = preprocessData(m4GyroZ, mvAvgG, outlierThresholdG);
end
if (OrntActive)
    CurrentlyA = false;
    CurrentlyG = false;
    CurrentlyO = true;
    CurrentlyO2 = false;
    m1OrntX = preprocessData(m1OrntX, mvAvgO, outlierThresholdO);
    m2OrntX = preprocessData(m2OrntX, mvAvgO, outlierThresholdO);
    m3OrntX = preprocessData(m3OrntX, mvAvgO, outlierThresholdO);
    m4OrntX = preprocessData(m4OrntX, mvAvgO, outlierThresholdO);
    m1OrntY = preprocessData(m1OrntY, mvAvgO, outlierThresholdO);
    m2OrntY = preprocessData(m2OrntY, mvAvgO, outlierThresholdO);
    m3OrntY = preprocessData(m3OrntY, mvAvgO, outlierThresholdO);
    m4OrntY = preprocessData(m4OrntY, mvAvgO, outlierThresholdO);
    m1OrntZ = preprocessData(m1OrntZ, mvAvgO, outlierThresholdO);
    m2OrntZ = preprocessData(m2OrntZ, mvAvgO, outlierThresholdO);
    m3OrntZ = preprocessData(m3OrntZ, mvAvgO, outlierThresholdO);
    m4OrntZ = preprocessData(m4OrntZ, mvAvgO, outlierThresholdO);
end
if (AnglActive)
    CurrentlyA = false;
    CurrentlyG = false;
    CurrentlyO = false;
    CurrentlyO2 = true;
    m1OrntX = preprocessData(m1OrntX, mvAvgO2, outlierThresholdO2);
    m2OrntX = preprocessData(m2OrntX, mvAvgO2, outlierThresholdO2);
    m3OrntX = preprocessData(m3OrntX, mvAvgO2, outlierThresholdO2);
    m4OrntX = preprocessData(m4OrntX, mvAvgO2, outlierThresholdO2);
    m1OrntY = preprocessData(m1OrntY, mvAvgO2, outlierThresholdO2);
    m2OrntY = preprocessData(m2OrntY, mvAvgO2, outlierThresholdO2);
    m3OrntY = preprocessData(m3OrntY, mvAvgO2, outlierThresholdO2);
    m4OrntY = preprocessData(m4OrntY, mvAvgO2, outlierThresholdO2);
    m1OrntZ = preprocessData(m1OrntZ, mvAvgO2, outlierThresholdO2);
    m2OrntZ = preprocessData(m2OrntZ, mvAvgO2, outlierThresholdO2);
    m3OrntZ = preprocessData(m3OrntZ, mvAvgO2, outlierThresholdO2);
    m4OrntZ = preprocessData(m4OrntZ, mvAvgO2, outlierThresholdO2);
end

%Plot data
if (AcclActive)
    CurrentlyA = true;
    CurrentlyG = false;
    CurrentlyO = false;
    CurrentlyO2 = false;
    figure('Color','white','DefaultAxesFontSize',plotFontSize,'Name','Accelerometer X','NumberTitle','off');
    plotAllMyoImuData(m1AcclX, m2AcclX, m3AcclX, m4AcclX, m1AcclY, m2AcclY, m3AcclY, m4AcclY, m1AcclZ, m2AcclZ, m3AcclZ, m4AcclZ, 2, 'r', 'b', '');
    figure('Color','white','DefaultAxesFontSize',plotFontSize,'Name','Accelerometer Y','NumberTitle','off');
    plotAllMyoImuData(m1AcclX, m2AcclX, m3AcclX, m4AcclX, m1AcclY, m2AcclY, m3AcclY, m4AcclY, m1AcclZ, m2AcclZ, m3AcclZ, m4AcclZ, 3, 'g', 'k', '');
    figure('Color','white','DefaultAxesFontSize',plotFontSize,'Name','Accelerometer Z','NumberTitle','off');
    plotAllMyoImuData(m1AcclX, m2AcclX, m3AcclX, m4AcclX, m1AcclY, m2AcclY, m3AcclY, m4AcclY, m1AcclZ, m2AcclZ, m3AcclZ, m4AcclZ, 4, 'b', 'c', '');
    figure('Color','white','DefaultAxesFontSize',plotFontSize,'Name','Accelerometer VecLen','NumberTitle','off');
    plotAllMyoImuData(m1AcclX, m2AcclX, m3AcclX, m4AcclX, m1AcclY, m2AcclY, m3AcclY, m4AcclY, m1AcclZ, m2AcclZ, m3AcclZ, m4AcclZ, 5, 'k', 'y', '');
end
if (GyroActive)
    CurrentlyA = false;
    CurrentlyG = true;
    CurrentlyO = false;
    CurrentlyO2 = false;
    figure('Color','white','DefaultAxesFontSize',plotFontSize,'Name','Gyroscope Combined','NumberTitle','off');
    plotAllGyroScalingData(m1GyroX, m2GyroX, m3GyroX, m4GyroX, m1GyroY, m2GyroY, m3GyroY, m4GyroY, m1GyroZ, m2GyroZ, m3GyroZ, m4GyroZ, '');
    %figure('Color','white','DefaultAxesFontSize',plotFontSize,'Name','Gyroscope X','NumberTitle','off');
    %plotAllMyoImuData(m1GyroX, m2GyroX, m3GyroX, m4GyroX, m1GyroY, m2GyroY, m3GyroY, m4GyroY, m1GyroZ, m2GyroZ, m3GyroZ, m4GyroZ, 2, 'r', 'b', '');
    %figure('Color','white','DefaultAxesFontSize',plotFontSize,'Name','Gyroscope Y','NumberTitle','off');
    %plotAllMyoImuData(m1GyroX, m2GyroX, m3GyroX, m4GyroX, m1GyroY, m2GyroY, m3GyroY, m4GyroY, m1GyroZ, m2GyroZ, m3GyroZ, m4GyroZ, 3, 'g', 'k', '');
    %figure('Color','white','DefaultAxesFontSize',plotFontSize,'Name','Gyroscope Z','NumberTitle','off');
    %plotAllMyoImuData(m1GyroX, m2GyroX, m3GyroX, m4GyroX, m1GyroY, m2GyroY, m3GyroY, m4GyroY, m1GyroZ, m2GyroZ, m3GyroZ, m4GyroZ, 4, 'b', 'c', '');
    %figure('Color','white','DefaultAxesFontSize',plotFontSize,'Name','Gyroscope VecLen','NumberTitle','off');
    %plotAllMyoImuData(m1GyroX, m2GyroX, m3GyroX, m4GyroX, m1GyroY, m2GyroY, m3GyroY, m4GyroY, m1GyroZ, m2GyroZ, m3GyroZ, m4GyroZ, 5, 'k', 'y');
end
if (OrntActive)
    CurrentlyA = false;
    CurrentlyG = false;
    CurrentlyO = true;
    CurrentlyO2 = false;
    figure('Color','white','DefaultAxesFontSize',plotFontSize,'Name','Orientation X','NumberTitle','off');
    plotAllMyoImuData(m1OrntX, m2OrntX, m3OrntX, m4OrntX, m1OrntY, m2OrntY, m3OrntY, m4OrntY, m1OrntZ, m2OrntZ, m3OrntZ, m4OrntZ, 2, 'r', 'b', '');
    figure('Color','white','DefaultAxesFontSize',plotFontSize,'Name','Orientation Y','NumberTitle','off');
    plotAllMyoImuData(m1OrntX, m2OrntX, m3OrntX, m4OrntX, m1OrntY, m2OrntY, m3OrntY, m4OrntY, m1OrntZ, m2OrntZ, m3OrntZ, m4OrntZ, 3, 'g', 'k', '');
    figure('Color','white','DefaultAxesFontSize',plotFontSize,'Name','Orientation Z','NumberTitle','off');
    plotAllMyoImuData(m1OrntX, m2OrntX, m3OrntX, m4OrntX, m1OrntY, m2OrntY, m3OrntY, m4OrntY, m1OrntZ, m2OrntZ, m3OrntZ, m4OrntZ, 4, 'b', 'c', '');
    figure('Color','white','DefaultAxesFontSize',plotFontSize,'Name','Orientation W','NumberTitle','off');
    plotAllMyoImuData(m1OrntX, m2OrntX, m3OrntX, m4OrntX, m1OrntY, m2OrntY, m3OrntY, m4OrntY, m1OrntZ, m2OrntZ, m3OrntZ, m4OrntZ, 5, 'k', 'y', '');
    %figure('Color','white','DefaultAxesFontSize',plotFontSize,'Name','Orientation VecLen','NumberTitle','off');
    %plotAllMyoImuData(m1OrntX, m2OrntX, m3OrntX, m4OrntX, m1OrntY, m2OrntY, m3OrntY, m4OrntY, m1OrntZ, m2OrntZ, m3OrntZ, m4OrntZ, 6, 'k', 'y');
end
if (AnglActive)
    CurrentlyA = false;
    CurrentlyG = false;
    CurrentlyO = false;
    CurrentlyO2 = true;
    figure('Color','white','DefaultAxesFontSize',plotFontSize,'Name','Angles Yaw','NumberTitle','off');
    plotAllMyoImuData(m1OrntX, m2OrntX, m3OrntX, m4OrntX, m1OrntY, m2OrntY, m3OrntY, m4OrntY, m1OrntZ, m2OrntZ, m3OrntZ, m4OrntZ, 2, 'r', 'b', '');
    figure('Color','white','DefaultAxesFontSize',plotFontSize,'Name','Angles Pitch','NumberTitle','off');
    plotAllMyoImuData(m1OrntX, m2OrntX, m3OrntX, m4OrntX, m1OrntY, m2OrntY, m3OrntY, m4OrntY, m1OrntZ, m2OrntZ, m3OrntZ, m4OrntZ, 3, 'g', 'k', '');
    figure('Color','white','DefaultAxesFontSize',plotFontSize,'Name','Angles Roll','NumberTitle','off');
    plotAllMyoImuData(m1OrntX, m2OrntX, m3OrntX, m4OrntX, m1OrntY, m2OrntY, m3OrntY, m4OrntY, m1OrntZ, m2OrntZ, m3OrntZ, m4OrntZ, 4, 'b', 'c', '');
    %figure('Color','white','DefaultAxesFontSize',plotFontSize,'Name','Orientation VecLen','NumberTitle','off');
    %plotAllMyoImuData(m1OrntX, m2OrntX, m3OrntX, m4OrntX, m1OrntY, m2OrntY, m3OrntY, m4OrntY, m1OrntZ, m2OrntZ, m3OrntZ, m4OrntZ, 6, 'k', 'y');
end

%done
clear;

function y = preprocessData(data, mvAvg, outlierThreshold)
    global CurrentlyA CurrentlyG CurrentlyO CurrentlyO2;
    global vecNorm;
    dataNum = 0;
    if (CurrentlyA || CurrentlyG || CurrentlyO2)
        dataNum = 3;
    end
    if (CurrentlyO)
        dataNum = 4;
    end
    finData = zeros(size(data,1), size(data,2)+dataNum+2);
    if (CurrentlyO2)
        finData(:,1) = data(:,1);
        finData(:,2:dataNum+1) = quat2angle(data(:,2:dataNum+2));
    else
        finData(:,1:dataNum+1) = data(:,1:dataNum+1);
    end
    if (outlierThreshold>0)
        for i = 2:dataNum+1
            meanVar = mean(finData(:,i));
            markedForRemoval = [];
            for j = 1:size(finData,1)
                if (finData(j,i)>meanVar+outlierThreshold) || (finData(j,i)<meanVar-outlierThreshold)
                    markedForRemoval(size(markedForRemoval)+1) = j;
                end
            end
            finData(markedForRemoval,:) = [];
        end
    end
    if (CurrentlyG)
        finData(:,2:3) = finData(:,2:3).*-1;
    end
    for i = 2:dataNum+1
        finData(:,dataNum+2) = finData(:,dataNum+2)+(abs(finData(:,i)).^vecNorm);
    end
    finData(:,dataNum+2) = finData(:,dataNum+2).^(1/vecNorm);
    for i = 2:dataNum+1
        for j = 1:size(finData,1)
            finData(j,i+dataNum+1) = mean(finData(max([1,(j-mvAvg)]):min([size(finData,1),(j+mvAvg)]),i));
        end
    end
    for i = dataNum+3:dataNum*2+2
        finData(:,dataNum*2+3) = finData(:,dataNum*2+3)+(abs(finData(:,i)).^vecNorm);
    end
    finData(:,dataNum*2+3) = finData(:,dataNum*2+3).^(1/vecNorm);
    y = finData;
end

function plotAllMyoImuData(m1X, m2X, m3X, m4X, m1Y, m2Y, m3Y, m4Y, m1Z, m2Z, m3Z, m4Z, i, c, c2, label)
    plotMyoImuData(i, c, c2, m1X, 'X-Aligned #1', 3, 4, 1, label);
    plotMyoImuData(i, c, c2, m2X, 'X-Aligned #2', 3, 4, 2, label);
    plotMyoImuData(i, c, c2, m3X, 'X-Aligned #3', 3, 4, 3, label);
    plotMyoImuData(i, c, c2, m4X, 'X-Aligned #4', 3, 4, 4, label);
    plotMyoImuData(i, c, c2, m1Y, 'Y-Aligned #1', 3, 4, 5, label);
    plotMyoImuData(i, c, c2, m2Y, 'Y-Aligned #2', 3, 4, 6, label);
    plotMyoImuData(i, c, c2, m3Y, 'Y-Aligned #3', 3, 4, 7, label);
    plotMyoImuData(i, c, c2, m4Y, 'Y-Aligned #4', 3, 4, 8, label);
    plotMyoImuData(i, c, c2, m1Z, 'Z-Aligned #1', 3, 4, 9, label);
    plotMyoImuData(i, c, c2, m2Z, 'Z-Aligned #2', 3, 4, 10, label);
    plotMyoImuData(i, c, c2, m3Z, 'Z-Aligned #3', 3, 4, 11, label);
    plotMyoImuData(i, c, c2, m4Z, 'Z-Aligned #4', 3, 4, 12, label);
end

function plotAllGyroScalingData(m1X, m2X, m3X, m4X, m1Y, m2Y, m3Y, m4Y, m1Z, m2Z, m3Z, m4Z, label)
    plotMyoImuData(2, 'r', 'b', m1X, 'X-Aligned #1', 3, 4, 1, label);
    plotMyoImuData(2, 'r', 'b', m2X, 'X-Aligned #2', 3, 4, 2, label);
    plotMyoImuData(2, 'r', 'b', m3X, 'X-Aligned #3', 3, 4, 3, label);
    plotMyoImuData(2, 'r', 'b', m4X, 'X-Aligned #4', 3, 4, 4, label);
    plotMyoImuData(3, 'g', 'k', m1Y, 'Y-Aligned #1', 3, 4, 5, label);
    plotMyoImuData(3, 'g', 'k', m2Y, 'Y-Aligned #2', 3, 4, 6, label);
    plotMyoImuData(3, 'g', 'k', m3Y, 'Y-Aligned #3', 3, 4, 7, label);
    plotMyoImuData(3, 'g', 'k', m4Y, 'Y-Aligned #4', 3, 4, 8, label);
    plotMyoImuData(4, 'b', 'c', m1Z, 'Z-Aligned #1', 3, 4, 9, label);
    plotMyoImuData(4, 'b', 'c', m2Z, 'Z-Aligned #2', 3, 4, 10, label);
    plotMyoImuData(4, 'b', 'c', m3Z, 'Z-Aligned #3', 3, 4, 11, label);
    plotMyoImuData(4, 'b', 'c', m4Z, 'Z-Aligned #4', 3, 4, 12, label);
end

function plotMyoImuData(i, c, c2, data, titleStr, subplotRows, subplotCols, subplotPos, label)
    global CurrentlyA CurrentlyG CurrentlyO CurrentlyO2;
    dataNum = 0;
    if (CurrentlyA || CurrentlyG || CurrentlyO2)
        dataNum = 3;
    end
    if (CurrentlyO)
        dataNum = 4;
    end
    ax = subplot(subplotRows,subplotCols,subplotPos);
    hold on;
    plot(ax,data(:,1),data(:,i),c);
    plot(ax,data(:,1),data(:,i+dataNum+1),c2);
    meanVar = mean(data(:,i));
    plot(ax,data(1,1),meanVar,'w');
    %plot(ax,data(:,1),zeros(size(data,1))+mean(data(:,2)),'r-');
    %plot(ax,data(:,1),data(:,3),'g');
    %plot(ax,data(:,1),zeros(size(data,1))+mean(data(:,3)),'g-');
    %plot(ax,data(:,1),data(:,4),'b');
    %plot(ax,data(:,1),zeros(size(data,1))+mean(data(:,4)),'b-');
    %plot(ax,data(:,1),data(:,5),'k');
    hold off;
    %xlabel('t');
    if CurrentlyA
        %axis(ax,[-inf inf meanVar-0.0235 meanVar+0.0235]);
        axis(ax,[-inf inf -inf inf]);
        %ylabel(label);
    end
    if CurrentlyG
        %axis(ax,[-inf inf -inf inf]);
        axis(ax,[-inf inf 725 950]);
        %axis(ax,[-inf inf -0.25 1]);
        %ylabel(label);
    end
    if CurrentlyO
        axis(ax,[-inf inf -inf inf]);
        %ylabel(label);
    end
    
    set(gca,'YTickLabel',num2str(get(gca,'YTick').'));
    title(titleStr);
    legendEntries={num2str(max(data(:,i))-min(data(:,i)),'%.5f'), num2str(max(data(:,i+4))-min(data(:,i+4)),'%.5f'), num2str(meanVar,'%.2f')};
    legend(legendEntries,'Interpreter','none');
end