o = Vec3SurfCompat();
coords1=[
    -1 0 1
    -1 -1 0
    -1 0 -1
    -1 1 0
    -1 0 1];
coords2=[
    1 0 1
    1 -1 0
    1 0 -1
    1 1 0
    1 0 1];
o.multiMerge([coords1, coords2]);
[x,y,z] = o.toSurfaceCoordinates();
surf(x,y,z);