classdef Vec3SurfCompat < handle
    properties
        mat
        dim
    end
    methods
        function obj = Vec3SurfCompat()
            
        end
        
        function multiMerge(this, coords)
            for i = 0:(round(size(coords,2)/3)-1)
                this.merge(coords(:,(i*3+1):(i*3+3)));
            end
        end

        function merge(this, src2)
            if (isempty(this.mat))
                this.mat = src2(:,1:3);
                this.dim = 1;
            else
                if (round(size(this.mat)/this.dim) ~= size(src2))
                    error('Size mismatch when trying to join');
                end
                src1 = this.mat;
                for i = 0:(size(src2,1)-1)
                    for j = 0:this.dim
                        srcIndex1 = (i*this.dim+j)+1;
                        srcIndex2 = i+1;
                        targetIndex = (i*(this.dim+1)+j)+1;
                        if j == this.dim
                            this.mat(targetIndex,:) = src2(srcIndex2,:);
                        else
                            this.mat(targetIndex,:) = src1(srcIndex1,:);
                        end
                    end
                end
                this.dim = this.dim+1;
            end
        end
        
        function [X,Y,Z] = toSurfaceCoordinates(this)
            X = vec2mat(this.mat(:,1),this.dim);
            Y = vec2mat(this.mat(:,2),this.dim);
            Z = vec2mat(this.mat(:,3),this.dim);
        end
    end
    
end

