clear;
clf;
clc;
%loadDataset: Folder names of datasets to load
%windowAssociations: Association of datasets into figure windows: TITLE_ROWS_COLS_POS where the numbers ROWS COLS POS describe the subfigure position, and how many rows and cols the figure window with the title TITLE has
%loadSettings: Settings file overwriting dataset settings (one file for all datasets basically)

%loadDataset = {'analysis\simtranslation'};
%windowAssociations = {};

%loadDataset = {'analysis\'};

%loadDataset = {'analysis\zero'};
%windowAssociations = {'Zero_1_1_1'};

%loadDataset = {'real\relaxed_fist1'};
%windowAssociations = {'Relaxed Fist 1_1_1_1'};

%loadDataset = {'analysis\rvc_fabian','analysis\rvc_ina','analysis\rvc_ioannis','analysis\rvc_johannes','analysis\rvc_tarek5','analysis\rvc_yannic'};
%windowAssociations = {'Analysis_2_3_1', 'Analysis_2_3_2', 'Analysis_2_3_3', 'Analysis_2_3_4', 'Analysis_2_3_5', 'Analysis_2_3_6'};
%loadSettings = 'analysis\RVC';

loadDataset = {'analysis\visualize'};
windowAssociations = {'Visualization_1_1_1'};
loadSettings = 'analysis\RVC';

%loadDataset = {'analysis\rvc_tarek1','analysis\rvc_tarek2','analysis\rvc_tarek3','analysis\rvc_tarek4','analysis\rvc_tarek5','analysis\rvc_tarek6','analysis\rvc_tarek8','analysis\rvc_tarek9','analysis\rvc_tarek11','analysis\rvc_tarek12'};
%windowAssociations = {'Analysis_2_5_1', 'Analysis_2_5_6', 'Analysis_2_5_2', 'Analysis_2_5_7', 'Analysis_2_5_3', 'Analysis_2_5_8', 'Analysis_2_5_4', 'Analysis_2_5_9', 'Analysis_2_5_5', 'Analysis_2_5_10'};
%loadSettings = 'analysis\REPEATABILITY';

%loadDataset = {'analysis\comparison_translation\low0','analysis\comparison_translation\low1','analysis\comparison_translation\low2','analysis\comparison_translation\low3','analysis\comparison_translation\low0','analysis\comparison_translation\low1','analysis\comparison_translation\low2','analysis\comparison_translation\low3','analysis\comparison_translation\high0','analysis\comparison_translation\high1','analysis\comparison_translation\high2','analysis\comparison_translation\high3','analysis\comparison_translation\high0','analysis\comparison_translation\high1','analysis\comparison_translation\high2','analysis\comparison_translation\high3'};
%windowAssociations = {'Analysis_4_4_1', 'Analysis_4_4_2', 'Analysis_4_4_3', 'Analysis_4_4_4', 'Analysis_4_4_5', 'Analysis_4_4_6', 'Analysis_4_4_7', 'Analysis_4_4_8', 'Analysis_4_4_9', 'Analysis_4_4_10', 'Analysis_4_4_11', 'Analysis_4_4_12', 'Analysis_4_4_13', 'Analysis_4_4_14', 'Analysis_4_4_15', 'Analysis_4_4_16'};
%loadSettings = 'analysis\COMPARISON_TRANSLATION';

%loadDataset = {'analysis\comparison_translation\low1','analysis\comparison_translation\low2','analysis\comparison_translation\low3','analysis\comparison_translation\low_simon1','analysis\comparison_translation\low_yannic','analysis\comparison_translation\low1','analysis\comparison_translation\low2','analysis\comparison_translation\low3','analysis\comparison_translation\low_simon1','analysis\comparison_translation\low_yannic','analysis\comparison_translation\high1','analysis\comparison_translation\high2','analysis\comparison_translation\high3','analysis\comparison_translation\high_simon1','analysis\comparison_translation\high_yannic','analysis\comparison_translation\high1','analysis\comparison_translation\high2','analysis\comparison_translation\high3','analysis\comparison_translation\high_simon1','analysis\comparison_translation\high_yannic'};
%windowAssociations = {'Analysis_4_5_1', 'Analysis_4_5_2', 'Analysis_4_5_3', 'Analysis_4_5_4', 'Analysis_4_5_5', 'Analysis_4_5_6', 'Analysis_4_5_7', 'Analysis_4_5_8', 'Analysis_4_5_9', 'Analysis_4_5_10', 'Analysis_4_5_11', 'Analysis_4_5_12', 'Analysis_4_5_13', 'Analysis_4_5_14', 'Analysis_4_5_15', 'Analysis_4_5_16', 'Analysis_4_5_17', 'Analysis_4_5_18', 'Analysis_4_5_19', 'Analysis_4_5_20'};
%loadSettings = 'analysis\COMPARISON_TRANSLATION_2_DEV_NORM';

%loadDataset = {'analysis\comparison_translation\low1','analysis\comparison_translation\low2','analysis\comparison_translation\low3','analysis\comparison_translation\high1','analysis\comparison_translation\high2','analysis\comparison_translation\high3'};
%windowAssociations = {'Analysis_2_3_1', 'Analysis_2_3_2', 'Analysis_2_3_3', 'Analysis_2_3_4', 'Analysis_2_3_5', 'Analysis_2_3_6'};
%loadSettings = 'analysis\COMPARISON_TRANSLATION_2_DEV';

%loadDataset = {'analysis\comparison_translation\low10','analysis\comparison_translation\mid10','analysis\comparison_translation\high10'};
%windowAssociations = {'Analysis_3_1_1', 'Analysis_3_1_2', 'Analysis_3_1_3'};
%loadSettings = 'analysis\COMPARISON_TRANSLATION_2_DEV_LMH';

%loadDataset = {'analysis\comparison_translation\low3','analysis\comparison_translation\low3','analysis\comparison_translation\high3','analysis\comparison_translation\high3'};
%windowAssociations = {'Analysis_2_2_1', 'Analysis_2_2_2', 'Analysis_2_2_3', 'Analysis_2_2_4'};
%loadSettings = 'analysis\COMPARISON_TRANSLATION_SINGLE';

%loadDataset = {'analysis\comparison_translation\low_simon1','analysis\comparison_translation\low_simon1','analysis\comparison_translation\high_simon1','analysis\comparison_translation\high_simon1'};
%windowAssociations = {'Analysis_2_2_1', 'Analysis_2_2_2', 'Analysis_2_2_3', 'Analysis_2_2_4'};
%loadSettings = 'analysis\COMPARISON_TRANSLATION_SINGLE';

%loadDataset = {'analysis\rvc_fabian','analysis\rvc_ina','analysis\rvc_fabian','analysis\rvc_ina'};
%windowAssociations = {'Analysis_2_2_1', 'Analysis_2_2_2', 'Analysis_2_2_3', 'Analysis_2_2_4'};
%loadSettings = 'analysis\COMPARISON';

%loadDataset = {'analysis\comparison_translation\sep_low0','analysis\comparison_translation\sep_perfect0','analysis\comparison_translation\sep_low0','analysis\comparison_translation\sep_low0','analysis\comparison_translation\sep_perfect0','analysis\comparison_translation\sep_low0'};
%loadDataset = {'analysis\comparison_translation\sep_high0','analysis\comparison_translation\sep_high1','analysis\comparison_translation\sep_high2','analysis\comparison_translation\sep_high0','analysis\comparison_translation\sep_high1','analysis\comparison_translation\sep_high2'};
%loadDataset = {'analysis\comparison_translation\low1', 'analysis\comparison_translation\low2', 'analysis\comparison_translation\low3', 'analysis\comparison_translation\low10', 'analysis\comparison_translation\low20', 'analysis\comparison_translation\low_simon0', 'analysis\comparison_translation\low_simon1', 'analysis\comparison_translation\low_yannic0', 'analysis\comparison_translation\mid10', 'analysis\comparison_translation\mid20', 'analysis\comparison_translation\high1', 'analysis\comparison_translation\high2', 'analysis\comparison_translation\high3', 'analysis\comparison_translation\high10', 'analysis\comparison_translation\high20', 'analysis\comparison_translation\high21', 'analysis\comparison_translation\high22', 'analysis\comparison_translation\high_simon0', 'analysis\comparison_translation\high_simon1', 'analysis\comparison_translation\high_simon10', 'analysis\comparison_translation\high_yannic0'};
%loadDataset = [loadDataset, loadDataset];
%windowAssociations = {'Dev2_3_11_1', 'Dev2_3_11_2', 'Dev2_3_11_3', 'Dev2_3_11_4', 'Dev2_3_11_5', 'Dev2_3_11_8', 'Dev2_3_11_9', 'Dev2_3_11_11', 'Dev2_3_11_15', 'Dev2_3_11_16', 'Dev2_3_11_23', 'Dev2_3_11_24', 'Dev2_3_11_25', 'Dev2_3_11_26', 'Dev2_3_11_27', 'Dev2_3_11_28', 'Dev2_3_11_29', 'Dev2_3_11_30', 'Dev2_3_11_31', 'Dev2_3_11_32', 'Dev2_3_11_33'};
%windowAssociations = [windowAssociations, {'Dev4_3_11_1', 'Dev4_3_11_2', 'Dev4_3_11_3', 'Dev4_3_11_4', 'Dev4_3_11_5', 'Dev4_3_11_8', 'Dev4_3_11_9', 'Dev4_3_11_11', 'Dev4_3_11_15', 'Dev4_3_11_16', 'Dev4_3_11_23', 'Dev4_3_11_24', 'Dev4_3_11_25', 'Dev4_3_11_26', 'Dev4_3_11_27', 'Dev4_3_11_28', 'Dev4_3_11_29', 'Dev4_3_11_30', 'Dev4_3_11_31', 'Dev4_3_11_32', 'Dev4_3_11_33'}];
%loadSettings = 'analysis\COMPARISON_TRANSLATION_ALL';

%close unrelated, still opened figure windows
allfigs = findall(0,'type','figure');
for f = 1:size(allfigs)
    fig = allfigs(f);
    %if size(find(strcmp(loadDataset, get(fig,'Name'))),2) == 0
        close(fig); %close all figure windows
    %end
end
%Iterate through datasets
for f = 1:size(loadDataset,2)
    folder = loadDataset{f};
    if (size(windowAssociations,2)>=f)
        windowAssoc = windowAssociations{f};
        windowAssoc = strsplit(windowAssoc,'_');
        windowAssoc{2} = str2num(windowAssoc{2});
        windowAssoc{3} = str2num(windowAssoc{3});
        windowAssoc{4} = str2num(windowAssoc{4});
    else
        windowAssoc = {folder,1,1,1};
    end
    %reload original settings
    Settings;
    %dataset settings
    datasetSettings = fullfile(cd, [folder,'\Settings.m']);
    if exist(datasetSettings, 'file') == 2
        run(datasetSettings);
    end
    %common settings
    if exist('loadSettings', 'var')
        commonSetting = fullfile(cd, [loadSettings,'.m']);
        if exist(commonSetting, 'file') == 2
            run(commonSetting);
        end
    end
    set(gca, 'colororder', plotColorOrder);
    if (f==1)
        close; %some settings create a new figure window, if there is none. close it
    end
    %Iterate through angle folders
    if ~loadAngles
        plotAngles = 0;
    end
    for i = 1:size(plotAngles,2)
        angle = plotAngles(i);
        if loadAngles
            anglefolder = [folder,'\angle_',num2str(angle,'%05.1f'),'\'];
        else
            anglefolder = [folder,'\'];
        end
        if myoLeft
            left = readMyoData([anglefolder,myoLeftMac,'_emg_data.csv'], loadLowerLimit, loadUpperLimit, loadSetLowerLimitZero);
            left = preprocessEmgData(left);
        end
        if myoRight
            right = readMyoData([anglefolder,myoRightMac,'_emg_data.csv'], loadLowerLimit, loadUpperLimit, loadSetLowerLimitZero);
            right = preprocessEmgData(right);
        end
        if (plot3dEnabled && interpolationEnabled)
            if myoLeft
                oLeft = interpolateEmgData(left, myoLeftUsbWrist, 0);
            end
            if myoRight
                oRight = interpolateEmgData(right, myoRightUsbWrist, angle);
            end
        end
        if (plot3dEnabled && interpolationEnabled)
            if myoLeft
                plotMyoInterpolatedEmgData(oLeft, [myoLeftLabel,iif(loadAngles,[' ',num2str(angle,'%05.1f'),'�'],'')], iif(myoRight, 2, 1), size(plotAngles,2), i);
            end
            if myoRight
                plotMyoInterpolatedEmgData(oRight, [myoRightLabel,iif(loadAngles,[' ',num2str(angle,'%05.1f'),'�'],'')], iif(myoLeft, 2, 1), size(plotAngles,2), i+iif(myoLeft, size(plotAngles,2), 0));
            end
        else
            if ((~loadAngles || size(plotAngles,2) == 0) && size(windowAssociations,2) > 0)
                if myoLeft
                    plotMyoEmgData(windowAssoc{1}, left, [myoLeftLabel,iif(loadAngles,[' ',num2str(angle,'%05.1f'),'�'],'')], 0, myoLeftUsbWrist, windowAssoc{2}, windowAssoc{3}, windowAssoc{4});
                end
                if myoRight
                    plotMyoEmgData(windowAssoc{1}, right, [myoRightLabel,iif(loadAngles,[' ',num2str(angle,'%05.1f'),'�'],'')], angle, myoRightUsbWrist, windowAssoc{2}, windowAssoc{3}, windowAssoc{4});
                end
            else
                if myoLeft
                    plotMyoEmgData(windowAssoc{1}, left, [myoLeftLabel,iif(loadAngles,[' ',num2str(angle,'%05.1f'),'�'],'')], 0, myoLeftUsbWrist, iif(myoRight, 2, 1), size(plotAngles,2), i);
                end
                if myoRight
                    plotMyoEmgData(windowAssoc{1}, right, [myoRightLabel,iif(loadAngles,[' ',num2str(angle,'%05.1f'),'�'],'')], angle, myoRightUsbWrist, iif(myoLeft, 2, 1), size(plotAngles,2), i+iif(myoLeft, size(plotAngles,2), 0));
                end
            end
            %plotMyoEmgData(folder, right, [myoRightLabel,' ',num2str(angle,'%05.1f'),'�'], 0, myoRightUsbWrist, 2, round(size(plotAngles,2)/2), i);
        end
    end
end
%done
clear;

function y = preprocessEmgData(data)
    global prepFullWaveRectification prepHalfWaveRectification prepMvAvgHalfWinSize prepNormalizationThreshold prepNormalization prepNormalizationSourceChannel;
    global normalizationFactor normalizationFactorMean prepNormalizationSeperate prepNormalizationTarget prepNormalizationHalfWinSize plotTotalMeanThresholdByMean;
    global prepChannelDeviationFromMean prepChannelSpreadThreshold prepChannelSpread prepChannelDeviationThreshold channelDeviation;
    global prepZeroCrossingChannels prepZeroCrossingHalfWinSize prepZeroCrossingScale;
    global fftEnabled fftData;
    %statically inverse error in myolib first
    data(:,2:9) = data(:,2:9) - 127;
    if (fftEnabled)
        fftData = data;
    end
    %zero crossing rate
    if (size(prepZeroCrossingChannels,2)>0)
        data(:,11) = zeros(size(data,1),1);
        for i = prepZeroCrossingChannels
            for j = 1:size(data,1)
                ndata = data(max([1,j-prepZeroCrossingHalfWinSize]):min([size(data,1),j+prepZeroCrossingHalfWinSize]),i+1);
                zcr = 0;
                for k = 2:size(ndata,1)
                    if (ndata(k-1)>0 && ndata(k)<0) || (ndata(k-1)<0 && ndata(k)>0)
                        zcr = zcr+1;
                    end
                end
                data(j,11) = data(j,11) + (zcr/(prepZeroCrossingHalfWinSize*2)*(prepZeroCrossingScale+1));
            end
        end
        data(:,11) = data(:,11) ./ size(prepZeroCrossingChannels,2);
    end
    %full wave rectification
    for i = 2:9
        if prepFullWaveRectification
            data(:,i) = abs(data(:,i));
        else
            if prepHalfWaveRectification
                data(:,i) = min(data(:,i),0);
            end
        end
    end
    %add mean column
    for j = 1:size(data,1)
        data(j,10) = mean(data(j,2:9),2);
    end
    %normalization
    if (prepNormalization)
        if (prepNormalizationSeperate)
            normalizationFactor = zeros(10);
            for k = 2:10
                ndata = data(:,k);
                for j = 1:size(ndata,1)
                    ndata(j) = mean(ndata(max([1,(j-prepNormalizationHalfWinSize)]):min([size(ndata,1),(j+prepNormalizationHalfWinSize)])));
                end
                ndata = ndata(ndata >= prepNormalizationThreshold); %threshold after averaging
                totalMean = mean(ndata);
                if (plotTotalMeanThresholdByMean)
                    normalizationFactor(k-1) = prepNormalizationTarget/mean(ndata((ndata >= totalMean)));
                else
                    normalizationFactor(k-1) = prepNormalizationTarget/totalMean;
                end
                data(:,k) = data(:,k) * normalizationFactor(k-1);
                if (fftEnabled && k < 10)
                    fftData(:,k) = fftData(:,k) * normalizationFactor(k-1);
                end
            end
            %recalc mean
            for j = 1:size(data,1)
                data(j,10) = mean(data(j,2:9),2);
            end
            normalizationFactorMean = normalizationFactor(9);
            normalizationFactor(10) = mean(normalizationFactor(1:8));
        else
            ndata = data(:,prepNormalizationSourceChannel+1);
            for j = 1:size(ndata,1)
                ndata(j) = mean(ndata(max([1,(j-prepNormalizationHalfWinSize)]):min([size(ndata,1),(j+prepNormalizationHalfWinSize)])));
            end
            ndata = ndata(ndata >= prepNormalizationThreshold); %threshold after averaging
            totalMean = mean(ndata);
            if (plotTotalMeanThresholdByMean)
                normalizationFactor = prepNormalizationTarget/mean(ndata((ndata >= totalMean)));
            else
                normalizationFactor = prepNormalizationTarget/totalMean;
            end
            data(:,2:10) = data(:,2:10) * normalizationFactor;
            if (fftEnabled)
                fftData(:,2:9) = fftData(:,2:9) * normalizationFactor;
            end
        end
    end
    %moving average
    for i = 2:10
        for j = 1:size(data,1)
            data(j,i) = mean(data(max([1,(j-prepMvAvgHalfWinSize)]):min([size(data,1),(j+prepMvAvgHalfWinSize)]),i));
        end
    end
    %spread
    if (size(prepChannelSpread,2) > 0)
        if (prepNormalization)
            prepChannelSpreadThreshold = prepChannelSpreadThreshold * normalizationFactor;
        end
        maxdata = max(data(:,(prepChannelSpread+1)),[],2);
        mindata = min(data(:,(prepChannelSpread+1)),[],2);
        thresholdindizes = (mindata >= prepChannelSpreadThreshold);
        data(thresholdindizes, 11) = maxdata(thresholdindizes)-mindata(thresholdindizes);
        %data(data(:,10) >= prepChannelSpreadThreshold,11) = max(data(data(:,10) >= prepChannelSpreadThreshold,(prepChannelSpread+1)),[],2)-min(data(data(:,10) >= prepChannelSpreadThreshold,(prepChannelSpread+1)),[],2);
        %data(:,11) = abs(data(:,prepChannelSpread(1)+1)-data(:,prepChannelSpread(2)+1)).*abs(data(:,prepChannelSpread(2)+1)-data(:,prepChannelSpread(3)+1));
    end
    %deviation
    if (prepChannelDeviationFromMean > 0)
        threshold = prepChannelDeviationThreshold;
        if (prepNormalization)
            if (prepNormalizationSeperate)
                threshold = threshold * normalizationFactor(prepChannelDeviationFromMean);
            else
                threshold = threshold * normalizationFactor;
            end
        end
        ndata = data(:,prepChannelDeviationFromMean+1);
        channelDeviation = zeros(size(data(ndata >= threshold,1),1),2);
        channelDeviation(:,1) = data(ndata >= threshold,1); %according timestamps
        channelDeviation(:,2) = data(ndata >= threshold, prepChannelDeviationFromMean+1);
        channelDeviation(:,2) = channelDeviation(:,2)-data(ndata >= threshold, 10);
    end
    %out
    y = data;
end

function y = interpolateEmgData(data, usbWrist, angle)
    global interpolationResolution plot3dAngularOffset interpolationLowerBound interpolationUpperBound;
    offset = (plot3dAngularOffset*2*pi/360);
    direction = 1;
    if (usbWrist)
        direction = -1;
    end
    y = Vec3SurfCompat();
    disp('0.00 %');
    xSrcPchip = (-1/8*2*pi):(1/8*2*pi):(9/8*2*pi);
    xPchip = (-1/8*2*pi):((1/8*2*pi)/interpolationResolution):(9/8*2*pi);
    for i = 1:size(data,1)
        if (data(i,1)<interpolationLowerBound)
            continue;
        end
        if (data(i,1)>interpolationUpperBound)
            break;
        end
        %clc;
        disp([num2str((i / size(data,1)*100),'%5.2f') ' %']);
        yPchip = pchip(xSrcPchip,[data(i,9) data(i,2:9) data(i,2:3)],xPchip);
        index = 1;
        fin = zeros(round(size(xPchip,2)*(8/10)),3);
        for j = 1:size(xPchip,2)
            if xPchip(j)>=0 && xPchip(j)<=2*pi
                rad = pi/2+direction*(offset-angle+(xPchip(j)));
                fin(index,:) = [data(i,1) (yPchip(j)*cos(rad)) (yPchip(j)*sin(rad))];
                index = index+1;
            end
        end
        fin = fin(1:index-1,:);
        y.merge(fin);
    end
end

function plotMyoInterpolatedEmgData(obj, titleStr, subplotRows, subplotCols, subplotPos)
    global plot3dAxisLimits;
    ax = subplot(subplotRows,subplotCols,subplotPos);
    [X,Y,Z] = obj.toSurfaceCoordinates();
    plot = surfl(ax, X, Y, Z);
    colormap('gray');
    plot.FaceAlpha = .5;
    plot.FaceLighting = 'gouraud';
    shading interp
    axis(ax, plot3dAxisLimits);
    title(titleStr);
end

function plot = plotLineIn3d(axis, xData, yData, color)
    global plotColorOrder;
    persistent plotColorIterator;
    persistent lastAxis;
    if (isempty(plotColorIterator) || isempty(lastAxis) || axis ~= lastAxis)
        plotColorIterator = 0;
        lastAxis = axis;
    end
    if (nargin < 4)
        color = plotColorOrder(plotColorIterator + 1,:);
        plotColorIterator = mod(plotColorIterator + 1, size(plotColorOrder,1));
    end
    [X, Y] = meshgrid(xData,[0 0]);
    Z = zeros(1, size(yData,1));
    Z(1,:) = yData(:,1)';
    Z(2,:) = zeros(1,size(Z,2));
    plot = surf(axis,X,Y,Z);
    plot.FaceColor = color;
    plot.EdgeColor = color;
    plot.FaceLighting = 'gouraud';
end

function plotMyoEmgData(figName, data, titleStr, angle, usbWrist, subplotRows, subplotCols, subplotPos)
    global plotColorOrder plotChannels plotMean plotChannelMads plotTotalMean plotTotalMeanThreshold plotTotalMeanThresholdSourceChannel plotTotalMeanThresholdByMean plot3dEnabled plot3dAxisLimits plot2dAxisLimits;
    global plotFontSize plotChannelSpreadAverage plotDisplayLegend plotUseGridLegend plot3dAngularOffset prepNormalizationSeperate;
    global prepNormalization plotMeanMax plotNormalizationFactor normalizationFactor prepZeroCrossingHalfWinSize;
    global prepChannelDeviationFromMean prepChannelSpread plotChannelDeviationAverage channelDeviation prepZeroCrossingChannels prepZeroCrossingScale;
    global fftData fftEnabled fftChannels fftSampleFrequency fftAxisLimits fftMvAvgHalfWinSize;
    direction = 1;
    if (usbWrist)
        direction = -1;
    end
    fig = findall(0,'type','figure','name',figName);
    if size(fig)==0
        fig = figure('Color','white','DefaultAxesFontSize',plotFontSize,'Name',figName,'NumberTitle','off');
        fig.OuterPosition = [20 50 fig.OuterPosition(1,3)*2.5 fig.OuterPosition(1,4)*2];
    end
    figure(fig);
    ax = subplot(subplotRows,subplotCols,subplotPos,'FontSize',plotFontSize);
    %close(fig1);
    %ax.get('Parent').set('DefaultAxesFontSize', plotFontSize);
    hold on;
    if (~fftEnabled)
        plots = zeros(iif(~plot3dEnabled && plotMean, 1, 0)+iif(~plot3dEnabled && (size(prepChannelSpread,2) > 0), iif(plotChannelSpreadAverage, 2, 1), 0)+iif(~plot3dEnabled && plotTotalMeanThreshold > 0, 1, 0)+iif(~plot3dEnabled && plotTotalMeanThresholdByMean, 1, 0)+iif(~plot3dEnabled && plotTotalMean, 1, 0)+iif(~plot3dEnabled && plotMeanMax, 1, 0)+iif(~plot3dEnabled && plotNormalizationFactor && prepNormalization, 1, 0)+iif(~plot3dEnabled && prepChannelDeviationFromMean > 0, iif(plotChannelDeviationAverage, 2, 1), 0)+iif(size(prepZeroCrossingChannels,2)>0, 1, 0)+size(plotChannels,2)+iif(~plot3dEnabled, size(plotChannelMads,2), 0),1);
        legendEntries = cell(1, iif(~plot3dEnabled && plotMean, 1, 0)+iif(~plot3dEnabled && (size(prepChannelSpread,2) > 0), iif(plotChannelSpreadAverage, 2, 1), 0)+iif(~plot3dEnabled && plotTotalMeanThreshold > 0, 1, 0)+iif(~plot3dEnabled && plotTotalMeanThresholdByMean, 1, 0)+iif(~plot3dEnabled && plotTotalMean, 1, 0)+iif(~plot3dEnabled && plotMeanMax, 1, 0)+iif(~plot3dEnabled && plotNormalizationFactor && prepNormalization, 1, 0)+iif(~plot3dEnabled && prepChannelDeviationFromMean > 0, iif(plotChannelDeviationAverage, 2, 1), 0)+iif(size(prepZeroCrossingChannels,2)>0, 1, 0)+size(plotChannels,2)+iif(~plot3dEnabled, size(plotChannelMads,2),0));
        entryIt = 1;
        for i = plotChannels
            if (plot3dEnabled)
                lastPlot = plotLineIn3d(ax, data(:,1), data(:, i+1));
                rotate(lastPlot, [1 0 0], direction*(plot3dAngularOffset-angle+(360/8)*(i-1)), [0 0 0]);
            else
                lastPlot = plot(ax, data(:,1), data(:,i+1));
                lastPlot.Color(1:3) = plotColorOrder(mod(entryIt-1,size(plotColorOrder,1))+1,:);
            end
            plots(entryIt) = lastPlot;
            legendEntries{entryIt} = ['Channel ', int2str(i)];
            entryIt=entryIt+1;
        end
        if (~plot3dEnabled)
            plotTotalMeanThresholdDouble = 0;
            if (plotTotalMeanThresholdByMean)
                plotTotalMeanThresholdDouble = 0.5*mean(data((data(:,plotTotalMeanThresholdSourceChannel+1) >= plotTotalMeanThreshold),plotTotalMeanThresholdSourceChannel+1));
            end
            if (plotMean)
                lastPlot = plot(ax, data(:,1), data(:,10));
                lastPlot.Color(1:3) = plotColorOrder(mod(entryIt-1,size(plotColorOrder,1))+1,:);
                plots(entryIt) = lastPlot;
                legendEntries{entryIt} = 'Channel Mean';
                entryIt=entryIt+1;
            end
            if (size(prepChannelSpread,2) > 0)
                if (size(data(data(:,11) > 0,1)) > 0)
                    lastPlot = plot(ax, data(data(:,11) > 0,1), data(data(:,11) > 0,11));
                else
                    lastPlot = plot(ax, data(:,1), zeros(size(data(:,1),1),1));    
                end
                lastPlot.Color(1:3) = plotColorOrder(mod(entryIt-1,size(plotColorOrder,1))+1,:);
                plots(entryIt) = lastPlot;
                legendEntries{entryIt} = ['Spread (',sprintf('%.0f,', prepChannelSpread),')'];
                entryIt=entryIt+1;
            end
            if (plotTotalMeanThreshold > 0)
                thresholdIndexes = (data(:,plotTotalMeanThresholdSourceChannel+1) > plotTotalMeanThreshold);
                lastPlot = plot(ax, data(thresholdIndexes,1), data(thresholdIndexes,plotTotalMeanThresholdSourceChannel+1));
                lastPlot.Color(1:3) = plotColorOrder(mod(entryIt-1,size(plotColorOrder,1))+1,:);
                plots(entryIt) = lastPlot;
                legendEntries{entryIt} = ['Threshold: ',num2str(plotTotalMeanThreshold)];
                entryIt=entryIt+1;
            end
            if (plotTotalMeanThresholdDouble > 0)
                thresholdIndexes = (data(:,plotTotalMeanThresholdSourceChannel+1) > plotTotalMeanThresholdDouble);
                lastPlot = plot(ax, data(thresholdIndexes,1), data(thresholdIndexes,plotTotalMeanThresholdSourceChannel+1));
                lastPlot.Color(1:3) = plotColorOrder(mod(entryIt-1,size(plotColorOrder,1))+1,:);
                plots(entryIt) = lastPlot;
                legendEntries{entryIt} = ['HalfMean Threshold (',num2str(plotTotalMeanThresholdDouble),')'];
                entryIt=entryIt+1;
            end
            if (plotMeanMax)
                maximum = max(data(:,10));
                lastPlot = plot(ax, data(:,1), zeros(size(data,1),1)+maximum);
                lastPlot.Color(1:3) = 1;
                lastPlot.Color(4) = 0;
                plots(entryIt) = lastPlot;
                legendEntries{entryIt} = ['Mean Maximum: ',num2str(maximum,'%.2f')];
                entryIt=entryIt+1;
            end
            if (plotTotalMean)
                if (plotTotalMeanThreshold > 0)
                    if (plotTotalMeanThresholdByMean)
                        totalMean = mean(data((data(:,10) > plotTotalMeanThresholdDouble),10));
                    else
                        totalMean = mean(data((data(:,10) > plotTotalMeanThreshold),10));
                    end
                else
                    if (plotTotalMeanThresholdByMean)
                        totalMean = mean(data((data(:,10) > plotTotalMeanThresholdDouble),10));
                    else
                        totalMean = mean(data(:,10));
                    end
                end
                lastPlot = plot(ax, data(:,1), zeros(size(data,1),1)+totalMean);
                lastPlot.Color(1:3) = 1;
                lastPlot.Color(4) = 0;
                plots(entryIt) = lastPlot;
                legendEntries{entryIt} = ['Total Mean: ',num2str(totalMean,'%.2f')];
                entryIt=entryIt+1;
            end
            if (size(prepChannelSpread,2) > 0 && plotChannelSpreadAverage)
                if (size(data(data(:,11) > 0, 11),1) > 0)
                    spreadAverage = mean(data(data(:,11) > 0,11));
                else
                    spreadAverage = 0;
                end
                lastPlot = plot(ax, data(:,1), zeros(size(data,1),1)+spreadAverage);
                lastPlot.Color(1:3) = 1;
                lastPlot.Color(4) = 0;
                plots(entryIt) = lastPlot;
                legendEntries{entryIt} = ['Spread Avg: ',num2str(spreadAverage,'%.2f')];
                entryIt=entryIt+1;
            end
            if (plotNormalizationFactor && prepNormalization)
                if (prepNormalizationSeperate)
                    lastPlot = plot(ax, data(:,1), zeros(size(data,1),1)+normalizationFactor(1));
                    lastPlot.Color(1:3) = 1;
                    lastPlot.Color(4) = 0;
                    plots(entryIt) = lastPlot;
                    str = '';
                    for k = 1:8
                        str = [iif(size(str) == 0,str,[str,', ']),num2str(normalizationFactor(k),'%.2f')];
                    end
                    str = [str,'| ',num2str(normalizationFactor(10),'%.2f'),'| ',num2str(normalizationFactor(9),'%.2f')];
                    legendEntries{entryIt} = ['N.-Factors: ',str];
                    entryIt=entryIt+1;
                else
                    lastPlot = plot(ax, data(:,1), zeros(size(data,1),1)+normalizationFactor);
                    lastPlot.Color(1:3) = 1;
                    lastPlot.Color(4) = 0;
                    plots(entryIt) = lastPlot;
                    legendEntries{entryIt} = ['N.-Factor: ',num2str(normalizationFactor,'%.2f')];
                    entryIt=entryIt+1;
                end
            end
            if (prepChannelDeviationFromMean > 0)
                lastPlot = plot(ax, channelDeviation(:,1), channelDeviation(:,2));
                lastPlot.Color(1:3) = plotColorOrder(mod(entryIt-1,size(plotColorOrder,1))+1,:);
                plots(entryIt) = lastPlot;
                legendEntries{entryIt} = ['Ch. ',num2str(prepChannelDeviationFromMean),' Dev.'];
                entryIt=entryIt+1;
                if (plotChannelDeviationAverage)
                    deviationAverage = mean(channelDeviation(:,2));
                    lastPlot = plot(ax, data(:,1), zeros(size(data,1),1)+deviationAverage);
                    lastPlot.Color(1:3) = 1;
                    lastPlot.Color(4) = 0;
                    plots(entryIt) = lastPlot;
                    legendEntries{entryIt} = ['Dev. Avg.: ',num2str(deviationAverage,'%.2f')];
                    entryIt=entryIt+1;
                end
            end
            if (size(prepZeroCrossingChannels,2)>0)
                lastPlot = plot(ax, data(:,1), data(:,11));
                lastPlot.Color(1:3) = plotColorOrder(mod(entryIt-1,size(plotColorOrder,1))+1,:);
                plots(entryIt) = lastPlot;
                legendEntries{entryIt} = ['ZCR per ',num2str(prepZeroCrossingScale+1),' samples (',num2str(prepZeroCrossingScale/200,'%.2f'),'s) - ',num2str(prepZeroCrossingHalfWinSize*2+1),' window (',num2str(prepZeroCrossingHalfWinSize*2/200),'s)'];
                entryIt=entryIt+1;
            end
            if size(plotChannelMads,2)>0
                for i = plotChannelMads
                    %variance = std(data(:,i+1));
                    mad = mean(abs(data(:,i+1)-mean(data(:,i+1))));
                    lastPlot = plot(ax, data(:,1), zeros(size(data,1),1)+mad);
                    lastPlot.Color(1:3) = 1;
                    lastPlot.Color(4) = 0;
                    plots(entryIt) = lastPlot;
                    if i<9
                        legendEntries{entryIt} = ['MAD Ch.',num2str(i),': ',num2str(mad,'%.2f')];
                    else
                        legendEntries{entryIt} = ['MAD Mean: ',num2str(mad,'%.2f')];
                    end
                    entryIt=entryIt+1;
                end
            end
        end
    else %fft enabled
        plots = zeros(size(fftChannels,2),1);
        legendEntries = cell(1, size(fftChannels,2));
        entryIt = 1;
        for i = fftChannels
            Y = fft(fftData(:,i+1));
            P1 = abs(Y/size(fftData,1));
            P1 = P1(1:size(fftData,1)/2+1);
            P1(2:end-1) = 2*P1(2:end-1);
            f = fftSampleFrequency*(0:(size(fftData,1)/2))/size(fftData,1);
            if (fftMvAvgHalfWinSize > 0)
                for j = 1:size(P1,1)
                    P1(j) = mean(P1(max([1,(j-fftMvAvgHalfWinSize)]):min([size(P1,1),(j+fftMvAvgHalfWinSize)])));
                    P1(j) = mean(P1);
                end
            end
            lastPlot = plot(ax, f, P1);
                lastPlot.Color(1:3) = plotColorOrder(mod(entryIt-1,size(plotColorOrder,1))+1,:);
            plots(entryIt) = lastPlot;
            legendEntries{entryIt} = ['FFT Channel ', int2str(i)];
            entryIt=entryIt+1;
        end
    end
    hold off;
    if (plot3dEnabled && ~fftEnabled)
        axis(ax, plot3dAxisLimits);
    else
        if (fftEnabled)
            axis(ax, fftAxisLimits);
        else
            axis(ax, plot2dAxisLimits);
        end
    end
    title(titleStr,'Interpreter','none');
    if (plotDisplayLegend && ~plot3dEnabled && plotUseGridLegend)
        gridLegend(plots,2,legendEntries,'location','north','Fontsize',plotFontSize,'Interpreter','none');
    else
        if plotDisplayLegend
            legend(legendEntries,'Interpreter','none');
        end
    end
end

function b = iif(condition, t, f)
    if (condition)
        b = t;
    else
        b = f;
    end
end