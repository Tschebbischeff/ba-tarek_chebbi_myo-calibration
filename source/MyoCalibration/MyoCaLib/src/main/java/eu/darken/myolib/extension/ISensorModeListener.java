package eu.darken.myolib.extension;

import eu.darken.myolib.Myo;
import eu.darken.myolib.MyoCmds;
import eu.darken.myolib.msgs.MyoMsg;

/**
 * Created by Tarek on 05.07.2017.
 */
public interface ISensorModeListener {

    void onSensorModeWritten(Myo myo, MyoCmds.ImuMode requestedImuMode, MyoCmds.EmgMode requestedEmgMode, MyoCmds.ClassifierMode requestedClassifierMode, MyoMsg msg);
}
