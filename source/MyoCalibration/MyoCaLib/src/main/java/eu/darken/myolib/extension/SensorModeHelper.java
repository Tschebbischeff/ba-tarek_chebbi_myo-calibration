package eu.darken.myolib.extension;

import eu.darken.myolib.Myo;
import eu.darken.myolib.MyoCmds;
import eu.darken.myolib.msgs.MyoMsg;

/**
 * Created by Tarek on 05.07.2017.
 */
public class SensorModeHelper implements Myo.MyoCommandCallback {

    private MyoCmds.ImuMode imuMode;
    private MyoCmds.EmgMode emgMode;
    private MyoCmds.ClassifierMode classifierMode;
    private ISensorModeListener source;

    private SensorModeHelper(MyoCmds.ImuMode imuMode, MyoCmds.EmgMode emgMode, MyoCmds.ClassifierMode classifierMode, ISensorModeListener source) {
        this.source = source;
        this.imuMode = imuMode;
        this.emgMode = emgMode;
        this.classifierMode = classifierMode;
    }

    public static void writeSleepMode(Myo myo, MyoCmds.ImuMode imuMode, MyoCmds.EmgMode emgMode, MyoCmds.ClassifierMode classifierMode, ISensorModeListener source) {
        SensorModeHelper h = new SensorModeHelper(imuMode, emgMode, classifierMode, source);
        myo.writeMode(emgMode, imuMode, classifierMode, h);
    }

    @Override
    public void onCommandDone(Myo myo, MyoMsg msg) {
        this.source.onSensorModeWritten(myo, this.imuMode, this.emgMode, this.classifierMode, msg);
    }
}
