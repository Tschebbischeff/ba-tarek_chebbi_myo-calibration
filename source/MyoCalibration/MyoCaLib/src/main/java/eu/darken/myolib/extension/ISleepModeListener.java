package eu.darken.myolib.extension;

import eu.darken.myolib.Myo;
import eu.darken.myolib.MyoCmds;
import eu.darken.myolib.msgs.MyoMsg;

/**
 *
 * Created by Tarek on 05.07.2017.
 */
public interface ISleepModeListener {

    void onSleepModeWritten(Myo myo, MyoCmds.SleepMode requestedSleepMode, MyoMsg msg);
}
