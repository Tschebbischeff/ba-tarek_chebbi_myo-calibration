package eu.darken.myolib.extension;

import eu.darken.myolib.Myo;
import eu.darken.myolib.MyoCmds;
import eu.darken.myolib.msgs.MyoMsg;

/**
 *
 * Created by Tarek on 05.07.2017.
 */
public class SleepModeHelper implements Myo.MyoCommandCallback {

    private MyoCmds.SleepMode sleepMode;
    private ISleepModeListener source;

    private SleepModeHelper(MyoCmds.SleepMode sleepMode, ISleepModeListener source) {
        this.source = source;
        this.sleepMode = sleepMode;
    }

    public static void writeSleepMode(Myo myo, MyoCmds.SleepMode sleepMode, ISleepModeListener source) {
        SleepModeHelper h = new SleepModeHelper(sleepMode, source);
        myo.writeSleepMode(sleepMode, h);
    }

    @Override
    public void onCommandDone(Myo myo, MyoMsg msg) {
        this.source.onSleepModeWritten(myo, this.sleepMode, msg);
    }
}
