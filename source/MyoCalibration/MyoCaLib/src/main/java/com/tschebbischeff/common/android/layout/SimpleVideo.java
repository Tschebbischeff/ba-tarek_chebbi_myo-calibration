package com.tschebbischeff.common.android.layout;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.graphics.SurfaceTexture;
import android.media.MediaPlayer;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;

import java.io.IOException;

/**
 * Realizes and plays a video from a resource in a given Texture View. The video must be started by obtaining the MediaPlayer
 * after isReady() returns true. An onClickListener is automatically added to the root view, that pauses and resumes the
 * video when triggered. The texture view is modified based on the aspect ratio of the video. The width of the view
 * is retained, while the height is modified to fit the aspect ratio.
 */
public class SimpleVideo implements MediaPlayer.OnPreparedListener, View.OnClickListener, TextureView.SurfaceTextureListener {

    /**
     * The texture view in which to display the video.
     */
    private final TextureView linkedTextureView;
    /**
     * An android context for API calls.
     */
    private final Context context;
    /**
     * The media player playing the video.
     */
    private MediaPlayer mediaPlayer;
    /**
     * The surface texture of the texture view, when ready.
     */
    private SurfaceTexture surfaceTexture;
    /**
     * The resource id of the video to play.
     */
    private int videoResource;

    /**
     * Constructor.
     *
     * @param linkedTextureView The texture view in which to play the video.
     * @param context           An android context for API calls.
     * @param videoResource     The resource id of the video to play.
     */
    public SimpleVideo(TextureView linkedTextureView, Context context, int videoResource) {
        this.linkedTextureView = linkedTextureView;
        this.linkedTextureView.setOnClickListener(this);
        this.videoResource = videoResource;
        this.context = context;
        this.linkedTextureView.setSurfaceTextureListener(this);
    }

    /**
     * Returns the media player if it was sucessfully prepared and is ready to play the video.
     *
     * @return The media player responsible for playing the video. Null if it is not yet ready.
     */
    public MediaPlayer getMediaPlayer() {
        return mediaPlayer;
    }

    /**
     * Whether the media player has been prepared
     *
     * @return True if, and only if, the media player is currently ready to play the video.
     */
    public boolean isReady() {
        return this.mediaPlayer != null;
    }

    /**
     * Initializes the media player with the parameters given at construction.
     * Does nothing if it is already intialized. Use this, if you previously called finish() and the
     * texture view has not been destroyed.
     * <p>
     * This method is automatically called upon construction.
     */
    public void init() {
        if (this.mediaPlayer == null) {
            MediaPlayer mediaPlayer = new MediaPlayer();
            mediaPlayer.setOnPreparedListener(this);
            Surface surface = new Surface(this.surfaceTexture);
            try {
                AssetFileDescriptor afd = this.context.getResources().openRawResourceFd(this.videoResource);
                mediaPlayer.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
                afd.close();
                mediaPlayer.setSurface(surface);
                mediaPlayer.prepare();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Use this method to stop playback and release all resources. You can restart the media player with the init() method.
     */
    public void finish() {
        if (this.mediaPlayer != null) {
            this.mediaPlayer.release();
            this.mediaPlayer = null;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onPrepared(MediaPlayer mediaPlayer) {
        this.handleAspectRatio(mediaPlayer);
        this.mediaPlayer = mediaPlayer;
        this.mediaPlayer.start();
        this.mediaPlayer.pause();
        this.mediaPlayer.seekTo(0);
    }

    private void handleAspectRatio(MediaPlayer mediaPlayer) {
        int viewWidth = this.linkedTextureView.getWidth();
        int viewHeight = this.linkedTextureView.getHeight();
        float videoWidth = mediaPlayer.getVideoWidth();
        float videoHeight = mediaPlayer.getVideoHeight();
        float aspectRatio = videoWidth / videoHeight;

        final ViewGroup.LayoutParams layoutParams = this.linkedTextureView.getLayoutParams();

        if (viewWidth / videoWidth > viewHeight / videoHeight) {
            layoutParams.width = (int) (viewHeight * aspectRatio);
            layoutParams.height = viewHeight;
        } else {
            layoutParams.width = viewWidth;
            layoutParams.height = (int) (viewWidth / aspectRatio);
        }

        this.linkedTextureView.post(new Runnable() {
            @Override
            public void run() {
                SimpleVideo.this.linkedTextureView.setLayoutParams(layoutParams);
            }
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onClick(View view) {
        if (this.isReady()) {
            if (this.mediaPlayer.isPlaying()) {
                this.mediaPlayer.pause();
            } else {
                this.mediaPlayer.start();
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int i, int i1) {
        this.surfaceTexture = surfaceTexture;
        this.init();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int i, int i1) {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
        this.finish();
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {

    }
}
