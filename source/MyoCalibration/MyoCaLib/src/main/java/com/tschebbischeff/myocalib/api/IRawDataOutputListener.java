package com.tschebbischeff.myocalib.api;

import eu.darken.myolib.Myo;

/**
 * Interface to implement in classes which should be able to receive incoming raw data from the myo.
 * The interface's methods may be called as soon as the interface implementing class was added as a listener
 * in {@link CalibrationProfile}.
 * <p>
 * It is advised you add listeners when you actually want to receive data and remove it as soon as you do not
 * want to receive data anymore, as the library will automatically shut down bluetooth services depending on
 * the number of listeners, which in turn will save the Myo's battery.
 */
public interface IRawDataOutputListener {

    /**
     * Called when the calibrated Myo receives EMG data, frequency is approximately 200Hz, make sure you handle data
     * accordingly fast. This method is not called, when the Myo is not calibrated.
     *
     * @param myo       The Myo sending the data.
     * @param timestamp The timestamp determined by myolib, at which the data package was received.
     * @param data      The received data package.
     */
    void onRawEmgData(Myo myo, long timestamp, double[] data);

    /**
     * Called when the calibrated Myo receives IMU data, frequency is approximately 50Hz, make sure you handle data
     * accordingly fast. This method is not called, when the Myo is not calibrated.
     *
     * @param myo               The Myo sending the data.
     * @param timestamp         The timestamp determined by myolib, at which the data packages were received.
     * @param accelerometerData The received data package containing accelerometer data.
     * @param gyroscopeData     The received data package containing gyroscope data.
     * @param orientationData   A quaternion describing the current rotation of the Myo in a global coordinate system.
     *                          [w,x,y,z]
     */
    void onRawImuData(Myo myo, long timestamp, double[] accelerometerData, double[] gyroscopeData, double[] orientationData);
}
