package com.tschebbischeff.myocalib.internal.lazyui.pages.calibration.actionlist;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.tschebbischeff.myocalib.R;
import com.tschebbischeff.myocalib.internal.lazyui.pages.calibration.PageCalibration;
import eu.darken.myolib.Myo;

/**
 * Realizes the {@link PageCalibrationActionListAdapter.ViewType#ARM} action.
 * <p>
 * Asks the user if the Myo is still on the same arm, as it was when the choice was made at connection-time.
 * Changes the calibration related settings depending on the user's answer.
 */
public class PageCalibrationActionListViewHolderArm extends PageCalibrationActionListViewHolder {

    /**
     * The text view containing the question, posed to the user (Whether the armband is still located on the same arm).
     */
    public TextView question;
    /**
     * The button with the YES option.
     */
    public Button left;
    /**
     * The button with the NO option.
     */
    public Button right;

    /**
     * Constructor.
     *
     * @param source   The calibration page, for callbacks.
     * @param activity  The android context, for calling certain android methods.
     * @param rootView The view at the root the cards layout. Simply forwarded to superclass' constructor.
     */
    public PageCalibrationActionListViewHolderArm(PageCalibration source, PageCalibrationActionListAdapter adapter, Activity activity, View rootView) {
        super(source, adapter, activity, PageCalibrationActionListAdapter.ViewType.ARM, rootView);
        this.question = rootView.findViewById(R.id.myocalib_lazyui_pages_calibration_cardsarm_question);
        this.question.setText(PageCalibrationActionListAdapter.ViewType.IMU_EXPLANATION.enabled(source.getSharedPageData().getApiService().getCalibrationSettings())
                || PageCalibrationActionListAdapter.ViewType.IMU_X.enabled(source.getSharedPageData().getApiService().getCalibrationSettings())
                || PageCalibrationActionListAdapter.ViewType.IMU_Y.enabled(source.getSharedPageData().getApiService().getCalibrationSettings())
                || PageCalibrationActionListAdapter.ViewType.IMU_Z.enabled(source.getSharedPageData().getApiService().getCalibrationSettings()) ?
                this.getActivity().getResources().getString(R.string.myocalib_lazyui_pages_calibration_cardsarm_questionafterimu) :
                this.getActivity().getResources().getString(R.string.myocalib_lazyui_pages_calibration_cardsarm_question));
        this.left = rootView.findViewById(R.id.myocalib_lazyui_pages_calibration_cardsarm_buttonleft);
        this.right = rootView.findViewById(R.id.myocalib_lazyui_pages_calibration_cardsarm_buttonright);
        PageCalibrationActionListOnClickListener onClickListener = new PageCalibrationActionListOnClickListener(source, this);
        this.left.setOnClickListener(onClickListener);
        this.right.setOnClickListener(onClickListener);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void reset() {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onDestroy() {

    }

    /**
     * {@inheritDoc}
     * @param view
     */
    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.myocalib_lazyui_pages_calibration_cardsarm_buttonleft) {
            this.getSource().getSharedPageData().getWritableApiInterface().getCalibrationProfile(this.getSource().getSharedPageData().getCalibratingMyo()).setLeftArm();
        } else if (view.getId() == R.id.myocalib_lazyui_pages_calibration_cardsarm_buttonright) {
            this.getSource().getSharedPageData().getWritableApiInterface().getCalibrationProfile(this.getSource().getSharedPageData().getCalibratingMyo()).setRightArm();
        }
        this.getSource().stepCompleted();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onRawEmgData(Myo myo, long timestamp, double[] data) {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onRawImuData(Myo myo, long timestamp, double[] accelerometerData, double[] gyroscopeData, double[] orientationData) {

    }
}
