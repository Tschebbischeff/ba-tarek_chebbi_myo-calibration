package com.tschebbischeff.common.math;

/**
 * Quaternion implementation with double precision.
 *
 * @author Tarek
 * @version 1.0.0
 */
public class Quat4d {

    /**
     * Factor defining, when the quaternion is considered to not be a unit quaternion anymore.
     */
    private static final double UNITY_FACTOR = 1.01d;
    /**
     * The upper bound determining unity of the quaternion, if the length of the quaternion is above this threshold
     * it is not considered a unit quaternion anymore.
     */
    private static final double UNITY_BOUND_HI = UNITY_FACTOR * UNITY_FACTOR;
    /**
     * The lower bound determining unity of the quaternion, if the length of the quaternion is below this threshold
     * it is not considered a unit quaternion anymore.
     */
    private static final double UNITY_BOUND_LO = (1.0d / UNITY_FACTOR) * (1.0d / UNITY_FACTOR);
    /**
     * Stores the quaternion data in a 4 element array
     */
    private double[] data;

    /**
     * Creates a quaternion that rolls pitches and yaws
     *
     * @param roll  The roll represented by the quaternion.
     * @param pitch The pitch represented by the quaternion.
     * @param yaw   The yaw represented by the quaternion.
     */
    public Quat4d(double roll, double pitch, double yaw) {
        this(Quat4d.identity().roll(roll).pitch(pitch).yaw(yaw).getData());
    }

    /**
     * Creates a quaternion representing the rotation from one vector to another.
     *
     * @param x The vector which is rotated by the resulting quaternion to obtain y.
     * @param y The vector that is the result of x's rotation by the resulting quaternion.
     */
    public Quat4d(Vector3d x, Vector3d y) {
        if (x.dot(y) > 0.999999d) {
            this.setData(Quat4d.identity().getData());
        } else if (x.dot(y) < -0.999999d) {
            this.setData(new Quat4d(0.0d, x.anyOrthogonal().normalize()).getData());
        } else {
            Vector3d prod = x.cross(y);
            this.setData(new Quat4d(Math.sqrt(x.len2() * y.len2()) + x.dot(y), prod).normalize().getData());
        }
    }

    /**
     * Creates a new quaternion with the given real and imaginary parts.
     *
     * @param real      The real part of the quaternion.
     * @param imaginary The imaginary parts of the quaternion.
     */
    public Quat4d(double real, Vector3d imaginary) {
        this(real, imaginary.getX(), imaginary.getY(), imaginary.getZ());
    }

    /**
     * Creates a new quaternion with the given real and imaginary parts.
     *
     * @param w The real part of the quaternion.
     * @param i The first imaginary part of the quaternion.
     * @param j The second imaginary part of the quaternion.
     * @param k The third imaginary part of the quaternion.
     */
    public Quat4d(double w, double i, double j, double k) {
        this(new double[]{w, i, j, k});
    }

    /**
     * Creates a new quaternion from a four dimensional array of doubles.
     * The array's first element corresponds to the real part of the quaternion, while the latter three
     * correspond to the three imaginary parts in order.
     *
     * @param v The array from which to take the data.
     */
    public Quat4d(double[] v) {
        this.data = v;
    }

    /**
     * Returns a new zero quaternion, that is a quaternion with zeroes on all parts.
     *
     * @return A quaternion containing only zeros.
     */
    public static Quat4d zero() {
        return new Quat4d(0.0d, 0.0d, 0.0d, 0.0d);
    }

    /**
     * Returns a new identity quaternion, that is a quaternion which does not alter other quaternions, when multiplied.
     *
     * @return An identity quaternion.
     */
    public static Quat4d identity() {
        return new Quat4d(1.0d, 0.0d, 0.0d, 0.0d);
    }

    /**
     * Get the real part of the quaternion.
     *
     * @return The real part of the quaternion.
     */
    public double getW() {
        return this.data[0];
    }

    /**
     * Set the real part of the quaternion.
     *
     * @param w The new real part of the quaternion.
     * @return This quaternion for fluent method calls.
     */
    public Quat4d setW(double w) {
        this.data[0] = w;
        return this;
    }

    /**
     * Get the first imaginary part of the quaternion.
     *
     * @return The first imaginary part of the quaternion.
     */
    public double getI() {
        return this.data[1];
    }

    /**
     * Set the first imaginary part of the quaternion.
     *
     * @param i The new first imaginary part of the quaternion.
     * @return This quaternion for fluent method calls.
     */
    public Quat4d setI(double i) {
        this.data[1] = i;
        return this;
    }

    /**
     * Get the second imaginary part of the quaternion.
     *
     * @return The second imaginary part of the quaternion.
     */
    public double getJ() {
        return this.data[2];
    }

    /**
     * Set the second imaginary part of the quaternion.
     *
     * @param j The new second imaginary part of the quaternion.
     * @return This quaternion for fluent method calls.
     */
    public Quat4d setJ(double j) {
        this.data[2] = j;
        return this;
    }

    /**
     * Get the third imaginary part of the quaternion.
     *
     * @return The third imaginary part of the quaternion.
     */
    public double getK() {
        return this.data[3];
    }

    /**
     * Set the third imaginary part of the quaternion.
     *
     * @param k The new third imaginary part of the quaternion.
     * @return This quaternion for fluent method calls.
     */
    public Quat4d setK(double k) {
        this.data[3] = k;
        return this;
    }

    /**
     * Get the quaternions data as an array of doubles.
     *
     * @return The quaternions data as a four dimensional array of doubles. The first part is the real part of the
     * quaternion, while the latter three are the imaginary parts in order.
     */
    public double[] getData() {
        return this.data;
    }

    /**
     * Set the quaternions data to an array of doubles.
     *
     * @param v The four dimensional array of data to set the quaternions data to. The first element must be the real part of the quaternion,
     *          while the latter three must be the three imaginary parts in order.
     * @return This quaternion for fluent method calls.
     */
    public Quat4d setData(double[] v) {
        this.data = v;
        return this;
    }

    /**
     * Set the quaternions data to the specified parts.
     *
     * @param w The new real part of the quaternion.
     * @param i The new first imaginary part of the quaternion.
     * @param j The new second imaginary part of the quaternion.
     * @param k The new third imaginary part of the quaternion.
     * @return This quaternion for fluent method calls.
     */
    public Quat4d setData(double w, double i, double j, double k) {
        return this.setData(new double[]{w, i, j, k});
    }

    /**
     * Checks this quaternion for being of unit length.
     *
     * @return A new quaternion describing the same rotation as this quaternion but being of unit length.
     */
    private Quat4d checkUnity() {
        double length = this.len2();
        if (length > UNITY_BOUND_HI || length < UNITY_BOUND_LO) {
            return this.normalize();
        }
        return this;
    }

    /**
     * Get the real part of the quaternion.
     *
     * @return The real part of this quaternion.
     */
    public double getRealPart() {
        return this.getW();
    }

    /**
     * Get the imaginary parts of the quaternion.
     *
     * @return The three imaginary parts of the quaternion in order.
     */
    public Vector3d getImaginaryPart() {
        return new Vector3d(this.getI(), this.getJ(), this.getK());
    }

    /**
     * Get a new quaternion, that describes this quaternions orientation and then a rotation around the x-axis
     * by the specified angle.
     *
     * @param roll The angle to turn around the x-axis, in degrees.
     * @return A new quaternion describing the specified orientation.
     */
    public Quat4d roll(double roll) {
        return new Quat4d(Math.cos(Math.toRadians(roll) * 0.5d), Math.sin(Math.toRadians(roll) * 0.5d), 0.0d, 0.0d).mult(this).checkUnity();
    }

    /**
     * Get a new quaternion, that describes this quaternions orientation and then a rotation around the y-axis
     * by the specified angle.
     *
     * @param pitch The angle to turn around the y-axis, in degrees.
     * @return A new quaternion describing the specified orientation.
     */
    public Quat4d pitch(double pitch) {
        return new Quat4d(Math.cos(Math.toRadians(pitch) * 0.5d), 0.0d, Math.sin(Math.toRadians(pitch) * 0.5d), 0.0d).mult(this).checkUnity();
    }

    /**
     * Get a new quaternion, that describes this quaternions orientation and then a rotation around the z-axis
     * by the specified angle.
     *
     * @param yaw The angle to turn around the z-axis, in degrees.
     * @return A new quaternion describing the specified orientation.
     */
    public Quat4d yaw(double yaw) {
        return new Quat4d(Math.cos(Math.toRadians(yaw) * 0.5d), 0.0d, 0.0d, Math.sin(Math.toRadians(yaw) * 0.5d)).mult(this).checkUnity();
    }

    /**
     * Get a new quaternion, that describes this quaternions orientation and then a rotation around the specified axis
     * by the specified angle.
     *
     * @param axis  The axis to turn around.
     * @param angle The angle to turn around the x-axis, in degrees.
     * @return A new quaternion describing the specified orientation.
     */
    public Quat4d rotate(Vector3d axis, double angle) {
        double sine = Math.sin(Math.toRadians(angle) * 0.5d);
        return new Quat4d(Math.cos(Math.toRadians(angle) * 0.5d), axis.getX() * sine, axis.getY() * sine, axis.getZ() * sine).mult(this).checkUnity();
    }

    /**
     * Get a new quaternion that is the sum of this quaternion and a specified other quaternion.
     *
     * @param b The quaternion to add to this quaternion.
     * @return A new quaternion that is the sum of this and the other quaternion.
     */
    public Quat4d add(Quat4d b) {
        return new Quat4d(this.getW() + b.getW(), this.getI() + b.getI(), this.getJ() + b.getJ(), this.getK() + b.getK());
    }

    /**
     * Calculate the dot-product of this and another quaternion.
     *
     * @param b The quaternion to calculate the dot product with.
     * @return The dot product.
     */
    public double dot(Quat4d b) {
        return this.getW() * b.getW() + this.getI() * b.getI() + this.getJ() * b.getJ() + this.getK() * b.getK();
    }

    /**
     * Calculate the cross-product of this quaternions imaginary parts and another quaternions imaginary parts.
     *
     * @param b The other quaternion.
     * @return A new quaternion with zero real part and the cross-product of the imaginary parts as new imaginary part.
     */
    public Quat4d cross(Quat4d b) {
        return new Quat4d(
                0.0d,
                this.getImaginaryPart().cross(b.getImaginaryPart())
        );
    }

    /**
     * Multiply this quaternion with another quaternion. The other quaternion is multiplied from the right.
     *
     * @param r The other quaternion.
     * @return The product of this quaternion and the other quaternion.
     */
    public Quat4d mult(Quat4d r) {
        double x0 = this.getRealPart();
        double y0 = r.getRealPart();
        Vector3d x = this.getImaginaryPart();
        Vector3d y = r.getImaginaryPart();
        return new Quat4d(
                x0 * y0 - x.dot(y),
                y.scale(x0).add(x.scale(y0)).add(x.cross(y))
        );
    }

    /**
     * Get a vector that is the product of this quaternion and another vector. I.e. the rotated specified vector,
     * where the rotation is specified by this quaternion.
     *
     * @param v The vector to rotate.
     * @return The rotated vector.
     */
    public Vector3d rotateVector(Vector3d v) {
        return this.toRotationMatrix().mult(v);
    }

    /**
     * Get the conjugate of this quaternion.
     *
     * @return A new quaternion, that is this quaternions conjugate.
     */
    public Quat4d conjugate() {
        return new Quat4d(this.getW(), -this.getI(), -this.getJ(), -this.getK());
    }

    /**
     * Get the squared length of this quaternion.
     *
     * @return This quaternions squared length.
     */
    public double len2() {
        return this.dot(this);
    }

    /**
     * Get this quaternions euclidean length.
     *
     * @return This quaternions length.
     */
    public double len() {
        return Math.sqrt(this.len2());
    }

    /**
     * Get a new quaternion that is this quaternion normalized. I.e. the length of the new quaternion is one.
     *
     * @return A new quaternion, that is equal to this quaternion normalized.
     */
    public Quat4d normalize() {
        double length = this.len();
        return new Quat4d(this.getW() / length, this.getI() / length, this.getJ() / length, this.getK() / length);
    }

    /**
     * Calculate a rotation matrix, describing the same rotation as this quaternion.
     *
     * @return A 3x3 rotation matrix describing the same rotation as this quaternion.
     */
    public Matrix3d toRotationMatrix() {
        double s2 = 2.0d / (this.len2() * this.len2());
        //RIGHT HANDED
        return new Matrix3d(
                1.0d - s2 * (this.getJ() * this.getJ() + this.getK() * this.getK()),
                s2 * (this.getI() * this.getJ() + this.getW() * this.getK()),
                s2 * (this.getI() * this.getK() - this.getW() * this.getJ()),

                s2 * (this.getI() * this.getJ() - this.getW() * this.getK()),
                1.0d - s2 * (this.getI() * this.getI() + this.getK() * this.getK()),
                s2 * (this.getJ() * this.getK() + this.getW() * this.getI()),

                s2 * (this.getI() * this.getK() + this.getW() * this.getJ()),
                s2 * (this.getJ() * this.getK() - this.getW() * this.getI()),
                1.0d - s2 * (this.getI() * this.getI() + this.getJ() * this.getJ())
        );
        //LEFT HANDED
        /*return new Matrix3d(
                1.0d - s2 * (this.getJ() * this.getJ() + this.getK() * this.getK()),
                s2 * (this.getI() * this.getJ() - this.getW() * this.getK()),
                s2 * (this.getI() * this.getK() + this.getW() * this.getJ()),

                s2 * (this.getI() * this.getJ() + this.getW() * this.getK()),
                1.0d - s2 * (this.getI() * this.getI() + this.getK() * this.getK()),
                s2 * (this.getJ() * this.getK() - this.getW() * this.getI()),

                s2 * (this.getI() * this.getK() - this.getW() * this.getJ()),
                s2 * (this.getJ() * this.getK() + this.getW() * this.getI()),
                1.0d - s2 * (this.getI() * this.getI() + this.getJ() * this.getJ())
        );*/
    }

    /* *
     * Calculate a rotation matrix that can be supplied to OpenGL methods.
     *
     * @return A rotation matrix equal to the one from {@link #toRotationMatrix()} except being of dimensionality 4x4
     * with the new elements equal to zero except the diagonal element, which is equal to one.
     */
    /*public Matrix4f toGlRotationMatrix() {
        double[][] rotation = this.toRotationMatrix().getData();
        return new Matrix4f(
                new Vector4f((float) rotation[0][0], (float) rotation[1][0], (float) rotation[2][0], 0f),
                new Vector4f((float) rotation[0][1], (float) rotation[1][1], (float) rotation[2][1], 0f),
                new Vector4f((float) rotation[0][2], (float) rotation[1][2], (float) rotation[2][2], 0f),
                new Vector4f(0f, 0f, 0f, 1f)
        ).transpose();
    }*/

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object r) {
        if (r instanceof Quat4d) {
            Quat4d q = (Quat4d) r;
            return (this.getW() == q.getW() && this.getI() == q.getI() && this.getJ() == q.getJ() && this.getK() == q.getK());
        }
        return false;
    }

    /**
     * Format this quaternion to a string suitable for output.
     *
     * @param precision The number of digits behind the comma to print.
     * @return A string describing the contents of this quaternion.
     */
    public String toString(int precision) {
        return String.format(
                "[ %1$." + precision + "f   %2$." + precision + "fi   %3$." + precision + "fj   %4$." + precision + "fk ]",
                this.data[0], this.data[1], this.data[2], this.data[3]);
    }

    /**
     * Format this quaternion to a string suitable for output.
     *
     * @return A string describing the contents of this quaternion.
     */
    @Override
    public String toString() {
        return this.toString(3);
    }
}
