package com.tschebbischeff.myocalib.internal.backend.security;

/**
 * Custom exception, thrown when a security token is checked and found not valid.
 */
public final class SecurityViolationException extends RuntimeException {

    /**
     * Constructs the exception with a default error message.
     */
    public SecurityViolationException() {
        super("Secured method called from outside library! Only use the provided API.");
    }
}
