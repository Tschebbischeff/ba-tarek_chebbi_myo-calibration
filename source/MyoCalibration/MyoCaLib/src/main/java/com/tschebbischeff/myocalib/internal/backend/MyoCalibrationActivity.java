package com.tschebbischeff.myocalib.internal.backend;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import com.tschebbischeff.common.android.logger.Logger;
import com.tschebbischeff.myocalib.R;
import com.tschebbischeff.myocalib.api.InternalApiInterface;
import com.tschebbischeff.myocalib.internal.backend.exceptions.UnfinishedSettingsException;
import com.tschebbischeff.myocalib.internal.backend.security.SecurityToken;
import com.tschebbischeff.myocalib.internal.lazyui.PageList;
import com.tschebbischeff.myocalib.internal.lazyui.SharedPageData;

/**
 * Implementation of the main activity.
 * The activity only changes the enclosed content with variable layouts.
 * These content layouts are defined in {@link PageList}.
 * <p>
 * The activity furthermore includes a navigation drawer as a means to navigate through the different content
 * in the app. The navigation drawer is always available, no matter which content is currently shown.
 * <p>
 * To ensure an correctly working API service, do not start this activity on your own!
 * Better bind to the API service and call {@link com.tschebbischeff.myocalib.api.MyoCaLib#startCalibrationActivity(Activity)}
 * with your activity as an argument! This ensures the lifecycle of the service remains active and connections to
 * Myos persist after this activity has been terminated.
 */
public class MyoCalibrationActivity extends AppCompatActivity
        implements View.OnClickListener {

    /**
     * A token authenticating this activity as the internal activity. Provides access to internal-only API calls.
     */
    private static final InternalSecurityToken securityToken = new InternalSecurityToken();
    /**
     * Receives Intents and forwards them to LazyUI.
     */
    private final BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            PageList.dispatchOnReceive(context, intent);
        }
    };
    /**
     * An interface for write-access to API fields.
     */
    private InternalApiInterface apiInterface;
    /**
     * An object encapsulating objects that can be used from any LazyUI page.
     */
    private SharedPageData sharedPageData;
    /**
     * Synchronizes execution of the page initialization with the service bounding
     */
    private int serviceAlReady;

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.serviceAlReady = 0;
        this.apiInterface = new InternalApiInterface(this, securityToken);
        this.sharedPageData = new SharedPageData(this.apiInterface);

        //Initialize
        setContentView(R.layout.myocalib_main_activity);
        Toolbar toolbar = (Toolbar) findViewById(R.id.myocalib_toolbar);
        setSupportActionBar(toolbar);

        LinearLayout doneButton = (LinearLayout) findViewById(R.id.myocalib_toolbar_donebutton);
        doneButton.setOnClickListener(this);

        registerReceiver(broadcastReceiver, new IntentFilter());

        PageList.dispatchOnCreate(this.sharedPageData, this);
        synchronized (this) {
            serviceAlReady++;
            if (serviceAlReady == 2) { //Calls initialize in case serviceReady was called before
                Logger.info("MyoCalibrationActivity", "Initializing page list in onCreate.");
                if (!this.apiInterface.getApiService().getCalibrationSettings().isImmutable()) {
                    throw new UnfinishedSettingsException();
                }
                PageList.initialize(this);
            }
        }
    }

    /**
     * Called when the api service is bound sucessfully
     */
    public void serviceReady() {
        synchronized (this) {
            serviceAlReady++;
            if (serviceAlReady == 2) { //Calls initialize in case dispatchOnCreate was called before and also not if serviceReady is called again
                Logger.info("MyoCalibrationActivity", "Initializing page list in serviceReady.");
                if (!this.apiInterface.getApiService().getCalibrationSettings().isImmutable()) {
                    throw new UnfinishedSettingsException();
                }
                PageList.initialize(this);
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onResume() {
        super.onResume();
        PageList.dispatchOnResume(this);
    }

    /**
     * Replaces the intent filter of the broadcast receiver with the current contents filter
     * Called automatically when the content changes.
     */
    public void refreshBroadcastReceiverFilter(String[] filterActions) {
        IntentFilter filter = new IntentFilter();
        for (String action : filterActions) {
            filter.addAction(action);
        }
        unregisterReceiver(broadcastReceiver);
        registerReceiver(broadcastReceiver, filter);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onPause() {
        super.onPause();
        PageList.dispatchOnPause(this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        PageList.dispatchOnDestroy();
        unregisterReceiver(broadcastReceiver);
        this.apiInterface.onDestroy();
        this.apiInterface = null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }

    /**
     * Overrides default back-button behaviour with one that closes the navigation drawer if it is open
     * and delegates the back button press to the shown content when it is not open.
     * The content decides, whether to close the app or not.
     */
    @Override
    public void onBackPressed() {
        if (this.apiInterface.isServiceReady()) { //Do not allow propagation, if service is not ready.
            if (PageList.dispatchOnBackPressed()) {
                super.onBackPressed();
            }
        }
    }

    /**
     * Handles and dispatches view clicks
     *
     * @param view The clicked view
     */
    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.myocalib_toolbar_donebutton) {
            this.finish();
        } else if (this.apiInterface.isServiceReady()) { //Do not allow propagation, if service is not ready.
            PageList.dispatchOnClick(view);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        PageList.dispatchOnRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }

    /**
     * A token, that can only be created by this class, and hence authenticates this class to other classes.
     */
    public static final class InternalSecurityToken extends SecurityToken {
        private InternalSecurityToken() {
        }
    }
}
