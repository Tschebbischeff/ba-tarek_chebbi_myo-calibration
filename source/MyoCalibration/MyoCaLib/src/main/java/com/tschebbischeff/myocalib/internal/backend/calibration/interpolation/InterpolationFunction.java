package com.tschebbischeff.myocalib.internal.backend.calibration.interpolation;

import java.util.TreeMap;

/**
 * Realizes an interpolation function.
 */
public abstract class InterpolationFunction {

    /**
     * The sample values of the interpolation function
     */
    protected TreeMap<Double, Double> values;

    /**
     * Constructor.
     */
    public InterpolationFunction() {
        this.values = new TreeMap<>();
    }

    /**
     * Add a value to the list of values (call this in order)
     */
    public void add(double domainValue, double value) {
        this.values.put(domainValue, value);
        this.recalculateParameters();
    }

    /**
     * Add all values from the specified treemap.
     *
     * @param c The list of values.
     */
    public void addAll(TreeMap<Double, Double> c) {
        this.values.putAll(c);
        this.recalculateParameters();
    }

    /**
     * Clear the list of values.
     */
    public void clear() {
        this.values.clear();
        this.recalculateParameters();
    }

    /**
     * Inheriting classes must recalculate interpolation relevant parameters in this method.
     * Automatically called when the list of values changes.
     */
    protected abstract void recalculateParameters();

    /**
     * Inheriting classes must return the calculated value of the interpolation function at the given point.
     *
     * @param domainValue The point at which to get the functions value.
     * @return The value of the function at the given point on the functions domain or null, if the function has no samples.
     */
    protected abstract Double executeSample(Double domainValue);

    /**
     * Sample the function and calculate the functions value at the given point
     *
     * @param domainValue The value in the domain of the function at which to sample.
     * @return The functions value at the given point or null, if the function has no samples.
     */
    public Double sampleAt(double domainValue) {
        if (this.values.size() == 0) {
            return null;
        }
        return this.executeSample(domainValue);
    }

    /**
     * Get the first point below the given point backed by a support/sample, without its value.
     *
     * @param domainValue The point from which to search for the next sample point.
     * @return The first sample point below and not equal to the parameter or null, if no such point exists.
     */
    protected Double getLowerSupportPoint(double domainValue) {
        return this.values.lowerKey(domainValue);
    }

    /**
     * Get the first point above the given point backed by a support/sample, without its value.
     *
     * @param domainValue The point from which to search for the next sample point.
     * @return The first sample point above and not equal to the parameter or null, if no such point exists.
     */
    protected Double getHigherSupportPoint(double domainValue) {
        return this.values.higherKey(domainValue);
    }

    /**
     * Check whether the supplied value is exactly equal to a support/sample of this function.
     *
     * @param domainValue The point on the functions domain to check for.
     * @return True if the supplied point on the functions domain is exactly a point contained in the sample set of this function.
     */
    protected boolean isSupportPoint(double domainValue) {
        return this.values.containsKey(domainValue);
    }

    /**
     * Returns the distance between two points on the domain of the function.
     *
     * @param a The first point on the domain of the function.
     * @param b The second point on the domain of the function.
     * @return The absolute distance between the two points.
     */
    protected double getDistance(double a, double b) {
        return Math.abs(b-a);
    }
}
