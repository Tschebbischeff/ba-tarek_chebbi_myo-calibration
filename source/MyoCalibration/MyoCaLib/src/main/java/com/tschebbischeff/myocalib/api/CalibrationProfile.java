package com.tschebbischeff.myocalib.api;

import com.tschebbischeff.common.android.logger.Logger;
import com.tschebbischeff.common.math.Quat4d;
import com.tschebbischeff.common.math.Vector3d;
import com.tschebbischeff.myocalib.internal.backend.tools.StaticLib;
import eu.darken.myolib.Myo;
import eu.darken.myolib.MyoCmds;
import eu.darken.myolib.extension.ISensorModeListener;
import eu.darken.myolib.extension.SensorModeHelper;
import eu.darken.myolib.msgs.MyoMsg;

import java.util.ArrayList;

/**
 * Encapsulates all Myo specific calibration parameters and listeners.
 * You can add listeners to individual Myos via objects of this class,
 * and get calibration statistics (e.g. on which arm the Myo is, what the normalization factor for EMG normalization is,
 * etc.)
 * Acquire objects of this class for any Myo over the according API call.
 * The API call ({@link MyoCaLib#getMyoCalibrationProfile(Myo)}) never returns null for a Myo contained in
 * {@link MyoCaLib#getConnectedMyos()}, which also means that uncalibrated Myo's have an associated profile as well.
 * You may either check the calibration status via the respective API calls ({@link MyoCaLib#getCalibratedMyos()} and {@link MyoCaLib#isMyoCalibrated(Myo)})
 * or via the {@link #isCalibrationComplete()} method directly.
 * Objects of this class are read-only apart from adding listeners.
 */
public class CalibrationProfile implements ISensorModeListener {

    /**
     * A list of all listeners, that wish to receive calibrated IMU related events.
     */
    protected final ArrayList<IDataOutputListener> imuCalibratedListeners;
    /**
     * A list of all listeners, that wish to receive calibrated EMG related events.
     */
    protected final ArrayList<IDataOutputListener> emgCalibratedListeners;
    /**
     * A list of all listeners, that wish to receive raw IMU related events.
     */
    protected final ArrayList<IRawDataOutputListener> imuRawListeners;
    /**
     * A list of all listeners, that wish to receive raw EMG related events.
     */
    protected final ArrayList<IRawDataOutputListener> emgRawListeners;
    /**
     * Whether the calibration of the myo associated with this profile is complete.
     */
    protected boolean calibrationComplete;
    /**
     * The arm on which the Myo is located.
     */
    protected WearableLocation wearableLocation;
    /**
     * The facing of the USB port.
     */
    protected UsbDirection usbDirection;
    /**
     * The normalization factor.
     */
    protected double normalizationFactor;
    /**
     * The accelerometers bias on the x-axis.
     */
    protected double accelerometerBiasX;
    /**
     * The accelerometers bias on the y-axis.
     */
    protected double accelerometerBiasY;
    /**
     * The accelerometers bias on the z-axis.
     */
    protected double accelerometerBiasZ;
    /**
     * The angle about which the Myo is rotated around the user's arm.
     * A positive angle means the armband is rotated "outwards"
     * (i.e. counter-clockwise on the left arm and clockwise on the right arm, when seen from the elbow to the wrist -
     * the users perspective).
     */
    protected double rotationAngle;
    /**
     * The orientation of the Myo relative to the Myo's internal global orientation, which is zero'd each time the
     * Myo comes awake.
     */
    protected Quat4d orientation;
    /**
     * The orientation of the Myo relative to the Myo's IMU coordinate system.
     */
    protected Quat4d imuOrientation;
    /**
     * The Myo with which this calibration profile is associated.
     */
    protected Myo associatedMyo;
    /**
     * The calibration settings.
     */
    protected CalibrationSettings calibrationSettings;
    /**
     * What kind of EMG data is currently being streamed from the Myo.
     */
    protected MyoCmds.EmgMode desiredEmgMode;
    /**
     * What kind of IMU data is currently being streamed from the Myo.
     */
    protected MyoCmds.ImuMode desiredImuMode;
    /**
     * Whether the command to refresh the modes is currently being executed.
     */
    private volatile boolean alredyRefreshingModes = false;

    /**
     * Constructor.
     */
    protected CalibrationProfile(Myo myo, CalibrationSettings calibrationSettings) {
        this.associatedMyo = myo;
        this.calibrationSettings = calibrationSettings;
        this.calibrationComplete = false;
        this.wearableLocation = WearableLocation.RIGHT_ARM;
        this.usbDirection = UsbDirection.TOWARDS_WRIST;
        this.normalizationFactor = 1.0d;
        this.accelerometerBiasX = 0.0d;
        this.accelerometerBiasY = 0.0d;
        this.accelerometerBiasZ = 0.0d;
        this.rotationAngle = 0.0d;
        this.orientation = Quat4d.identity();
        this.imuCalibratedListeners = new ArrayList<>();
        this.emgCalibratedListeners = new ArrayList<>();
        this.imuRawListeners = new ArrayList<>();
        this.emgRawListeners = new ArrayList<>();
        this.desiredEmgMode = MyoCmds.EmgMode.NONE;
        this.desiredImuMode = MyoCmds.ImuMode.NONE;
    }

    /**
     * Whether the Myo is located on the left arm.
     *
     * @return True if, the Myo is mounted on the user's left arm, as defined by the user during the calibration.
     */
    public boolean isMyoOnLeftArm() {
        return this.wearableLocation == WearableLocation.LEFT_ARM;
    }

    /**
     * Whether the Myo is located on the right arm.
     *
     * @return True if, the Myo is mounted on the user's right arm, as defined by the user during the calibration.
     */
    public boolean isMyoOnRightArm() {
        return this.wearableLocation == WearableLocation.RIGHT_ARM;
    }

    /**
     * Whether the Myo's USB port is facing the user's wrist.
     *
     * @return True if the Myo's USB port is facing towards the user's wrist and not his elbow.
     */
    public boolean isUsbFacingWrist() {
        return this.usbDirection == UsbDirection.TOWARDS_WRIST;
    }

    /**
     * Whether the Myo's USB port is facing the user's elbow.
     *
     * @return True if the Myo's USB port is facing towards the user's elbow and not his wrist.
     */
    public boolean isUsbFacingElbow() {
        return this.usbDirection == UsbDirection.TOWARDS_ELBOW;
    }

    /**
     * Whether the Myo has been successfully calibrated.
     *
     * @return True if, and only if, the calibration of the Myo has been completed successfully.
     */
    public boolean isCalibrationComplete() {
        return this.calibrationComplete;
    }

    /**
     * Gets the normalization factor calculated by the calibration.
     * The raw data is multiplied by this factor to obtain a value independent from static session-dependent influences.
     *
     * @return The normalization factor calculated by the calibration.
     */
    public double getNormalizationFactor() {
        return this.normalizationFactor;
    }

    /**
     * Gets the bias of the accelerometer on the x-axis as calculated by the calibration.
     * This is subtracted from the raw accelerometers x-axis measurements.
     *
     * @return The accelerometer's x-axis bias.
     */
    public double getAccelerometerBiasX() {
        return this.accelerometerBiasX;
    }

    /**
     * Gets the bias of the accelerometer on the y-axis as calculated by the calibration.
     * This is subtracted from the raw accelerometers y-axis measurements.
     *
     * @return The accelerometer's y-axis bias.
     */
    public double getAccelerometerBiasY() {
        return this.accelerometerBiasY;
    }

    /**
     * Gets the bias of the accelerometer on the z-axis as calculated by the calibration.
     * This is subtracted from the raw accelerometers z-axis measurements.
     *
     * @return The accelerometer's z-axis bias.
     */
    public double getAccelerometerBiasZ() {
        return this.accelerometerBiasZ;
    }

    /**
     * Gets the angle by which the Myo is rotated around the user's arm.
     * A positive angle means, the Myo is rotated "outwards", that is in counter-clockwise direction around
     * the left arm or clockwise direction around the right arm if seen from the user's perspective.
     * The angle is in radians.
     *
     * @return The angle by which the Myo is rotated around the user's arm.
     */
    public double getRotationAngle() {
        return this.rotationAngle;
    }

    /**
     * Specify an implementation of {@link IDataOutputListener}, which receives any future calibrated IMU *AND* EMG data
     * sent by the myo.
     * Shortcut for calling {@link #addEmgListener(IDataOutputListener)} and {@link #addImuListener(IDataOutputListener)}
     * in sequence.
     *
     * @param listener An implementation of the {@link IDataOutputListener} interface.
     */
    public void addImuEmgListener(IDataOutputListener listener) {
        this.addImuListener(listener);
        this.addEmgListener(listener);
    }

    /**
     * Specify an implementation of {@link IRawDataOutputListener}, which receives any future raw IMU *AND* EMG data
     * sent by the myo.
     * Shortcut for calling {@link #addRawEmgListener(IRawDataOutputListener)} and {@link #addRawImuListener(IRawDataOutputListener)}
     * in sequence.
     *
     * @param listener An implementation of the {@link IRawDataOutputListener} interface.
     */
    public void addRawImuEmgListener(IRawDataOutputListener listener) {
        this.addRawImuListener(listener);
        this.addRawEmgListener(listener);
    }

    /**
     * Specify an implementation of {@link IDataOutputListener}, that was added via {@link #addEmgListener(IDataOutputListener)},
     * {@link #addImuListener(IDataOutputListener)} or {@link #addImuEmgListener(IDataOutputListener)},
     * which will NOT receive any future calibrated EMG *AND* will NOT receive any future calibrated IMU data sent by the myo anymore.
     * Shortcut for calling {@link #removeEmgListener(IDataOutputListener)} and {@link #removeImuListener(IDataOutputListener)}
     * in sequence.
     *
     * @param listener An implementation of the {@link IDataOutputListener} interface.
     */
    public void removeImuEmgListener(IDataOutputListener listener) {
        this.removeImuListener(listener);
        this.removeEmgListener(listener);
    }

    /**
     * Specify an implementation of {@link IRawDataOutputListener}, that was added via {@link #addRawEmgListener(IRawDataOutputListener)},
     * {@link #addRawImuListener(IRawDataOutputListener)} or {@link #addRawImuEmgListener(IRawDataOutputListener)},
     * which will NOT receive any future raw EMG *AND* will NOT receive any future raw IMU data sent by the myo anymore.
     * Shortcut for calling {@link #removeRawEmgListener(IRawDataOutputListener)} and {@link #removeRawImuListener(IRawDataOutputListener)}
     * in sequence.
     *
     * @param listener An implementation of the {@link IRawDataOutputListener} interface.
     */
    public void removeRawImuEmgListener(IRawDataOutputListener listener) {
        this.removeRawImuListener(listener);
        this.removeRawEmgListener(listener);
    }

    /**
     * Specify an implementation of {@link IDataOutputListener}, which receives any future calibrated IMU data
     * sent by the myo.
     *
     * @param listener An implementation of the {@link IDataOutputListener} interface.
     */
    public void addImuListener(IDataOutputListener listener) {
        synchronized (this) {
            if (listener != null) {
                this.imuCalibratedListeners.add(listener);
                if (this.imuCalibratedListeners.size() == 1 && this.imuRawListeners.size() == 0) {
                    this.desiredImuMode = MyoCmds.ImuMode.DATA;
                    this.refreshReceivingMode();
                }
            }
        }
    }

    /**
     * Specify an implementation of {@link IRawDataOutputListener}, which receives any future raw IMU data
     * sent by the myo.
     *
     * @param listener An implementation of the {@link IRawDataOutputListener} interface.
     */
    public void addRawImuListener(IRawDataOutputListener listener) {
        synchronized (this) {
            if (listener != null) {
                this.imuRawListeners.add(listener);
                if (this.imuRawListeners.size() == 1 && this.imuCalibratedListeners.size() == 0) {
                    this.desiredImuMode = MyoCmds.ImuMode.DATA;
                    this.refreshReceivingMode();
                }
            }
        }
    }

    /**
     * Specify an implementation of {@link IDataOutputListener}, that was added via {@link #addImuListener(IDataOutputListener)},
     * which will NOT receive any future calibrated IMU data sent by the myo anymore.
     *
     * @param listener An implementation of the {@link IDataOutputListener} interface.
     */
    public void removeImuListener(IDataOutputListener listener) {
        synchronized (this) {
            if (listener != null) {
                this.imuCalibratedListeners.remove(listener);
                if (this.imuCalibratedListeners.size() == 0 && this.imuRawListeners.size() == 0) {
                    this.desiredImuMode = MyoCmds.ImuMode.NONE;
                    this.refreshReceivingMode();
                }
            }
        }
    }

    /**
     * Specify an implementation of {@link IDataOutputListener}, that was added via {@link #addRawImuListener(IRawDataOutputListener)},
     * which will NOT receive any future raw IMU data sent by the myo anymore.
     *
     * @param listener An implementation of the {@link IDataOutputListener} interface.
     */
    public void removeRawImuListener(IRawDataOutputListener listener) {
        synchronized (this) {
            if (listener != null) {
                this.imuRawListeners.remove(listener);
                if (this.imuRawListeners.size() == 0 && this.imuCalibratedListeners.size() == 0) {
                    this.desiredImuMode = MyoCmds.ImuMode.NONE;
                    this.refreshReceivingMode();
                }
            }
        }
    }

    /**
     * Specify an implementation of {@link IDataOutputListener}, which receives any future calibrated EMG data
     * sent by the myo.
     *
     * @param listener An implementation of the {@link IDataOutputListener} interface.
     */
    public void addEmgListener(IDataOutputListener listener) {
        synchronized (this) {
            if (listener != null) {
                this.emgCalibratedListeners.add(listener);
                if (this.emgCalibratedListeners.size() == 1 && this.emgRawListeners.size() == 0) {
                    this.desiredEmgMode = MyoCmds.EmgMode.FILTERED;
                    this.refreshReceivingMode();
                }
            }
        }
    }

    /**
     * Specify an implementation of {@link IRawDataOutputListener}, which receives any future raw EMG data
     * sent by the myo.
     *
     * @param listener An implementation of the {@link IRawDataOutputListener} interface.
     */
    public void addRawEmgListener(IRawDataOutputListener listener) {
        synchronized (this) {
            if (listener != null) {
                this.emgRawListeners.add(listener);
                if (this.emgRawListeners.size() == 1 && this.emgCalibratedListeners.size() == 0) {
                    this.desiredEmgMode = MyoCmds.EmgMode.FILTERED;
                    this.refreshReceivingMode();
                }
            }
        }
    }

    /**
     * Specify an implementation of {@link IDataOutputListener}, that was added via {@link #addEmgListener(IDataOutputListener)},
     * which will NOT receive any future calibrated EMG data sent by the myo anymore.
     *
     * @param listener An implementation of the {@link IDataOutputListener} interface.
     */
    public void removeEmgListener(IDataOutputListener listener) {
        synchronized (this) {
            if (listener != null) {
                this.emgCalibratedListeners.remove(listener);
                if (this.emgCalibratedListeners.size() == 0 && this.emgRawListeners.size() == 0) {
                    this.desiredEmgMode = MyoCmds.EmgMode.NONE;
                    this.refreshReceivingMode();
                }
            }
        }
    }

    /**
     * Specify an implementation of {@link IDataOutputListener}, that was added via {@link #addRawEmgListener(IRawDataOutputListener)},
     * which will NOT receive any future raw EMG data sent by the myo anymore.
     *
     * @param listener An implementation of the {@link IRawDataOutputListener} interface.
     */
    public void removeRawEmgListener(IRawDataOutputListener listener) {
        synchronized (this) {
            if (listener != null) {
                this.emgRawListeners.remove(listener);
                if (this.emgRawListeners.size() == 0 && this.emgCalibratedListeners.size() == 0) {
                    this.desiredEmgMode = MyoCmds.EmgMode.NONE;
                    this.refreshReceivingMode();
                }
            }
        }
    }

    /**
     * Sets what kind of EMG, IMU and classifier data is streamed from the Myo to the currently set modes.
     */
    protected void refreshReceivingMode() {
        synchronized (this) {
            if (!this.alredyRefreshingModes) { //Do not update again, callback will update again automatically
                this.alredyRefreshingModes = true;
                SensorModeHelper.writeSleepMode(this.associatedMyo, this.desiredImuMode, this.desiredEmgMode, MyoCmds.ClassifierMode.DISABLED, this);
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onSensorModeWritten(Myo myo, MyoCmds.ImuMode requestedImuMode, MyoCmds.EmgMode requestedEmgMode, MyoCmds.ClassifierMode requestedClassifierMode, MyoMsg msg) {
        synchronized (this) {
            if (msg.getState() != MyoMsg.State.SUCCESS || requestedImuMode != this.desiredImuMode || requestedEmgMode != this.desiredEmgMode) {
                Logger.warning("CalibrationProfile", "Sensor mode was wrong or updated during previous update. Refreshing. To many of these messages indicate abnormal failure.");
                SensorModeHelper.writeSleepMode(this.associatedMyo, this.desiredImuMode, this.desiredEmgMode, MyoCmds.ClassifierMode.DISABLED, this);
            }
            this.alredyRefreshingModes = false;
        }
    }

    /**
     * The possible locations, where the armband can be worn.
     */
    protected enum WearableLocation {
        RIGHT_ARM, LEFT_ARM
    }

    /**
     * The possible directions, in which the USB port of the Myo may face.
     */
    protected enum UsbDirection {
        TOWARDS_WRIST, TOWARDS_ELBOW
    }

    /**
     * Applies calibration to an EMG signal, based on the set parameters in this calibration profile.
     *
     * @param signal The raw signal obtained from the Myo.
     * @return The calibrated signal.
     */
    protected InterpolatedEmgSignal applyEmgCalibration(double[] signal) {
        double[] result = signal;
        if (this.calibrationSettings.emg.isCalibrationEnabled()) {
            int[] rotationPermutation;
            if (this.isMyoOnLeftArm() ^ this.isUsbFacingElbow()) {
                //Make the pod with the LEDs the new index zero but don't inverse order
                rotationPermutation = new int[]{3, 4, 5, 6, 7, 0, 1, 2};
            } else {
                //Make the pod with the LEDs the new index zero and inverse order
                rotationPermutation = new int[]{3, 2, 1, 0, 7, 6, 5, 4};
            }
            //Counter rotate
            result = StaticLib.arrayPermutation(result, rotationPermutation);
            for (int i = 0; i < result.length; i++) {
                //Full wave rectification & Normalization
                result[i] = Math.abs(result[i]) * (this.calibrationSettings.emg.isNormalizationEnabled() ? this.normalizationFactor : 1d);
            }
        }
        if (this.calibrationSettings.emg.isCalibrationEnabled() && this.calibrationSettings.emg.isRotationCalibrationEnabled()) {
            return new InterpolatedEmgSignal(result, this.rotationAngle, this.calibrationSettings.emg.getInterpolationMode().createNewFunction());
        } else {
            return new InterpolatedEmgSignal(result, 0d, EmgCalibrationSettings.EmgInterpolationMode.NEAREST_NEIGHBOR.createNewFunction());
        }
    }

    /**
     * Applies calibration to an IMUs accelerometer signal, based on the set parameters in this calibration profile.
     *
     * @param accelerometerSignal The raw signal obtained from the Myo.
     * @return The calibrated signal.
     */
    protected Vector3d applyAccelerometerCalibration(double[] accelerometerSignal, Quat4d calibratedOrientation) {
        Vector3d result = new Vector3d(accelerometerSignal);
        if (this.calibrationSettings.imu.isCalibrationEnabled()) {
            if (this.calibrationSettings.imu.isAccelerometerBiasCalibrationEnabled()) {
                result.setX(result.getX() - this.getAccelerometerBiasX());
                result.setY(result.getY() - this.getAccelerometerBiasY());
                result.setZ(result.getZ() - this.getAccelerometerBiasZ());
            }
            if (this.calibrationSettings.imu.isGlobalCoordinateSystemEnabled()) {
                result = this.imuOrientation.rotateVector(result);
                result = calibratedOrientation.conjugate().rotateVector(result);
            }
        }
        return result;
    }

    /**
     * Applies calibration to an IMUs gyroscope signal, based on the set parameters in this calibration profile.
     *
     * @param gyroscopeSignal The raw signal obtained from the Myo.
     * @return The calibrated signal.
     */
    protected Vector3d applyGyroscopeCalibration(double[] gyroscopeSignal, Quat4d calibratedOrientation) {
        Vector3d result = new Vector3d(gyroscopeSignal);
        if (this.calibrationSettings.imu.isCalibrationEnabled() && this.calibrationSettings.imu.isGlobalCoordinateSystemEnabled()) {
            result = this.imuOrientation.rotateVector(result);
            result = calibratedOrientation.conjugate().rotateVector(result);
        }
        return result;
    }

    /**
     * Applies calibration to an IMUs orientation signal, based on the set parameters in this calibration profile.
     *
     * @param orientationSignal The raw signal obtained from the Myo.
     * @return The calibrated signal. [w,x,y,z]
     */
    protected Quat4d applyOrientationCalibration(double[] orientationSignal) {
        Quat4d result = new Quat4d(orientationSignal);
        if (this.calibrationSettings.imu.isCalibrationEnabled() && this.calibrationSettings.imu.isGlobalCoordinateSystemEnabled()) {
            result = result.mult(this.orientation).normalize();
        }
        return result;
    }
}
