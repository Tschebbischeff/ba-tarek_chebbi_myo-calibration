package com.tschebbischeff.common.android.layout.managers;

import android.content.Context;

/**
 * A linear layout manager that does not permit the user to scroll it.
 */
public class UnscrollableLinearLayoutManager extends WrapContentLinearLayoutManager {

    /**
     * Delegating standard constructor.
     *
     * @param context The context creating the linear layout manager.
     */
    public UnscrollableLinearLayoutManager(Context context) {
        super(context);
    }

    /**
     * {@inheritDoc}
     * <p>
     * (This overriding implementation returns false, always.)
     *
     * @return {@inheritDoc} (Overriden to false, always.)
     */
    @Override
    public boolean canScrollVertically() {
        return false;
    }

    /**
     * {@inheritDoc}
     * <p>
     * (This overriding implementation returns false, always.)
     *
     * @return {@inheritDoc} (Overriden to false, always.)
     */
    @Override
    public boolean canScrollHorizontally() {
        return false;
    }
}
