package com.tschebbischeff.myocalib.internal.backend.calibration;

import com.tschebbischeff.myocalib.internal.backend.calibration.interpolation.CyclicInterpolationFunction;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.TreeMap;

/**
 * Calculates the distance between the two values at two points on an interpolation function.
 */
public class InterpolatingDistanceFilter extends Filter1D<Double> {

    /**
     * The interpolation function to apply to the data.
     */
    private CyclicInterpolationFunction interpolationFunction;
    /**
     * The values of the filter.
     */
    private ArrayList<Double> values;
    /**
     * One of two points for which to calculate the distance.
     */
    private double lowPoint;
    /**
     * The second of two points for which to calculate the distance.
     */
    private double highPoint;
    /**
     * The offset supplied to the cyclic interpolation function.
     */
    private double cyclicOffset;

    /**
     * Constructor.
     *
     * @param a                     The first point on the interpolation function for whose value to calculate the distance.
     * @param b                     The second point on the interpolation function for whose value to calculate the distance.
     * @param interpolationFunction The interpolation function to apply.
     * @param cyclicOffset          The offset supplied to the cyclic interpolation function, to obtain sample points.
     */
    public InterpolatingDistanceFilter(double a, double b, CyclicInterpolationFunction interpolationFunction, double cyclicOffset) {
        this.values = new ArrayList<>();
        this.interpolationFunction = interpolationFunction;
        this.lowPoint = a;
        this.highPoint = b;
        this.cyclicOffset = cyclicOffset;
    }

    @Override
    public void applyWithoutResult(Double value) {
        this.values.add(value);
    }

    @Override
    public Double getResult() {
        double[] samplePoints = this.interpolationFunction.getEvenlyDistributedDomainSamples(this.values.size(), Math.toRadians(this.cyclicOffset));
        Double[] sampleValues = this.values.toArray(new Double[]{});
        TreeMap<Double, Double> sampleMap = new TreeMap<>();
        for (int i = 0; i < this.values.size(); i++) {
            sampleMap.put(samplePoints[i], this.values.get(i));
        }
        this.interpolationFunction.addAll(sampleMap);
        return Math.abs(this.interpolationFunction.sampleAt(this.lowPoint) - this.interpolationFunction.sampleAt(this.highPoint));
    }

    @Override
    protected Double[] apply(Double[] values) {
        this.values.addAll(Arrays.asList(values));
        return new Double[]{this.getResult()};
    }

    @Override
    public void reset() {

    }
}
