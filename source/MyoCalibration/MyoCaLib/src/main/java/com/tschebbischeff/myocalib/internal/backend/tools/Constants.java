package com.tschebbischeff.myocalib.internal.backend.tools;

/**
 * Contains public constants.
 */
public class Constants {
    /**
     * The request code for the permissions requested in the welcome content screen.
     */
    public static final int REQUEST_CODE_PERMISSIONS_WELCOME_SCREEN = 1;
}
