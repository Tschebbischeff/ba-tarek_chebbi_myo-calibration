package com.tschebbischeff.myocalib.internal.backend.calibration;

import com.tschebbischeff.common.math.Quat4d;
import com.tschebbischeff.common.math.Vector3d;
import com.tschebbischeff.myocalib.api.*;
import com.tschebbischeff.myocalib.internal.backend.connection.ConnectionHandler;
import com.tschebbischeff.myocalib.internal.lazyui.pages.calibration.PageCalibration;
import eu.darken.myolib.Myo;
import eu.darken.myolib.MyoCmds;
import eu.darken.myolib.processor.classifier.ClassifierProcessor;
import eu.darken.myolib.processor.emg.EmgProcessor;
import eu.darken.myolib.processor.imu.ImuProcessor;

/**
 * Writable interface for internal classes, which can set
 * Is able to set calibration related events, that an API caller should not be able to set.
 */
public final class WritableCalibrationProfile extends CalibrationProfile {

    /**
     * The connection handler containing all calibration profiles, for callbacks.
     */
    private ConnectionHandler connectionHandler;
    /**
     * The instance directly receiving the Myo's EMG and IMU data.
     */
    private DataReceiver dataReceiver;
    /**
     * Myolib's EMG processor
     */
    private EmgProcessor emgProcessor;
    /**
     * Myolib's IMU processor
     */
    private ImuProcessor imuProcessor;
    /**
     * Myolib's classifier processor
     */
    private ClassifierProcessor classifierProcessor;
    /**
     * The calibration page for callbacks during calibration.
     */
    private PageCalibration calibrationPage;


    /**
     * Constructor.
     *
     * @param myo The Myo with which this calibration profile is associated.
     */
    public WritableCalibrationProfile(Myo myo, ConnectionHandler callbackHandler, CalibrationSettings calibrationSettings) {
        super(myo, calibrationSettings);
        this.connectionHandler = callbackHandler;
        this.calibrationPage = null;
        this.dataReceiver = new DataReceiver(this);
        this.emgProcessor = new EmgProcessor();
        this.associatedMyo.addProcessor(this.emgProcessor);
        this.emgProcessor.addListener(this.dataReceiver);
        this.imuProcessor = new ImuProcessor();
        this.associatedMyo.addProcessor(this.imuProcessor);
        this.imuProcessor.addListener(this.dataReceiver);
    }

    /**
     * Sets that the Myo is located on the left forearm.
     *
     * @return This instance for fluent method calls.
     */
    public WritableCalibrationProfile setLeftArm() {
        this.wearableLocation = WearableLocation.LEFT_ARM;
        return this;
    }

    /**
     * Sets that the Myo is located on the right forearm.
     *
     * @return This instance for fluent method calls.
     */
    public WritableCalibrationProfile setRightArm() {
        this.wearableLocation = WearableLocation.RIGHT_ARM;
        return this;
    }

    /**
     * Sets that the Myo's USB port is facing the user's wrist.
     *
     * @return This instance for fluent method calls.
     */
    public WritableCalibrationProfile setUsbTowardsWrist() {
        this.usbDirection = UsbDirection.TOWARDS_WRIST;
        return this;
    }

    /**
     * Sets that the Myo's USB port is facing the user's elbow.
     *
     * @return This instance for fluent method calls.
     */
    public WritableCalibrationProfile setUsbTowardsElbow() {
        this.usbDirection = UsbDirection.TOWARDS_ELBOW;
        return this;
    }

    /**
     * Sets the normalization factor, calculated by the calibration.
     *
     * @param normalizationFactor The new normalization factor.
     * @return This instance for fluent method calls.
     */
    public WritableCalibrationProfile setNormalizationFactor(double normalizationFactor) {
        this.normalizationFactor = normalizationFactor;
        return this;
    }

    /**
     * Sets the accelerometers bias on the x-axis, calculated by the calibration.
     *
     * @param accelerometerBiasX The new x-axis bias of the accelerometer.
     * @return This instance for fluent method calls.
     */
    public WritableCalibrationProfile setAccelerometerBiasX(double accelerometerBiasX) {
        this.accelerometerBiasX = accelerometerBiasX;
        return this;
    }

    /**
     * Sets the accelerometers bias on the y-axis, calculated by the calibration.
     *
     * @param accelerometerBiasY The new y-axis bias of the accelerometer.
     * @return This instance for fluent method calls.
     */
    public WritableCalibrationProfile setAccelerometerBiasY(double accelerometerBiasY) {
        this.accelerometerBiasY = accelerometerBiasY;
        return this;
    }

    /**
     * Sets the accelerometers bias on the z-axis, calculated by the calibration.
     *
     * @param accelerometerBiasZ The new z-axis bias of the accelerometer.
     * @return This instance for fluent method calls.
     */
    public WritableCalibrationProfile setAccelerometerBiasZ(double accelerometerBiasZ) {
        this.accelerometerBiasZ = accelerometerBiasZ;
        return this;
    }

    /**
     * Sets the angle, in radians, by which the Myo is rotated around the user's arm.
     *
     * @param rotationAngle The new rotation angle of the Myo.
     * @return This instance for fluent method calls.
     */
    public WritableCalibrationProfile setRotationAngle(double rotationAngle) {
        this.rotationAngle = Math.max(-Math.PI, Math.min(Math.PI, rotationAngle));
        return this;
    }

    /**
     * Gets the orientation relative to the Myo's internal orientation.
     *
     * @return The orientation to rotate the Myo's internal orientation with to obtain an orientation in the
     * user's global coordinate system.
     */
    public Quat4d getOrientation() {
        return this.orientation;
    }

    /**
     * Sets the orientation relative to the Myo's internal orientation.
     */
    public WritableCalibrationProfile setOrientation(Quat4d orientation) {
        this.orientation = orientation;
        return this;
    }

    /**
     * Gets the orientation relative to the Myo's IMU coordinate system.
     *
     * @return The rotation to apply to the IMU.
     */
    public Quat4d getImuOrientation() {
        return this.imuOrientation;
    }

    /**
     * Sets the orientation relative to the Myo's IMU coordinate system.
     */
    public WritableCalibrationProfile setImuOrientation(Quat4d orientation) {
        this.imuOrientation = orientation;
        return this;
    }

    /**
     * Sets that the calibration of the Myo was successfully completed.
     * Must only be called, when the Myo associated with this calibration profile is currently connected.
     */
    public void calibrationComplete() {
        this.calibrationComplete = true;
        this.removeRawImuEmgListener(this.calibrationPage);
        this.calibrationPage = null;
        this.connectionHandler.onMyoCalibrated(this.associatedMyo);
        this.desiredEmgMode = this.emgCalibratedListeners.size() > 0 || this.emgRawListeners.size() > 0 ? MyoCmds.EmgMode.FILTERED : MyoCmds.EmgMode.NONE;
        this.desiredImuMode = this.imuCalibratedListeners.size() > 0 || this.imuRawListeners.size() > 0 ? MyoCmds.ImuMode.DATA : MyoCmds.ImuMode.NONE;
        this.refreshReceivingMode();
    }

    /**
     * Sets that the calibration of the Myo, if successfully completed before, is not valid anymore
     * and must be redone.
     * Must only be called, when the myo associated with this calibration profile is currently connected.
     */
    public void invalidateCalibration() {
        this.calibrationComplete = false;
        this.connectionHandler.onMyoUncalibrated(this.associatedMyo);
        this.desiredEmgMode = this.emgRawListeners.size() > 0 ? MyoCmds.EmgMode.FILTERED : MyoCmds.EmgMode.NONE;
        this.desiredImuMode = this.imuRawListeners.size() > 0 ? MyoCmds.ImuMode.DATA : MyoCmds.ImuMode.NONE;
        this.refreshReceivingMode();
    }

    /**
     * Notifies the profile, that the calibration on the associated Myo is beginning
     *
     * @param calibrationPage The calibration page for callbacks.
     */
    public void startCalibration(PageCalibration calibrationPage) {
        this.calibrationPage = calibrationPage;
        this.addRawImuEmgListener(this.calibrationPage);
        this.desiredEmgMode = MyoCmds.EmgMode.FILTERED;
        this.desiredImuMode = MyoCmds.ImuMode.DATA;
        this.refreshReceivingMode();
    }

    /**
     * Notifies the profile, that the calibration of the associated Myo was aborted, and that the callback
     * is no longer valid.
     */
    public void abortCalibration() {
        this.removeRawImuEmgListener(this.calibrationPage);
        this.calibrationPage = null;
        this.desiredEmgMode = MyoCmds.EmgMode.NONE;
        this.desiredImuMode = MyoCmds.ImuMode.NONE;
        this.refreshReceivingMode();
    }

    /**
     * Sets what kind of EMG, IMU and classifier data is streamed from the Myo to the currently set modes.
     */
    public void refreshReceivingMode() {
        super.refreshReceivingMode();
    }

    /**
     * Called automatically whenever a listener is registered and the Myo associated with this calibration profile
     * sends new EMG data.
     *
     * @param timestamp The timestamp, as submitted by myolib.
     * @param data      The data that was sent by the Myo, previously converted to double-representation.
     */
    void onEmgData(long timestamp, double[] data) {
        synchronized (this) {
            for (IRawDataOutputListener listener : this.emgRawListeners) {
                listener.onRawEmgData(this.associatedMyo, timestamp, data);
            }
            if (this.isCalibrationComplete()) {
                InterpolatedEmgSignal emgSignal = this.applyEmgCalibration(data);
                if (this.isCalibrationComplete()) {
                    for (IDataOutputListener listener : this.emgCalibratedListeners) {
                        listener.onEmgData(this.associatedMyo, timestamp, emgSignal);
                    }
                }
            }
        }
    }

    /**
     * Called automatically whenever a listener is registered and the Myo associated with this calibration profile
     * sends new IMU data.
     *
     * @param timestamp         The timestamp, as submitted by myolib.
     * @param accelerometerData The data that was sent by the Myo's accelerometer.
     * @param gyroData          The data that was sent by the Myo's gyroscope.
     * @param orientationData   The orientation data that was calculated and sent by the Myo. [w,x,y,z]
     */
    void onImuData(long timestamp, double[] accelerometerData, double[] gyroData, double[] orientationData) {
        synchronized (this) {
            for (IRawDataOutputListener listener : this.imuRawListeners) {
                listener.onRawImuData(this.associatedMyo, timestamp, accelerometerData, gyroData, orientationData);
            }
            if (this.isCalibrationComplete()) {
                Quat4d orientationSignal = this.applyOrientationCalibration(orientationData);
                Vector3d accelerometerSignal = this.applyAccelerometerCalibration(accelerometerData, orientationSignal);
                Vector3d gyroSignal = this.applyGyroscopeCalibration(gyroData, orientationSignal);
                if (this.isCalibrationComplete()) {
                    for (IDataOutputListener listener : this.imuCalibratedListeners) {
                        listener.onImuData(this.associatedMyo, timestamp, accelerometerSignal.getData(), gyroSignal.getData(), orientationSignal.getData());
                    }
                }
            }
        }
    }

}
