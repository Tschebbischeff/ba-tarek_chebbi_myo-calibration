package com.tschebbischeff.myocalib.internal.backend.calibration.interpolation;

import java.util.Map;
import java.util.TreeMap;

/**
 * Limits the domain of the function, and sets values outside of the domain to their "modulus"
 */
public abstract class CyclicInterpolationFunction extends InterpolationFunction {

    /**
     * The upper limit of the functions domain.
     */
    private double domainMinimum;
    /**
     * The lower limit of the functions domain.
     */
    private double domainMaximum;

    /**
     * Constructor.
     *
     * @param firstDomainLimit  One of the two limits of the functions domain.
     * @param secondDomainLimit The other of the two limits of the functions domain.
     */
    public CyclicInterpolationFunction(double firstDomainLimit, double secondDomainLimit) {
        this.domainMinimum = Math.min(firstDomainLimit, secondDomainLimit);
        this.domainMaximum = Math.max(firstDomainLimit, secondDomainLimit);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void add(double domainValue, double value) {
        super.add(this.getInnerDomainValue(domainValue), value);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addAll(TreeMap<Double, Double> c) {
        for (Map.Entry<Double, Double> e : c.entrySet()) {
            this.values.put(this.getInnerDomainValue(e.getKey()), e.getValue());
        }
        this.recalculateParameters();
    }

    /**
     * Distributes the values in the given order along the domain of the function.
     * Jumps some internal method calls to improve performance.
     *
     * @param values The values to distribute.
     * @param offset The offset from the minimum of the domain.
     */
    public void addEvenlyDistributed(double[] values, Double offset) {
        double[] domainSamples = this.getEvenlyDistributedDomainSamples(values.length, offset);
        for (int i = 0; i < values.length; i++) {
            this.values.put(domainSamples[i], values[i]);
        }
        this.recalculateParameters();
    }

    /**
     * Limits a value to the domain of the function.
     *
     * @param domainValue The original domain value.
     * @return The respective domain value inside the functions domain.
     */
    private Double getInnerDomainValue(Double domainValue) {
        return this.domainMinimum + ((domainValue - this.domainMinimum) % (this.domainMaximum - this.domainMinimum));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Double sampleAt(double domainValue) {
        if (this.values.size() == 0) {
            return null;
        }
        return this.executeSample(this.getInnerDomainValue(domainValue));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Double getLowerSupportPoint(double domainValue) {
        Double result = super.getLowerSupportPoint(domainValue);
        if (result != null) {
            return result;
        } else {
            return this.values.lastKey();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Double getHigherSupportPoint(double domainValue) {
        Double result = super.getHigherSupportPoint(domainValue);
        if (result != null) {
            return result;
        } else {
            return this.values.firstKey();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected double getDistance(double a, double b) {
        a = getInnerDomainValue(a);
        b = getInnerDomainValue(b);
        double firstDistance = Math.abs(b - a);
        double secondDistance = (Math.min(a, b) - this.domainMinimum) + (this.domainMaximum - Math.max(a, b));
        return Math.min(firstDistance, secondDistance);
    }

    /**
     * Gets the minimum value still in the domain of the function.
     *
     * @return The minimum domain value of the cyclic function.
     */
    public double getDomainMinimum() {
        return this.domainMinimum;
    }

    /**
     * Gets the maximum value still in the domain of the function.
     *
     * @return The maximum domain value of the cyclic function.
     */
    public double getDomainMaximum() {
        return this.domainMinimum;
    }

    /**
     * Gets a number of sample points which are evenly distributed on the domain of the function.
     * I.e. for two neighbouring points their distance is the same as the distance of all other pairs of neighboring points.
     *
     * @param numberOfSamples The number of samples to distribute.
     * @param offset          The offset from the minimum of the domain, where the first point is located. If null, the offset is
     *                        half the distance of two neighboring points.
     * @return A set of ordered sample points evenly distributed on the domain of the function. The number of sample points
     * is equal to the supplied parameter.
     */
    public double[] getEvenlyDistributedDomainSamples(int numberOfSamples, Double offset) {
        double stepSize = (this.domainMaximum - this.domainMinimum) / (numberOfSamples + 1);
        if (offset == null) {
            offset = stepSize / 2;
        }
        double[] result = new double[numberOfSamples];
        for (int i = 0; i < numberOfSamples; i++) {
            result[i] = this.getInnerDomainValue(this.domainMinimum + offset + i * stepSize);
        }
        return result;
    }
}
