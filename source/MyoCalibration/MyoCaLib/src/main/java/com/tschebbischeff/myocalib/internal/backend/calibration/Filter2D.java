package com.tschebbischeff.myocalib.internal.backend.calibration;

import android.support.v4.util.Pair;

import java.util.TreeMap;

/**
 * Superclass for any kind of two-dimensional filters that can be applied to data.
 *
 * @param <I> The type of the index, used to order the data.
 * @param <T> The type of data this filter can be applied to.
 */
public abstract class Filter2D<I, T> {

    /**
     * Applies the filter to one value.
     *
     * @param index The index used to order the value.
     * @param value The value to which to apply the filter.
     * @return The result of the application.
     */
    public abstract Pair<I, T> apply(I index, T value);

    /**
     * Applies the filter to a number of key-value pairs, this method is only called if index and value array have
     * the same length.
     *
     * @param indices An array containing the indices.
     * @param values  An array containing the values.
     * @return A map ordered by the indice array as keys and their respective values, after application of the filter.
     */
    protected abstract TreeMap<I, T> apply(I[] indices, T[] values);

    /**
     * Applies the filter to a number of associated indices and values. The index at position i in the array must
     * correspond to the value at position i in the value array.
     * Both arrays need to be of same length.
     *
     * @param indices The indices by which to order the values.
     * @param values  The values in order to which to apply the filter.
     * @return The result of the application, or null, if the arrays are not of same length.
     */
    public TreeMap<I, T> batchApply(I[] indices, T[] values) {
        if (indices.length == values.length) {
            return this.apply(indices, values);
        }
        return null;
    }

    /**
     * Resets the filter to a state at which it is as if it was just created.
     */
    public abstract void reset();
}
