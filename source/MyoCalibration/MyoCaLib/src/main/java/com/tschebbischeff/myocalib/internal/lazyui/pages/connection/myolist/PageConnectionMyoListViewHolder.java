package com.tschebbischeff.myocalib.internal.lazyui.pages.connection.myolist;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.widget.*;
import com.tschebbischeff.myocalib.R;
import com.tschebbischeff.myocalib.internal.lazyui.pages.connection.PageConnection;

/**
 * Saves references to the views in a single card view of the Connection pages Myo-List recyclerview.
 */
public class PageConnectionMyoListViewHolder extends RecyclerView.ViewHolder {
    /**
     * The text view containing the Myo's name.
     */
    public TextView deviceName;
    /**
     * The text view containing the Myo's device id.
     */
    public TextView deviceId;
    /**
     * The image view showing a picture of the Myo's approximate battery level.
     */
    public ImageView batteryIcon;
    /**
     * The text view showing the Myo's current battery level.
     */
    public TextView batteryLevel;
    /**
     * The informational text on the bottom of the card.
     */
    public TextView infoText;
    /**
     * The cardview to touch, when the Myo needs to be calibrated.
     */
    public CardView cardView;
    /**
     * The button to connect to the Myo.
     */
    public Button connectButton;

    /**
     * Automatically created each time a card is created in the recycler view.
     *
     * @param source   The connection page object, for callbacks.
     * @param rootView The root view containing the layout of a myo card.
     */
    public PageConnectionMyoListViewHolder(PageConnection source, LinearLayout rootView) {
        super(rootView);
        cardView = rootView.findViewById(R.id.myocalib_lazyui_pages_connection_cardsmyo_cardview);
        deviceName = rootView.findViewById(R.id.myocalib_lazyui_pages_connection_cardsmyo_devicename);
        deviceId = rootView.findViewById(R.id.myocalib_lazyui_pages_connection_cardsmyo_deviceid);
        batteryIcon = rootView.findViewById(R.id.myocalib_lazyui_pages_connection_cardsmyo_batteryicon);
        batteryLevel = rootView.findViewById(R.id.myocalib_lazyui_pages_connection_cardsmyo_batterylevel);
        connectButton = rootView.findViewById(R.id.myocalib_lazyui_pages_connection_cardsmyo_connectbutton);
        infoText = rootView.findViewById(R.id.myocalib_lazyui_pages_connection_cardsmyo_infotext);
        PageConnectionMyoListOnClickListener onClickListener = new PageConnectionMyoListOnClickListener(source, this);
        connectButton.setOnClickListener(onClickListener);
        cardView.setOnClickListener(onClickListener);
    }
}