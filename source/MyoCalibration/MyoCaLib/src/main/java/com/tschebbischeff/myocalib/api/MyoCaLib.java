package com.tschebbischeff.myocalib.api;

import android.app.Activity;
import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import com.tschebbischeff.common.android.logger.Logger;
import com.tschebbischeff.myocalib.internal.backend.MyoCalibrationActivity;
import com.tschebbischeff.myocalib.internal.backend.connection.ConnectionHandler;
import eu.darken.myolib.Myo;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Serves as a single point of controlling the whole calibration process in an easy way.
 * The API is provided as a service.
 * To ensure a lifecycle connected to the calling activity's, simply bind to this service in your main activity's
 * onCreate and unbind in your onDestroy. Android handles creation and destruction of the service.
 * When you bind in several build-up methods make sure you unbind in the appropriate tear-down methods to ensure
 * the service's lifecycle is correctly handled by Android.
 * This means you can only call the API from one process, as it does not implement IPC.
 * <p>
 * Certain modules of the calibration are bound to the lifecycle of this service.
 * <b>Do not unbind from this service from all sources, until you do not want to use any functionality anymore.</b>
 * This specifically includes the connection to the Myo armbands, which is terminated, when this service terminates.
 * <p>
 * Make sure to open the calibration activity by first binding to this service and then using its {@link MyoCaLib#startCalibrationActivity(Activity)}
 * method instead of starting it directly, as it will bind to this service and unbind when it is closed.
 */
public final class MyoCaLib extends Service {

    /**
     * Android service binder, which is returned to activites, services and content providers, which bind to this service.
     */
    private final IBinder mBinder = new LocalBinder();
    /**
     * Handles connections to Myo
     */
    protected ConnectionHandler connectionHandler;
    /**
     * Contains values and objects regarding settings of the whole calibration process.
     */
    private CalibrationSettings calibrationSettings;

    /**
     * {@inheritDoc}
     */
    @Override
    public IBinder onBind(Intent intent) {
        return this.mBinder;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onCreate() {
        Logger.debug("MyoCaLib", "Service started!");
        this.calibrationSettings = new CalibrationSettings(this.getApplicationContext());
        this.connectionHandler = new ConnectionHandler(this, this.calibrationSettings);
    }

    /**
     * {@inheritDoc}
     */
    public void onDestroy() {
        Logger.debug("MyoCaLib", "Service stopped!");
        this.connectionHandler.onDestroy();
    }

    /**
     * Convenience method for showing the main activity of the calibration library.
     *
     * @param sourceActivity The activity of the calling application.
     */
    public void startCalibrationActivity(Activity sourceActivity) {
        Intent intent = new Intent(sourceActivity, MyoCalibrationActivity.class);
        sourceActivity.startActivity(intent);
    }

    /**
     * Get an object of all settings, that are relevant for calibration. Use this object to modify the
     * calibration process. It has attached subobjects, which seperate the settings into the different areas of
     * the calibration, and contains methods for saving and loading of the settings to/from storage.
     *
     * @return An object containing calibration settings related methods and objects.
     */
    public CalibrationSettings getCalibrationSettings() {
        return this.calibrationSettings;
    }

    /**
     * Returns a list of ALL myos currently connected to by the user's request in the connection page of the library,
     * which were also successfully calibrated in the calibration page of the library.
     * The list is constructed in this method, and is therefore not kept up-to-date via reference.
     *
     * @return A list of currently connected Myo's that are calibrated.
     */
    public ArrayList<Myo> getCalibratedMyos() {
        ArrayList<Myo> result = this.connectionHandler.getConnectedMyosCopy();
        Iterator<Myo> i = result.iterator();
        while (i.hasNext()) {
            Myo myo = i.next();
            if (!this.isMyoCalibrated(myo)) {
                i.remove();
            }
        }
        return result;
    }

    /**
     * Returns all myos, which are connected but not calibrated, in the order they were added to the main list of
     * connected myos.
     * The list is constructed in this method, and is therefore not kept up-to-date via reference.
     *
     * @return A sorted list of connected myos which are not calibrated.
     */
    public ArrayList<Myo> getUncalibratedMyos() {
        ArrayList<Myo> result = this.connectionHandler.getConnectedMyosCopy();
        Iterator<Myo> i = result.iterator();
        while (i.hasNext()) {
            Myo myo = i.next();
            if (this.isMyoCalibrated(myo)) {
                i.remove();
            }
        }
        return result;
    }

    /**
     * Returns a copy of a list of ALL myos currently connected to by the user's request in the connection page of the library.
     * These Myos may or may not be calibrated.
     * The list is constructed in this method, and is therefore not kept up-to-date via reference.
     *
     * @return A list of currently connected Myo's.
     */
    public ArrayList<Myo> getConnectedMyos() {
        return this.connectionHandler.getConnectedMyosCopy();
    }

    /**
     * Searches through connected myos, for a myo with the specified device address.
     *
     * @param deviceId The device address of the connected myo.
     * @return The object representing the connected myo with the specified device address, or null if no myo
     * with the specified address could be found.
     */
    public Myo getConnectedMyoByDeviceId(String deviceId) {
        return this.connectionHandler.getConnectedMyoByDeviceId(deviceId);
    }

    /**
     * Returns the number of myos currently connected without creating a copy of the original list.
     *
     * @return The number of currently connected Myos.
     */
    public int getConnectedMyosNum() {
        return this.connectionHandler.getConnectedMyosSize();
    }

    /**
     * Whether the parameter myo has been calibrated successfully.
     *
     * @param myo The myo to check calibration status on.
     * @return True if, and only if, the supplied myo was calibrated successfully via the calibration page of the
     * library.
     */
    public boolean isMyoCalibrated(Myo myo) {
        return this.getMyoCalibrationProfile(myo).isCalibrationComplete();
    }

    /**
     * Get all parameters acquired during the calibration of a specified myo.
     * {@link CalibrationProfile} for information regarding which data can be read and set.
     *
     * @param myo The myo of which to get the calibration related parameters.
     * @return An object encapsulating the calibration parameters of the specified myo.
     */
    public CalibrationProfile getMyoCalibrationProfile(Myo myo) {
        return this.connectionHandler.getCalibrationProfile(myo);
    }

    /**
     * Add a listener, to receive callbacks, whenever certain states in the connection chain of a Myo are reached.
     * To check which events trigger callbacks, see {@link IMyoStateListener}.
     *
     * @param myoStateListener The connection state listener to register.
     */
    public void addMyoStateListener(IMyoStateListener myoStateListener) {
        this.connectionHandler.addMyoStateListener(myoStateListener);
    }

    /**
     * Remove a listener, which previously received connection related callbacks from the list of
     * receiving listeners. No more callbacks will be called on the supplied object.
     *
     * @param myoStateListener The connection state listener to unregister.
     */
    public void removeMyoStateListener(IMyoStateListener myoStateListener) {
        this.connectionHandler.removeMyoStateListener(myoStateListener);
    }

    /**
     * Local binder for communication between the service and clients.
     * Contains a reference to the one and only validly initialized object of this service.
     */
    public class LocalBinder extends Binder {
        public MyoCaLib getService() {
            return MyoCaLib.this;
        }
    }
}
