package com.tschebbischeff.myocalib.internal.backend.connection;

/**
 * The current connection stage a Myo can be in.
 */
public enum ConnectionState {
    /**
     * The state any Myo that is currently not in vicinity is in. I.e. any Myo not in the list of Myos.
     */
    UNKNOWN,
    /**
     * The Myo is currently connected to to get the device name and the battery level.
     */
    CONNECTING_FOR_PREPARE,
    /**
     * The device name and battery level are being read.
     */
    PREPARING,
    /**
     * The Myo is done preparing and is disconnecting.
     */
    DISCONNECTING_AFTER_PREPARE,
    /**
     * The Myo has recently been disconnected and may not yet show up in scans, the next scan will set the Myo
     * as ready for connections.
     */
    WAITING_FOR_SCAN,
    /**
     * The Myo is prepared and waiting for the user to click the connect button.
     */
    WAITING_FOR_CONNECT,
    /**
     * The Myo was requested to be connected to by the user, and if it disconnects it should be reconnected immediately.
     */
    CONNECTING,
    /**
     * The Myo is currently connected.
     */
    CONNECTED,
    /**
     * The Myo is currently disconnecting, because the user initiated a disconnect.
     */
    DISCONNECTING,
    /**
     * The Myo is currently disconnected, but should return to connected state automatically, as soon as possible.
     */
    DISCONNECTED_SHOULD_CONNECT,
    /**
     * The Myo is disconnecting, because the service was unbound from all sources.
     */
    DESTROYING;

    /**
     * Check whether this object is in the supplied list of connection stages.
     *
     * @param stages An array containing any combination of enum constants of type {@link ConnectionState}.
     * @return True if, and only if, this enum constant is listed in the supplied array.
     */
    public boolean oneOf(ConnectionState... stages) {
        for (ConnectionState stage : stages) {
            if (this == stage) {
                return true;
            }
        }
        return false;
    }
}
