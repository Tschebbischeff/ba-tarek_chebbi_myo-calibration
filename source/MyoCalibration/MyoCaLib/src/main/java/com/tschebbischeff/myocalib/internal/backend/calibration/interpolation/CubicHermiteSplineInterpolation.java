package com.tschebbischeff.myocalib.internal.backend.calibration.interpolation;

import java.util.HashMap;
import java.util.Map;

/**
 * Interpolates the values of the function with cubic hermite splines.
 * The chosen tangents on the endpoints of an interval are the finite differences of the respective three points.
 */
public class CubicHermiteSplineInterpolation extends CyclicInterpolationFunction {

    /**
     * The precalculated tangents for the given index, which corresponds to the index in the values map.
     */
    private HashMap<Double, Double> finiteDifferences;

    /**
     * Constructor.
     */
    public CubicHermiteSplineInterpolation() {
        super(0, 2 * Math.PI);
        this.finiteDifferences = new HashMap<>();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void recalculateParameters() {
        //Precalculate finiteDifferences a.k.a tangents at endpoints
        Map.Entry<Double, Double> current = this.values.firstEntry();
        while (current != null) {
            double lowerKey = this.getLowerSupportPoint(current.getKey());
            double higherKey = this.getHigherSupportPoint(current.getKey());
            this.finiteDifferences.put(current.getKey(), 0.5d * (
                    ((this.values.get(higherKey) - current.getValue()) / (higherKey - current.getKey()))
                            + ((current.getValue() - this.values.get(lowerKey)) / (current.getKey() - lowerKey))
            ));
            current = this.values.higherEntry(current.getKey());
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Double executeSample(Double domainValue) {
        if (this.isSupportPoint(domainValue)) {
            return this.values.get(domainValue);
        }
        /*
        IMPLEMENTATION PER WIKIPEDIA ARTICLE, SEE: https://en.wikipedia.org/wiki/Cubic_Hermite_spline#Interpolation_on_an_arbitrary_interval
        double lowerSupportPoint = this.getFloorSupportPoint(domainValue);
        double higherSupportPoint = this.getCeilingSupportPoint(domainValue);
        double lowerSampleValue = this.values.get(this.getFloorSupportPoint(domainValue));
        double higherSampleValue = this.values.get(this.getCeilingSupportPoint(domainValue));
        double affineScaling = (higherSupportPoint - lowerSupportPoint);
        double t = (domainValue - lowerSupportPoint) / affineScaling;
        double hermiteA = 2 * (t * t * t) - 3 * (t * t) + 1;
        double hermiteB = (t * t * t) - 2 * (t * t) + t;
        double hermiteC = -2 * (t * t * t) + 3 * (t * t);
        double hermiteD = (t * t * t) - (t * t);
        return hermiteA * lowerSampleValue + hermiteB * affineScaling * this.finiteDifferences.get(lowerSupportPoint) + hermiteC * higherSampleValue + hermiteD * affineScaling * this.finiteDifferences.get(higherSupportPoint);*/

        //Collapsed space-efficient, but less straight forward:
        double lowerSupportPoint = this.getLowerSupportPoint(domainValue);
        double higherSupportPoint = this.getHigherSupportPoint(domainValue);
        double affineScaling = (higherSupportPoint - lowerSupportPoint);
        double t = (domainValue - lowerSupportPoint) / affineScaling;
        return (2 * (t * t * t) - 3 * (t * t) + 1) * this.values.get(this.getLowerSupportPoint(domainValue)) +
                ((t * t * t) - 2 * (t * t) + t) * affineScaling * this.finiteDifferences.get(lowerSupportPoint) +
                (-2 * (t * t * t) + 3 * (t * t)) * this.values.get(this.getHigherSupportPoint(domainValue)) +
                ((t * t * t) - (t * t)) * affineScaling * this.finiteDifferences.get(higherSupportPoint);
    }
}
