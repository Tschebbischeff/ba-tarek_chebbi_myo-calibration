package com.tschebbischeff.myocalib.internal.lazyui.pages.calibration.actionlist;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.tschebbischeff.common.android.logger.Logger;
import com.tschebbischeff.myocalib.R;
import com.tschebbischeff.myocalib.api.CalibrationSettings;
import com.tschebbischeff.myocalib.api.EmgCalibrationSettings;
import com.tschebbischeff.myocalib.api.IRawDataOutputListener;
import com.tschebbischeff.myocalib.api.ImuCalibrationSettings;
import com.tschebbischeff.myocalib.internal.lazyui.pages.calibration.PageCalibration;
import eu.darken.myolib.Myo;

import java.util.ArrayList;
import java.util.EnumSet;

import static com.tschebbischeff.myocalib.internal.lazyui.pages.calibration.actionlist.PageCalibrationActionListViewHolderDone.RotationTranslationResult;

/**
 * Adapter for the recycler view containing the actions that are needed to be performed one after another
 * to calibrate the device.
 */
public class PageCalibrationActionListAdapter extends RecyclerView.Adapter<PageCalibrationActionListViewHolder> implements IRawDataOutputListener {

    /**
     * The calibration page, for callbacks.
     */
    private PageCalibration source;
    /**
     * The android context, for calling certain android methods.
     */
    private Activity activity;
    /**
     * An array containing the steps that must actually be shown
     */
    private ArrayList<ViewType> steps;
    /**
     * An array for the error page to query which errors happened.
     */
    private RotationTranslationResult[] latestErrorResult;

    /**
     * Constructor.
     *
     * @param source   The calibration page for callbacks.
     * @param activity The android context, for calling certain android methods.
     */
    public PageCalibrationActionListAdapter(PageCalibration source, Activity activity) {
        this.source = source;
        this.activity = activity;
        //Reset view holders
        ViewType.removeAll();
        //Create array of necessary steps
        this.steps = new ArrayList<>();
        for (ViewType vt : ViewType.values()) {
            vt.init(activity.getApplicationContext(), source.getSharedPageData().getCalibratingMyo());
            if (vt.enabled(source.getSharedPageData().getApiService().getCalibrationSettings())) {
                this.steps.add(vt);
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PageCalibrationActionListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return ViewType.values()[viewType].getUniqueViewHolder(this.source, this, this.activity, parent);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onBindViewHolder(PageCalibrationActionListViewHolder holder, int position) {
        holder.onBind(position);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getItemViewType(int position) {
        return this.steps.get(position).ordinal();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getItemCount() {
        return this.steps.size();
    }

    /**
     * Called when the activity is closed.
     */
    public void onDestroy() {
        if (this.steps.size() > 0) {
            this.steps.get(0).getUniqueViewHolder().onDestroy();
            this.steps.get(0).getUniqueViewHolder().reset();
        }
    }

    /**
     * Called each time a step in the calibration process has been completed.
     *
     * @return True if, and only if, this was the last step of the action list.
     */
    public boolean stepCompleted() {
        this.steps.get(0).getUniqueViewHolder().reset();
        this.steps.remove(0);
        try {
            this.notifyItemRemoved(0);
        } catch (Exception ignored) {
            Logger.warning("PageCalibrationActionListAdapter", "Update of recycler view failed due to IllegalStateException");
        }
        try {
            this.notifyItemRangeChanged(0, this.getItemCount());
        } catch (Exception ignored) {
            Logger.warning("PageCalibrationActionListAdapter", "Update of recycler view failed due to IllegalStateException");
        }
        return this.steps.size() == 0;
    }

    /**
     * Called when a recycler view element from the action list recycler view is clicked.
     *
     * @param view       The view that was clicked.
     * @param viewHolder The viewHolder containing the view, i.e. the card that was clicked.
     */
    public void onClick(View view, PageCalibrationActionListViewHolder viewHolder) {
        if (this.steps.size() > 0 && this.steps.get(0).getUniqueViewHolder() == viewHolder) {
            viewHolder.onClick(view);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onRawEmgData(Myo myo, long timestamp, double[] data) {
        if (this.steps.size() > 0) {
            PageCalibrationActionListViewHolder vh = this.steps.get(0).getUniqueViewHolder();
            if (vh != null) {
                vh.onRawEmgData(myo, timestamp, data);
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onRawImuData(Myo myo, long timestamp, double[] accelerometerData, double[] gyroscopeData, double[] orientationData) {
        if (this.steps.size() > 0) {
            PageCalibrationActionListViewHolder vh = this.steps.get(0).getUniqueViewHolder();
            if (vh != null) {
                vh.onRawImuData(myo, timestamp, accelerometerData, gyroscopeData, orientationData);
            }
        }
    }

    /**
     * Returns an array with descriptors of the latest error result returned by the rotation and translation analysis.
     *
     * @return An array containing one or more error descriptors.
     */
    public RotationTranslationResult[] getLatestErrorResult() {
        return latestErrorResult;
    }

    public void handleRotationTranslationError(EnumSet<RotationTranslationResult> rotationTranslationResult) {
        CalibrationSettings cs = this.source.getSharedPageData().getApiService().getCalibrationSettings();
        //re-add necessary previous steps
        this.steps.add(1, ViewType.DONE);
        if (ViewType.RVC_FORWARD_RELAX.enabled(cs)) {
            this.steps.add(1, ViewType.RVC_FORWARD_RELAX);
        }
        if (ViewType.RVC_FORWARD_EXTENSION.enabled(cs)) {
            this.steps.add(1, ViewType.RVC_FORWARD_EXTENSION);
        }
        if (ViewType.RVC_FORWARD_INIT.enabled(cs)) {
            this.steps.add(1, ViewType.RVC_FORWARD_INIT);
        }
        if (rotationTranslationResult.contains(RotationTranslationResult.TRANSLATION_WRIST) ||
                rotationTranslationResult.contains(RotationTranslationResult.TRANSLATION_ELBOW)) {
            if (ViewType.RVC_DOWN_RELAX.enabled(cs)) {
                this.steps.add(1, ViewType.RVC_DOWN_RELAX);
            }
            if (ViewType.RVC_DOWN_EXTENSION.enabled(cs)) {
                this.steps.add(1, ViewType.RVC_DOWN_EXTENSION);
            }
            if (ViewType.RVC_DOWN_INIT.enabled(cs)) {
                this.steps.add(1, ViewType.RVC_DOWN_INIT);
            }
        }
        //add error description card to the beginning
        this.steps.add(1, ViewType.ROTATION_TRANSLATION_ERROR);
        //create error-array for the card
        this.latestErrorResult = new RotationTranslationResult[rotationTranslationResult.size()];
        int i = 0;
        for (RotationTranslationResult r : rotationTranslationResult) {
            this.latestErrorResult[i] = r;
            i++;
        }
    }

    /**
     * Enum describing the possible actions, of which each is used as a view type supplied to the creator of the
     * view holders automatically.
     * <p>
     * The actions are listed in the order they need to be performed. The correct view holder is created
     * in the {@link #onCreateViewHolder(ViewGroup, int)} method, which gets the index of the enum constant as a second
     * argument, as calculated by {@link #getItemViewType(int)}, which bases the decision off the number of completed
     * steps (as offset) and the position of the element.
     */
    public enum ViewType {
        IMU_EXPLANATION, IMU_X, IMU_Y, IMU_Z,
        ARM,
        WARM_UP,
        RVC_DOWN_INIT, RVC_DOWN_EXTENSION, RVC_DOWN_RELAX,
        RVC_FORWARD_INIT, RVC_FORWARD_EXTENSION, RVC_FORWARD_RELAX,
        DONE,
        ROTATION_TRANSLATION_ERROR;

        /**
         * As each card should be used only once, each enum constant has its own singular view holder. There will never be more than one view holder per card.
         */
        PageCalibrationActionListViewHolder uniqueViewHolder = null;
        /**
         * Whether the biases were measured before and are saved and readable in a shared preferences file.
         */
        private boolean biasesSavedAndReadable = false;

        /**
         * Deletes all view holders, so they can be reinstantiated (when a new adapter was created)
         */
        public static void removeAll() {
            for (ViewType vt : ViewType.values()) {
                vt.uniqueViewHolder = null;
            }
        }

        public void init(Context appContext, Myo calibratingMyo) {
            SharedPreferences sharedPref = appContext.getSharedPreferences(appContext.getString(R.string.myocalib_sharedprefs_path_myosettings) + "_" + calibratingMyo.getDeviceAddress(), Context.MODE_PRIVATE);
            this.biasesSavedAndReadable = sharedPref.contains(appContext.getString(R.string.myocalib_myosettings_acclBiasX)) &&
                    sharedPref.contains(appContext.getString(R.string.myocalib_myosettings_acclBiasY)) &&
                    sharedPref.contains(appContext.getString(R.string.myocalib_myosettings_acclBiasZ));
        }

        /**
         * Gets the unique view holder for this card. If none exists yet, it is created.
         *
         * @param source  The calibration page for callbacks.
         * @param adapter The adapter for callbacks.
         * @param parent  The parent view, in which to inflate the correct XML.
         * @return The unique view holder of this card.
         */
        public PageCalibrationActionListViewHolder getUniqueViewHolder(PageCalibration source, PageCalibrationActionListAdapter adapter, Activity activity, ViewGroup parent) {
            if (this.uniqueViewHolder == null) {
                switch (this) {
                    case IMU_EXPLANATION:
                        this.uniqueViewHolder = new PageCalibrationActionListViewHolderImuExplanation(source, adapter, activity,
                                LayoutInflater.from(parent.getContext()).inflate(R.layout.myocalib_lazyui_pages_calibration_cardsimuexplanation, parent, false));
                        break;
                    case IMU_X:
                        this.uniqueViewHolder = new PageCalibrationActionListViewHolderImuX(source, adapter, activity,
                                LayoutInflater.from(parent.getContext()).inflate(R.layout.myocalib_lazyui_pages_calibration_cardsimux, parent, false));
                        break;
                    case IMU_Y:
                        this.uniqueViewHolder = new PageCalibrationActionListViewHolderImuY(source, adapter, activity,
                                LayoutInflater.from(parent.getContext()).inflate(R.layout.myocalib_lazyui_pages_calibration_cardsimuy, parent, false));
                        break;
                    case IMU_Z:
                        this.uniqueViewHolder = new PageCalibrationActionListViewHolderImuZ(source, adapter, activity,
                                LayoutInflater.from(parent.getContext()).inflate(R.layout.myocalib_lazyui_pages_calibration_cardsimuz, parent, false));
                        break;
                    case ARM:
                        this.uniqueViewHolder = new PageCalibrationActionListViewHolderArm(source, adapter, activity,
                                LayoutInflater.from(parent.getContext()).inflate(R.layout.myocalib_lazyui_pages_calibration_cardsarm, parent, false));
                        break;
                    case WARM_UP:
                        this.uniqueViewHolder = new PageCalibrationActionListViewHolderWarmUp(source, adapter, activity,
                                LayoutInflater.from(parent.getContext()).inflate(R.layout.myocalib_lazyui_pages_calibration_cardswarmup, parent, false));
                        break;
                    case RVC_DOWN_INIT:
                        this.uniqueViewHolder = new PageCalibrationActionListViewHolderRvcDownInit(source, adapter, activity,
                                LayoutInflater.from(parent.getContext()).inflate(R.layout.myocalib_lazyui_pages_calibration_cardsrvcdowninit, parent, false));
                        break;
                    case RVC_DOWN_EXTENSION:
                        this.uniqueViewHolder = new PageCalibrationActionListViewHolderRvcDownExtension(source, adapter, activity,
                                LayoutInflater.from(parent.getContext()).inflate(R.layout.myocalib_lazyui_pages_calibration_cardsrvcdownextension, parent, false));
                        break;
                    case RVC_DOWN_RELAX:
                        this.uniqueViewHolder = new PageCalibrationActionListViewHolderRvcDownRelax(source, adapter, activity,
                                LayoutInflater.from(parent.getContext()).inflate(R.layout.myocalib_lazyui_pages_calibration_cardsrvcdownrelax, parent, false));
                        break;
                    case RVC_FORWARD_INIT:
                        this.uniqueViewHolder = new PageCalibrationActionListViewHolderRvcForwardInit(source, adapter, activity,
                                LayoutInflater.from(parent.getContext()).inflate(R.layout.myocalib_lazyui_pages_calibration_cardsrvcforwardinit, parent, false));
                        break;
                    case RVC_FORWARD_EXTENSION:
                        this.uniqueViewHolder = new PageCalibrationActionListViewHolderRvcForwardExtension(source, adapter, activity,
                                LayoutInflater.from(parent.getContext()).inflate(R.layout.myocalib_lazyui_pages_calibration_cardsrvcforwardextension, parent, false));
                        break;
                    case RVC_FORWARD_RELAX:
                        this.uniqueViewHolder = new PageCalibrationActionListViewHolderRvcForwardRelax(source, adapter, activity,
                                LayoutInflater.from(parent.getContext()).inflate(R.layout.myocalib_lazyui_pages_calibration_cardsrvcforwardrelax, parent, false));
                        break;
                    case DONE:
                        this.uniqueViewHolder = new PageCalibrationActionListViewHolderDone(source, adapter, activity,
                                LayoutInflater.from(parent.getContext()).inflate(R.layout.myocalib_lazyui_pages_calibration_cardsdone, parent, false));
                        break;
                    case ROTATION_TRANSLATION_ERROR:
                        this.uniqueViewHolder = new PageCalibrationActionListViewHolderRotationTranslationError(source, adapter, activity,
                                LayoutInflater.from(parent.getContext()).inflate(R.layout.myocalib_lazyui_pages_calibration_cardsrotationtranslationerror, parent, false));
                        break;
                    default: //impossible, throws exception
                        Logger.error("PageCalibrationActionListAdapter", "Unimplemented behavior, view type was: " + this.toString());
                        return null;
                }
            }
            return this.uniqueViewHolder;
        }

        /**
         * Gets the unique view holder for this card. If none exists yet, none is created and null is returned.
         *
         * @return The unique view holder of this card.
         */
        public PageCalibrationActionListViewHolder getUniqueViewHolder() {
            return this.uniqueViewHolder;
        }

        /**
         * Defines whether the card defined by the enum constant must be shown or not (depending on calibration settings).
         *
         * @return True if, and only if, a card with this view type must be included in the steps.
         */
        public boolean enabled(CalibrationSettings calibrationSettings) {
            ImuCalibrationSettings csImu = calibrationSettings.imu;
            EmgCalibrationSettings csEmg = calibrationSettings.emg;
            switch (this) {
                case IMU_EXPLANATION:
                case IMU_X:
                case IMU_Y:
                case IMU_Z:
                    return csImu.isCalibrationEnabled() && csImu.isAccelerometerBiasCalibrationEnabled()
                            && (!this.biasesSavedAndReadable || csImu.isForceAccelerometerBiasCalibrationEnabled());
                case ARM:
                    return csEmg.isCalibrationEnabled() || (csImu.isCalibrationEnabled() && csImu.isGlobalCoordinateSystemEnabled());
                case WARM_UP:
                    return RVC_DOWN_EXTENSION.enabled(calibrationSettings)
                            || RVC_FORWARD_EXTENSION.enabled(calibrationSettings);
                case RVC_DOWN_INIT:
                    return RVC_DOWN_EXTENSION.enabled(calibrationSettings)
                            || (csImu.isCalibrationEnabled() && csImu.isGlobalCoordinateSystemEnabled())
                            || (csEmg.isCalibrationEnabled() && (csEmg.isRotationCalibrationEnabled() || csEmg.isTranslationCalibrationEnabled())); //USB direction needed for rotation and translation
                case RVC_DOWN_EXTENSION:
                    return (csEmg.isCalibrationEnabled() && csEmg.isNormalizationEnabled());
                case RVC_DOWN_RELAX:
                    return RVC_DOWN_EXTENSION.enabled(calibrationSettings);
                case RVC_FORWARD_INIT:
                    return RVC_FORWARD_EXTENSION.enabled(calibrationSettings)
                            || (csImu.isCalibrationEnabled() && csImu.isGlobalCoordinateSystemEnabled());
                case RVC_FORWARD_EXTENSION:
                    return (csEmg.isCalibrationEnabled() && (csEmg.isNormalizationEnabled() || csEmg.isRotationCalibrationEnabled() || csEmg.isTranslationCalibrationEnabled()));
                case RVC_FORWARD_RELAX:
                    return RVC_FORWARD_EXTENSION.enabled(calibrationSettings);
                case DONE:
                    return true;
                case ROTATION_TRANSLATION_ERROR:
                    return false;
            }
            return false;
        }
    }

}
