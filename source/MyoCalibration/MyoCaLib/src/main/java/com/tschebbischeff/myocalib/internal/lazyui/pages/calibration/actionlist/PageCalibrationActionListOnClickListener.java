package com.tschebbischeff.myocalib.internal.lazyui.pages.calibration.actionlist;

import android.view.View;
import com.tschebbischeff.myocalib.internal.lazyui.pages.calibration.PageCalibration;

/**
 * Handles click events on the action list.
 */
public class PageCalibrationActionListOnClickListener implements View.OnClickListener {

    /**
     * The calibration page for callbacks.
     */
    private PageCalibration source;
    /**
     * The view holder which created this listener, to reference when calling the calibration pages callback.
     */
    private PageCalibrationActionListViewHolder viewHolder;

    /**
     * Constructor.
     *
     * @param source The calibration page object for callbacks.
     * @param vh     The view holder creating this listener.
     */
    public PageCalibrationActionListOnClickListener(PageCalibration source, PageCalibrationActionListViewHolder vh) {
        this.source = source;
        this.viewHolder = vh;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onClick(View v) {
        this.source.onRecyclerViewClick(v, this.viewHolder);
    }
}