package com.tschebbischeff.myocalib.internal.lazyui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.LinearLayout;
import com.tschebbischeff.common.android.logger.Logger;
import com.tschebbischeff.myocalib.R;
import com.tschebbischeff.myocalib.internal.backend.MyoCalibrationActivity;
import com.tschebbischeff.myocalib.internal.lazyui.pages.Page;
import com.tschebbischeff.myocalib.internal.lazyui.pages.calibration.PageCalibration;
import com.tschebbischeff.myocalib.internal.lazyui.pages.connection.PageConnection;
import com.tschebbischeff.myocalib.internal.lazyui.pages.defaults.PageEmpty;
import com.tschebbischeff.myocalib.internal.lazyui.pages.welcome.PageWelcome;

/**
 * This enum is handled like a class with a set amount of predefined objects,
 * which contain all data that is used to identify a page layout in the main view.
 */
public enum PageList {

    /**
     * An empty layout, should never be shown in the finished app.
     * Exists solely for the purpose of a placeholder.
     */
    EMPTY(0, new PageEmpty()),
    /**
     * The first layout to show as the page. Contains a welcome message and some explanation on how to use the app.
     */
    WELCOME(1, new PageWelcome()),
    /**
     * The layout showing everything regarding the connection status to one or multiple Myos.
     */
    CONNECTION(2, new PageConnection()),
    /**
     * The layout showing the user through the calibration process
     */
    CALIBRATION(3, new PageCalibration());


    /**
     * The main activity
     */
    private static MyoCalibrationActivity myoCalibrationActivity;
    /**
     * The id of the currently shown page, to manage callbacks.
     */
    private static int selectedPage = EMPTY.ordinal();
    /**
     * The id of the menu item that inflates the registered layout when clicked.
     */
    private final int menuItemResourceId;
    /**
     * The class handling callbacks for the page.
     */
    private final Page pageHandler;

    /**
     * Private constructor, binding static data in the object.
     *
     * @param menuItemResourceId {@link PageList#menuItemResourceId}
     */
    PageList(int menuItemResourceId, Page pageHandler) {
        this.menuItemResourceId = menuItemResourceId;
        this.pageHandler = pageHandler;
    }

    /**
     * Selects a predefined layout page from the above list and shows it.
     * The selected layout page is the first layout page shown in the app upon startup.
     *
     * @param activity The activity containing the wrapper layout in which to inflate the new page
     */
    public static void initialize(MyoCalibrationActivity activity) {
        myoCalibrationActivity = activity;
        WELCOME.show(activity);
    }

    /**
     * @deprecated This library does not use a menu to switch to pages anymore.
     * Selects the correct layout page by the registered resource id of the menu item in
     * the navigation drawer.
     *
     * @param activity   The activity containing the wrapper layout in which to inflate the new page
     * @param resourceId The resource id of the menu item, registered with the layout page
     */
    public static void changeToId(Activity activity, int resourceId) {
        for (PageList page : PageList.values()) {
            if (page.menuItemResourceId == resourceId) {
                page.show(activity);
                break;
            }
        }
    }

    /**
     * Dispatches the onCreate event to all pages
     */
    public static void dispatchOnCreate(SharedPageData sharedPageData, Activity activity) {
        for (PageList layout : PageList.values()) {
            layout.pageHandler.dispatchOnCreate(sharedPageData, activity);
        }
    }

    /**
     * Dispatches the onAfterShow event to the currently selected page
     */
    public static void dispatchOnResume(Activity activity) {
        PageList.values()[selectedPage].pageHandler.dispatchOnBeforeShow(true);
        LinearLayout pageWrapper = activity.findViewById(R.id.myocalib_page_wrapper);
        if (pageWrapper != null) {
            activity.getLayoutInflater().inflate(PageList.values()[selectedPage].pageHandler.dispatchGetLayoutResourceId(), pageWrapper);
        } else Logger.warning("PageList", "Page wrapper not found, could not refresh UI onAfterShow!");
        //Resume even if there was an error in refreshing the UI (failsafe, will trigger only a visual bug)
        PageList.values()[selectedPage].pageHandler.dispatchOnAfterShow();
    }

    /**
     * Dispatches the onBeforeHide event to the currently selected page
     */
    public static void dispatchOnPause(Activity activity) {
        PageList.values()[selectedPage].pageHandler.dispatchOnBeforeHide(true);
        LinearLayout pageWrapper = activity.findViewById(R.id.myocalib_page_wrapper);
        if (pageWrapper != null) {
            pageWrapper.removeAllViews();
        } else Logger.warning("PageList", "Page wrapper not found, could not refresh UI onAfterShow!");
        PageList.values()[selectedPage].pageHandler.dispatchOnAfterHide();
    }

    /**
     * Dispatches the onDestroy event to all pages
     */
    public static void dispatchOnDestroy() {
        for (PageList layout : PageList.values()) {
            layout.pageHandler.dispatchOnDestroy();
        }
        selectedPage = EMPTY.ordinal();
    }

    /**
     * Dispatches back button presses to the currently shown page
     *
     * @return True if the app should be closed in response to the back button press, False otherwise.
     */
    public static boolean dispatchOnBackPressed() {
        return PageList.values()[selectedPage].pageHandler.onBackPressed();
    }

    /**
     * Dispatches clicks to the currently shown page
     *
     * @param view The clicked view
     */
    public static void dispatchOnClick(View view) {
        PageList.values()[selectedPage].pageHandler.onClick(view);
    }

    /**
     * Dispatches request results of permissions to the currently shown page
     *
     * @param requestCode  The request code supplied at the request of the permissions.
     * @param permissions  The permissions requested.
     * @param grantResults The results of the request per permission.
     */
    public static void dispatchOnRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        PageList.values()[selectedPage].pageHandler.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    /**
     * Dispatches received intents to the currently shown page
     *
     * @param context The context
     * @param intent  The received intent
     */
    public static void dispatchOnReceive(Context context, Intent intent) {
        PageList.values()[selectedPage].pageHandler.onReceive(context, intent);
    }

    /**
     * Shows this page layout.
     *
     * @param activity The activity containing the wrapper layout in which to inflate the new page
     */
    public void show(Activity activity) {
        LinearLayout pageWrapper = activity.findViewById(R.id.myocalib_page_wrapper);
        if (pageWrapper != null) {
            if (PageList.values()[selectedPage].pageHandler.dispatchOnBeforeHide(false)) {
                pageWrapper.removeAllViews();
                PageList.values()[selectedPage].pageHandler.dispatchOnAfterHide();
                if (this.pageHandler.dispatchOnBeforeShow(false)) {
                    activity.getLayoutInflater().inflate(this.pageHandler.dispatchGetLayoutResourceId(), pageWrapper);
                    selectedPage = this.ordinal();
                    if (this.pageHandler.dispatchOnAfterShow()) {
                        myoCalibrationActivity.refreshBroadcastReceiverFilter(this.pageHandler.dispatchGetIntentFilterActions());
                        Logger.info("PageList", "Page fully changed to " + this.toString() + ".");
                    } else {
                        Logger.info("PageList", "Page change to " + this.toString() + " flowed into page change to " + PageList.values()[selectedPage].toString() + ".");
                    }
                } else {
                    PageList.values()[selectedPage].pageHandler.dispatchOnBeforeShow(true);
                    activity.getLayoutInflater().inflate(PageList.values()[selectedPage].pageHandler.dispatchGetLayoutResourceId(), pageWrapper);
                    if (PageList.values()[selectedPage].pageHandler.dispatchOnAfterShow()) {
                        myoCalibrationActivity.refreshBroadcastReceiverFilter(PageList.values()[selectedPage].pageHandler.dispatchGetIntentFilterActions());
                    }
                }
            }
            /*NavigationView navigationView = activity.findViewById(R.id.myocalib_nav_view);
            if (navigationView != null) {
                navigationView.setCheckedItem(PageList.values()[selectedPage].menuItemResourceId);
            } else {
                Logger.error("PageList", "Navigation drawer view could not be found. Can not set selected item!");
            }*/
        } else Logger.error("PageList", "Page wrapper layout could not be found!");
    }
}
