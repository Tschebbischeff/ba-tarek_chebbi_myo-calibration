package com.tschebbischeff.myocalib.internal.lazyui.pages.calibration.actionlist;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.TextView;
import com.tschebbischeff.myocalib.R;
import com.tschebbischeff.myocalib.internal.lazyui.pages.calibration.PageCalibration;
import eu.darken.myolib.Myo;

/**
 * Realizes the {@link PageCalibrationActionListAdapter.ViewType#IMU_EXPLANATION} action.
 * <p>
 * Displays an explanation of the accelerometer bias calibration.
 */
public class PageCalibrationActionListViewHolderImuExplanation extends PageCalibrationActionListViewHolder {
    /**
     * The text view showing stub text.
     */
    public TextView text;

    /**
     * Constructor.
     *
     * @param source   The calibration page, for callbacks.
     * @param activity  The android context, for calling certain android methods.
     * @param rootView The view at the root of the cards layout.
     */
    public PageCalibrationActionListViewHolderImuExplanation(PageCalibration source, PageCalibrationActionListAdapter adapter, Activity activity, View rootView) {
        super(source, adapter, activity, PageCalibrationActionListAdapter.ViewType.RVC_DOWN_INIT, rootView);
        this.text = rootView.findViewById(R.id.myocalib_lazyui_pages_calibration_cardsimuexplanation_text);
        PageCalibrationActionListOnClickListener onClickListener = new PageCalibrationActionListOnClickListener(source, this);
        this.cardView.setOnClickListener(onClickListener);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void reset() {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onDestroy() {

    }

    /**
     * {@inheritDoc}
     * @param view
     */
    @Override
    public void onClick(View view) {
        this.getSource().stepCompleted();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onRawEmgData(Myo myo, long timestamp, double[] data) {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onRawImuData(Myo myo, long timestamp, double[] accelerometerData, double[] gyroscopeData, double[] orientationData) {
    }
}
