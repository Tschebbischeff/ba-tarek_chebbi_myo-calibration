package com.tschebbischeff.myocalib.internal.backend.connection;

import com.tschebbischeff.myocalib.api.IMyoStateListener;
import eu.darken.myolib.Myo;

public interface IMyoInternalStateListener extends IMyoStateListener {

    /**
     * The Myo has automatically been connected, due to it being a new Myo in the lists of Myos.
     * Preparing the Myo must be initiated.
     *
     * @param myo The Myo that has just been connected for preparing.
     */
    void onMyoConnectedForPreparations(Myo myo);

    /**
     * The Myo has been disconnected during the preparation stage.
     * A new preparation will be initiated if it comes back.
     *
     * @param myo The Myo that has been disconnected.
     */
    void onMyoPreparationsAborted(Myo myo);

    /**
     * Called when the Myo is completely done preparing. I.e. the Myo has its device name and battery status read, and has disconnected
     * after being connected to read the values.
     *
     * @param myo The Myo that is done preparing.
     */
    void onMyoPreparationsComplete(Myo myo);
}
