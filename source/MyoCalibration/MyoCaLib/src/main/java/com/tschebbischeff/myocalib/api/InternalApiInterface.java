package com.tschebbischeff.myocalib.api;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import com.tschebbischeff.common.android.logger.Logger;
import com.tschebbischeff.myocalib.internal.backend.MyoCalibrationActivity;
import com.tschebbischeff.myocalib.internal.backend.calibration.WritableCalibrationProfile;
import com.tschebbischeff.myocalib.internal.backend.connection.ConnectionHandler;
import eu.darken.myolib.Myo;

/**
 * This class is used to provide a broader API as well as a connection to the API service
 * to the internal classes of the library.
 * You can not and should not use it.
 */
public final class InternalApiInterface implements ServiceConnection {

    /**
     * The internal activity, which is the only creator of this class.
     */
    private MyoCalibrationActivity sourceActivity;
    /**
     * The API service, providing the API methods available to the application, as well as package-local access to its
     * elsewise read-only fields.
     */
    private MyoCaLib apiService;
    /**
     * Whether the API service is bound and ready to use.
     */
    private boolean serviceBound;

    /**
     * Securely creates a new instance of the interface. Can only be called from the internal activity.
     *
     * @param sourceActivity The internal activity.
     * @param securityToken  The security token authenticating the internal activity.
     */
    public InternalApiInterface(MyoCalibrationActivity sourceActivity, MyoCalibrationActivity.InternalSecurityToken securityToken) {
        this.sourceActivity = sourceActivity;
        this.serviceBound = false;
        Intent intent = new Intent(this.sourceActivity, MyoCaLib.class);
        if (!this.sourceActivity.bindService(intent, this, Context.BIND_AUTO_CREATE)) {
            //this.sourceActivity.unbindService(this); //Done in onDestroy, called through activity finish!
            Logger.error("InternalApiInterface", "Could not bind to service.");
            this.sourceActivity.finish();
        }
    }

    /**
     * Called, when the internal activity is destroyed.
     */
    public void onDestroy() {
        this.sourceActivity.unbindService(this);
        Logger.debug("InternalApiInterface", "Service unbound.");
    }

    /**
     * Whether the API service is currently bound.
     *
     * @return True if, and only if, the API service is bound at the moment.
     */
    public boolean isServiceReady() {
        return this.serviceBound;
    }

    /**
     * Provides access to application-visible API.
     *
     * @return The API service, that provides read-only access.
     */
    public MyoCaLib getApiService() {
        return this.apiService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        this.apiService = ((MyoCaLib.LocalBinder) iBinder).getService();
        this.serviceBound = true;
        Logger.debug("InternalApiInterface", "Service bound.");
        this.sourceActivity.serviceReady();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onServiceDisconnected(ComponentName componentName) {
        this.serviceBound = false;
        Logger.warning("InternalApiInterface", "Service prematurely unbound.");
    }

    /**
     * Gets the connection handler instanced by the API, used to automatically handle Myo connections.
     *
     * @return The connection handler bound to the services lifecycle.
     */
    public ConnectionHandler getConnectionHandler() {
        return this.apiService.connectionHandler;
    }

    /**
     * Gets the write-access instance of the calibration profile associated with a Myo.
     *
     * @param myo The Myo of which to get the profile.
     * @return The calibration profile of the specified Myo, or null if none exists.
     */
    public WritableCalibrationProfile getCalibrationProfile(Myo myo) {
        return this.apiService.connectionHandler.getCalibrationProfile(myo);
    }

    /**
     * Sets the write-access calibration profile of a Myo.
     *
     * @param myo The Myo for which to set a new calibration profile.
     */
    public void newCalibrationProfile(Myo myo) {
        this.apiService.connectionHandler.setCalibrationProfile(myo, new WritableCalibrationProfile(myo, this.apiService.connectionHandler, this.apiService.getCalibrationSettings()));
    }
}
