package com.tschebbischeff.common.android.layout;

import android.app.Activity;
import android.view.View;
import android.widget.ImageView;

/**
 * Animates between different ImageViews, by switching from one to the next after a certain time interval.
 */
public class ImageViewAnimator implements Runnable {
    /**
     * Activity to run view related stuff on UI thread.
     */
    private Activity activity;
    /**
     * The thread running the animation.
     */
    private Thread animationThread;
    /**
     * The lengths in milliseconds for the corresponding animation frame.
     */
    private long[] animationLengths;
    /**
     * References to the animation frames, all loaded and invisible.
     */
    private ImageView[] animationFrames = null;
    /**
     * Whether the animation is currently ready to start.
     */
    private boolean ready = true;
    /**
     * Whether the animation thread is currently running.
     */
    private volatile boolean running = false;
    /**
     * True while waiting on the UI to perform the view visibility switch.
     */
    private volatile boolean waitingForUi = false;

    /**
     * Constructor.
     *
     * @param activity The activity in which ALL image views must reside.
     */
    public ImageViewAnimator(Activity activity) {
        this.activity = activity;
    }

    /**
     * Set animation frame image views and their lengths. If this runnable is run without setting
     * this, it will instantly terminate. Calling this method while the animation is running will have no effect.
     */
    public void setData(final long[] animationLengths, final ImageView[] animationFrames) {
        if (!this.running) {
            if (animationLengths.length == 0 || animationFrames.length == 0 || animationLengths.length != animationFrames.length) {
                this.animationLengths = null;
                this.animationFrames = null;
                return;
            }
            this.animationLengths = animationLengths;
            this.animationFrames = animationFrames;
        }
    }

    /**
     * Waits until the UI is finished as noted by the waitingForUi variable.
     */
    private void waitForUi() {
        while (this.waitingForUi) {
            try {
                Thread.sleep(100L);
            } catch (InterruptedException ignored) {
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void run() {
        if (this.activity == null || this.animationLengths == null || this.animationFrames == null) {
            return;
        }
        int animationFrame = 0;
        long frameStart, remainingTime;
        //Reset animation
        this.waitingForUi = true;
        this.activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < animationFrames.length; i++) {
                    if (i == 0) {
                        animationFrames[i].setVisibility(View.VISIBLE);
                    } else {
                        animationFrames[i].setVisibility(View.INVISIBLE);
                    }
                }
                waitingForUi = false;
            }
        });
        this.waitForUi();
        //Animate
        while (this.running) {
            frameStart = System.currentTimeMillis();
            remainingTime = this.animationLengths[animationFrame];
            while (remainingTime > 0) {
                try {
                    Thread.sleep(remainingTime);
                } catch (InterruptedException ignored) {
                    if (!this.running) {
                        break;
                    }
                }
                remainingTime = this.animationLengths[animationFrame] - (System.currentTimeMillis() - frameStart);
            }
            if (this.running) {
                final ImageView thisAnimationFrame = this.animationFrames[animationFrame];
                final ImageView nextAnimationFrame = this.animationFrames[(animationFrame + 1) % this.animationLengths.length];
                this.waitingForUi = true;
                this.activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        nextAnimationFrame.setVisibility(View.VISIBLE);
                        thisAnimationFrame.setVisibility(View.INVISIBLE);
                        waitingForUi = false;
                    }
                });
                this.waitForUi();
            }
            animationFrame = (animationFrame + 1) % this.animationLengths.length;
        }
    }

    /**
     * Starts the animation, stops it before that.
     */
    public void start() {
        if (this.ready) {
            this.ready = false;
            this.stop();
            this.running = true;
            this.animationThread = new Thread(this);
            this.animationThread.start();
        }
    }

    /**
     * Stops the running of the animation
     */
    public void stop() {
        if (this.running) {
            this.running = false;
            this.animationThread.interrupt();
            this.waitForUi(); //In case the thread itself is currently waiting for the UI, we do to.
            //reset animation
            this.waitingForUi = true;
            this.activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    for (ImageView frame: animationFrames) {
                        frame.setVisibility(View.INVISIBLE);
                    }
                    if (animationFrames.length > 0) {
                        animationFrames[0].setVisibility(View.VISIBLE);
                    }
                    waitingForUi = false;
                }
            });
            this.waitForUi();
            this.ready = true;
        }
    }

    /**
     * Whether the animation is currently running or not.
     *
     * @return True if the animation is currently running.
     */
    public boolean isRunning() {
        return !this.ready;
    }
}
