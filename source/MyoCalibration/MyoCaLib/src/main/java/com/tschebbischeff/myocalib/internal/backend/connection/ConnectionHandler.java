package com.tschebbischeff.myocalib.internal.backend.connection;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import com.tschebbischeff.common.android.logger.Logger;
import com.tschebbischeff.myocalib.api.CalibrationSettings;
import com.tschebbischeff.myocalib.api.IMyoStateListener;
import com.tschebbischeff.myocalib.internal.backend.calibration.WritableCalibrationProfile;
import com.tschebbischeff.myocalib.internal.lazyui.pages.connection.PageConnection;
import eu.darken.myolib.BaseMyo;
import eu.darken.myolib.Myo;
import eu.darken.myolib.MyoCmds;
import eu.darken.myolib.MyoConnector;
import eu.darken.myolib.extension.ISleepModeListener;
import eu.darken.myolib.extension.SleepModeHelper;
import eu.darken.myolib.msgs.MyoMsg;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Handles Myo connection related events from myolib. Bound to services lifecycle instead of activities.
 */
public class ConnectionHandler implements BaseMyo.ConnectionListener, ISleepModeListener {
    /**
     * Searches for Myos in vicinity.
     */
    private MyoConnector myoConnector = null;
    /**
     * The settings of the calibration, as set by the application.
     */
    private final CalibrationSettings calibrationSettings;
    /**
     * A list of all connected myos, calibrated and not calibrated.
     */
    private ArrayList<Myo> connectedMyos;
    /**
     * A list of all myos, that the current instance is a connection listener of.
     */
    private ArrayList<Myo> discoveredMyos;
    /**
     * The stages of connection each Myo is currently in.
     *
     * @see ConnectionState
     */
    private HashMap<Myo, ConnectionState> myoConnectionStates = null;
    /**
     * The internal connection state listener. Forwards certain callbacks to the
     * connection page. Allows to listen for preparing events.
     */
    private IMyoInternalStateListener internalMyoStateListener = null;
    /**
     * Application implemented connection state listeners. Allow to listen for more clear, myo-specific
     * connection events, than the basic bluetooth connection listener.
     */
    private ArrayList<IMyoStateListener> myoStateListeners = null;
    /**
     * An association from any connected Myo's device ID to a calibration profile, containing settings and
     * statistics, as well as listeners for each of the Myos.
     */
    private HashMap<String, WritableCalibrationProfile> calibrationProfiles;

    /**
     * Constructor.
     */
    public ConnectionHandler(Context context, CalibrationSettings calibrationSettings) {
        this.myoConnector = new MyoConnector(context);
        this.calibrationSettings = calibrationSettings;
        this.connectedMyos = new ArrayList<>();
        this.discoveredMyos = new ArrayList<>();
        this.myoStateListeners = new ArrayList<>();
        this.calibrationProfiles = new HashMap<>();
        this.myoConnectionStates = new HashMap<>();
    }

    /**
     * Myolibs connection handler.
     *
     * @return The connector used by myolib to search for Myos in vicinity.
     */
    public MyoConnector getMyoConnector() {
        return this.myoConnector;
    }

    /**
     * The settings of the calibration as defined by the application.
     *
     * @return The application-defined calibration settings.
     */
    public CalibrationSettings getCalibrationSettings() {
        return calibrationSettings;
    }

    /**
     * Adds an application implementable connection state listener to the list of listeners.
     *
     * @param myoStateListener The connection listener to add to the list.
     */
    public void addMyoStateListener(IMyoStateListener myoStateListener) {
        if (!this.myoStateListeners.contains(myoStateListener)) {
            this.myoStateListeners.add(myoStateListener);
        }
    }

    /**
     * Removes an application implementable connection state listener from the list of listeners.
     *
     * @param myoStateListener The connection listener to remove from the list.
     */
    public void removeMyoStateListener(IMyoStateListener myoStateListener) {
        this.myoStateListeners.remove(myoStateListener);
    }

    /**
     * Gets the write-access instance of the calibration profile associated with a Myo.
     *
     * @param myo The Myo of which to get the profile.
     * @return The calibration profile of the specified Myo, or null if none exists.
     */
    public WritableCalibrationProfile getCalibrationProfile(Myo myo) {
        return this.calibrationProfiles.get(myo.getDeviceAddress());
    }

    /**
     * Sets the write-access calibration profile of a Myo.
     *
     * @param myo                        The Myo for which to set a new calibration profile.
     * @param writableCalibrationProfile The new profile to associate with the specified Myo.
     */
    public void setCalibrationProfile(Myo myo, WritableCalibrationProfile writableCalibrationProfile) {
        if (!this.calibrationProfiles.containsKey(myo.getDeviceAddress())) { //retains the first calibration profile created for a myo
            this.calibrationProfiles.put(myo.getDeviceAddress(), writableCalibrationProfile);
        }
    }

    /**
     * Called when a myo was discovered in the BT scan, which was not yet listed, it may be an unkown one.
     * If it is unknown, the parameter is the result, if it was known before, the saved reference is the result.
     *
     * @param myo The myo discovered in the BT scan.
     */
    public void onMyoDiscovered(Myo myo) {
        if (!this.discoveredMyos.contains(myo)) {
            this.discoveredMyos.add(myo);
            myo.addConnectionListener(this);
        } else {
            myo.removeConnectionListener(this);
            myo.addConnectionListener(this);
        }
    }

    /**
     * Sets the connection page, for callbacks.
     *
     * @param internalMyoStateListener The currently shown connection page instance.
     */
    public void setInternalMyoStateListener(PageConnection internalMyoStateListener) {
        this.internalMyoStateListener = internalMyoStateListener;
    }

    /**
     * Disconnects all currently connected Myos and resets their connection stages
     */
    public void onDestroy() {
        for (Myo myo : this.discoveredMyos) {
            if (this.connectedMyos.contains(myo)) {
                this.setMyoConnectionState(myo, ConnectionState.DESTROYING);
                myo.disconnect();
            } else {
                this.discoveredMyos.remove(myo);
                this.removeMyoConnectionState(myo);
            }
        }
        this.connectedMyos.clear();
    }

    /**
     * Get all currently connected Myos, calibrated and not calibrated.
     *
     * @return All currently connected Myos. Created by copy constructor.
     */
    public ArrayList<Myo> getConnectedMyosCopy() {
        return new ArrayList<>(this.connectedMyos);
    }

    /**
     * Gets the number of connected Myos, without creating a copy of the list.
     *
     * @return The number of currently connected Myos.
     */
    public int getConnectedMyosSize() {
        return this.connectedMyos.size();
    }

    /**
     * Searches through connected myos, for a myo with the specified device address.
     *
     * @param deviceId The device address of the connected myo.
     * @return The object representing the connected myo with the specified device address, or null if no myo
     * with the specified address could be found.
     */
    public Myo getConnectedMyoByDeviceId(String deviceId) {
        for (Myo myo : this.connectedMyos) {
            if (myo.getDeviceAddress().equals(deviceId)) {
                return myo;
            }
        }
        return null;
    }

    /**
     * Searches through all Myos that this instance is a connection listener of for one that has the specified device
     * id.
     *
     * @param deviceId The device address of the Myo to search for.
     * @return The searched for Myo if this instance is a connection listener of it, or null else.
     */
    private Myo getDiscoveredMyoByDeviceId(String deviceId) {
        for (Myo myo : this.discoveredMyos) {
            if (myo.getDeviceAddress().equals(deviceId)) {
                return myo;
            }
        }
        return null;
    }

    /**
     * Sets the connection stage of a Myo to a specified value, regardless of whether it had a stage before.
     *
     * @param myo             The Myo to set the connection stage for.
     * @param connectionState The new connection stage for the specified Myo.
     */
    public void setMyoConnectionState(Myo myo, ConnectionState connectionState) {
        this.myoConnectionStates.put(myo, connectionState);
    }

    /**
     * Gets the stage in which a Myo currently is connection-wise.
     *
     * @param myo The Myo of which to check the connection stage.
     * @return The connection stage in which the specified Myo currently is.
     */
    public ConnectionState getMyoConnectionState(Myo myo) {
        return this.myoConnectionStates.containsKey(myo) ? this.myoConnectionStates.get(myo) : ConnectionState.UNKNOWN;
    }

    /**
     * Removes the connection stage entry for the specified Myo from the list.
     *
     * @param myo The Myo of which to reset the connection stage.
     */
    public void removeMyoConnectionState(Myo myo) {
        this.myoConnectionStates.remove(myo);
    }

    /**
     * Gets the entry set of the mapping from Myo's to their respective connection stages.
     * Used for iteration.
     *
     * @return The entry set of the connection stages HashMap.
     */
    public Set<Map.Entry<Myo, ConnectionState>> getConnectionStateEntrySet() {
        return this.myoConnectionStates.entrySet();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onSleepModeWritten(Myo myo, MyoCmds.SleepMode requestedSleepMode, MyoMsg msg) {
        if (msg.getState() == MyoMsg.State.NEW) {
            Logger.error("ConnectionHandler", "onSleepModeWritten was called with a new MyoMsg!");
            return;
        }
        if (myo.getSleepMode() != requestedSleepMode && msg.getState() != MyoMsg.State.SUCCESS) {
            SleepModeHelper.writeSleepMode(myo, requestedSleepMode, this);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onConnectionStateChanged(BaseMyo baseMyo, BaseMyo.ConnectionState state) {
        final Myo myo = this.getDiscoveredMyoByDeviceId(baseMyo.getDeviceAddress());
        if (myo == null) {
            Logger.error("ConnectionHandler", "Unknown myo changed connection state. This class was registered as a connection listener, without using the supplied onMyoDiscovered method.");
            return;
        }
        Logger.info("ConnectionHandler", "Myo with device address '" + baseMyo.getDeviceAddress() + "' changed state to: " + state.toString());
        if (myo.getDeviceName() != null && myo.getBatteryLevel() > -1) { //Myo is fully prepared
            if (this.getMyoConnectionState(myo) == ConnectionState.PREPARING) { //Safety check if myo was really set to disconnecting
                Logger.warning("ConnectionHandler", "Myo was not properly set to disconnecting after prepare, but connection state changed.");
                this.setMyoConnectionState(myo, ConnectionState.DISCONNECTING_AFTER_PREPARE);
            }
            switch (state) {
                case CONNECTED:
                    if (this.getMyoConnectionState(myo).oneOf(ConnectionState.CONNECTING, ConnectionState.DISCONNECTED_SHOULD_CONNECT)) {
                        if (!this.connectedMyos.contains(myo)) {
                            this.connectedMyos.add(myo);
                        }
                        //Myo connected for usage (maybe after disconnect), keep alive and refresh EMG and IMU streaming modes.
                        SleepModeHelper.writeSleepMode(myo, MyoCmds.SleepMode.NEVER, this);
                        this.getCalibrationProfile(myo).refreshReceivingMode();
                    }
                    if (this.getMyoConnectionState(myo) == ConnectionState.CONNECTING) { //Connected per button click
                        this.setMyoConnectionState(myo, ConnectionState.CONNECTED);
                        if (this.internalMyoStateListener != null) {
                            this.internalMyoStateListener.onMyoConnectedForUsage(myo);
                        }
                        for (IMyoStateListener connectionStateListener : this.myoStateListeners) {
                            connectionStateListener.onMyoConnectedForUsage(myo);
                        }
                    } else if (this.getMyoConnectionState(myo) == ConnectionState.DISCONNECTED_SHOULD_CONNECT) { //Connected after abnormal disconnect
                        this.setMyoConnectionState(myo, ConnectionState.CONNECTED);
                        if (this.internalMyoStateListener != null) {
                            this.internalMyoStateListener.onMyoConnectedAfterDisconnect(myo);
                        }
                        for (IMyoStateListener connectionStateListener : this.myoStateListeners) {
                            connectionStateListener.onMyoConnectedAfterDisconnect(myo);
                        }
                    } else { //Connection unrequested, disconnect
                        Logger.info("ConnectionHandler", "Myo connected unrequested! May be due to bluetooth commands. Disconnecting in 100ms. Connection state was: " + this.getMyoConnectionState(myo).name());
                        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                myo.disconnect();
                            }
                        }, 100);
                    }
                    break;
                case DISCONNECTED:
                    if (this.getMyoConnectionState(myo).oneOf(ConnectionState.CONNECTING, ConnectionState.CONNECTED)) { //Disconnected abnormally, trying to reconnect
                        this.setMyoConnectionState(myo, ConnectionState.DISCONNECTED_SHOULD_CONNECT);
                        myo.connect();
                        if (this.internalMyoStateListener != null) {
                            this.internalMyoStateListener.onMyoDisconnectedAbnormally(myo);
                        }
                        for (IMyoStateListener connectionStateListener : this.myoStateListeners) {
                            connectionStateListener.onMyoDisconnectedAbnormally(myo);
                        }
                    } else { //Disconnected per Button-click or after preparations are done or when shutting down
                        if (this.getMyoConnectionState(myo) == ConnectionState.DESTROYING) { //Disconnect on shutdown
                            myo.removeConnectionListener(this);
                            this.discoveredMyos.remove(myo);
                            this.removeMyoConnectionState(myo);
                            if (this.internalMyoStateListener != null) {
                                this.internalMyoStateListener.onMyoDisconnectedAtShutdown(myo);
                            }
                            for (IMyoStateListener connectionStateListener : this.myoStateListeners) {
                                connectionStateListener.onMyoDisconnectedAtShutdown(myo);
                            }
                        } else if (this.getMyoConnectionState(myo) == ConnectionState.DISCONNECTING) { //Disconnect through user
                            this.setMyoConnectionState(myo, ConnectionState.WAITING_FOR_SCAN);
                            this.connectedMyos.remove(myo);
                            if (this.internalMyoStateListener != null) {
                                this.internalMyoStateListener.onMyoDisconnected(myo);
                            }
                            for (IMyoStateListener connectionStateListener : this.myoStateListeners) {
                                connectionStateListener.onMyoDisconnected(myo);
                            }
                        } else if (this.getMyoConnectionState(myo) == ConnectionState.DISCONNECTING_AFTER_PREPARE) { //Disconnecting after preparations are done
                            this.setMyoConnectionState(myo, ConnectionState.WAITING_FOR_SCAN);
                            if (this.internalMyoStateListener != null) {
                                this.internalMyoStateListener.onMyoPreparationsComplete(myo);
                            }
                        }
                    }
                    break;
            }
        } else { //"YOU are not prepared"
            switch (state) {
                case CONNECTED: //Connected to a device in preparation, delegate
                    if (this.getMyoConnectionState(myo) != ConnectionState.PREPARING) { //Check this here, so that devices automatically reconnecting from myolib get readded to the preparation list
                        this.setMyoConnectionState(myo, ConnectionState.PREPARING);
                    }
                    if (this.internalMyoStateListener != null) {
                        this.internalMyoStateListener.onMyoConnectedForPreparations(myo);
                    }
                    break;
                case DISCONNECTED: //Disconnected before preparations were complete, remove from stage list alltogether, such that it restarts when found again
                    this.removeMyoConnectionState(myo);
                    if (this.internalMyoStateListener != null) {
                        this.internalMyoStateListener.onMyoPreparationsAborted(myo);
                    }
                    break;
            }
            Logger.info("ConnectionHandler", "Preparation of '" + baseMyo.getDeviceAddress() + "' is incomplete");
        }
    }

    /**
     * Delivers the Myo is done calibrating message to recipients.
     * This callback is not called when an already calibrated Myo has been reconnected.
     *
     * @param myo The Myo that has successfully been calibrated.
     */
    public void onMyoCalibrated(Myo myo) {
        if (this.internalMyoStateListener != null) {
            this.internalMyoStateListener.onMyoCalibrated(myo);
        }
        for (IMyoStateListener connectionStateListener : this.myoStateListeners) {
            connectionStateListener.onMyoCalibrated(myo);
        }
    }

    /**
     * Delivers the Myo needs recalibration message to recipients.
     * This callback is not called when a Myo is first connected.
     *
     * @param myo The Myo which's calibration is not valid anymore.
     */
    public void onMyoUncalibrated(Myo myo) {
        if (this.internalMyoStateListener != null) {
            this.internalMyoStateListener.onMyoUncalibrated(myo);
        }
        for (IMyoStateListener connectionStateListener : this.myoStateListeners) {
            connectionStateListener.onMyoUncalibrated(myo);
        }
    }
}
