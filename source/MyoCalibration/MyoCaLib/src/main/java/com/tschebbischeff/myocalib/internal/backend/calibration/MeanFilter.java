package com.tschebbischeff.myocalib.internal.backend.calibration;

import java.util.ArrayList;

public class MeanFilter extends Filter1D<Double> {

    /**
     * Saves the values.
     */
    private ArrayList<Double> values;

    /**
     * Constructor.
     */
    public MeanFilter() {
        this.values = new ArrayList<>();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void applyWithoutResult(Double value) {
        this.values.add(value);
    }

    @Override
    public Double getResult() {
        double sum = 0;
        for (Double v : this.values) {
            sum += v;
        }
        return sum / this.values.size();
    }

    /**
     * {@inheritDoc}
     * <p>
     * <strong>Only one value is returned, that is the mean of all values every supplied plus those supplied as parameter.</strong>
     */
    @Override
    protected Double[] apply(Double[] values) {
        double sum = 0;
        for (Double v : this.values) {
            sum += v;
        }
        for (Double v : values) {
            sum += v;
            this.values.add(v);
        }
        return new Double[]{sum / this.values.size()};
    }

    @Override
    public void reset() {
        this.values.clear();
    }
}
