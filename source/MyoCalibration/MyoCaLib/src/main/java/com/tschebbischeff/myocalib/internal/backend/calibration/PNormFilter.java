package com.tschebbischeff.myocalib.internal.backend.calibration;

import java.util.ArrayList;

public class PNormFilter extends Filter1D<Double> {

    /**
     * The values stored by this filter.
     */
    ArrayList<Double> values;
    /**
     * The p-value of the p-norm represented by this filter.
     */
    double p;

    /**
     * Constructor.
     *
     * @param p The p-value of the p-norm represented by this filter.
     */
    public PNormFilter(double p) {
        this.values = new ArrayList<>();
        this.p = p;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void applyWithoutResult(Double value) {
        this.values.add(value);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Double getResult() {
        double result = 0;
        for (Double v : this.values) {
            result += Math.pow(Math.abs(v), this.p);
        }
        return Math.pow(result, (1d / this.p));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Double[] apply(Double[] values) {
        double result = 0;
        for (Double v : this.values) {
            result += Math.pow(Math.abs(v), this.p);
        }
        for (Double v : values) {
            result += Math.pow(Math.abs(v), this.p);
            this.values.add(v);
        }
        return new Double[]{Math.pow(result, (1d / this.p))};
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void reset() {
        this.values.clear();
    }
}
