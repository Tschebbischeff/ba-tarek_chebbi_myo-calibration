package com.tschebbischeff.myocalib.internal.backend.calibration;

import com.tschebbischeff.common.android.logger.Logger;
import com.tschebbischeff.myocalib.internal.backend.calibration.interpolation.InterpolationFunction;
import com.tschebbischeff.myocalib.internal.backend.tools.StaticLib;

import java.util.Arrays;
import java.util.Map;
import java.util.TreeMap;

/**
 * Records data in memory.
 * This data is not saved to disk or anything, it is just temporarily recorded.
 * Do not use this over longer periods of time.
 * This is mainly used for recording calibration data.
 */
public class DataCollector {
    /**
     * The values saved by this collector. Ordered by timestamp.
     */
    private TreeMap<Long, Double[]> recordedValues;

    /**
     * Constructor.
     */
    public DataCollector() {
        this.recordedValues = new TreeMap<>();
    }

    /**
     * Store a pair of timestamp and value in the recorder.
     *
     * @param timestamp The timestamp of the data.
     * @param values    The data to store.
     * @return This DataCollector for fluent method calls.
     */
    public DataCollector store(long timestamp, double[] values) {
        this.recordedValues.put(timestamp, StaticLib.boxDoubleArray(values));
        return this;
    }

    /**
     * Resets the collector, empties all recorded values.
     */
    public void reset() {
        this.recordedValues.clear();
    }

    /**
     * Applies a one-dimensional filter to all values and retains their association to the timestamp.
     * Application of filters to a batch of values can take time.
     * The filter is reset for each value-array.
     *
     * @param filter The one-dimensional filter to apply.
     * @return This collector, for fluent method calls.
     */
    public DataCollector applyFilterPerTimestamp(Filter1D<Double> filter) {
        Map.Entry<Long, Double[]> current = this.recordedValues.firstEntry();
        while (current != null) {
            filter.reset();
            this.recordedValues.put(current.getKey(), filter.batchApply(current.getValue()));
            current = this.recordedValues.higherEntry(current.getKey());
        }
        return this;
    }

    /**
     * Applies a two-dimensional filter to all values. The filter is batch applied to each element of the map, i.e.
     * to the whole array of values. Use this to e.g. combine data from the different indices of the value array.
     * The timestamp is copied to an array of value-array's length for batch application and the resulting timestamps
     * from the filter application are ignored. (I.e. key-modifying filters have part of their result ignored)
     * Application of filters to a batch of values can take time.
     * The filter is reset before application.
     *
     * @param filter The filter to apply.
     * @return This collector, for fluent method calls.
     */
    public DataCollector applyFilterPerTimestamp(Filter2D<Long, Double> filter) {
        filter.reset();
        TreeMap<Long, Double[]> newValues = new TreeMap<>();
        TreeMap<Long, Double> temp;
        Map.Entry<Long, Double[]> current = this.recordedValues.firstEntry();
        while (current != null) {
            Long[] indexCopies = new Long[current.getValue().length];
            Arrays.fill(indexCopies, current.getKey());
            this.recordedValues.put(current.getKey(), filter.batchApply(indexCopies, current.getValue()).values().toArray(new Double[]{}));
            current = this.recordedValues.higherEntry(current.getKey());
        }
        return this;
    }

    /**
     * Applies a one-dimensional filter to all values. The filter is applied on a per-index basis of the data-array.
     * Application of filters to a batch of values can take time.
     * The filter is reset for each index. Values that do not have the specified index are ignored.
     * If the specified indices only contain null, or is an empty array, the filter is applied to ALL indices
     * on a per-index basis. It is however more performant to supply the indices, as the values don't have to be
     * searched for the highest possible index.
     *
     * @param filter  The filter to apply.
     * @param indices The indices to apply the filter to.
     * @return This collector, for fluent method calls.
     */
    public DataCollector applyFilterPerIndex(Filter1D<Double> filter, Integer... indices) {
        //Check validity of index-array
        boolean invalid = true;
        for (Integer i : indices) {
            if (i != null) {
                invalid = false;
                break;
            }
        }
        if (invalid) {
            int maxLength = 0;
            Map.Entry<Long, Double[]> current = this.recordedValues.firstEntry();
            while (current != null) {
                if (current.getValue().length > maxLength) {
                    maxLength = current.getValue().length;
                }
                current = this.recordedValues.higherEntry(current.getKey());
            }
            indices = new Integer[maxLength];
            for (int i = 0; i < maxLength; i++) {
                indices[i] = i;
            }
        }
        //Apply filter on per-index basis
        for (Integer i : indices) {
            filter.reset();
            Map.Entry<Long, Double[]> current = this.recordedValues.firstEntry();
            while (current != null) {
                Double[] values = current.getValue();
                if (values != null && values.length > i) {
                    values[i] = filter.apply(current.getValue()[i]);
                    this.recordedValues.put(current.getKey(), values);
                }
                current = this.recordedValues.higherEntry(current.getKey());
            }
        }
        return this;
    }

    /**
     * Applies a two-dimensional filter to all values. The filter is applied on a per-index basis of the data-array.
     * Application of filters to a batch of values can take time.
     * The filter is reset for each index. Values that do not have the specified index are ignored.
     * If the specified indices only contain null, or is an empty array, the filter is applied to ALL indices
     * on a per-index basis. It is however more performant to supply the indices, as the values don't have to be
     * searched for the highest possible index.
     *
     * @param filter  The filter to apply.
     * @param indices The indices to apply the filter to.
     * @return This collector, for fluent method calls.
     */
    public DataCollector applyFilterPerIndex(Filter2D<Long, Double> filter, Integer... indices) {
        //Check validity of index-array
        boolean invalid = true;
        for (Integer i : indices) {
            if (i != null) {
                invalid = false;
                break;
            }
        }
        if (invalid) {
            int maxLength = 0;
            Map.Entry<Long, Double[]> current = this.recordedValues.firstEntry();
            while (current != null) {
                if (current.getValue().length > maxLength) {
                    maxLength = current.getValue().length;
                }
                current = this.recordedValues.higherEntry(current.getKey());
            }
            indices = new Integer[maxLength];
            for (int i = 0; i < maxLength; i++) {
                indices[i] = i;
            }
        }
        //Apply filter on per-index basis
        for (Integer i : indices) {
            filter.reset();
            Map.Entry<Long, Double[]> current = this.recordedValues.firstEntry();
            while (current != null) {
                Double[] values = current.getValue();
                if (values != null && values.length > i) {
                    values[i] = filter.apply(current.getKey(), current.getValue()[i]).second;
                    this.recordedValues.put(current.getKey(), values);
                }
                current = this.recordedValues.higherEntry(current.getKey());
            }
        }
        return this;
    }

    /**
     * Merges the data of this collector with the data of another one. It is possible that the two data collectors data
     * have different value array sizes, as it is also possible that one data collector has different.
     * Keys that are present in both datasets have their values replaced by the new datasets value.
     *
     * @param dataCollector The data collector to merge with.
     * @return This collector, for fluent method calls.
     */
    public DataCollector merge(DataCollector dataCollector) {
        this.recordedValues.putAll(dataCollector.recordedValues);
        return this;
    }

    /**
     * Returns the recorded values in their current state.
     *
     * @return A reference to the recorded values.
     */
    public TreeMap<Long, Double[]> getRecordedValues() {
        return recordedValues;
    }

    /**
     * @deprecated This method is for debugging purposes only, it prints the collected data to the android log.
     */
    public DataCollector debug() {
        Logger.error("DataCollector", "BEGINNING DATA OUTPUT");
        Map.Entry<Long, Double[]> current = this.recordedValues.firstEntry();
        int i = 0;
        while (current != null) {
            StringBuilder value = new StringBuilder("[");
            for (Double d : current.getValue()) {
                value.append(value.length() > 1 ? "," + d.toString() : d.toString());
            }
            value.append("]");
            Logger.error("DataCollector", "(" + i + ")" + current.getKey() + "->" + value.toString());
            current = this.recordedValues.higherEntry(current.getKey());
            i++;
        }
        Logger.error("DataCollector", "END DATA OUTPUT");
        return this;
    }

    /**
     * Returns the last saved value array.
     *
     * @return The last value array that is saved in the map, i.e. the value array with the highest timestamp, or null,
     * if no values are saved in the map at all.
     */
    public double[] getLastValue() {
        if (this.recordedValues.size() > 0) {
            return StaticLib.unboxDoubleArray(this.recordedValues.lastEntry().getValue());
        } else {
            return null;
        }
    }

    /**
     * Returns the entry with the specified index from the last saved value array.
     *
     * @param index The index of the entry from the last value array to get.
     * @return The entry of the last value array with the given index, or 0d, if the last value array has no such index.
     */
    public double getLastValue(int index) {
        if (this.recordedValues.lastEntry().getValue().length > index) {
            return this.recordedValues.lastEntry().getValue()[index];
        } else {
            return 0d;
        }
    }

    /**
     * Returns a new DataCollector that contains the same mappings as this one.
     *
     * @return A DataCollector, which is a copy of this DataCollector.
     */
    public DataCollector copy() {
        return (new DataCollector()).merge(this);
    }

    /**
     * Permutates each dataset recorded by this collector.
     *
     * @param permutation The permutation to apply.
     * @return This DataCollector for fluent method calls.
     */
    public DataCollector applyPermutation(int[] permutation) {
        Map.Entry<Long, Double[]> current = this.recordedValues.firstEntry();
        while (current != null) {
            this.recordedValues.put(current.getKey(), StaticLib.boxDoubleArray(StaticLib.arrayPermutation(StaticLib.unboxDoubleArray(current.getValue()), permutation)));
            current = this.recordedValues.higherEntry(current.getKey());
        }
        return this;
    }
}
