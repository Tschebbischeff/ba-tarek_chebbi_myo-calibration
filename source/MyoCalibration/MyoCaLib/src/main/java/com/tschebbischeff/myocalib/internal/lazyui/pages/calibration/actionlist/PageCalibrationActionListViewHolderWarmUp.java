package com.tschebbischeff.myocalib.internal.lazyui.pages.calibration.actionlist;

import android.app.Activity;
import android.view.TextureView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.tschebbischeff.common.android.layout.SimpleVideo;
import com.tschebbischeff.myocalib.R;
import com.tschebbischeff.myocalib.internal.lazyui.pages.calibration.PageCalibration;
import eu.darken.myolib.Myo;

/**
 * Realizes the {@link PageCalibrationActionListAdapter.ViewType#WARM_UP} action.
 * <p>
 * Displays a detailed explanation on how the calibration process will work.
 */
public class PageCalibrationActionListViewHolderWarmUp extends PageCalibrationActionListViewHolder {
    /**
     * The maximum time to wait for the Myo to warm up.
     */
    private static final int MAX_WARM_UP = 1000 * 60 * 5;
    /**
     * The text view showing the text above the video.
     */
    public TextView text;
    /**
     * The video view playing the explanation video
     */
    public TextureView explanationVideoSurfaceView;
    /**
     * The text view showing the text below the progress bar.
     */
    public TextView text2;
    /**
     * The video wrapper playing the video.
     */
    private SimpleVideo explanationVideo;
    /**
     * Whether the explanation video has been started already.
     */
    private boolean explanationVideoStarted;
    /**
     * The progress bar, which is showing the user how long to hold the extension.
     */
    public ProgressBar progressBar;
    /**
     * The time at which this card was first detected as the top card.
     */
    private long startTimestamp;

    /**
     * Constructor.
     *
     * @param source   The calibration page, for callbacks.
     * @param activity  The android context, for calling certain android methods.
     * @param rootView The view at the root of the cards layout.
     */
    public PageCalibrationActionListViewHolderWarmUp(PageCalibration source, PageCalibrationActionListAdapter adapter, Activity activity, View rootView) {
        super(source, adapter, activity, PageCalibrationActionListAdapter.ViewType.WARM_UP, rootView);
        this.text = rootView.findViewById(R.id.myocalib_lazyui_pages_calibration_cardswarmup_text);
        this.explanationVideoSurfaceView = rootView.findViewById(R.id.myocalib_lazyui_pages_calibration_cardswarmup_explanationvideo);
        this.explanationVideo = new SimpleVideo(this.explanationVideoSurfaceView, activity, R.raw.explanation_video_reverse);
        this.text2 = rootView.findViewById(R.id.myocalib_lazyui_pages_calibration_cardswarmup_text2);
        this.progressBar = rootView.findViewById(R.id.myocalib_lazyui_pages_calibration_cardswarmup_progress);
        this.progressBar.setMax(MAX_WARM_UP);
        PageCalibrationActionListOnClickListener onClickListener = new PageCalibrationActionListOnClickListener(this.getSource(), this);
        this.cardView.setOnClickListener(onClickListener);
        this.startTimestamp = -1;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void reset() {
        this.startTimestamp = -1;
        this.progressBar.setProgress(0);
        if (this.explanationVideo != null) {
            this.explanationVideo.init();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onDestroy() {
        this.explanationVideo.finish();
        this.explanationVideo = null;
    }

    /**
     * {@inheritDoc}
     *
     * @param view
     */
    @Override
    public void onClick(View view) {
        this.explanationVideo.finish();
        this.getSource().stepCompleted();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onRawEmgData(Myo myo, long timestamp, double[] data) {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onRawImuData(Myo myo, long timestamp, double[] accelerometerData, double[] gyroscopeData, double[] orientationData) {
        //Use as update method with 50Hz
        if (this.startTimestamp == -1) {
            this.startTimestamp = System.currentTimeMillis();
        }
        if (this.explanationVideo.isReady()) {
            if (!this.explanationVideoStarted) {
                this.explanationVideo.getMediaPlayer().setVolume(0f, 0f);
                this.explanationVideo.getMediaPlayer().setLooping(true);
                this.explanationVideo.getMediaPlayer().start();
                this.explanationVideoStarted = true;
            }
        }
        if (this.startTimestamp > 0) {
            long elapsedTime = System.currentTimeMillis() - this.startTimestamp;
            if (elapsedTime >= MAX_WARM_UP) {
                this.startTimestamp = -2;
            } else {
                this.progressBar.setProgress((int) elapsedTime);
            }
        }
    }
}
