package com.tschebbischeff.myocalib.internal.backend.calibration;

import eu.darken.myolib.processor.emg.EmgData;
import eu.darken.myolib.processor.emg.EmgProcessor;
import eu.darken.myolib.processor.imu.ImuData;
import eu.darken.myolib.processor.imu.ImuProcessor;

public class DataReceiver implements EmgProcessor.EmgDataListener, ImuProcessor.ImuDataListener {

    /**
     * The calibration profile, that created this data receiver, for callbacks.
     */
    private WritableCalibrationProfile mainDataListener;

    /**
     * Constructor.
     *
     * @param mainDataListener The calibration profile creating this receiver, for callbacks.
     */
    public DataReceiver(WritableCalibrationProfile mainDataListener) {
        this.mainDataListener = mainDataListener;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onNewEmgData(EmgData emgData) {
        //Convert from byte to double, more useful for calibration
        byte[] originalData = emgData.getData();
        double[] data = new double[originalData.length];
        for (int i = 0; i < originalData.length; i++) {
            data[i] = (double) originalData[i];
        }
        synchronized (this) {
            this.mainDataListener.onEmgData(emgData.getTimestamp(), data);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onNewImuData(ImuData imuData) {
        synchronized (this) {
            this.mainDataListener.onImuData(imuData.getTimeStamp(), imuData.getAccelerometerData(), imuData.getGyroData(), imuData.getOrientationData());
        }
    }
}
