package com.tschebbischeff.myocalib.internal.backend.calibration;

/**
 * Applies full wave rectification to arrays of data.
 */
public class FullWaveRectificationFilter extends Filter1D<Double> {

    /**
     * The last supplied value
     */
    private Double lastValue = 0d;

    /**
     * {@inheritDoc}
     */
    @Override
    public void applyWithoutResult(Double value) {
        this.lastValue = value;
    }

    @Override
    public Double getResult() {
        return Math.abs(this.lastValue);
    }

    /**
     * {@inheritDoc}
     */
    public Double[] apply(Double[] values) {
        for (int i = 0; i < values.length; i++) {
            values[i] = Math.abs(values[i]);
        }
        return values;
    }

    @Override
    public void reset() {
        //Nothing to reset.
    }
}
