package com.tschebbischeff.myocalib.internal.backend.calibration;

import android.support.v4.util.Pair;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

/**
 * Implements a moving average filter with a certain window size (given as time).
 * The implementation is meant to be used at runtime.
 */
public class MovingAverageFilter extends Filter2D<Long, Double> {

    /**
     * The window size in milliseconds, over which to calculate the average value.
     */
    private final long windowSize;
    /**
     * The array containing all values, which are currently of interest for calculation of the moving average.
     */
    private HashMap<Long, Double> values;
    /**
     * The latest timestamp in the dataset.
     */
    private long highestTimestamp;
    /**
     * The dataset may contain values, that should be removed, this is not set to true in the case a value is supplied,
     * that contains a lower timestamp than the highest.
     */
    private boolean needsFiltering;

    /**
     * Constructor.
     * Note that the window size directly influences the delay introduced by the filter.
     * If you supply a window size of for example 100 ms. Each result will be the result of 50ms before the latest
     * supplied value.
     *
     * @param windowSize The window size of the moving average filter.
     */
    public MovingAverageFilter(long windowSize) {
        this.values = new HashMap<>();
        this.windowSize = windowSize;
        this.highestTimestamp = 0;
        this.needsFiltering = false;
    }

    /**
     * Notifies the moving average calculator that a new value has arrived, which needs to be considered in the result.
     *
     * @param timestamp The timestamp of the new data value.
     * @param value     The actual value.
     */
    private void nextValue(long timestamp, double value) {
        while (this.values.containsKey(timestamp)) { //ability to store the same timestamp twice?
            timestamp++;
        }
        if (timestamp > this.highestTimestamp) {
            this.highestTimestamp = timestamp;
            this.needsFiltering = true;
        }
        this.values.put(timestamp, value);
    }

    /**
     * Calculates and returns the current result of the moving average filter.
     *
     * @return The average of all values supplied with a timestamp that is no less than the highest supplied timestamp
     * minus the window size.
     */
    private double getResult() {
        if (this.needsFiltering) {
            long minTimestamp = this.highestTimestamp - this.windowSize;
            Iterator<Map.Entry<Long, Double>> itF = this.values.entrySet().iterator();
            while (itF.hasNext()) {
                Map.Entry<Long, Double> pair = itF.next();
                if (pair.getKey() < minTimestamp) {
                    itF.remove();
                }
            }
        }
        Iterator<Double> itD = this.values.values().iterator();
        double average = 0;
        double iteration = 1;
        while (itD.hasNext()) { //Calculation of average tries to keep values low
            Double value = itD.next();
            average = average * (1 - 1 / iteration) + value * (1 / iteration);
            iteration++;
        }
        return average;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Pair<Long, Double> apply(Long index, Double value) {
        this.nextValue(index, value);
        return new Pair<>((long) Math.round(this.highestTimestamp - (this.windowSize / 2)), this.getResult());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected TreeMap<Long, Double> apply(Long[] indices, Double[] values) {
        TreeMap<Long, Double> result = new TreeMap<>();
        Pair<Long, Double> temp;
        for (int i = 0; i < indices.length; i++) {
            temp = this.apply(indices[i], values[i]);
            result.put(temp.first, temp.second);
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void reset() {
        this.values.clear();
        this.highestTimestamp = 0;
        this.needsFiltering = false;
    }
}
