package com.tschebbischeff.myocalib.internal.lazyui.pages.connection;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import com.tschebbischeff.common.android.layout.managers.WrapContentLinearLayoutManager;
import com.tschebbischeff.common.android.logger.Logger;
import com.tschebbischeff.myocalib.R;
import com.tschebbischeff.myocalib.internal.backend.calibration.WritableCalibrationProfile;
import com.tschebbischeff.myocalib.internal.backend.connection.ConnectionState;
import com.tschebbischeff.myocalib.internal.backend.connection.IMyoInternalStateListener;
import com.tschebbischeff.myocalib.internal.lazyui.PageList;
import com.tschebbischeff.myocalib.internal.lazyui.pages.Page;
import com.tschebbischeff.myocalib.internal.lazyui.pages.connection.myolist.PageConnectionMyoListAdapter;
import com.tschebbischeff.myocalib.internal.lazyui.pages.connection.myolist.PageConnectionMyoListViewHolder;
import eu.darken.myolib.Myo;
import eu.darken.myolib.MyoConnector;
import eu.darken.myolib.msgs.MyoMsg;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The page on which Myos are listed that are in vicinity.
 * Provides anything related to connection of the Myos.
 * Does *NOT* automatically disconnect from the Myos when shutting down the activity.
 * Disconnecting on shutdown is handled when the API service is shutting down.
 */
public class PageConnection extends Page implements MyoConnector.ScannerCallback, Myo.ReadDeviceNameCallback, Myo.BatteryCallback, IMyoInternalStateListener {

    /**
     * Whether bluetooth is currently enabled on the device.
     */
    private boolean bluetoothEnabled = false;
    /**
     * Adapter for the recycler view listing all available Myos.
     */
    private PageConnectionMyoListAdapter myoListAdapter = null;
    /**
     * How many tries are remaining before a read of the device name or battery state is considered failed.
     */
    private HashMap<Myo, Integer> readFailRetries = null;
    /**
     * A list of Myos that are currently being prepared.
     */
    private ArrayList<Myo> myosInPreparation = null;

    /**
     * {@inheritDoc}
     */
    @Override
    protected int getLayoutResourceId() {
        return this.bluetoothEnabled ? R.layout.myocalib_lazyui_pages_connection_main : R.layout.myocalib_lazyui_pages_connection_enablebt;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String[] getIntentFilterActions() {
        return new String[]{
                BluetoothAdapter.ACTION_STATE_CHANGED
        };
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onCreate() {
        this.myoListAdapter = new PageConnectionMyoListAdapter(this, this.getActivity());
        this.readFailRetries = new HashMap<>();
        this.myosInPreparation = new ArrayList<>();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean onBeforeShow(boolean forced) {
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (bluetoothAdapter != null) {
            this.bluetoothEnabled = bluetoothAdapter.isEnabled();
        } else {
            this.setError(this.getActivity().getString(R.string.myocalib_errors_bt_unavailable_title), this.getActivity().getString(R.string.myocalib_errors_bt_unavailable_message));
        }
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean onAfterShow() {
        if (this.bluetoothEnabled) {
            RecyclerView myoList = this.getActivity().findViewById(R.id.myocalib_lazyui_pages_connection_main_myolist);
            myoList.setHasFixedSize(true);
            WrapContentLinearLayoutManager linearLayoutManager = new WrapContentLinearLayoutManager(this.getActivity());
            myoList.setLayoutManager(linearLayoutManager);
            myoList.setAdapter(this.myoListAdapter);
            this.myoListAdapter.onAfterShow(this.getActivity());
            this.getSharedPageData().getWritableApiInterface().getConnectionHandler().setInternalMyoStateListener(this);
            //Fast scan, for immediately showing still connected devices, next scans will actually scan
            this.getSharedPageData().getWritableApiInterface().getConnectionHandler().getMyoConnector().scan(100, this);
        }
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean onBeforeHide(boolean forced) {
        this.getSharedPageData().getWritableApiInterface().getConnectionHandler().setInternalMyoStateListener(null);
        //Abort all preparations, as we can not receive callbacks anymore.
        for (Myo myo : this.myosInPreparation) {
            this.getSharedPageData().getWritableApiInterface().getConnectionHandler().removeMyoConnectionState(myo);
            myo.disconnect();
        }
        this.myosInPreparation.clear();
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onAfterHide() {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onDestroy() {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onScanFinished(List<Myo> myos) {
        if (this.isActive()) {
            for (Myo myo : myos) {
                if (myo.getDeviceName() == null || myo.getBatteryLevel() < 0) {
                    if (this.getSharedPageData().getWritableApiInterface().getConnectionHandler().getMyoConnectionState(myo).oneOf(
                            ConnectionState.UNKNOWN,
                            ConnectionState.CONNECTING_FOR_PREPARE)) { //If the myo is not already preparing or done, connect //resend if in CONNECTING_FOR_PREPARE state
                        this.getSharedPageData().getWritableApiInterface().getConnectionHandler().setMyoConnectionState(myo, ConnectionState.CONNECTING_FOR_PREPARE);
                        this.getSharedPageData().getWritableApiInterface().getConnectionHandler().onMyoDiscovered(myo);
                        myo.connect();
                    }
                }
            }
            /*for (Myo m : this.getSharedPageData().getApiService().getConnectedMyos()) { //Connected Myos are not found via bt scan anymore, but should still remain in the list
                if (!myos.contains(m)) {
                    myos.add(m);
                }
            }*/
            //Show all Myos that are currently preparing or already connected or in later states, they may vanish, because they don't show up in scans, because they are officially "connected"
            for (Map.Entry<Myo, ConnectionState> entry : this.getSharedPageData().getWritableApiInterface().getConnectionHandler().getConnectionStateEntrySet()) { //Myos in preparation may temporarily vanish due to being connected, prevent this
                if (entry.getValue().oneOf(
                        ConnectionState.PREPARING,
                        ConnectionState.DISCONNECTING_AFTER_PREPARE,
                        ConnectionState.WAITING_FOR_SCAN,
                        ConnectionState.CONNECTING,
                        ConnectionState.CONNECTED,
                        ConnectionState.DISCONNECTING,
                        ConnectionState.DISCONNECTED_SHOULD_CONNECT) && !myos.contains(entry.getKey())) {
                    myos.add(entry.getKey());
                }
                //Myo was waiting for the next scan to begin, so it can show up, if it doesn't show up next time, it will be gone.
                if (entry.getValue() == ConnectionState.WAITING_FOR_SCAN) {
                    this.getSharedPageData().getWritableApiInterface().getConnectionHandler().setMyoConnectionState(entry.getKey(), ConnectionState.WAITING_FOR_CONNECT);
                }
            }
            this.myoListAdapter.datasetUpdate(myos);
            if (this.bluetoothEnabled) {
                this.getSharedPageData().getWritableApiInterface().getConnectionHandler().getMyoConnector().scan(2000, this);
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.myocalib_lazyui_pages_connection_enablebt_enablebtbutton) {
            BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
            if (bluetoothAdapter != null) {
                if (!bluetoothAdapter.isEnabled()) bluetoothAdapter.enable();
            } else
                Logger.error("PageConnection", "Can not find bluetooth adapter even though it should be available!");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onReceive(Context context, Intent intent) {
        final String action = intent.getAction();
        if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
            final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE,
                    BluetoothAdapter.ERROR);
            switch (state) {
                case BluetoothAdapter.STATE_OFF:
                    PageList.CONNECTION.show(this.getActivity());
                    break;
                case BluetoothAdapter.STATE_ON:
                    PageList.CONNECTION.show(this.getActivity());
                    break;
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onDeviceNameRead(Myo myo, MyoMsg msg, String deviceName) {
        if (msg.getState() == MyoMsg.State.NEW) {
            Logger.error("PageConnection", "onDeviceNameRead was called with a new MyoMsg!");
            return;
        }
        if (msg.getState() == MyoMsg.State.SUCCESS) {
            if (this.getSharedPageData().getWritableApiInterface().getConnectionHandler().getMyoConnectionState(myo) == ConnectionState.PREPARING) {
                this.myoListAdapter.datasetUpdateItem(myo);
                this.readFailRetries.put(myo, 5); //for battery read during preparations
                myo.readBatteryLevel(this);
            }
        } else { //check retries
            if (this.readFailRetries.get(myo) > 0) {
                this.readFailRetries.put(myo, this.readFailRetries.get(myo) - 1);
                Logger.info("PageConnection", "Device name read failed, retrying...");
                myo.readDeviceName(this);
            } else {
                Logger.info("PageConnection", "Device name read failed, terminating and aborting preparation!");
                this.getSharedPageData().getWritableApiInterface().getConnectionHandler().removeMyoConnectionState(myo);
                this.myosInPreparation.remove(myo);
                myo.disconnect();
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onBatteryLevelRead(Myo myo, MyoMsg msg, int batteryLevel) {
        if (msg.getState() == MyoMsg.State.NEW) {
            Logger.error("PageConnection", "onBatteryLevelRead was called with a new MyoMsg!");
            return;
        }
        if (msg.getState() == MyoMsg.State.SUCCESS) {
            if (this.getSharedPageData().getWritableApiInterface().getConnectionHandler().getMyoConnectionState(myo) == ConnectionState.PREPARING) {
                //Preparations complete, update entry
                this.getSharedPageData().getWritableApiInterface().getConnectionHandler().setMyoConnectionState(myo, ConnectionState.DISCONNECTING_AFTER_PREPARE);
                //Preparations are done, disconnect and wait for user to connect manually
                myo.disconnect();
            }
        } else { //check remaining retries
            if (this.readFailRetries.get(myo) > 0) {
                this.readFailRetries.put(myo, this.readFailRetries.get(myo) - 1);
                Logger.info("PageConnection", "Battery level read failed, retrying...");
                myo.readBatteryLevel(this);
            } else {
                Logger.info("PageConnection", "Battery level read failed, terminating and aborting preparation!");
                this.getSharedPageData().getWritableApiInterface().getConnectionHandler().removeMyoConnectionState(myo);
                this.myosInPreparation.remove(myo);
                myo.disconnect();
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    public void onMyoConnectedForPreparations(Myo myo) {
        this.readFailRetries.put(myo, 5); //for device name
        this.myosInPreparation.add(myo);
        myo.readDeviceName(this);
    }

    /**
     * {@inheritDoc}
     */
    public void onMyoPreparationsAborted(Myo myo) {
        this.myosInPreparation.remove(myo);
    }

    /**
     * {@inheritDoc}
     */
    public void onMyoPreparationsComplete(Myo myo) {
        Logger.info("PageConnection", "Preparation of '" + myo.getDeviceAddress() + "' completed");
        this.myosInPreparation.remove(myo);
        this.myoListAdapter.datasetUpdateItem(myo);
    }

    /**
     * {@inheritDoc}
     */
    public void onMyoConnectedForUsage(Myo myo) {
        try {
            this.myoListAdapter.notifyItemChanged(this.myoListAdapter.getPositionByDeviceId(myo.getDeviceAddress()));
        } catch (IllegalStateException ex) {
            Logger.warning("PageConnection", "Update of recycler view failed due to IllegalStateException");
        }
    }

    /**
     * {@inheritDoc}
     */
    public void onMyoDisconnectedAbnormally(Myo myo) {
    }

    /**
     * {@inheritDoc}
     */
    public void onMyoConnectedAfterDisconnect(Myo myo) {
    }

    /**
     * {@inheritDoc}
     */
    public void onMyoDisconnected(Myo myo) {
        try {
            this.myoListAdapter.notifyItemChanged(this.myoListAdapter.getPositionByDeviceId(myo.getDeviceAddress()));
        } catch (IllegalStateException ex) {
            Logger.warning("PageConnection", "Update of recycler view failed due to IllegalStateException");
        }
    }

    /**
     * {@inheritDoc}
     */
    public void onMyoDisconnectedAtShutdown(Myo myo) {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onMyoCalibrated(Myo myo) {
        try {
            this.myoListAdapter.notifyItemChanged(this.myoListAdapter.getPositionByDeviceId(myo.getDeviceAddress()));
        } catch (IllegalStateException ex) {
            Logger.warning("PageConnection", "Update of recycler view failed due to IllegalStateException");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onMyoUncalibrated(Myo myo) {
        try {
            this.myoListAdapter.notifyItemChanged(this.myoListAdapter.getPositionByDeviceId(myo.getDeviceAddress()));
        } catch (IllegalStateException ex) {
            Logger.warning("PageConnection", "Update of recycler view failed due to IllegalStateException");
        }
    }

    /**
     * Called when elements in a recycler view card are clicked.
     *
     * @param view       The view that was clicked.
     * @param viewHolder The viewHolder containing the view, i.e. the card that was clicked.
     */
    public void onRecyclerViewClick(View view, PageConnectionMyoListViewHolder viewHolder) {
        String deviceId = viewHolder.deviceId.getText().toString();
        Myo myo = this.myoListAdapter.getMyoByDeviceId(deviceId);
        WritableCalibrationProfile writableCalibrationProfile = this.getSharedPageData().getWritableApiInterface().getCalibrationProfile(myo);
        if (myo != null && writableCalibrationProfile != null) {
            if (view.getId() == R.id.myocalib_lazyui_pages_connection_cardsmyo_connectbutton) {
                ConnectionState connectionState = this.getSharedPageData().getWritableApiInterface().getConnectionHandler().getMyoConnectionState(myo);
                if (connectionState.oneOf(ConnectionState.WAITING_FOR_CONNECT)) {
                    this.getSharedPageData().getWritableApiInterface().getConnectionHandler().setMyoConnectionState(myo, ConnectionState.CONNECTING);
                    myo.connect();
                } else if (connectionState.oneOf(
                        ConnectionState.CONNECTED,
                        ConnectionState.DISCONNECTED_SHOULD_CONNECT
                )) {
                    this.getSharedPageData().getWritableApiInterface().getConnectionHandler().setMyoConnectionState(myo, ConnectionState.DISCONNECTING);
                    myo.disconnect();
                }
            } else if (this.getSharedPageData().getApiService().getConnectedMyos().contains(myo)) {
                if (writableCalibrationProfile.isCalibrationComplete()) {
                    writableCalibrationProfile.invalidateCalibration();
                } else {
                    this.getSharedPageData().setCalibratingMyo(myo);
                    PageList.CALIBRATION.show(this.getActivity());
                }
            }
        }
    }
}
