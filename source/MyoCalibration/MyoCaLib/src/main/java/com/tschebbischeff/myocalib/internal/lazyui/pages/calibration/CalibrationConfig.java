package com.tschebbischeff.myocalib.internal.lazyui.pages.calibration;

public class CalibrationConfig {

    /**
     * The total time required for one cycle of the progres bar animation
     */
    public static final long INDETERMINATE_ANIMATION_CYCLE_TIME = 5000L;
    /**
     * The total time required until the indeterminate progress starts (i.e. the bar ends)
     */
    public static final long INDETERMINATE_ANIMATION_BAR_SIZE = 1500L;
    /**
     * The speed of the animation
     */
    public static final long INDETERMINATE_ANIMATION_SPEED = 3L;
    /**
     * The threshold above which EMG data is considered to mean the user's arm is not relaxed.
     */
    public static final double DOWN_INIT_THRESHOLD = 5d;
    /**
     * The threshold above which EMG data is considered to mean the user is executing the RVC.
     */
    public static final double DOWN_RVC_THRESHOLD = 5d;
    /**
     * The threshold above which EMG data is considered to mean the user's arm is not relaxed.
     */
    public static final double DOWN_RELAXED_THRESHOLD = 7d;
    /**
     * The threshold above which EMG data is considered to mean the user's arm is not relaxed (when arm is held forward).
     */
    public static final double FORWARD_INIT_THRESHOLD = 7d;
    /**
     * The threshold above which EMG data is considered to mean the user is executing the RVC (when arm is held forward).
     */
    public static final double FORWARD_RVC_THRESHOLD = 7d;
    /**
     * The threshold above which EMG data is considered to mean the user's arm is not relaxed (when arm is held forward).
     */
    public static final double FORWARD_RELAXED_THRESHOLD = 10d;
    /**
     * The window size of the moving average filter applied to the EMG signal. In milliseconds.
     */
    public static final long EMG_MOVING_AVERAGE_WINDOW_SIZE = 250L;
    /**
     * The window size of the moving average filter applied to the IMU signal. In milliseconds.
     */
    public static final long IMU_MOVING_AVERAGE_WINDOW_SIZE = 50L;
    /**
     * The perfect value of the absolute x value of the accelerometer when the arm is relaxed left hanging.
     */
    public static final double IMU_DOWN_X_EXPECTED = 1.0d;
    /**
     * The tolerance of the x-axis specifying the range in which IMU values are considered to represent
     * an arm that is left hanging.
     */
    public static final double IMU_DOWN_X_TOLERANCE = 0.10d;
    /**
     * The perfect value of the absolute x value of the accelerometer when the arm is relaxed left hanging.
     */
    public static final double IMU_FORWARD_X_EXPECTED = 0.0d;
    /**
     * The tolerance of the x-axis specifying the range in which IMU values are considered to represent
     * an arm that is left hanging.
     */
    public static final double IMU_FORWARD_X_TOLERANCE = 0.15d;
    /**
     * Time for which the Myo must be left undisturbed to take the measurement of the accelerometer's bias.
     */
    public static final int IMU_HOLD_TIME = 2500;
    /**
     * Time for which any relaxing pose must be held, for the step to be completed. In milliseconds.
     */
    public static final int INIT_HOLD_TIME = 2000;
    /**
     * Time for which the actual RVC pose must be held, for the step to be completed. In milliseconds.
     */
    public static final int RVC_HOLD_TIME = 4000;
    /**
     * Time for which the actual RVC pose must be held, for the step to be completed. In milliseconds.
     */
    public static final int RELAX_HOLD_TIME = 3000;
    /**
     * The moving average window size used for normalization.
     */
    public static final long NORMALIZATION_MV_AVG_WINDOW_SIZE = 250L;
    /**
     * The target value of the normalization. The normalization factor is calculated by this value divided by the mean
     * of the RVC executions.
     */
    public static final double NORMALIZATION_TARGET_VALUE = 32.0d;
}
