package com.tschebbischeff.myocalib.internal.lazyui.pages.welcome;

import android.Manifest;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import com.tschebbischeff.myocalib.R;
import com.tschebbischeff.myocalib.internal.backend.tools.Constants;
import com.tschebbischeff.myocalib.internal.lazyui.PageList;
import com.tschebbischeff.myocalib.internal.lazyui.pages.Page;

/**
 * Handles callbacks for the welcome page
 */
public class PageWelcome extends Page {

    /**
     * Array containing the required permissions.
     */
    private static final String[] requiredPermissions = {
            Manifest.permission.BLUETOOTH,
            Manifest.permission.BLUETOOTH_ADMIN,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    /**
     * Whether the requiredPermissions are granted already.
     */
    private boolean permissionsGranted = false;

    /**
     * {@inheritDoc}
     */
    @Override
    protected int getLayoutResourceId() {
        return this.permissionsGranted ? R.layout.myocalib_lazyui_pages_welcome_main : R.layout.myocalib_lazyui_pages_welcome_permissions;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String[] getIntentFilterActions() {
        return new String[]{};
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onCreate() {
        this.permissionsGranted = true;
        for (String requiredPermission : requiredPermissions) {
            if (ActivityCompat.checkSelfPermission(this.getActivity(), requiredPermission) != android.content.pm.PackageManager.PERMISSION_GRANTED)
                this.permissionsGranted = false;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean onBeforeShow(boolean forced) {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean onAfterShow() {
        if (this.permissionsGranted) {
            PageList.CONNECTION.show(this.getActivity());
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean onBeforeHide(boolean forced) {
        return forced || this.permissionsGranted;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onAfterHide() {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onDestroy() {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.myocalib_lazyui_pages_welcome_permissions_grantbutton) {
            ActivityCompat.requestPermissions(this.getActivity(),
                    requiredPermissions,
                    Constants.REQUEST_CODE_PERMISSIONS_WELCOME_SCREEN);
        }
    }

    /**
     * Searches a String-Array in a linear way, i.e. without needing the array to be sorted.
     *
     * @param array The string array in which to search
     * @param value The value for which to search
     * @return The first index of the value in the array or -1 if the value is not contained in the array.
     */
    private int arrayLinearSearch(String[] array, String value) {
        for (int i = 0; i < array.length; i++) {
            if (array[i].equals(value)) return i;
        }
        return -1;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        this.permissionsGranted = true;
        for (String requiredPermission : requiredPermissions) {
            int searchResult = arrayLinearSearch(permissions, requiredPermission);
            if (searchResult >= 0) {
                if (grantResults[searchResult] != android.content.pm.PackageManager.PERMISSION_GRANTED) {
                    this.permissionsGranted = false;
                    break;
                }
            }
        }
    }
}
