package com.tschebbischeff.myocalib.api;

import com.tschebbischeff.myocalib.internal.backend.calibration.interpolation.CyclicInterpolationFunction;

/**
 * Describes the signal that is interpolated after calibration is applied.
 */
public final class InterpolatedEmgSignal {

    /**
     * The interpolation function used.
     */
    private CyclicInterpolationFunction interpolationFunction;

    /**
     * Creates a new interpolated emg signal.
     *
     * @param signal                The precalibrated signal.
     * @param angularOffset         The offset on the domain of the cyclic function, depending on the rotation measured by
     *                              the calibration.
     * @param interpolationFunction A <strong>new instance</strong> of the interpolation function to use.
     */
    protected InterpolatedEmgSignal(double[] signal, double angularOffset, CyclicInterpolationFunction interpolationFunction) {
        this.interpolationFunction = interpolationFunction;
        this.interpolationFunction.clear();
        this.interpolationFunction.addEvenlyDistributed(signal, angularOffset);
    }

    /**
     * Get the value of the signal at a certain angle. The angle is zero on top of the arm and increases towards the
     * elbow. That means the angle is in clockwise direction on the right arm, while it is in counter-clockwise direction
     * on the left arm.
     *
     * @param angle The angle at which to sample the signal in degrees.
     */
    public double getValue(double angle) {
        return this.interpolationFunction.sampleAt(Math.toRadians(angle));
    }

    /**
     * Convenience function to get the values at 0, 45, 90, 135, 180, 225, 270 and 315 degrees.
     *
     * @return The values at the specified points on the function.
     */
    public double[] getValues() {
        double[] result = new double[8];
        for (int i=0; i<8; i++) {
            result[i] = this.interpolationFunction.sampleAt(i * (Math.PI / 4d));
        }
        return result;
    }
}
