package com.tschebbischeff.common.android.logger;


import com.tschebbischeff.common.android.logger.outputs.LoggerOutput;
import com.tschebbischeff.common.android.logger.outputs.LoggerOutputConsole;
import com.tschebbischeff.myocalib.BuildConfig;

import java.util.ArrayList;

/**
 * Wraps static method for message printing across multiple or single outputs, such as files and console.
 */
public class Logger {

    /**
     * The default preamble of the Log Tag supplied to androids logger.
     */
    private static String LOG_TAG = "MyoCalibration";
    /**
     * The default preamble of the Log Tag supplied to androids logger, when using performance measuring methods from
     * this class.
     */
    private static String TIMING_IDENTIFIER = "Timing";
    /**
     * The start timestamp of the first performance measurement related call.
     */
    private static long timingStartTimestamp = 0;
    /**
     * The timestamp of the last performance measurement related call.
     */
    private static long timingLastTimestamp = 0;
    /**
     * A list of outputs that the error messages are written to.
     */
    private static ArrayList<LoggerOutput> outputs = null;

    /**
     * Returns the list of all outputs registered to the logger.
     *
     * @return Implementations of {@link LoggerOutput}, handling output of any error type.
     */
    private static ArrayList<LoggerOutput> getOutputs() {
        if (outputs == null) {
            outputs = new ArrayList<>();
            outputs.add(new LoggerOutputConsole());
        }
        return outputs;
    }

    /**
     * Adds an output, which handles writing of messages to any medium.
     *
     * @param output The implementation of {@link LoggerOutput} to add to the output list.
     */
    public static void addOutput(LoggerOutput output) {
        if (!getOutputs().contains(output)) {
            getOutputs().add(output);
        }
    }

    /**
     * Get all outputs of a certain class.
     *
     * @param c The class extending {@link LoggerOutput} of which all instances that are currently added should be returned.
     * @return All instances of class c, that were added to the logger as outputs.
     */
    public static ArrayList<LoggerOutput> getSpecificOutputs(Class<? extends LoggerOutput> c) {
        ArrayList<LoggerOutput> outputs = new ArrayList<>();
        for (LoggerOutput o : getOutputs()) {
            if (c.isInstance(o)) {
                outputs.add(o);
            }
        }
        return outputs;
    }

    /**
     * Remove an output from the logger.
     *
     * @param output The implementation of {@link LoggerOutput} to remove from the output list.
     */
    public static void removeOutput(LoggerOutput output) {
        getOutputs().remove(output);
    }

    /**
     * Initializes a performance measuring operation.
     *
     * @param identifier A String identifying the performance measuring. It is printed in the message in the outputs.
     */
    public static void timingStart(String identifier) {
        if (BuildConfig.LOGGER_ENABLED && BuildConfig.LOGGER_TIMING_ENABLED) {
            timingStartTimestamp = System.currentTimeMillis();
            timingLastTimestamp = timingStartTimestamp;
            Logger.d(LOG_TAG + "/" + TIMING_IDENTIFIER,
                    "Starting timelogs for '" + identifier + "' @" + String.valueOf(
                            timingStartTimestamp), LoggerOutputConsole.equalityConstant);
        }
    }

    /**
     * Creates a lap, by printing the time since the last execution of this message or since calling {@link #timingStart(String)}
     * if this method was never called before.
     *
     * @param message The message to print before the elapsed time in the logger outputs.
     */
    public static void timingNext(String message) {
        if (BuildConfig.LOGGER_ENABLED && BuildConfig.LOGGER_TIMING_ENABLED) {
            if (timingStartTimestamp > 0 && timingLastTimestamp > 0) {
                Logger.d(LOG_TAG + "/" + TIMING_IDENTIFIER, message +
                                String.valueOf(System.currentTimeMillis() - timingLastTimestamp) + "ms",
                        LoggerOutputConsole.equalityConstant);
                timingLastTimestamp = System.currentTimeMillis();
            }
        }
    }

    /**
     * Finishes performance measuring, by printing the time since the performance measurement was started.
     * Does not print an additional lap, as {@link #timingNext(String)} does.
     *
     * @param identifier A string identifying the performance measuring. It is printed in the message in the outputs.
     */
    public static void timingFinish(String identifier) {
        if (BuildConfig.LOGGER_ENABLED && BuildConfig.LOGGER_TIMING_ENABLED) {
            if (timingStartTimestamp > 0 && timingLastTimestamp > 0) {
                Logger.d(LOG_TAG + "/" + TIMING_IDENTIFIER, identifier + " finished after: " +
                                String.valueOf(System.currentTimeMillis() - timingStartTimestamp) + "ms",
                        LoggerOutputConsole.equalityConstant);
                timingStartTimestamp = 0;
                timingLastTimestamp = 0;
            }
        }
    }

    /**
     * Tries to convey to each registered output that a message of severity "error" should be printed.
     * Checks whether error reporting is enabled etc.
     *
     * @param identifier A string placed directly after the preamble to search for messages of certain types faster.
     * @param message    The message to output.
     */
    public static void error(String identifier, String message) {
        error(identifier, message, null);
    }

    /**
     * Tries to convey to one registered output that a message of severity "error" should be printed.
     * Checks whether error reporting is enabled etc.
     *
     * @param identifier  A string placed directly after the preamble to search for messages of certain types faster.
     * @param message     The message to output.
     * @param limitLogger A string identifying the output(s), to which the message is to conveyed. If null, the message
     *                    is conveyed to all outputs.
     */
    public static void error(String identifier, String message, String limitLogger) {
        if (BuildConfig.LOGGER_ENABLED && BuildConfig.LOGGER_ERROR_ENABLED) {
            Logger.e(LOG_TAG + "/" + identifier.replaceAll(" ", ""), message, limitLogger);
        }
    }

    /**
     * Tries to convey to each registered output that a message of severity "warning" should be printed.
     * Checks whether error reporting is enabled etc.
     *
     * @param identifier A string placed directly after the preamble to search for messages of certain types faster.
     * @param message    The message to output.
     */
    public static void warning(String identifier, String message) {
        warning(identifier, message, null);
    }

    /**
     * Tries to convey to one registered output that a message of severity "warning" should be printed.
     * Checks whether error reporting is enabled etc.
     *
     * @param identifier  A string placed directly after the preamble to search for messages of certain types faster.
     * @param message     The message to output.
     * @param limitLogger A string identifying the output(s), to which the message is to conveyed. If null, the message
     *                    is conveyed to all outputs.
     */
    public static void warning(String identifier, String message, String limitLogger) {
        if (BuildConfig.LOGGER_ENABLED && BuildConfig.LOGGER_WARNING_ENABLED) {
            Logger.w(LOG_TAG + "/" + identifier.replaceAll(" ", ""), message, limitLogger);
        }
    }

    /**
     * Tries to convey to each registered output that a message of severity "info" should be printed.
     * Checks whether error reporting is enabled etc.
     *
     * @param identifier A string placed directly after the preamble to search for messages of certain types faster.
     * @param message    The message to output.
     */
    public static void info(String identifier, String message) {
        info(identifier, message, null);
    }

    /**
     * Tries to convey to one registered output that a message of severity "info" should be printed.
     * Checks whether error reporting is enabled etc.
     *
     * @param identifier  A string placed directly after the preamble to search for messages of certain types faster.
     * @param message     The message to output.
     * @param limitLogger A string identifying the output(s), to which the message is to conveyed. If null, the message
     *                    is conveyed to all outputs.
     */
    public static void info(String identifier, String message, String limitLogger) {
        if (BuildConfig.LOGGER_ENABLED && BuildConfig.LOGGER_INFO_ENABLED) {
            Logger.i(LOG_TAG + "/" + identifier.replaceAll(" ", ""), message, limitLogger);
        }
    }

    /**
     * Tries to convey to each registered output that a message of severity "debug" should be printed.
     * Checks whether error reporting is enabled etc.
     *
     * @param identifier A string placed directly after the preamble to search for messages of certain types faster.
     * @param message    The message to output.
     */
    public static void debug(String identifier, String message) {
        debug(identifier, message, null);
    }

    /**
     * Tries to convey to one registered output that a message of severity "debug" should be printed.
     * Checks whether error reporting is enabled etc.
     *
     * @param identifier  A string placed directly after the preamble to search for messages of certain types faster.
     * @param message     The message to output.
     * @param limitLogger A string identifying the output(s), to which the message is to conveyed. If null, the message
     *                    is conveyed to all outputs.
     */
    public static void debug(String identifier, String message, String limitLogger) {
        if (BuildConfig.LOGGER_ENABLED && BuildConfig.LOGGER_DEBUG_ENABLED) {
            Logger.d(LOG_TAG + "/" + identifier.replaceAll(" ", ""), message, limitLogger);
        }
    }

    /**
     * Tries to convey to each registered output that a message of severity "verbose" should be printed.
     * Checks whether error reporting is enabled etc.
     *
     * @param identifier A string placed directly after the preamble to search for messages of certain types faster.
     * @param message    The message to output.
     */
    public static void verbose(String identifier, String message) {
        verbose(identifier, message, null);
    }

    /**
     * Tries to convey to one registered output that a message of severity "verbose" should be printed.
     * Checks whether error reporting is enabled etc.
     *
     * @param identifier  A string placed directly after the preamble to search for messages of certain types faster.
     * @param message     The message to output.
     * @param limitLogger A string identifying the output(s), to which the message is to conveyed. If null, the message
     *                    is conveyed to all outputs.
     */
    public static void verbose(String identifier, String message, String limitLogger) {
        if (BuildConfig.LOGGER_ENABLED && BuildConfig.LOGGER_VERBOSE_ENABLED) {
            Logger.v(LOG_TAG + "/" + identifier.replaceAll(" ", ""), message, limitLogger);
        }
    }

    /**
     * Conveys messages to outputs, limited to either one by a String or all by "null".
     * Handles messages of severity "error".
     *
     * @param identifier  The complete identifier forwarded to the output(s).
     * @param message     The message forwarded to the output(s).
     * @param limitLogger A String identifying output(s), if null conveys the message to all outputs.
     */
    private static void e(String identifier, String message, String limitLogger) {
        for (LoggerOutput o : getOutputs()) {
            if (limitLogger == null || o.getEqualityConstant().equals(limitLogger)) {
                o.error(identifier, message);
            }
        }
    }

    /**
     * Conveys messages to outputs, limited to either one by a String or all by "null".
     * Handles messages of severity "warning".
     *
     * @param identifier  The complete identifier forwarded to the output(s).
     * @param message     The message forwarded to the output(s).
     * @param limitLogger A String identifying output(s), if null conveys the message to all outputs.
     */
    private static void w(String identifier, String message, String limitLogger) {
        for (LoggerOutput o : getOutputs()) {
            if (limitLogger == null || o.getEqualityConstant().equals(limitLogger)) {
                o.warning(identifier, message);
            }
        }
    }

    /**
     * Conveys messages to outputs, limited to either one by a String or all by "null".
     * Handles messages of severity "info".
     *
     * @param identifier  The complete identifier forwarded to the output(s).
     * @param message     The message forwarded to the output(s).
     * @param limitLogger A String identifying output(s), if null conveys the message to all outputs.
     */
    private static void i(String identifier, String message, String limitLogger) {
        for (LoggerOutput o : getOutputs()) {
            if (limitLogger == null || o.getEqualityConstant().equals(limitLogger)) {
                o.info(identifier, message);
            }
        }
    }

    /**
     * Conveys messages to outputs, limited to either one by a String or all by "null".
     * Handles messages of severity "debug".
     *
     * @param identifier  The complete identifier forwarded to the output(s).
     * @param message     The message forwarded to the output(s).
     * @param limitLogger A String identifying output(s), if null conveys the message to all outputs.
     */
    private static void d(String identifier, String message, String limitLogger) {
        for (LoggerOutput o : getOutputs()) {
            if (limitLogger == null || o.getEqualityConstant().equals(limitLogger)) {
                o.debug(identifier, message);
            }
        }
    }

    /**
     * Conveys messages to outputs, limited to either one by a String or all by "null".
     * Handles messages of severity "verbose".
     *
     * @param identifier  The complete identifier forwarded to the output(s).
     * @param message     The message forwarded to the output(s).
     * @param limitLogger A String identifying output(s), if null conveys the message to all outputs.
     */
    private static void v(String identifier, String message, String limitLogger) {
        for (LoggerOutput o : getOutputs()) {
            if (limitLogger == null || o.getEqualityConstant().equals(limitLogger)) {
                o.verbose(identifier, message);
            }
        }
    }
}
