package com.tschebbischeff.myocalib.internal.backend.security;

/**
 * Class that can be extended for creating own cross-class authentication tokens.
 * The constructor of the extending class must be private, and the extending class must be an inner class of
 * the class that needs to authenticate itself.
 * The static method {@link #validOrThrow(SecurityToken)} in this class can then verify that the token is originating
 * from the class that it is an inner class of.
 */
public abstract class SecurityToken {

    /**
     * Protected default constructor, allows overriding from extending class.
     */
    protected SecurityToken() {
    }

    /**
     * Checks whether any extending class of this security token class is a valid, existing instance and
     * throws a runtime exception if not.
     *
     * @param securityToken The security token to check.
     * @throws SecurityViolationException If, and only if, the token is null.
     */
    public static void validOrThrow(SecurityToken securityToken) throws SecurityViolationException {
        if (securityToken == null) {
            throw new SecurityViolationException();
        }
    }
}
