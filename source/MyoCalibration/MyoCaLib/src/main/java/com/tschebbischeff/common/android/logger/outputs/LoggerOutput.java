package com.tschebbischeff.common.android.logger.outputs;

/**
 * Superclass for all outputs, to which messages of the logger can be forwarded.
 */

public abstract class LoggerOutput {

    /**
     * A string identifying a subset of outputs.
     *
     * @return A string identifying a subset of outputs.
     */
    public abstract String getEqualityConstant();

    /**
     * Prints a message of severity "error".
     *
     * @param tag     The tag associated with the message.
     * @param message The message to print.
     */
    public abstract void error(String tag, String message);

    /**
     * Prints a message of severity "warning".
     *
     * @param tag     The tag associated with the message.
     * @param message The message to print.
     */
    public abstract void warning(String tag, String message);

    /**
     * Prints a message of severity "info".
     *
     * @param tag     The tag associated with the message.
     * @param message The message to print.
     */
    public abstract void info(String tag, String message);

    /**
     * Prints a message of severity "debug".
     *
     * @param tag     The tag associated with the message.
     * @param message The message to print.
     */
    public abstract void debug(String tag, String message);

    /**
     * Prints a message of severity "verbose".
     *
     * @param tag     The tag associated with the message.
     * @param message The message to print.
     */
    public abstract void verbose(String tag, String message);

    /**
     * Whether this output is virtually equal to another, based on the defined equality constant.
     *
     * @param o An object to compare this objects equality with.
     * @return True if, and only if, the supplied object is an instance of this class and has the same equality
     * constant as this instance.
     */
    @Override
    public boolean equals(Object o) {
        return o instanceof LoggerOutput &&
                ((LoggerOutput) o).getEqualityConstant().equals(this.getEqualityConstant());
    }
}
