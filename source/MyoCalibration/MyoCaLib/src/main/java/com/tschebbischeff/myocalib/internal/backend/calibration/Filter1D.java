package com.tschebbischeff.myocalib.internal.backend.calibration;

/**
 * Superclass for any kind of one-dimensional filters that can be applied to data.
 *
 * @param <T> The type of data this filter can be applied to.
 */
public abstract class Filter1D<T> {

    /**
     * Applies the filter to one value.
     *
     * @param value The value to which to apply the filter.
     * @return The result of the application.
     */
    public T apply(T value) {
        this.applyWithoutResult(value);
        return this.getResult();
    }

    /**
     * Applies the filter without returning the result.
     *
     * @param value For some filters used in realtime, this may be performance efficient.
     */
    public abstract void applyWithoutResult(T value);

    /**
     * Returns the current result, without adding new values.
     *
     * @return The current result of the filter.
     */
    public abstract T getResult();

    /**
     * Applies the filter to a number of values.
     *
     * @param values An array containing the values.
     * @return the result of the application.
     */
    protected abstract T[] apply(T[] values);

    /**
     * Applies the filter to a number of values.
     *
     * @param values The values, to which to apply the filter.
     * @return The result of the application.
     */
    public T[] batchApply(T[] values) {
        return this.apply(values);
    }

    /**
     * Resets the filter to a state at which it is as if it was just created.
     */
    public abstract void reset();
}
