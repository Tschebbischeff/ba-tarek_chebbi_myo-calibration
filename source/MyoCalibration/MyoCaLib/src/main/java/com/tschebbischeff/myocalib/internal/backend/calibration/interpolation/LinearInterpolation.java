package com.tschebbischeff.myocalib.internal.backend.calibration.interpolation;

/**
 * Interpolates linearly from one sample point to the next.
 */
public class LinearInterpolation extends CyclicInterpolationFunction {

    /**
     * Constructor.
     */
    public LinearInterpolation() {
        super(0, 2 * Math.PI);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void recalculateParameters() {
        //Nothing to do here
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Double executeSample(Double domainValue) {
        if (this.isSupportPoint(domainValue)) {
            return this.values.get(domainValue);
        }
        Double lowKey = this.getLowerSupportPoint(domainValue);
        Double highKey = this.getHigherSupportPoint(domainValue);
        double result = this.values.get(lowKey) + (getDistance(domainValue, lowKey) * ((this.values.get(highKey) - this.values.get(lowKey)) / getDistance(highKey, lowKey)));
        return result;
    }

}
