package com.tschebbischeff.myocalib.internal.backend.calibration.interpolation;

/**
 * Returns the nearest sample point to the given point on the domain of the function.
 */
public class NearestNeighborInterpolation extends CyclicInterpolationFunction {

    /**
     * Constructor.
     */
    public NearestNeighborInterpolation() {
        super(0, 2 * Math.PI);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void recalculateParameters() {
        //Nothing to do here
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Double executeSample(Double domainValue) {
        if (this.isSupportPoint(domainValue)) {
            return this.values.get(domainValue);
        }
        Double lowKey = this.getLowerSupportPoint(domainValue);
        Double highKey = this.getHigherSupportPoint(domainValue);
        if (getDistance(domainValue, lowKey) > getDistance(domainValue, highKey)) {
            return this.values.get(highKey);
        } else {
            return this.values.get(lowKey);
        }
    }

}
