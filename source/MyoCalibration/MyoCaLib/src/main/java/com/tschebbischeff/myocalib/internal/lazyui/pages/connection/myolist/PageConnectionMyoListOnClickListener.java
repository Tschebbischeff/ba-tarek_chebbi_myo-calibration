package com.tschebbischeff.myocalib.internal.lazyui.pages.connection.myolist;

import android.view.View;
import com.tschebbischeff.myocalib.internal.lazyui.pages.connection.PageConnection;

/**
 * Handles click events on recycler view cards.
 */
public class PageConnectionMyoListOnClickListener implements View.OnClickListener {

    /**
     * The connection page object for callbacks.
     */
    private PageConnection source;
    /**
     * The view holder that created this listener, to reference when calling the callback on the connection page.
     */
    private PageConnectionMyoListViewHolder viewHolder;

    /**
     * Constructor.
     *
     * @param source The connection page object for callbacks.
     * @param vh     The view holder creating this listener.
     */
    public PageConnectionMyoListOnClickListener(PageConnection source, PageConnectionMyoListViewHolder vh) {
        this.source = source;
        this.viewHolder = vh;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onClick(View v) {
        this.source.onRecyclerViewClick(v, this.viewHolder);
    }
}