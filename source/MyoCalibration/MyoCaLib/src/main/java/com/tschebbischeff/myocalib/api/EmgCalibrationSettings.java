package com.tschebbischeff.myocalib.api;

import com.tschebbischeff.myocalib.internal.backend.calibration.interpolation.CubicHermiteSplineInterpolation;
import com.tschebbischeff.myocalib.internal.backend.calibration.interpolation.CyclicInterpolationFunction;
import com.tschebbischeff.myocalib.internal.backend.calibration.interpolation.LinearInterpolation;
import com.tschebbischeff.myocalib.internal.backend.calibration.interpolation.NearestNeighborInterpolation;

/**
 * Encapsulates all settings regarding the EMG calibration.
 * The design follows the principle of fluent method calls via the encapsulating {@link CalibrationSettings} class.
 * The returned instance of that class is always the instance which this object is the {@link CalibrationSettings#emg}
 * instance of.
 */
public final class EmgCalibrationSettings {

    /**
     * Whether the calibration settings are marked as final by the application.
     */
    private boolean immutable;
    /**
     * The source that is used for fluent method calls, including method calls to other sub settings
     */
    private CalibrationSettings calibrationSettings;
    /**
     * Whether the calibration of EMG data is activated at all.
     */
    private boolean calibrationEnabled = true;
    /**
     * Whether the calibration of the translation of the armband on the users arm is activated.
     */
    private boolean translationCalibrationEnabled = true;
    /**
     * Whether the calibration of the rotation of the armband on the user's arm is activated.
     */
    private boolean rotationCalibrationEnabled = true;
    /**
     * Whether the EMG signals are normalized, to eliminate session-dependent influences.
     */
    private boolean normalizationEnabled = true;
    /**
     * The interpolation method used to interpolate EMG data.
     */
    private EmgInterpolationMode interpolationMode = EmgInterpolationMode.CUBIC_HERMITE_SPLINE;
    /**
     * The maximum angular deviation from the zero point allowed when the user is forced to rotate the armband.
     */
    private double forceRotationThreshold = 360d;
    /**
     * The threshold for the translation calibration, determining whether the armband is situated to low on the arm.
     */
    private double translationThresholdLow = 30d;
    /**
     * The threshold for the translation calibration, determining whether the armband is situated to high on the arm.
     */
    private double translationThresholdHigh = 30d;

    /**
     * Protected constructor, objects may only be created by {@link CalibrationSettings}.
     */
    protected EmgCalibrationSettings(CalibrationSettings source) {
        this.calibrationSettings = source;
        this.immutable = false;
    }

    /**
     * Sets the settings as final, called when calling the general settings class done-method.
     * Action can not be undone, until a new object of this class is created.
     */
    protected void done() {
        this.immutable = true;
    }

    /**
     * Enables the calibration of the EMG data.
     * This is turned on per default.
     *
     * @return The {@link CalibrationSettings} objects containing this object for fluent method calls, including all settings.
     */
    public CalibrationSettings enableCalibration() {
        if (!this.immutable) {
            this.calibrationEnabled = true;
        }
        return this.calibrationSettings;
    }

    /**
     * Disables the calibration of the EMG data.
     * This is turned on per default.
     *
     * @return The {@link CalibrationSettings} objects containing this object for fluent method calls, including all settings.
     */
    public CalibrationSettings disableCalibration() {
        if (!this.immutable) {
            this.calibrationEnabled = false;
        }
        return this.calibrationSettings;
    }

    /**
     * Whether the calibration of EMG data is currently enabled.
     * This is turned on per default.
     *
     * @return True if, and only if, the EMG data forwarded to listeners is currently activated, which it is per default.
     */
    public boolean isCalibrationEnabled() {
        return this.calibrationEnabled;
    }

    /**
     * Whether the force rotation mode is currently activated or not.
     *
     * @return True if, and only if, the user is forced to rotate the armband under a specific angle from the defined
     * zero.
     */
    public boolean isForceRotationEnabled() {
        return this.forceRotationThreshold < 360d;
    }

    /**
     * Whether the calibration of translation is enabled or not.
     *
     * @return True if, and only if, the translation of the armband is checked and needs to be inside a certain range
     * of positions on the user's arm.
     */
    public boolean isTranslationCalibrationEnabled() {
        return this.translationCalibrationEnabled;
    }

    /**
     * Whether the calibration of rotation is enabled or not.
     *
     * @return True if, and only if, the rotation of the armband is checked and calibrated.
     */
    public boolean isRotationCalibrationEnabled() {
        return this.rotationCalibrationEnabled;
    }

    /**
     * Whether the normalization of the EMG values is enabled or not.
     *
     * @return True if, and only if, the normalization of the EMG signal is activated.
     */
    public boolean isNormalizationEnabled() {
        return this.normalizationEnabled;
    }

    /**
     * Sets via which interpolation function the data is interpolated.
     * The default interpolation mode is {@link EmgInterpolationMode#CUBIC_HERMITE_SPLINE}.
     *
     * @param interpolationMode The new mode describing an interpolation function to apply to the data.
     * @return The {@link CalibrationSettings} objects containing this object for fluent method calls, including all settings.
     */
    public CalibrationSettings setInterpolationMode(EmgInterpolationMode interpolationMode) {
        if (!this.immutable) {
            this.interpolationMode = interpolationMode;
        }
        return this.calibrationSettings;
    }

    /**
     * Get a descriptor of the currently used interpolation function.
     * The default interpolation mode is {@link EmgInterpolationMode#CUBIC_HERMITE_SPLINE}.
     *
     * @return One of the enum constants describing which interpolation function is currently used to interpolate the
     * data.
     */
    public EmgInterpolationMode getInterpolationMode() {
        return this.interpolationMode;
    }

    /**
     * Sets that the user is required to rotate the armband to complete the calibration.
     * The user is required to rotate the armband with an allowed angular deviation of
     * maximumAngle towards the zero point, which is defined with the main pod being perfectly on
     * top of the arm. (The main pod is the pod, with the USB port)
     * This is turned off per default.
     *
     * @param maximumAngle The angle that must be higher than the actual measured angle for the user to
     *                     successfully finish the calibration. The angle is capped to values between 5 and 180 degrees.
     * @return The {@link CalibrationSettings} objects containing this object for fluent method calls, including all settings.
     */
    public CalibrationSettings enableForceRotation(double maximumAngle) {
        if (!this.immutable) {
            this.forceRotationThreshold = Math.max(5d, Math.min(180d, maximumAngle));
        }
        return this.calibrationSettings;
    }

    /**
     * Sets that the user is NOT required to rotate the armband to complete the calibration.
     * See {@link #enableForceRotation(double)} for more information.
     * This is the default setting.
     * This is turned off per default.
     *
     * @return The {@link CalibrationSettings} objects containing this object for fluent method calls, including all settings.
     */
    public CalibrationSettings disableForceRotation() {
        if (!this.immutable) {
            this.forceRotationThreshold = 360d;
        }
        return this.calibrationSettings;
    }

    /**
     * The maximum angle at which the rotation of the armband is correct when the user is forced to rotate the armband.
     *
     * @return The maximum force rotation angle.
     */
    public double getForceRotationThreshold() {
        return this.forceRotationThreshold;
    }

    /**
     * Enables the calibration of the translation of the armband on the users arm.
     * This is turned on per default.
     *
     * @return The {@link CalibrationSettings} objects containing this object for fluent method calls, including all settings.
     */
    public CalibrationSettings enableTranslationCalibration() {
        if (!this.immutable) {
            this.translationCalibrationEnabled = true;
        }
        return this.calibrationSettings;
    }

    /**
     * Disables the calibration of the translation of the armband on the users arm.
     * This is turned on per default.
     *
     * @return The {@link CalibrationSettings} objects containing this object for fluent method calls, including all settings.
     */
    public CalibrationSettings disableTranslationCalibration() {
        if (!this.immutable) {
            this.translationCalibrationEnabled = false;
        }
        return this.calibrationSettings;
    }

    /**
     * Enables the calibration of the rotation of the armband on the user's arm.
     * This is turned on per default. If this is turned off, the user is not forced to rotate the armband if the
     * force rotation is activated.
     *
     * @return The {@link CalibrationSettings} object containing this object for fluent method calls including all settings.
     */
    public CalibrationSettings enableRotationCalibration() {
        if (!this.immutable) {
            this.rotationCalibrationEnabled = true;
        }
        return this.calibrationSettings;
    }

    /**
     * Disables the calibration of the rotation of the armband on the user's arm.
     * This is turned on per default. If this is turned off, the user is not forced to rotate the armband if the
     * force rotation is activated.
     *
     * @return The {@link CalibrationSettings} object containing this object for fluent method calls including all settings.
     */
    public CalibrationSettings disableRotationCalibration() {
        if (!this.immutable) {
            this.rotationCalibrationEnabled = false;
        }
        return this.calibrationSettings;
    }

    /**
     * Enables the normalization of the EMG signals to get rid of session-dependent influences.
     * This is turned on per default.
     *
     * @return The {@link CalibrationSettings} object containing this object for fluent method calls including all settings.
     */
    public CalibrationSettings enableNormalization() {
        if (!this.immutable) {
            this.normalizationEnabled = true;
        }
        return this.calibrationSettings;
    }

    /**
     * Disables the normalization of the EMG signals to get rid of session-dependent influences.
     * This is turned on per default.
     *
     * @return The {@link CalibrationSettings} object containing this object for fluent method calls including all settings.
     */
    public CalibrationSettings disableNormalization() {
        if (!this.immutable) {
            this.normalizationEnabled = false;
        }
        return this.calibrationSettings;
    }

    /**
     * Sets the threshold, which defines that the armband is situated to low on the users arm. The armband
     * is situated to low, when the calculated value is above the defined threshold. The value is low-capped at 10.
     * The default value is 20.
     *
     * @param lowThreshold The new threshold to consider during calibration.
     * @return The {@link CalibrationSettings} objects containing this object for fluent method calls, including all settings.
     */
    public CalibrationSettings setTranslationLowThreshold(double lowThreshold) {
        if (!this.immutable) {
            this.translationThresholdLow = Math.max(10d, lowThreshold);
        }
        return this.calibrationSettings;
    }

    /**
     * Gets the threshold, which defines that the armband is situated too low on the user's arm.
     *
     * @return The low threshold for the translation calibration.
     */
    public double getTranslationThresholdLow() {
        return this.translationThresholdLow;
    }

    /**
     * Sets the threshold, which defines that the armband is situated to high on the users arm. The armband
     * is situated to high, when the calculated value is above the defined threshold. The value is low-capped at 10.
     * The default value is 30.
     *
     * @param highThreshold The new threshold to consider during calibration.
     * @return The {@link CalibrationSettings} objects containing this object for fluent method calls, including all settings.
     */
    public CalibrationSettings setTranslationHighThreshold(double highThreshold) {
        if (!this.immutable) {
            this.translationThresholdHigh = Math.max(10d, highThreshold);
        }
        return this.calibrationSettings;
    }

    /**
     * Gets the threshold, which defines that the armband is situated too high on the user's arm.
     *
     * @return The high threshold for the translation calibration.
     */
    public double getTranslationThresholdHigh() {
        return this.translationThresholdHigh;
    }

    /**
     * Describes the different modes, that can be used when interpolating the values of the EMG signal.
     */
    public enum EmgInterpolationMode {
        /**
         * The value at the given position is equal to the nearest measured value.
         */
        NEAREST_NEIGHBOR,
        /**
         * Interpolates via a line from measured value to measured value.
         */
        LINEAR,
        /**
         * Cubic hermite spline interpolation is applied to the data.
         */
        CUBIC_HERMITE_SPLINE;

        protected CyclicInterpolationFunction createNewFunction() {
            switch(this) {
                case NEAREST_NEIGHBOR:
                    return new NearestNeighborInterpolation();
                case LINEAR:
                    return new LinearInterpolation();
                case CUBIC_HERMITE_SPLINE:
                    return new CubicHermiteSplineInterpolation();
                default:
                    return new NearestNeighborInterpolation();
            }
        }
    }

}
