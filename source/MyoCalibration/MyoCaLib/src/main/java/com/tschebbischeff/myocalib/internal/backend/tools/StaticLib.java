package com.tschebbischeff.myocalib.internal.backend.tools;

/**
 * A collection of pure static methods.
 */
public class StaticLib {

    /**
     * Boxes an array of double values into an array of Double values.
     *
     * @param values The array of double values to box.
     * @return The boxed Double array.
     */
    public static Double[] boxDoubleArray(double[] values) {
        Double[] result = new Double[values.length];
        for (int i = 0; i < values.length; i++) {
            result[i] = values[i];
        }
        return result;
    }

    /**
     * Unboxes an array of Double values into an array of double values.
     *
     * @param values The array of Double values to unbox.
     * @return The unboxed double array.
     */
    public static double[] unboxDoubleArray(Double[] values) {
        double[] result = new double[values.length];
        for (int i = 0; i < values.length; i++) {
            result[i] = values[i];
        }
        return result;
    }

    /**
     * Creates a permutation of the input array based on the index mapping supplied in the form of the two
     * parameter arrays.
     *
     * @param input The array to permutate.
     * @param from  The array containing the indices from which to copy the values.
     * @return A permutation of the input array specified by the two arrays containing indices.
     */
    public static double[] arrayPermutation(double[] input, int[] from) {
        double[] result = new double[input.length];
        for (int i = 0; i < from.length; i++) {
            if (from[i] >= input.length) {
                continue;
            }
            result[i] = input[from[i]];
        }
        return result;
    }
}
