package com.tschebbischeff.common.android.logger.outputs;

import android.util.Log;

/**
 * Prints logging messages to the android console.
 */

public class LoggerOutputConsole extends LoggerOutput {

    /**
     * The equality constant defining all instances of this class as equal.
     */
    public static final String equalityConstant = "LoggerOutputConsole";

    /**
     * {@inheritDoc}
     */
    @Override
    public String getEqualityConstant() {
        return equalityConstant;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void error(String tag, String message) {
        Log.e(tag, message);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void warning(String tag, String message) {
        Log.w(tag, message);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void info(String tag, String message) {
        Log.i(tag, message);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void debug(String tag, String message) {
        Log.d(tag, message);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void verbose(String tag, String message) {
        Log.v(tag, message);
    }
}
