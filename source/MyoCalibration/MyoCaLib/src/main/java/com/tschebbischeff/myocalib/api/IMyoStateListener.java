package com.tschebbischeff.myocalib.api;

import eu.darken.myolib.Myo;

/**
 * Implementors of this interface can be added via {@link MyoCaLib#addMyoStateListener(IMyoStateListener)} and
 * are automatically notified, whenever a Myo changes state either connection-wise or calibration-wise.
 * See the individual methods documentation for more information.
 */
public interface IMyoStateListener {

    /**
     * The user has issued a connect via the connect button in the internal activity and the Myo was successfully
     * connected.
     *
     * @param myo The Myo that has been connected due to the user clicking the connect button.
     */
    void onMyoConnectedForUsage(Myo myo);

    /**
     * The Myo has been disconnected without the user clicking the disconnect button.
     * It may or may not be back in the future. A reconnect is already issued.
     *
     * @param myo The Myo that has disconnected abnormally and will be reconnected ASAP.
     */
    void onMyoDisconnectedAbnormally(Myo myo);

    /**
     * The Myo has been automatically reconnected after being disconnected for an uncertain amount of time.
     *
     * @param myo The Myo that has been reconnected.
     */
    void onMyoConnectedAfterDisconnect(Myo myo);

    /**
     * The Myo has been disconnected due to the user clicking the disconnect button.
     *
     * @param myo The myo that has been disconnected.
     */
    void onMyoDisconnected(Myo myo);

    /**
     * The Myo has been disconnected, due to the API service shutting down.
     *
     * @param myo The Myo that has been disconnected.
     */
    void onMyoDisconnectedAtShutdown(Myo myo);

    /**
     * A connected Myo has been successfully calibrated.
     *
     * @param myo The Myo that has been calibrated.
     */
    void onMyoCalibrated(Myo myo);

    /**
     * A prior to this call's Myo's calibration is now not valid anymore, either due to
     * the user manually voiding it, or due to changing calibration factors being detected.
     *
     * @param myo The Myo which's calibration is no longer valid.
     */
    void onMyoUncalibrated(Myo myo);
}
