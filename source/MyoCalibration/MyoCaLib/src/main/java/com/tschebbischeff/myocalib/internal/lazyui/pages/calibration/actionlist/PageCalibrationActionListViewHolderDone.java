package com.tschebbischeff.myocalib.internal.lazyui.pages.calibration.actionlist;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.tschebbischeff.common.android.logger.Logger;
import com.tschebbischeff.common.math.Quat4d;
import com.tschebbischeff.common.math.Vector3d;
import com.tschebbischeff.myocalib.R;
import com.tschebbischeff.myocalib.api.EmgCalibrationSettings;
import com.tschebbischeff.myocalib.api.ImuCalibrationSettings;
import com.tschebbischeff.myocalib.internal.backend.calibration.*;
import com.tschebbischeff.myocalib.internal.backend.calibration.interpolation.LinearInterpolation;
import com.tschebbischeff.myocalib.internal.lazyui.pages.calibration.CalibrationConfig;
import com.tschebbischeff.myocalib.internal.lazyui.pages.calibration.PageCalibration;
import eu.darken.myolib.Myo;

import java.util.EnumSet;

/**
 * Realizes the {@link PageCalibrationActionListAdapter.ViewType#DONE} action.
 * <p>
 * Displays the message, that the calibration is successfully done.
 */
public class PageCalibrationActionListViewHolderDone extends PageCalibrationActionListViewHolder {

    /**
     * The text view showing the stub done-message.
     */
    public TextView text;
    /**
     * The progress bar, which is showing the user how long to hold the extension.
     */
    public ProgressBar progressBar;
    /**
     * Indeterminate starting timestamp for the animation
     */
    private long indeterminateTimestamp;
    /**
     * Whether all results are calculated.
     */
    private volatile boolean done;
    /**
     * Whether all results are calculated.
     */
    private volatile EnumSet<RotationTranslationResult> rotationTranslationResult;
    /**
     * The thread executing the background computation.
     */
    private volatile boolean terminateBackgroundTask;
    /**
     * The runnable calculating the results (to not block the UI thread)
     */
    private Runnable threadedComputation = new Runnable() {
        @Override
        public void run() {
            //Get data collectors, calibration settings and some stuff
            DataCollector biasXCollector = getSource().getBiasXCollector();
            DataCollector biasYCollector = getSource().getBiasYCollector();
            DataCollector biasZCollector = getSource().getBiasZCollector();
            DataCollector rvcDownEmgCollector = getSource().getRvcDownEmgCollector();
            DataCollector rvcDownImuAcclCollector = getSource().getRvcDownImuAcclCollector();
            DataCollector rvcDownImuGyroCollector = getSource().getRvcDownImuGyroCollector();
            DataCollector rvcDownImuOrientationCollector = getSource().getRvcDownImuOrientationCollector();
            DataCollector rvcForwardEmgCollector = getSource().getRvcForwardEmgCollector();
            DataCollector rvcForwardImuAcclCollector = getSource().getRvcForwardImuAcclCollector();
            DataCollector rvcForwardImuGyroCollector = getSource().getRvcForwardImuGyroCollector();
            DataCollector rvcForwardImuOrientationCollector = getSource().getRvcForwardImuOrientationCollector();
            DataCollector rvcEmgCollectorMerged = rvcDownEmgCollector.copy().merge(rvcForwardEmgCollector);
            DataCollector rvcImuAcclCollectorMerged = rvcDownImuAcclCollector.copy().merge(rvcForwardImuAcclCollector);
            DataCollector rvcImuGyroCollectorMerged = rvcDownImuGyroCollector.copy().merge(rvcForwardImuGyroCollector);
            DataCollector rvcImuOrientationCollectorMerged = rvcDownImuOrientationCollector.copy().merge(rvcForwardImuOrientationCollector);
            ImuCalibrationSettings csImu = getSource().getSharedPageData().getApiService().getCalibrationSettings().imu;
            EmgCalibrationSettings csEmg = getSource().getSharedPageData().getApiService().getCalibrationSettings().emg;
            WritableCalibrationProfile calibrationProfile = getSource().getSharedPageData().getWritableApiInterface().getCalibrationProfile(getSource().getSharedPageData().getCalibratingMyo());
            String myoDeviceAddress = getSource().getSharedPageData().getCalibratingMyo().getDeviceAddress();
            //IMU
            if (csImu.isCalibrationEnabled()) {
                if (csImu.isGlobalCoordinateSystemEnabled()) {
                    //Orientation: in forward state is defined as zero, so invert the internal rotation of the Myo from that state
                    calibrationProfile.setOrientation(
                            new Quat4d(rvcForwardImuOrientationCollector.copy()
                                    //.applyFilterPerIndex(new MeanFilter()) //NOTE: This only works, if the quaternions are roughly similar!
                                    .getLastValue())
                                    .normalize()
                                    .conjugate()
                    );
                    calibrationProfile.setImuOrientation(
                    new Quat4d(new Vector3d(rvcForwardImuAcclCollector.applyFilterPerIndex(new MeanFilter()).getLastValue()), new Vector3d(0d,0d,1d)));
                }
                if (csImu.isAccelerometerBiasCalibrationEnabled()) {
                    Context appContext = getActivity().getApplicationContext();
                    SharedPreferences sharedPref = appContext.getSharedPreferences(appContext.getString(R.string.myocalib_sharedprefs_path_myosettings) + "_" + myoDeviceAddress, Context.MODE_PRIVATE);
                    if (csImu.isForceAccelerometerBiasCalibrationEnabled() ||
                            !sharedPref.contains(appContext.getString(R.string.myocalib_myosettings_acclBiasX)) ||
                            !sharedPref.contains(appContext.getString(R.string.myocalib_myosettings_acclBiasY)) ||
                            !sharedPref.contains(appContext.getString(R.string.myocalib_myosettings_acclBiasZ))) {
                        double xBias = biasXCollector.copy()
                                .applyFilterPerTimestamp(new PNormFilter(3d))
                                .applyFilterPerIndex(new MeanFilter(), 0)
                                .getLastValue(0);
                        xBias = xBias - (xBias > 0 ? 1 : -1);
                        Logger.debug("CalibrationCalculations", "xBias is: " + xBias);
                        double yBias = biasYCollector.copy()
                                .applyFilterPerTimestamp(new PNormFilter(3d))
                                .applyFilterPerIndex(new MeanFilter(), 0)
                                .getLastValue(0);
                        yBias = yBias - (yBias > 0 ? 1 : -1);
                        Logger.debug("CalibrationCalculations", "yBias is: " + yBias);
                        double zBias = biasZCollector.copy()
                                .applyFilterPerTimestamp(new PNormFilter(3d))
                                .applyFilterPerIndex(new MeanFilter(), 0)
                                .getLastValue(0);
                        zBias = zBias - (zBias > 0 ? 1 : -1);
                        Logger.debug("CalibrationCalculations", "zBias is: " + zBias);
                        calibrationProfile
                                .setAccelerometerBiasX(xBias)
                                .setAccelerometerBiasY(yBias)
                                .setAccelerometerBiasZ(zBias);
                        SharedPreferences.Editor editor = sharedPref.edit();
                        editor.putFloat(appContext.getString(R.string.myocalib_myosettings_acclBiasX), (float) xBias);
                        editor.putFloat(appContext.getString(R.string.myocalib_myosettings_acclBiasY), (float) yBias);
                        editor.putFloat(appContext.getString(R.string.myocalib_myosettings_acclBiasZ), (float) zBias);
                        if (!editor.commit()) {
                            Logger.warning("CalibrationCalculations", "Could not save biases! Recalibration needed next time!");
                        }
                    } else {
                        Logger.debug("CalibrationCalculations", "Loading biases from file ...");
                        calibrationProfile
                                .setAccelerometerBiasX(sharedPref.getFloat(appContext.getString(R.string.myocalib_myosettings_acclBiasX), 0f))
                                .setAccelerometerBiasY(sharedPref.getFloat(appContext.getString(R.string.myocalib_myosettings_acclBiasY), 0f))
                                .setAccelerometerBiasZ(sharedPref.getFloat(appContext.getString(R.string.myocalib_myosettings_acclBiasZ), 0f));
                    }
                }
            }
            if (terminateBackgroundTask) {
                return;
            }
            //EMG
            if (csEmg.isCalibrationEnabled()) {
                if (csEmg.isNormalizationEnabled()) {
                    double normalizationFactor = CalibrationConfig.NORMALIZATION_TARGET_VALUE / rvcEmgCollectorMerged.copy()
                            .applyFilterPerTimestamp(new FullWaveRectificationFilter())
                            .applyFilterPerTimestamp(new MeanFilter())
                            .applyFilterPerIndex(new MovingAverageFilter(CalibrationConfig.NORMALIZATION_MV_AVG_WINDOW_SIZE), 0)
                            .applyFilterPerIndex(new MeanFilter(), 0)
                            .getLastValue(0);
                    Logger.debug("CalibrationCalculations", "Normalization factor is: " + normalizationFactor);
                    calibrationProfile.setNormalizationFactor(normalizationFactor);
                }
                if (terminateBackgroundTask) {
                    return;
                }
                if (csEmg.isRotationCalibrationEnabled() || csEmg.isTranslationCalibrationEnabled()) {
                    //Rotation angle used for both
                    Vector3d gRef = new Vector3d(new double[]{0d, 0d, 1d});
                    Vector3d gRotYZ = new Vector3d(rvcForwardImuAcclCollector.copy()
                            .applyFilterPerIndex(new MeanFilter(), 0, 1, 2)
                            .getLastValue());
                    gRotYZ = gRotYZ.setX(0d).normalize();
                    Vector3d gCross = gRef.cross(gRotYZ);
                    double directionFactor = (calibrationProfile.isMyoOnRightArm() ? 1 : -1) *
                            (calibrationProfile.isUsbFacingWrist() ? 1 : -1) *
                            (gCross.getX() > 0 ? 1 : -1);
                    double rotationAngle = directionFactor * Math.acos(gRef.dot(gRotYZ) / (gRotYZ.len()));
                    if (terminateBackgroundTask) {
                        return;
                    }

                    //Rotation
                    if (csEmg.isRotationCalibrationEnabled()) {
                        Logger.debug("CalibrationCalculations", "Rotation angle is: " + rotationAngle + "(" + Math.toDegrees(rotationAngle) + ")");
                        if (csEmg.isForceRotationEnabled() && Math.abs(rotationAngle) > Math.toRadians(csEmg.getForceRotationThreshold())) {
                            if (rotationAngle > 0) {
                                rotationTranslationResult.add(RotationTranslationResult.ROTATION_COUNTERCLOCKWISE);
                            } else {
                                rotationTranslationResult.add(RotationTranslationResult.ROTATION_CLOCKWISE);
                            }
                            rotationTranslationResult.remove(RotationTranslationResult.SUCCESS);
                        } else {
                            calibrationProfile.setRotationAngle(rotationAngle);
                        }
                    }
                    if (terminateBackgroundTask) {
                        return;
                    }

                    //Translation
                    if (csEmg.isTranslationCalibrationEnabled()) {
                        int[] permutation;
                        if (calibrationProfile.isMyoOnLeftArm() ^ calibrationProfile.isUsbFacingElbow()) {
                            //Make the pod with the LEDs the new index zero but don't inverse order
                            permutation = new int[]{3, 4, 5, 6, 7, 0, 1, 2};
                        } else {
                            //Make the pod with the LEDs the new index zero and inverse order
                            permutation = new int[]{3, 2, 1, 0, 7, 6, 5, 4};
                        }
                        //Fix rotation, rectify, normalize, average and apply spread-interpolation
                        DataCollector preprocessedData = rvcForwardEmgCollector.copy()
                                .applyPermutation(permutation)
                                .applyFilterPerTimestamp(new FullWaveRectificationFilter())
                                .applyFilterPerTimestamp(new ScalingFilter(calibrationProfile.getNormalizationFactor()))
                                .applyFilterPerIndex(new MovingAverageFilter(CalibrationConfig.EMG_MOVING_AVERAGE_WINDOW_SIZE));
                        if (terminateBackgroundTask) {
                            return;
                        }

                        double sLow = new DataCollector().store(0, preprocessedData.copy()
                                .applyFilterPerIndex(new MeanFilter())
                                .getLastValue())
                                .applyFilterPerTimestamp(new InterpolatingDistanceFilter(Math.PI / 4d, Math.PI / 2d, new LinearInterpolation(), rotationAngle))
                                .getLastValue(0) / calibrationProfile.getNormalizationFactor();
                        /*double sLow = preprocessedData.copy()
                                .applyFilterPerTimestamp(new InterpolatingDistanceFilter(Math.PI / 4d, Math.PI / 2d, new LinearInterpolation(), rotationAngle))
                                .applyFilterPerIndex(new MeanFilter(), 0)
                                .getLastValue(0);*/
                        Logger.debug("CalibrationCalculations", "sLow is: " + sLow);// + "| sLowA: " + sLowA);
                        if (terminateBackgroundTask) {
                            return;
                        }

                        double sHigh = new DataCollector().store(0, preprocessedData.copy()
                                .applyFilterPerIndex(new MeanFilter())
                                .getLastValue())
                                .applyFilterPerTimestamp(new InterpolatingDistanceFilter(0d, Math.PI / 4d, new LinearInterpolation(), rotationAngle))
                                .getLastValue(0) / calibrationProfile.getNormalizationFactor();
                        /*double sHigh = preprocessedData.copy()
                                .applyFilterPerTimestamp(new InterpolatingDistanceFilter(0d, Math.PI / 4d, new LinearInterpolation(), rotationAngle))
                                .applyFilterPerIndex(new MeanFilter(), 0)
                                .getLastValue(0);*/
                        Logger.debug("CalibrationCalculations", "sHigh is: " + sHigh);// + "| sHighA: " + sHighA);
                        if (terminateBackgroundTask) {
                            return;
                        }
                        if (sLow > csEmg.getTranslationThresholdLow() || sHigh > csEmg.getTranslationThresholdHigh()) {
                            if (sLow > csEmg.getTranslationThresholdLow()) {
                                rotationTranslationResult.add(RotationTranslationResult.TRANSLATION_ELBOW);
                            } else if (sHigh > csEmg.getTranslationThresholdHigh()) { //sHigh may be enormous if armband is placed to low. sLow takes preference
                                rotationTranslationResult.add(RotationTranslationResult.TRANSLATION_WRIST);
                            }
                            rotationTranslationResult.remove(RotationTranslationResult.SUCCESS);
                        }
                    }
                    if (terminateBackgroundTask) {
                        return;
                    }
                }
            }
            if (terminateBackgroundTask) {
                return;
            }
            if (!rotationTranslationResult.contains(RotationTranslationResult.SUCCESS)) {
                getAdapter().handleRotationTranslationError(rotationTranslationResult);
            }
            //Done, free all data, only relevant for this run
            rvcEmgCollectorMerged.reset();
            rvcImuAcclCollectorMerged.reset();
            rvcImuGyroCollectorMerged.reset();
            rvcImuOrientationCollectorMerged.reset();
            done = true;
        }
    };

    /**
     * Constructor.
     *
     * @param source   The calibration page, for callbacks.
     * @param activity The android context, for calling certain android methods.
     * @param rootView The view at the root of the cards layout.
     */
    public PageCalibrationActionListViewHolderDone(PageCalibration source, PageCalibrationActionListAdapter adapter, Activity activity, View rootView) {
        super(source, adapter, activity, PageCalibrationActionListAdapter.ViewType.DONE, rootView);
        this.text = rootView.findViewById(R.id.myocalib_lazyui_pages_calibration_cardsdone_text);
        this.progressBar = rootView.findViewById(R.id.myocalib_lazyui_pages_calibration_cardsdone_progress);
        this.progressBar.setMax((int) (CalibrationConfig.INDETERMINATE_ANIMATION_CYCLE_TIME - CalibrationConfig.INDETERMINATE_ANIMATION_BAR_SIZE));
        this.reset();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void reset() {
        this.indeterminateTimestamp = -1;
        this.rotationTranslationResult = EnumSet.of(RotationTranslationResult.SUCCESS);
        this.done = false;
        this.terminateBackgroundTask = false;
        this.progressBar.setProgress(0);
        this.progressBar.setSecondaryProgress(0);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onDestroy() {
        this.terminateBackgroundTask = true;
    }

    /**
     * {@inheritDoc}
     *
     * @param view
     */
    @Override
    public void onClick(View view) {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onRawEmgData(Myo myo, long timestamp, double[] data) {
        if (this.indeterminateTimestamp == -1) {
            this.indeterminateTimestamp = System.currentTimeMillis();
            new Thread(this.threadedComputation).start();
        }
        long elapsedAnimationTime = (System.currentTimeMillis() - this.indeterminateTimestamp) * CalibrationConfig.INDETERMINATE_ANIMATION_SPEED;
        while (elapsedAnimationTime > CalibrationConfig.INDETERMINATE_ANIMATION_CYCLE_TIME) {
            this.indeterminateTimestamp += CalibrationConfig.INDETERMINATE_ANIMATION_CYCLE_TIME / CalibrationConfig.INDETERMINATE_ANIMATION_SPEED;
            elapsedAnimationTime -= CalibrationConfig.INDETERMINATE_ANIMATION_CYCLE_TIME / CalibrationConfig.INDETERMINATE_ANIMATION_SPEED;
        }
        if (elapsedAnimationTime < CalibrationConfig.INDETERMINATE_ANIMATION_BAR_SIZE) {
            this.progressBar.setSecondaryProgress(0);
        }
        this.progressBar.setSecondaryProgress((int) Math.max(0, elapsedAnimationTime - CalibrationConfig.INDETERMINATE_ANIMATION_BAR_SIZE));
        this.progressBar.setProgress((int) Math.min(elapsedAnimationTime, CalibrationConfig.INDETERMINATE_ANIMATION_CYCLE_TIME - CalibrationConfig.INDETERMINATE_ANIMATION_BAR_SIZE));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onRawImuData(Myo myo, long timestamp, double[] accelerometerData, double[] gyroscopeData, double[] orientationData) {
        if (this.done) { //Don't check as often as the animation (This is called with approx. 50Hz)
            this.getSource().stepCompleted();
        }
    }

    /**
     * Describes the result, that the calculation of the rotation and translation returned.
     */
    public enum RotationTranslationResult {
        /**
         * Everything is calculated and no errors were detected.
         */
        SUCCESS,
        /**
         * The armband must be rotated clockwise.
         */
        ROTATION_CLOCKWISE,
        /**
         * The armband must be rotated counterclockwise.
         */
        ROTATION_COUNTERCLOCKWISE,
        /**
         * The armband must be moved towards the wrist.
         */
        TRANSLATION_WRIST,
        /**
         * The armband must be moved towards the elbow.
         */
        TRANSLATION_ELBOW
    }
}
