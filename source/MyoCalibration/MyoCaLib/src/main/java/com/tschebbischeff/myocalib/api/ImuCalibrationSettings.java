package com.tschebbischeff.myocalib.api;

/**
 * Encapsulates all settings regarding the IMU calibration.
 * The design follows the principle of fluent method calls via the encapsulating {@link CalibrationSettings} class.
 * The returned instance of that class is always the instance which this object is the {@link CalibrationSettings#imu}
 * instance of.
 */
public final class ImuCalibrationSettings {

    /**
     * Whether the calibration settings are marked as final by the application.
     */
    private boolean immutable;
    /**
     * The source that is used for fluent method calls, including method calls to other sub settings
     */
    private CalibrationSettings calibrationSettings;
    /**
     * Whether the calibration of IMU data is activated at all.
     */
    private boolean calibrationEnabled = true;
    /**
     * Whether the calibration of the accelerometers possible bias is activated.
     */
    private boolean accelerometerBiasCalibrationEnabled = false;
    /**
     * Whether to force the recalibration of the accelerometer bias.
     */
    private boolean forceAccelerometerBiasCalibrationEnabled = false;
    /**
     * Whether the data is transformed into a coordinate system implicitly spanned by the user providing
     * a forward vector or not. It does not affect length of the actual calibration by the user,
     * but it may affect performance.
     */
    private boolean globalCoordinateSystemEnabled = true;

    /**
     * Protected constructor, objects may only be created by {@link CalibrationSettings}.
     */
    protected ImuCalibrationSettings(CalibrationSettings source) {
        this.calibrationSettings = source;
        this.immutable = false;
    }

    /**
     * Sets the settings as final, called when calling the general settings class done-method.
     * Action can not be undone, until a new object of this class is created.
     */
    protected void done() {
        this.immutable = true;
    }

    /**
     * Enables the calibration of the IMU data.
     * This is turned on per default.
     *
     * @return This object for fluent method calls.
     */
    public CalibrationSettings enableCalibration() {
        if (!this.immutable) {
            this.calibrationEnabled = true;
        }
        return this.calibrationSettings;
    }

    /**
     * Disables the calibration of the IMU data.
     * This is turned on per default.
     *
     * @return The {@link CalibrationSettings} objects containing this object for fluent method calls, including all settings.
     */
    public CalibrationSettings disableCalibration() {
        if (!this.immutable) {
            this.calibrationEnabled = false;
        }
        return this.calibrationSettings;
    }

    /**
     * Activates the calibration of the accelerometer bias.
     * This significantly increases the workload on the user!
     * This is turned off per default.
     *
     * @return The {@link CalibrationSettings} objects containing this object for fluent method calls, including all settings.
     */
    public CalibrationSettings enableAccelerometerBiasCalibration() {
        if (!this.immutable) {
            this.accelerometerBiasCalibrationEnabled = true;
        }
        return this.calibrationSettings;
    }

    /**
     * Deactivates the calibration of the accelerometer bias.
     * This is turned off per default.
     *
     * @return The {@link CalibrationSettings} objects containing this object for fluent method calls, including all settings.
     */
    public CalibrationSettings disableAccelerometerBiasCalibration() {
        if (!this.immutable) {
            this.accelerometerBiasCalibrationEnabled = false;
        }
        return this.calibrationSettings;
    }

    /**
     * Forces the user to recalibrate the accelerometer bias, even if saved biases are found for the Myo.
     * This applies only if the calibration of the accelerometer bias is enabled.
     *
     * @return The {@link CalibrationSettings} object containing this object for fluent method calls including all settings.
     */
    public CalibrationSettings enableForceAccelerometerBiasCalibration() {
        if (!this.immutable) {
            this.forceAccelerometerBiasCalibrationEnabled = true;
        }
        return this.calibrationSettings;
    }

    /**
     * Does not force the user to recalibrate the accelerometer bias, if saved biases are found for the Myo.
     * The saved biases are loaded instead.
     * This applies only if the calibration of the accelerometer bias is enabled.
     *
     * @return The {@link CalibrationSettings} object containing this object for fluent method calls including all settings.
     */
    public CalibrationSettings disableForceAccelerometerBiasCalibration() {
        if (!this.immutable) {
            this.forceAccelerometerBiasCalibrationEnabled = false;
        }
        return this.calibrationSettings;
    }

    /**
     * Activates the transformation of the IMUs data into a global coordinate system.
     * The global coordinate system is spanned from the forward vector and downward vector provided by the user.
     * Yes this works in space. Detection of the USB port however does sadly not.
     * This is turned on per default.
     *
     * @return The {@link CalibrationSettings} objects containing this object for fluent method calls, including all settings.
     */
    public CalibrationSettings enableGlobalCoordinateSystem() {
        if (!this.immutable) {
            this.globalCoordinateSystemEnabled = true;
        }
        return this.calibrationSettings;
    }

    /**
     * Deactivates the transformation of the IMUs data into a global coordinate system.
     * This is turned on per default.
     *
     * @return The {@link CalibrationSettings} objects containing this object for fluent method calls, including all settings.
     */
    public CalibrationSettings disableGlobalCoordinateSystem() {
        if (!this.immutable) {
            this.globalCoordinateSystemEnabled = false;
        }
        return this.calibrationSettings;
    }

    /**
     * Whether the calibration of IMU data is currently enabled.
     *
     * @return True if, and only if, the calibration of IMU data forwarded to listeners is currently activated, which it is per default.
     */
    public boolean isCalibrationEnabled() {
        return this.calibrationEnabled;
    }

    /**
     * Whether the calibration of the accelerometer's bias is currently enabled.
     *
     * @return True if, and only if, the calibration of the accelerometer's bias is enabled, which it is NOT per default.
     */
    public boolean isAccelerometerBiasCalibrationEnabled() {
        return accelerometerBiasCalibrationEnabled;
    }

    /**
     * Whether the calibration of the accelerometer's bias is currently forced even if saved biases exist.
     *
     * @return True if, and only if, the calibration of the accelerometer's bias is forced, even if saved biases are found.
     */
    public boolean isForceAccelerometerBiasCalibrationEnabled() {
        return forceAccelerometerBiasCalibrationEnabled;
    }

    /**
     * Whether all data recorded by the IMU is transformed into a global coordinate system, which is provided by
     * the user in form of down and forward vectors.
     *
     * @return True if, and only if, all IMU data is currently transformed into a global coordinate system.
     */
    public boolean isGlobalCoordinateSystemEnabled() {
        return globalCoordinateSystemEnabled;
    }
}
