package com.tschebbischeff.myocalib.internal.backend.exceptions;

public class UnfinishedSettingsException extends RuntimeException {

    public UnfinishedSettingsException() {
        super("Tried to show activity before the calibration settings were finalized via the done method. Make sure you handle settings before starting the activity.");
    }
}
