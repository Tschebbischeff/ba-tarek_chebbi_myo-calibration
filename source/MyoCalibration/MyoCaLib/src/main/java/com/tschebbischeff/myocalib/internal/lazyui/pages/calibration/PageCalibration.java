package com.tschebbischeff.myocalib.internal.lazyui.pages.calibration;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import com.tschebbischeff.common.android.layout.managers.UnscrollableLinearLayoutManager;
import com.tschebbischeff.common.android.logger.Logger;
import com.tschebbischeff.myocalib.R;
import com.tschebbischeff.myocalib.api.IRawDataOutputListener;
import com.tschebbischeff.myocalib.internal.backend.calibration.DataCollector;
import com.tschebbischeff.myocalib.internal.lazyui.PageList;
import com.tschebbischeff.myocalib.internal.lazyui.pages.Page;
import com.tschebbischeff.myocalib.internal.lazyui.pages.calibration.actionlist.PageCalibrationActionListAdapter;
import com.tschebbischeff.myocalib.internal.lazyui.pages.calibration.actionlist.PageCalibrationActionListViewHolder;
import eu.darken.myolib.Myo;

/**
 * Shows all of the calibration process to the user.
 * Including the list of Myos and whether they are calibrated, as well as the calibration process itself,
 * by showing a list of actions the user has to perform one after another.
 */
public class PageCalibration extends Page implements IRawDataOutputListener {

    /**
     * The adapter of the action list recycler view.
     */
    private PageCalibrationActionListAdapter actionListAdapter;
    /**
     * Stores the EMG data collected by the cards during performances of the RVC estimation.
     */
    private DataCollector rvcDownEmgCollector;
    /**
     * Stores the IMU accelerometer data collected by the cards during performances of the RVC estimation.
     */
    private DataCollector rvcDownImuAcclCollector;
    /**
     * Stores the IMU accelerometer data collected by the cards during performances of the RVC estimation.
     */
    private DataCollector rvcDownImuGyroCollector;
    /**
     * Stores the IMU orientation data collected by the cards during performances of the RVC estimation.
     */
    private DataCollector rvcDownImuOrientationCollector;
    /**
     * Stores the EMG data collected by the cards during performances of the RVC estimation.
     */
    private DataCollector rvcForwardEmgCollector;
    /**
     * Stores the IMU accelerometer data collected by the cards during performances of the RVC estimation.
     */
    private DataCollector rvcForwardImuAcclCollector;
    /**
     * Stores the IMU accelerometer data collected by the cards during performances of the RVC estimation.
     */
    private DataCollector rvcForwardImuGyroCollector;
    /**
     * Stores the IMU orientation data collected by the cards during performances of the RVC estimation.
     */
    private DataCollector rvcForwardImuOrientationCollector;
    /**
     * Collects IMU accelerometer data relevant for bias calculation on x-axis.
     */
    private DataCollector biasXCollector;
    /**
     * Collects IMU accelerometer data relevant for bias calculation on y-axis.
     */
    private DataCollector biasYCollector;
    /**
     * Collects IMU accelerometer data relevant for bias calculation on z-axis.
     */
    private DataCollector biasZCollector;

    /**
     * {@inheritDoc}
     */
    @Override
    protected int getLayoutResourceId() {
        return R.layout.myocalib_lazyui_pages_calibration_calibrate;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onCreate() {
        this.rvcDownEmgCollector = new DataCollector();
        this.rvcDownImuAcclCollector = new DataCollector();
        this.rvcDownImuGyroCollector = new DataCollector();
        this.rvcDownImuOrientationCollector = new DataCollector();
        this.rvcForwardEmgCollector = new DataCollector();
        this.rvcForwardImuAcclCollector = new DataCollector();
        this.rvcForwardImuGyroCollector = new DataCollector();
        this.rvcForwardImuOrientationCollector = new DataCollector();
        this.biasXCollector = new DataCollector();
        this.biasYCollector = new DataCollector();
        this.biasZCollector = new DataCollector();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String[] getIntentFilterActions() {
        return new String[0];
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean onAfterShow() {
        if (this.getSharedPageData().getCalibratingMyo() == null) {
            Logger.error("PageCalibration", "Calibrating Myo was not set, switching back to connection page.");
            PageList.CONNECTION.show(this.getActivity());
            return false;
        }
        RecyclerView actionList = this.getActivity().findViewById(R.id.myocalib_lazyui_pages_calibration_calibrate_actionlist);
        actionList.setHasFixedSize(true);
        actionList.setLayoutManager(new UnscrollableLinearLayoutManager(this.getActivity()));
        this.actionListAdapter = new PageCalibrationActionListAdapter(this, this.getActivity());
        actionList.setAdapter(this.actionListAdapter);
        this.getSharedPageData().getWritableApiInterface().getCalibrationProfile(this.getSharedPageData().getCalibratingMyo()).startCalibration(this);
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean onBeforeShow(boolean forced) {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean onBeforeHide(boolean forced) {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onAfterHide() {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onDestroy() {
        if (this.getSharedPageData().getCalibratingMyo() != null) {
            this.getSharedPageData().getWritableApiInterface().getCalibrationProfile(this.getSharedPageData().getCalibratingMyo()).abortCalibration();
            this.actionListAdapter.onDestroy();
            //free collector data
            this.rvcDownEmgCollector.reset();
            this.rvcDownImuAcclCollector.reset();
            this.rvcDownImuGyroCollector.reset();
            this.rvcDownImuOrientationCollector.reset();
            this.rvcForwardEmgCollector.reset();
            this.rvcForwardImuAcclCollector.reset();
            this.rvcForwardImuGyroCollector.reset();
            this.rvcForwardImuOrientationCollector.reset();
            this.biasXCollector.reset();
            this.biasYCollector.reset();
            this.biasZCollector.reset();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean onBackPressed() {
        this.getSharedPageData().getWritableApiInterface().getCalibrationProfile(this.getSharedPageData().getCalibratingMyo()).abortCalibration();
        this.getSharedPageData().setCalibratingMyo(null);
        PageList.CONNECTION.show(this.getActivity());
        return false;
    }

    /**
     * Called whenever an action of the calibration process has been completed. Automatically terminates and
     * resets the page state, when the action list is empty and adds listeners for EMG and IMU data to the calibrated
     * Myo.
     */
    public void stepCompleted() {
        if (this.actionListAdapter.stepCompleted()) {
            this.getSharedPageData().getWritableApiInterface().getCalibrationProfile(this.getSharedPageData().getCalibratingMyo()).calibrationComplete();
            this.getSharedPageData().setCalibratingMyo(null);
            //free collector data
            this.rvcDownEmgCollector.reset();
            this.rvcDownImuAcclCollector.reset();
            this.rvcDownImuGyroCollector.reset();
            this.rvcDownImuOrientationCollector.reset();
            this.rvcForwardEmgCollector.reset();
            this.rvcForwardImuAcclCollector.reset();
            this.rvcForwardImuGyroCollector.reset();
            this.rvcForwardImuOrientationCollector.reset();
            this.biasXCollector.reset();
            this.biasYCollector.reset();
            this.biasZCollector.reset();
            this.getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    PageList.CONNECTION.show(PageCalibration.this.getActivity());
                }
            });
        }
    }

    /**
     * Called when a recycler view element from the action list recycler view is clicked.
     *
     * @param view       The view that was clicked.
     * @param viewHolder The viewHolder containing the view, i.e. the card that was clicked.
     */
    public void onRecyclerViewClick(View view, PageCalibrationActionListViewHolder viewHolder) {
        this.actionListAdapter.onClick(view, viewHolder);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onRawEmgData(Myo myo, long timestamp, double[] data) {
        //propagate
        this.actionListAdapter.onRawEmgData(myo, timestamp, data);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onRawImuData(Myo myo, long timestamp, double[] accelerometerData, double[] gyroscopeData, double[] orientationData) {
        //propagate
        this.actionListAdapter.onRawImuData(myo, timestamp, accelerometerData, gyroscopeData, orientationData);
    }

    /**
     * Gets the instance of the DataCollector in which data relevant for normalization is stored.
     *
     * @return The collector used to collect data relevant for normalization.
     */
    public DataCollector getRvcDownEmgCollector() {
        return this.rvcDownEmgCollector;
    }

    /**
     * Gets the instance of the DataCollector in which data relevant for normalization is stored.
     *
     * @return The collector used to collect data relevant for normalization.
     */
    public DataCollector getRvcForwardEmgCollector() {
        return this.rvcForwardEmgCollector;
    }

    /**
     * Gets the instance of the DataCollector in which data relevant for rotation is stored.
     *
     * @return The collector used to collect data relevant for rotation.
     */
    public DataCollector getRvcDownImuAcclCollector() {
        return this.rvcDownImuAcclCollector;
    }

    /**
     * Gets the instance of the DataCollector in which data relevant for rotation is stored.
     *
     * @return The collector used to collect data relevant for rotation.
     */
    public DataCollector getRvcForwardImuAcclCollector() {
        return this.rvcForwardImuAcclCollector;
    }

    /**
     * Gets the instance of the DataCollector in which data relevant for rotation is stored.
     *
     * @return The collector used to collect data relevant for rotation.
     */
    public DataCollector getRvcDownImuGyroCollector() {
        return this.rvcDownImuGyroCollector;
    }

    /**
     * Gets the instance of the DataCollector in which data relevant for rotation is stored.
     *
     * @return The collector used to collect data relevant for rotation.
     */
    public DataCollector getRvcForwardImuGyroCollector() {
        return this.rvcForwardImuGyroCollector;
    }

    /**
     * Gets the instance of the DataCollector in which data relevant for rotation is stored.
     *
     * @return The collector used to collect data relevant for rotation.
     */
    public DataCollector getRvcDownImuOrientationCollector() {
        return this.rvcDownImuOrientationCollector;
    }

    /**
     * Gets the instance of the DataCollector in which data relevant for rotation is stored.
     *
     * @return The collector used to collect data relevant for rotation.
     */
    public DataCollector getRvcForwardImuOrientationCollector() {
        return this.rvcForwardImuOrientationCollector;
    }

    /**
     * Gets the instance of the DataCollector in which data relevant for accelerometer x bias estimation is stored.
     *
     * @return The collector used to collect data relevant for bias on x-axis.
     */
    public DataCollector getBiasXCollector() {
        return this.biasXCollector;
    }

    /**
     * Gets the instance of the DataCollector in which data relevant for accelerometer y bias estimation is stored.
     *
     * @return The collector used to collect data relevant for bias on y-axis.
     */
    public DataCollector getBiasYCollector() {
        return this.biasYCollector;
    }

    /**
     * Gets the instance of the DataCollector in which data relevant for accelerometer z bias estimation is stored.
     *
     * @return The collector used to collect data relevant for bias on z-axis.
     */
    public DataCollector getBiasZCollector() {
        return this.biasZCollector;
    }
}
