package com.tschebbischeff.myocalib.internal.lazyui.pages.calibration.actionlist;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import com.tschebbischeff.myocalib.R;
import com.tschebbischeff.myocalib.api.IRawDataOutputListener;
import com.tschebbischeff.myocalib.internal.lazyui.pages.calibration.PageCalibration;

/**
 * Superclass for all action list cards.
 * Encapsulates those views, that each of the cards must have. Extending classes add views inherent to the action.
 */
public abstract class PageCalibrationActionListViewHolder extends RecyclerView.ViewHolder implements IRawDataOutputListener {

    /**
     * The number of steps until the tint reaches a complete white. A TINT_STEPS of X means, that X cards will be
     * gradually whiter, beginning with 0, in steps of 1/X, until the X+1th card reaches the value 1, meaning it will
     * be completely white.
     */
    private static final int TINT_STEPS = 3;
    /**
     * The view type associated with this view holder.
     */
    public PageCalibrationActionListAdapter.ViewType viewType;
    /**
     * The card view at the root of the layout.
     */
    public CardView cardView;
    /**
     * The translucent overlaying view, which is colored to create the card-to-card fading effect.
     */
    public View tintOverlay;
    /**
     * The calibration page for callbacks.
     */
    private PageCalibration source;
    /**
     * The adapter handling this holder.
     */
    private PageCalibrationActionListAdapter adapter;
    /**
     * The android activity for calling certain android methods.
     */
    private Activity activity;

    /**
     * Constructor.
     *
     * @param source   The calibration page, for callbacks.
     * @param activity  The android context, for calling certain android methods.
     * @param viewType The view type associated with this view holder.
     * @param rootView The view at the root of the cards layout.
     */
    public PageCalibrationActionListViewHolder(PageCalibration source, PageCalibrationActionListAdapter adapter, Activity activity, PageCalibrationActionListAdapter.ViewType viewType, View rootView) {
        super(rootView);
        this.source = source;
        this.adapter = adapter;
        this.activity = activity;
        this.viewType = viewType;
        this.cardView = rootView.findViewById(R.id.myocalib_lazyui_pages_calibration_cardsall_cardview);
        this.tintOverlay = rootView.findViewById(R.id.myocalib_lazyui_pages_calibration_cardsall_tintoverlay);
    }

    /**
     * Implementations of this method should clear the card and reset it to a state in which it can be shown as if it
     * was the first time.
     */
    public abstract void reset();

    /**
     * Relays the calibration page to extending classes.
     *
     * @return The calibration page, for callbacks.
     */
    protected PageCalibration getSource() {
        return this.source;
    }

    /**
     * Relays the adapter to extending classes.
     *
     * @return The adapter handling this view holder, for callbacks.
     */
    protected PageCalibrationActionListAdapter getAdapter() {
        return this.adapter;
    }

    /**
     * Relays the android context to extending classes.
     *
     * @return The android context, for calling certain android methods.
     */
    protected Activity getActivity() {
        return this.activity;
    }

    /**
     * Called when the android system has created an instance of this class and delegates the onBindViewHolder
     * to the adapter.
     *
     * @param position This view holders position in the recycler views list.
     */
    public void onBind(int position) {
        int backgroundColor = ContextCompat.getColor(this.activity, R.color.myocalib_colorBackgroundPrimary);
        int alpha = Math.min(255, Math.round(255f * ((float) position / (float) (TINT_STEPS))));
        this.tintOverlay.setBackgroundColor(Color.argb(
                alpha,
                Color.red(backgroundColor), Color.green(backgroundColor), Color.blue(backgroundColor)
        ));
    }

    /**
     * Called when the activity is closed.
     */
    public abstract void onDestroy();

    /**
     * This method is called, whenever an onClick event is registered by the calibration page.
     *
     * @param view The view that has been clicked.
     */
    abstract public void onClick(View view);
}
