package com.tschebbischeff.myocalib.internal.lazyui.pages.calibration.actionlist;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.tschebbischeff.common.android.logger.Logger;
import com.tschebbischeff.myocalib.R;
import com.tschebbischeff.myocalib.internal.lazyui.pages.calibration.CalibrationConfig;
import com.tschebbischeff.myocalib.internal.lazyui.pages.calibration.PageCalibration;
import eu.darken.myolib.Myo;

/**
 * Realizes the {@link PageCalibrationActionListAdapter.ViewType#IMU_X} action.
 * <p>
 * Displays progress on the calibration of the IMUs x-axis.
 */
public class PageCalibrationActionListViewHolderImuX extends PageCalibrationActionListViewHolder {
    /**
     * The text view showing stub text.
     */
    public TextView text;
    /**
     * The progress bar, which is showing the user how long to not move the arm.
     */
    public ProgressBar progressBar;
    /**
     * The view showing the descriptive image.
     */
    public ImageView imageView;
    /**
     * The timestamp at which the user was last recognized as not relaxed.
     */
    private long beginTimestamp;

    /**
     * Constructor.
     *
     * @param source   The calibration page, for callbacks.
     * @param activity  The android context, for calling certain android methods.
     * @param rootView The view at the root of the cards layout.
     */
    public PageCalibrationActionListViewHolderImuX(PageCalibration source, PageCalibrationActionListAdapter adapter, Activity activity, View rootView) {
        super(source, adapter, activity, PageCalibrationActionListAdapter.ViewType.RVC_DOWN_INIT, rootView);
        this.text = rootView.findViewById(R.id.myocalib_lazyui_pages_calibration_cardsimux_text);
        this.progressBar = rootView.findViewById(R.id.myocalib_lazyui_pages_calibration_cardsimux_progress);
        this.imageView = rootView.findViewById(R.id.myocalib_lazyui_pages_calibration_cardsimux_image);
        this.progressBar.setMax(CalibrationConfig.IMU_HOLD_TIME);
        PageCalibrationActionListOnClickListener onClickListener = new PageCalibrationActionListOnClickListener(source, this);
        this.cardView.setOnClickListener(onClickListener);
        this.reset();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void reset() {
        this.beginTimestamp = -2;
        this.progressBar.setProgress(0);
    }

    /**
     * {@inheritDoc}
     */
    public void onBind(int position) {
        super.onBind(position);
        if (this.beginTimestamp > -2) {
            this.progressBar.setVisibility(View.VISIBLE);
        } else {
            this.progressBar.setVisibility(View.GONE);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onDestroy() {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onClick(View view) {
        if (this.beginTimestamp == -2) {
            this.beginTimestamp = -1;
            try {
                this.getAdapter().notifyItemChanged(0);
            } catch (Exception ignored) {
                Logger.warning("PageCalibrationActionListViewHolderImuX", "Update of recycler view failed due to IllegalStateException");
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onRawEmgData(Myo myo, long timestamp, double[] data) {
        if (this.beginTimestamp > 0) {
            int elapsedTime = (int) (System.currentTimeMillis() - this.beginTimestamp);
            if (elapsedTime > CalibrationConfig.IMU_HOLD_TIME) {
                this.beginTimestamp = -2;
                this.getSource().stepCompleted();
            } else {
                this.progressBar.setProgress(elapsedTime);
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onRawImuData(Myo myo, long timestamp, double[] accelerometerData, double[] gyroscopeData, double[] orientationData) {
        if (this.beginTimestamp == -1) {
            this.beginTimestamp = System.currentTimeMillis();
        }
        if (this.beginTimestamp > 0) {
            this.getSource().getBiasXCollector().store(timestamp, accelerometerData);
        }
    }
}
