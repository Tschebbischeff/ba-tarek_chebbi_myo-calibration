package com.tschebbischeff.myocalib.internal.lazyui.pages.calibration.actionlist;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.tschebbischeff.myocalib.R;
import com.tschebbischeff.myocalib.internal.backend.calibration.DataCollector;
import com.tschebbischeff.myocalib.internal.backend.calibration.FullWaveRectificationFilter;
import com.tschebbischeff.myocalib.internal.backend.calibration.MeanFilter;
import com.tschebbischeff.myocalib.internal.backend.calibration.MovingAverageFilter;
import com.tschebbischeff.myocalib.internal.backend.tools.StaticLib;
import com.tschebbischeff.myocalib.internal.lazyui.pages.calibration.CalibrationConfig;
import com.tschebbischeff.myocalib.internal.lazyui.pages.calibration.PageCalibration;
import eu.darken.myolib.Myo;

/**
 * Realizes the {@link PageCalibrationActionListAdapter.ViewType#RVC_DOWN_EXTENSION} action.
 * <p>
 * Displays progress on the user performing the reference voluntary contraction extension of the wrist,
 * while the arm is in hanging position.
 */
public class PageCalibrationActionListViewHolderRvcDownExtension extends PageCalibrationActionListViewHolder {
    /**
     * The text view showing stub text.
     */
    public TextView text;
    /**
     * The progress bar, which is showing the user how long to hold the extension.
     */
    public ProgressBar progressBar;
    /**
     * The view showing the descriptive image.
     */
    public ImageView imageView;
    /**
     * Applies full wave rectification to the EMG signal.
     */
    private final FullWaveRectificationFilter fullWaveRectificationFilter;
    /**
     * A moving average filter applied to the EMG signal.
     */
    private final MovingAverageFilter movingAverageFilter;
    /**
     * Calculates the mean of values.
     */
    private final MeanFilter meanFilter;
    /**
     * Collects EMG data from the RVC phase to calculate necessary values.
     */
    private final DataCollector emgCollector;
    /**
     * Collects IMU acceleration data from the RVC phase to calculate necessary values.
     */
    private final DataCollector imuAcclCollector;
    /**
     * Collects IMU gyroscope data from the RVC phase to calculate necessary values.
     */
    private final DataCollector imuGyroCollector;
    /**
     * Collects IMU gyroscope data from the RVC phase to calculate necessary values.
     */
    private final DataCollector imuOrientationCollector;
    /**
     * The timestamp at which the user was last recognized as not relaxed.
     */
    private long beginTimestamp;

    /**
     * Constructor.
     *
     * @param source   The calibration page, for callbacks.
     * @param activity  The android context, for calling certain android methods.
     * @param rootView The view at the root of the cards layout.
     */
    public PageCalibrationActionListViewHolderRvcDownExtension(PageCalibration source, PageCalibrationActionListAdapter adapter, Activity activity, View rootView) {
        super(source, adapter, activity, PageCalibrationActionListAdapter.ViewType.RVC_DOWN_EXTENSION, rootView);
        this.text = rootView.findViewById(R.id.myocalib_lazyui_pages_calibration_cardsrvcdownextension_text);
        this.progressBar = rootView.findViewById(R.id.myocalib_lazyui_pages_calibration_cardsrvcdownextension_progress);
        this.imageView = rootView.findViewById(R.id.myocalib_lazyui_pages_calibration_cardsrvcdownextension_image);
        this.progressBar.setMax(CalibrationConfig.RVC_HOLD_TIME);
        this.fullWaveRectificationFilter = new FullWaveRectificationFilter();
        this.meanFilter = new MeanFilter();
        this.movingAverageFilter = new MovingAverageFilter(CalibrationConfig.EMG_MOVING_AVERAGE_WINDOW_SIZE);
        this.emgCollector = source.getRvcDownEmgCollector();
        this.imuAcclCollector = source.getRvcDownImuAcclCollector();
        this.imuGyroCollector = source.getRvcDownImuGyroCollector();
        this.imuOrientationCollector = source.getRvcDownImuOrientationCollector();
        this.reset();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void reset() {
        this.beginTimestamp = -1;
        this.progressBar.setProgress(0);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onDestroy() {

    }

    /**
     * {@inheritDoc}
     * @param view
     */
    @Override
    public void onClick(View view) {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onRawEmgData(Myo myo, long timestamp, double[] data) {
        if (this.beginTimestamp > 0) {
            this.meanFilter.reset();
            double filterResult =
                    this.movingAverageFilter.apply(timestamp,
                            this.meanFilter.batchApply(
                                    this.fullWaveRectificationFilter.batchApply(StaticLib.boxDoubleArray(data))
                            )[0]
                    ).second;
            if (filterResult < CalibrationConfig.DOWN_RVC_THRESHOLD) {
                this.beginTimestamp = System.currentTimeMillis();
                this.emgCollector.reset();
                this.imuAcclCollector.reset();
                this.imuGyroCollector.reset();
                this.imuOrientationCollector.reset();
            }
            this.emgCollector.store(timestamp, data);
            int elapsedTime = (int) (System.currentTimeMillis() - this.beginTimestamp);
            if (elapsedTime > CalibrationConfig.RVC_HOLD_TIME) {
                //Stop collecting data and free space
                this.beginTimestamp = -2;
                this.movingAverageFilter.reset();
                this.fullWaveRectificationFilter.reset();
                this.meanFilter.reset();
                this.getSource().stepCompleted();
            } else {
                this.progressBar.setProgress(elapsedTime);
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onRawImuData(Myo myo, long timestamp, double[] accelerometerData, double[] gyroscopeData, double[] orientationData) {
        if (this.beginTimestamp == -1) {
            this.beginTimestamp = System.currentTimeMillis();
            this.emgCollector.reset();
            this.imuAcclCollector.reset();
            this.imuGyroCollector.reset();
            this.imuOrientationCollector.reset();
        }
        if (this.beginTimestamp > 0) {
            this.imuAcclCollector.store(timestamp, accelerometerData);
            this.imuGyroCollector.store(timestamp, gyroscopeData);
            this.imuOrientationCollector.store(timestamp, orientationData);
            //Check whether the arm is still held in a downwards position (i.e. x-axis accelerometer is near the defined center
            double xAbs = Math.abs(accelerometerData[0]);
            if (xAbs < (CalibrationConfig.IMU_DOWN_X_EXPECTED - CalibrationConfig.IMU_DOWN_X_TOLERANCE) || xAbs > CalibrationConfig.IMU_DOWN_X_EXPECTED + CalibrationConfig.IMU_DOWN_X_TOLERANCE) {
                this.beginTimestamp = System.currentTimeMillis();
                this.emgCollector.reset();
                this.imuAcclCollector.reset();
                this.imuGyroCollector.reset();
                this.imuOrientationCollector.reset();
            }
        }
    }
}
