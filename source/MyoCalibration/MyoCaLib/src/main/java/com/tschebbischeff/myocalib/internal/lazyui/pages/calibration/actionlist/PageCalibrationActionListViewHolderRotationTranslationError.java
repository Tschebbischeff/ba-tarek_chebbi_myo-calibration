package com.tschebbischeff.myocalib.internal.lazyui.pages.calibration.actionlist;

import android.app.Activity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.tschebbischeff.common.android.layout.ImageViewAnimator;
import com.tschebbischeff.common.android.logger.Logger;
import com.tschebbischeff.myocalib.R;
import com.tschebbischeff.myocalib.internal.lazyui.pages.calibration.PageCalibration;
import eu.darken.myolib.Myo;

/**
 * Realizes the {@link PageCalibrationActionListAdapter.ViewType#ROTATION_TRANSLATION_ERROR} action.
 * <p>
 * Displays the error that the armband is not optimally positioned.
 */
public class PageCalibrationActionListViewHolderRotationTranslationError extends PageCalibrationActionListViewHolder {
    /**
     * The text view showing that an error has occured.
     */
    public TextView text;
    /**
     * The text view showing the description of the error.
     */
    public TextView errorText;
    /**
     * The view showing the descriptive image.
     */
    public ImageView imageView;
    /**
     * The view showing the clockwise rotation.
     */
    public ImageView imageRotationClockwise;
    /**
     * The view showing the counter clockwise rotation.
     */
    public ImageView imageRotationCounterClockwise;
    /**
     * The view showing the translation towards the wrist.
     */
    public ImageView imageTranslationWrist;
    /**
     * The view showing the translation towards the elbow.
     */
    public ImageView imageTranslationElbow;
    /**
     * The runable executing the animation and handling the timeout.
     */
    private final ImageViewAnimator imageViewAnimator;
    /**
     * The number of the currently shown error.
     */
    private int currentError;
    /**
     * Whether the current error still needs to be displayed.
     */
    private volatile boolean displayError;
    /**
     * The errors to display to the user in order.
     */
    private PageCalibrationActionListViewHolderDone.RotationTranslationResult[] rotationTranslationErrorTypes;

    /**
     * Constructor.
     *
     * @param source   The calibration page, for callbacks.
     * @param activity The android context, for calling certain android methods.
     * @param rootView The view at the root of the cards layout.
     */
    public PageCalibrationActionListViewHolderRotationTranslationError(PageCalibration source, PageCalibrationActionListAdapter adapter, Activity activity, View rootView) {
        super(source, adapter, activity, PageCalibrationActionListAdapter.ViewType.ROTATION_TRANSLATION_ERROR, rootView);
        this.text = rootView.findViewById(R.id.myocalib_lazyui_pages_calibration_cardsrotationtranslationerror_text);
        this.errorText = rootView.findViewById(R.id.myocalib_lazyui_pages_calibration_cardsrotationtranslationerror_errortext);
        this.imageView = rootView.findViewById(R.id.myocalib_lazyui_pages_calibration_cardsrotationtranslationerror_image);
        this.imageRotationClockwise = rootView.findViewById(R.id.myocalib_lazyui_pages_calibration_cardsrotationtranslationerror_rotation_clockwise);
        this.imageRotationCounterClockwise = rootView.findViewById(R.id.myocalib_lazyui_pages_calibration_cardsrotationtranslationerror_rotation_counterclockwise);
        this.imageTranslationWrist = rootView.findViewById(R.id.myocalib_lazyui_pages_calibration_cardsrotationtranslationerror_translation_wrist);
        this.imageTranslationElbow = rootView.findViewById(R.id.myocalib_lazyui_pages_calibration_cardsrotationtranslationerror_translation_elbow);
        this.imageViewAnimator = new ImageViewAnimator(activity);
        PageCalibrationActionListOnClickListener onClickListener = new PageCalibrationActionListOnClickListener(source, this);
        this.cardView.setOnClickListener(onClickListener);
        this.reset();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void reset() {
        this.currentError = 0;
        this.displayError = true;
        this.imageViewAnimator.stop();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onDestroy() {

    }

    /**
     * {@inheritDoc}
     *
     * @param view
     */
    @Override
    public void onClick(View view) {
        if (this.currentError < this.getAdapter().getLatestErrorResult().length - 1) {
            this.currentError++;
            this.displayError = true;
        } else {
            this.getSource().stepCompleted();
        }
    }

    /**
     * Starts periodically flashing the imageview via visible and invisible.
     *
     * @param overlay The image view to periodically make visible and invisible
     */
    private void startPictureAnimation(ImageView overlay) {
        this.imageViewAnimator.stop();
        this.imageViewAnimator.setData(new long[]{500L, 500L}, new ImageView[]{this.imageView, overlay});
        this.imageViewAnimator.start();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onRawEmgData(Myo myo, long timestamp, double[] data) {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onRawImuData(Myo myo, long timestamp, double[] accelerometerData, double[] gyroscopeData, double[] orientationData) {
        if (this.displayError) {
            this.imageViewAnimator.stop();
            this.getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    switch (getAdapter().getLatestErrorResult()[currentError]) {
                        case ROTATION_CLOCKWISE:
                            startPictureAnimation(imageRotationClockwise);
                            errorText.setText(getActivity().getString(R.string.myocalib_lazyui_pages_calibration_cardsrotationtranslationerror_rotationclockwise));
                            break;
                        case ROTATION_COUNTERCLOCKWISE:
                            startPictureAnimation(imageRotationCounterClockwise);
                            errorText.setText(getActivity().getString(R.string.myocalib_lazyui_pages_calibration_cardsrotationtranslationerror_rotationcounterclockwise));
                            break;
                        case TRANSLATION_WRIST:
                            startPictureAnimation(imageTranslationWrist);
                            errorText.setText(getActivity().getString(R.string.myocalib_lazyui_pages_calibration_cardsrotationtranslationerror_translationwrist));
                            break;
                        case TRANSLATION_ELBOW:
                            startPictureAnimation(imageTranslationElbow);
                            errorText.setText(getActivity().getString(R.string.myocalib_lazyui_pages_calibration_cardsrotationtranslationerror_translationelbow));
                            break;
                        default:
                            break;
                    }
                }
            });
            this.displayError = false;
            try {
                this.getAdapter().notifyItemChanged(0);
            } catch (Exception ignored) {
                Logger.warning("PageCalibrationActionListViewHolderRotationTranslationError", "Update of recycler view failed due to IllegalStateException");
            }
        }
    }
}
