package com.tschebbischeff.myocalib.internal.backend.calibration;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Scales the input data by a defined factor.
 */
public class ScalingFilter extends Filter1D<Double> {

    /**
     * The values saved for the computation.
     */
    private ArrayList<Double> values;
    /**
     * The factor to scale the data by.
     */
    private final double scalingFactor;

    public ScalingFilter(double scalingFactor) {
        this.values = new ArrayList<>();
        this.scalingFactor = scalingFactor;
    }

    @Override
    public void applyWithoutResult(Double value) {
        this.values.add(value);
    }

    @Override
    public Double getResult() {
        return this.values.get(this.values.size() - 1) * this.scalingFactor;
    }

    @Override
    protected Double[] apply(Double[] values) {
        this.values.addAll(Arrays.asList(values));
        Double[] result = new Double[values.length];
        for (int i = 0; i < values.length; i++) {
            result[i] = values[i] * this.scalingFactor;
        }
        return result;
    }

    @Override
    public void reset() {
        this.values.clear();
    }
}
