package com.tschebbischeff.myocalib.internal.lazyui.pages.connection.myolist;

import android.app.Activity;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.tschebbischeff.common.android.logger.Logger;
import com.tschebbischeff.myocalib.R;
import com.tschebbischeff.myocalib.internal.backend.calibration.WritableCalibrationProfile;
import com.tschebbischeff.myocalib.internal.backend.connection.ConnectionState;
import com.tschebbischeff.myocalib.internal.lazyui.pages.connection.PageConnection;
import eu.darken.myolib.Myo;

import java.util.ArrayList;
import java.util.List;

/**
 * Adapter for the recycler view containing the myo's in vicinity
 * Wraps a list of myo objects that are shown.
 */
public class PageConnectionMyoListAdapter extends RecyclerView.Adapter<PageConnectionMyoListViewHolder> {

    /**
     * Activity for API calls
     */
    private Activity activity;
    /**
     * The connection page for callbacks.
     */
    private PageConnection source;
    /**
     * A list of Myos that are shown in the recyclerviews, each with its own card.
     */
    private ArrayList<Myo> myoList;
    /**
     * The text view to show when no Myo's are currently detected.
     */
    private TextView noMyo = null;
    /**
     * The recyclerview this adapter is attached to.
     */
    private RecyclerView recyclerView = null;

    /**
     * Constructor. Instantiates a new adapter, that can be associated with a recycler view.
     *
     * @param source The connection page for callbacks.
     */
    public PageConnectionMyoListAdapter(PageConnection source, Activity activity) {
        this.activity = activity;
        this.source = source;
        this.myoList = new ArrayList<>();
    }

    /**
     * Get a myo shown in the list (connected and not connected) by its device id.
     *
     * @param deviceId The device id, by which to search for the Myo.
     * @return The Myo with the specified device id, or null if no Myo with the specified device id is in the list.
     */
    public Myo getMyoByDeviceId(String deviceId) {
        for (Myo myo : this.myoList) {
            if (myo.getDeviceAddress().equals(deviceId)) {
                return myo;
            }
        }
        return null;
    }

    /**
     * Get the position of a Myo with a device id in the list.
     *
     * @param deviceId The device id, by which to search for the Myo.
     * @return The position in the list of the Myo with the specified device id, or -1, if no Myo with the
     * specified device id is in the list.
     */
    public int getPositionByDeviceId(String deviceId) {
        for (Myo myo : this.myoList) {
            if (myo.getDeviceAddress().equals(deviceId)) {
                return this.myoList.indexOf(myo);
            }
        }
        return -1;
    }

    /**
     * Update a single item in the recycler views list.
     * Tells the recycler view, that the data of the specified Myo has changed.
     *
     * @param myo The Myo of which the connection state or data has changed.
     */
    public void datasetUpdateItem(Myo myo) {
        for (int i = 0; i < this.myoList.size(); i++) {
            if (this.myoList.get(i).getDeviceAddress().equals(myo.getDeviceAddress())) {
                this.myoList.set(i, myo);
                try {
                    this.notifyItemChanged(i);
                } catch (IllegalStateException ex) {
                    Logger.warning("PageConnection", "Update of recycler view failed due to IllegalStateException");
                }
            }
        }
    }

    /**
     * Updates the whole dataset, checks for differences between the supplies list of Myos and the internal
     * list of Myos and automatically flags Myos as removed or added.
     *
     * @param myos The new list of Myo devices.
     */
    public void datasetUpdate(List<Myo> myos) {
        //Check for removed myos and remove instantly
        for (int myoListPosition = 0; myoListPosition < this.myoList.size(); myoListPosition++) {
            Myo myo = this.myoList.get(myoListPosition);
            if (!myos.contains(myo)) {
                myos.remove(myo);
                //myo.removeConnectionListener(this.source); never remove connection listeners
                this.myoList.remove(myoListPosition);
                try {
                    this.notifyItemChanged(myoListPosition);
                } catch (IllegalStateException ex) {
                    Logger.warning("PageConnection", "Update of recycler view failed due to IllegalStateException");
                }
                myoListPosition--; //all positions are -1
            }
        }
        //Check for new myos
        for (Myo myo : myos) {
            if (!this.myoList.contains(myo)) {
                this.source.getSharedPageData().getWritableApiInterface().newCalibrationProfile(myo); //creates a new calibration profile, in case none exists yet
                this.myoList.add(myo);
                try {
                    this.notifyItemInserted(this.getItemCount());
                } catch (IllegalStateException ex) {
                    Logger.warning("PageConnection", "Update of recycler view failed due to IllegalStateException");
                }
            }
        }
        this.activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //Show "No Myos" message if list is empty.
                if (noMyo != null && recyclerView != null) {
                    if (myoList.size() == 0 && noMyo.getVisibility() == View.GONE) {
                        noMyo.setVisibility(View.VISIBLE);
                        recyclerView.setVisibility(View.GONE);
                    } else if (myoList.size() > 0 && noMyo.getVisibility() == View.VISIBLE){
                        noMyo.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);
                    }
                }
            }
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PageConnectionMyoListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LinearLayout v = (LinearLayout) LayoutInflater.from(parent.getContext()).inflate(R.layout.myocalib_lazyui_pages_connection_cardsmyo, parent, false);
        return new PageConnectionMyoListViewHolder(this.source, v);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onBindViewHolder(PageConnectionMyoListViewHolder holder, int position) {
        Myo myo = this.myoList.get(position);
        WritableCalibrationProfile writableCalibrationProfile = this.source.getSharedPageData().getWritableApiInterface().getCalibrationProfile(myo);
        if (myo != null) {
            if (this.source.getSharedPageData().getWritableApiInterface().getConnectionHandler().getMyoConnectionState(myo)
                    .oneOf(ConnectionState.CONNECTING_FOR_PREPARE, ConnectionState.PREPARING, ConnectionState.DISCONNECTING_AFTER_PREPARE)) {
                holder.connectButton.setText(R.string.myocalib_generic_preparing);
                if (holder.connectButton.isEnabled()) {
                    holder.connectButton.setEnabled(false);
                }
            } else {
                if (!holder.connectButton.isEnabled()) {
                    holder.connectButton.setEnabled(true);
                }
                if (this.source.getSharedPageData().getApiService().getConnectedMyos().contains(myo)) {
                    holder.connectButton.setText(R.string.myocalib_generic_disconnect);
                } else {
                    holder.connectButton.setText(R.string.myocalib_generic_connect);
                }
            }
            if (myo.getDeviceName() == null || myo.getDeviceName().isEmpty()) {
                holder.deviceName.setText(R.string.myocalib_generic_unknown);
            } else {
                holder.deviceName.setText(myo.getDeviceName());
            }
            if (this.source.getSharedPageData().getApiService().getConnectedMyos().contains(myo)) {
                if (writableCalibrationProfile.isCalibrationComplete()) {
                    holder.deviceName.setTextColor(ContextCompat.getColor(this.activity, R.color.myocalib_colorEmotionPositive));
                    holder.deviceName.setShadowLayer(1f, 4f, 4f, ContextCompat.getColor(this.activity, R.color.myocalib_textColorSecondary));
                    holder.infoText.setVisibility(View.GONE);
                } else {
                    holder.deviceName.setTextColor(ContextCompat.getColor(this.activity, R.color.myocalib_colorEmotionNegative));
                    holder.deviceName.setShadowLayer(1f, 4f, 4f, ContextCompat.getColor(this.activity, R.color.myocalib_textColorSecondary));
                    holder.infoText.setVisibility(View.VISIBLE);
                }
            } else {
                holder.deviceName.setTextColor(ContextCompat.getColor(this.activity, R.color.myocalib_textColorPrimary));
                holder.deviceName.setShadowLayer(0f, 0f, 0f, ContextCompat.getColor(this.activity, R.color.myocalib_textColorSecondary));
                holder.infoText.setVisibility(View.GONE);
            }
            if (myo.getBatteryLevel() > -1) {
                if (myo.getBatteryLevel() > 90) {
                    holder.batteryIcon.setImageResource(R.drawable.myocalib_ic_battery_full_white_24dp);
                } else if (myo.getBatteryLevel() > 80) {
                    holder.batteryIcon.setImageResource(R.drawable.myocalib_ic_battery_90_white_24dp);
                } else if (myo.getBatteryLevel() > 60) {
                    holder.batteryIcon.setImageResource(R.drawable.myocalib_ic_battery_80_white_24dp);
                } else if (myo.getBatteryLevel() > 50) {
                    holder.batteryIcon.setImageResource(R.drawable.myocalib_ic_battery_60_white_24dp);
                } else if (myo.getBatteryLevel() > 30) {
                    holder.batteryIcon.setImageResource(R.drawable.myocalib_ic_battery_50_white_24dp);
                } else if (myo.getBatteryLevel() > 20) {
                    holder.batteryIcon.setImageResource(R.drawable.myocalib_ic_battery_30_white_24dp);
                } else {
                    holder.batteryIcon.setImageResource(R.drawable.myocalib_ic_battery_20_white_24dp);
                }
                holder.batteryLevel.setText(String.format("%s%%", String.valueOf(myo.getBatteryLevel())));
            } else {
                holder.batteryIcon.setImageResource(R.drawable.myocalib_ic_battery_unknown_white_24dp);
                holder.batteryLevel.setText("");
            }
            holder.deviceId.setText(myo.getDeviceAddress());
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getItemCount() {
        return this.myoList.size();
    }

    public void onAfterShow(Activity activity) {
        this.noMyo = activity.findViewById(R.id.myocalib_lazyui_pages_connection_main_nomyo);
        this.recyclerView = activity.findViewById(R.id.myocalib_lazyui_pages_connection_main_myolist);
    }
}
