package com.tschebbischeff.myocalib.api;

import android.content.Context;
import android.content.SharedPreferences;
import com.tschebbischeff.myocalib.R;

/**
 * Stores the settings defined by an application regarding the calibration process.
 * Encapsulates the different areas of settings, and can save and load them from storage
 * based on the applications package name.
 */
public final class CalibrationSettings {

    /**
     * All settings regarding the calibration of the EMG signal.
     */
    public final EmgCalibrationSettings emg;
    /**
     * All settings regarding the calibration of the IMU signal.
     */
    public final ImuCalibrationSettings imu;
    /**
     * Whether the calibration settings are marked as final by the application.
     */
    private boolean immutable;
    /**
     * The version of the settings. Increment this whenever new settings are added, to invalidate old settings.
     */
    private static final int SETTINGS_VERSION = 1;
    /**
     * Package name of the application. Supplied by constructor from the API service.
     */
    private final Context applicationContext;

    /**
     * Protected constructor, objects may only be created by {@link MyoCaLib}
     *
     * @param applicationContext The context of the library-utilizing application.
     */
    protected CalibrationSettings(Context applicationContext) {
        this.applicationContext = applicationContext;
        this.immutable = false;
        this.emg = new EmgCalibrationSettings(this);
        this.imu = new ImuCalibrationSettings(this);
    }

    /**
     * Loads settings from storage. Different settings can be identified by the file suffix parameter.
     * In case of a version mismatch, false is returned.
     *
     * @param fileSuffix Determines which file to load the settings from. null results in loaded the settings from "default".
     * @return True, if and only if, settings existed and they were completely successfully loaded.
     */
    public boolean loadSettings(String fileSuffix) {
        if (fileSuffix == null) {
            fileSuffix = "default";
        }
        SharedPreferences sharedPref = this.applicationContext.getSharedPreferences(this.applicationContext.getString(R.string.myocalib_sharedprefs_path_calibrationsettings) + "_" + fileSuffix, Context.MODE_PRIVATE);
        //Early fail
        if (this.immutable || sharedPref.getAll().size() == 0 ||
                sharedPref.getInt(this.applicationContext.getString(R.string.myocalib_calibrationsettings_version), SETTINGS_VERSION) != SETTINGS_VERSION) {
            return false;
        }
        //Load settings
        //IMU
        if (sharedPref.getBoolean(this.applicationContext.getString(R.string.myocalib_calibrationsettings_imu_calibrationEnabled), this.imu.isCalibrationEnabled())) {
            this.imu.enableCalibration();
        } else {
            this.imu.disableCalibration();
        }
        if (sharedPref.getBoolean(this.applicationContext.getString(R.string.myocalib_calibrationsettings_imu_accelerometerBiasCalibrationEnabled), this.imu.isAccelerometerBiasCalibrationEnabled())) {
            this.imu.enableAccelerometerBiasCalibration();
        } else {
            this.imu.disableAccelerometerBiasCalibration();
        }
        if (sharedPref.getBoolean(this.applicationContext.getString(R.string.myocalib_calibrationsettings_imu_globalCoordinateSystemEnabled), this.imu.isGlobalCoordinateSystemEnabled())) {
            this.imu.enableGlobalCoordinateSystem();
        } else {
            this.imu.disableGlobalCoordinateSystem();
        }
        if (sharedPref.getBoolean(this.applicationContext.getString(R.string.myocalib_calibrationsettings_imu_forceAccelerometerBiasCalibrationEnabled), this.imu.isForceAccelerometerBiasCalibrationEnabled())) {
            this.imu.enableForceAccelerometerBiasCalibration();
        } else {
            this.imu.disableForceAccelerometerBiasCalibration();
        }
        //EMG
        if (sharedPref.getBoolean(this.applicationContext.getString(R.string.myocalib_calibrationsettings_emg_calibrationEnabled), this.emg.isCalibrationEnabled())) {
            this.emg.enableCalibration();
        } else {
            this.emg.disableCalibration();
        }
        if (sharedPref.getBoolean(this.applicationContext.getString(R.string.myocalib_calibrationsettings_emg_translationCalibrationEnabled), this.emg.isTranslationCalibrationEnabled())) {
            this.emg.enableTranslationCalibration();
        } else {
            this.emg.disableTranslationCalibration();
        }
        if (sharedPref.getBoolean(this.applicationContext.getString(R.string.myocalib_calibrationsettings_emg_rotationCalibrationEnabled), this.emg.isRotationCalibrationEnabled())) {
            this.emg.enableRotationCalibration();
        } else {
            this.emg.disableRotationCalibration();
        }
        if (sharedPref.getBoolean(this.applicationContext.getString(R.string.myocalib_calibrationsettings_emg_normalizationEnabled), this.emg.isNormalizationEnabled())) {
            this.emg.enableNormalization();
        } else {
            this.emg.disableNormalization();
        }
        this.emg.setInterpolationMode(EmgCalibrationSettings.EmgInterpolationMode.values()[sharedPref.getInt(this.applicationContext.getString(R.string.myocalib_calibrationsettings_emg_interpolationMode), this.emg.getInterpolationMode().ordinal())]);
        double forceRotationAngle = sharedPref.getFloat(this.applicationContext.getString(R.string.myocalib_calibrationsettings_emg_forceRotationThreshold), (float) this.emg.getForceRotationThreshold());
        if (forceRotationAngle < 360d) {
            this.emg.enableForceRotation(forceRotationAngle);
        } else {
            this.emg.disableForceRotation();
        }
        this.emg.setTranslationLowThreshold(sharedPref.getFloat(this.applicationContext.getString(R.string.myocalib_calibrationsettings_emg_translationThresholdLow), (float) this.emg.getTranslationThresholdLow()));
        this.emg.setTranslationLowThreshold(sharedPref.getFloat(this.applicationContext.getString(R.string.myocalib_calibrationsettings_emg_translationThresholdHigh), (float) this.emg.getTranslationThresholdHigh()));
        return true;
    }

    /**
     * Stores settings on storage. Different  settings can be identified by the file suffix parameter.
     *
     * @param fileSuffix Determines which file to save the settings to. null results in storing the settings to "default".
     * @return True if, and only if, settings were successfully saved to the file on storage.
     */
    public boolean saveSettings(String fileSuffix) {
        if (fileSuffix == null) {
            fileSuffix = "default";
        }
        SharedPreferences sharedPref = this.applicationContext.getSharedPreferences(this.applicationContext.getString(R.string.myocalib_sharedprefs_path_calibrationsettings) + "_" + fileSuffix, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt(this.applicationContext.getString(R.string.myocalib_calibrationsettings_version), SETTINGS_VERSION);
        editor.putBoolean(this.applicationContext.getString(R.string.myocalib_calibrationsettings_imu_calibrationEnabled), this.imu.isCalibrationEnabled());
        editor.putBoolean(this.applicationContext.getString(R.string.myocalib_calibrationsettings_imu_accelerometerBiasCalibrationEnabled), this.imu.isAccelerometerBiasCalibrationEnabled());
        editor.putBoolean(this.applicationContext.getString(R.string.myocalib_calibrationsettings_imu_globalCoordinateSystemEnabled), this.imu.isGlobalCoordinateSystemEnabled());
        editor.putBoolean(this.applicationContext.getString(R.string.myocalib_calibrationsettings_imu_forceAccelerometerBiasCalibrationEnabled), this.imu.isForceAccelerometerBiasCalibrationEnabled());
        editor.putBoolean(this.applicationContext.getString(R.string.myocalib_calibrationsettings_emg_calibrationEnabled), this.emg.isCalibrationEnabled());
        editor.putBoolean(this.applicationContext.getString(R.string.myocalib_calibrationsettings_emg_translationCalibrationEnabled), this.emg.isTranslationCalibrationEnabled());
        editor.putBoolean(this.applicationContext.getString(R.string.myocalib_calibrationsettings_emg_rotationCalibrationEnabled), this.emg.isRotationCalibrationEnabled());
        editor.putBoolean(this.applicationContext.getString(R.string.myocalib_calibrationsettings_emg_normalizationEnabled), this.emg.isNormalizationEnabled());
        editor.putInt(this.applicationContext.getString(R.string.myocalib_calibrationsettings_emg_interpolationMode), this.emg.getInterpolationMode().ordinal());
        editor.putFloat(this.applicationContext.getString(R.string.myocalib_calibrationsettings_emg_forceRotationThreshold), (float) this.emg.getForceRotationThreshold());
        editor.putFloat(this.applicationContext.getString(R.string.myocalib_calibrationsettings_emg_translationThresholdLow), (float) this.emg.getTranslationThresholdLow());
        editor.putFloat(this.applicationContext.getString(R.string.myocalib_calibrationsettings_emg_translationThresholdHigh), (float) this.emg.getTranslationThresholdHigh());
        return editor.commit();
    }

    /**
     * Call this method when you are done setting the calibration settings.
     * Calibration of devices is not performed until the settings are final.
     * You have to call this method even if the settings are loaded from disk.
     * This action can not be undone, until a restart of the API service.
     *
     * @return This object for fluent method calls.
     */
    public CalibrationSettings done() {
        this.immutable = true;
        this.emg.done();
        this.imu.done();
        return this;
    }

    /**
     * Returns whether the settings have been marked as final by the application via the {@link #done()} method.
     * This means that settings can not be changed without restarting the API service (The internal activity
     * must be implicitly closed, for the service to shut down).
     *
     * @return True if, and only if, this objects {@link #done()} method has been called.
     */
    public boolean isImmutable() {
        return this.immutable;
    }
}
