package com.tschebbischeff.myocalib.internal.lazyui;

import com.tschebbischeff.myocalib.api.InternalApiInterface;
import com.tschebbischeff.myocalib.api.MyoCaLib;
import eu.darken.myolib.Myo;

/**
 * Contains setter and getter to share data between different content pages
 */
public class SharedPageData {

    /**
     * API interface, that gives write-access to API fields.
     */
    private InternalApiInterface apiInterface;

    /**
     * The Myo as set by the connection page, that the calibration page should calibrate
     */
    private Myo calibratingMyo;

    /**
     * Constructor
     *
     * @param apiInterface The API interface giving write-access to API fields.
     */
    public SharedPageData(InternalApiInterface apiInterface) {
        this.apiInterface = apiInterface;
    }

    /**
     * Used to get the service instance providing the read-access API.
     *
     * @return The open, read-only API, which can also be used by applications using this library, or null, if
     * the service is not bound.
     */
    public MyoCaLib getApiService() {
        return this.apiInterface.isServiceReady() ? this.apiInterface.getApiService() : null;
    }

    /**
     * Used to get the API part providing write-access to internal classes.
     *
     * @return The protected write-access API, which can only be used from internal classes, or null, if
     * the read-only API service is not bound.
     */
    public InternalApiInterface getWritableApiInterface() {
        return this.apiInterface.isServiceReady() ? this.apiInterface : null;
    }

    /**
     * Sets which Myo is selected for calibration.
     *
     * @param myo The myo selected for calibration.
     */
    public void setCalibratingMyo(Myo myo) {
        this.calibratingMyo = myo;
    }

    /**
     * Gets the Myo that was set by the connection page for calibration.
     *
     * @return The Myo that should be calibrated by the calibration page.
     */
    public Myo getCalibratingMyo() {
        return calibratingMyo;
    }
}
