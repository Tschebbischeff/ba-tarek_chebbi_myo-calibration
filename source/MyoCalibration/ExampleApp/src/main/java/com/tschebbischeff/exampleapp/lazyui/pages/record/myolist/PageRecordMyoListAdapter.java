package com.tschebbischeff.exampleapp.lazyui.pages.record.myolist;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import com.tschebbischeff.exampleapp.R;
import com.tschebbischeff.exampleapp.lazyui.pages.record.PageRecord;
import eu.darken.myolib.Myo;

import java.util.Locale;


public class PageRecordMyoListAdapter extends RecyclerView.Adapter<PageRecordMyoListViewHolder> {

    private PageRecord source;

    public PageRecordMyoListAdapter(PageRecord source) {
        this.source = source;
    }

    @Override
    public PageRecordMyoListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LinearLayout v = (LinearLayout) LayoutInflater.from(parent.getContext()).inflate(R.layout.exampleapp_lazyui_pages_record_cardsmyo, parent, false);
        return new PageRecordMyoListViewHolder(v);
    }

    @Override
    public void onBindViewHolder(PageRecordMyoListViewHolder holder, int position) {
        final Myo myo = this.source.getSharedPageData().getReadyMyos().get(position);
        if (this.source.isRecording()) {
            holder.checkBox.setEnabled(false);
        } else {
            holder.checkBox.setEnabled(true);
        }
        if (holder.checkBox.isChecked()) {
            if (!source.getCheckedMyos().contains(myo)) {
                source.getCheckedMyos().add(myo);
            }
        } else {
            source.getCheckedMyos().remove(myo);
        }
        holder.deviceName.setText(myo.getDeviceName());
        holder.deviceId.setText(myo.getDeviceAddress());
        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    if (!source.getCheckedMyos().contains(myo)) {
                        source.getCheckedMyos().add(myo);
                    }
                } else {
                    source.getCheckedMyos().remove(myo);
                }
            }
        });
    }

    private String format(double d) {
        return String.format(Locale.getDefault(), "%+.3f", d);
    }

    @Override
    public int getItemCount() {
        return this.source.getSharedPageData().getReadyMyos().size();
    }

}
