package com.tschebbischeff.exampleapp.lazyui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.view.View;
import android.widget.LinearLayout;
import com.tschebbischeff.common.android.logger.Logger;
import com.tschebbischeff.exampleapp.MainActivity;
import com.tschebbischeff.exampleapp.R;
import com.tschebbischeff.exampleapp.lazyui.pages.Page;
import com.tschebbischeff.exampleapp.lazyui.pages.defaults.PageEmpty;
import com.tschebbischeff.exampleapp.lazyui.pages.live.emg.PageLiveEmgGraph;
import com.tschebbischeff.exampleapp.lazyui.pages.live.imu.PageLiveRotGraph;
import com.tschebbischeff.exampleapp.lazyui.pages.live.threed.PageLive3D;
import com.tschebbischeff.exampleapp.lazyui.pages.record.PageRecord;
import com.tschebbischeff.exampleapp.lazyui.pages.welcome.PageWelcome;

/**
 * This enum is handled like a class with a set amount of predefined objects,
 * which contain all data that is used to identify a content layout in the main view.
 */
public enum PageList {
    /**
     * An empty layout, should never be shown in the finished app.
     * Exists solely for the purpose of a placeholder.
     */
    EMPTY(0, new PageEmpty()),
    /**
     * The first layout to show as the content. Contains a welcome message and some explanation on how to use the app.
     */
    WELCOME(R.id.exampleapp_nav_welcome, new PageWelcome()),
    /**
     * The layout showing everything regarding recording data.
     */
    RECORD(R.id.exampleapp_nav_record, new PageRecord()),
    /**
     * The layout showing a 3D OpenGL view of the users arm with live rotation and EMG data.
     */
    LIVE_3D(R.id.exampleapp_nav_live_3d, new PageLive3D()),
    /**
     * The layout showing graphs on a simple 2D surface containing the eight EMG channels.
     */
    LIVE_EMG_GRAPH(R.id.exampleapp_nav_live_emg_graph, new PageLiveEmgGraph()),
    /**
     * The layout showing graphs on a simple 2D surface containing the rotations around the three axes.
     */
    LIVE_ROT_GRAPH(R.id.exampleapp_nav_live_rot_graph, new PageLiveRotGraph());


    /**
     * The main activity
     */
    private static MainActivity mainActivity;
    /**
     * The id of the currently shown content, to manage callbacks.
     */
    private static int selectedContent = EMPTY.ordinal();
    /**
     * The id of the menu item that inflates the registered layout when clicked.
     */
    private final int menuItemResourceId;
    /**
     * The class handling callbacks for the content.
     */
    private final Page pageHandler;

    /**
     * Private constructor, binding static data in the object.
     *
     * @param menuItemResourceId {@link PageList#menuItemResourceId}
     */
    PageList(int menuItemResourceId, Page pageHandler) {
        this.menuItemResourceId = menuItemResourceId;
        this.pageHandler = pageHandler;
    }

    /**
     * Selects a predefined layout content from the above list and shows it.
     * The selected layout content is the first layout content shown in the app upon startup.
     *
     * @param activity The activity containing the wrapper layout in which to inflate the new content
     */
    public static void initialize(MainActivity activity) {
        mainActivity = activity;
        WELCOME.show(activity);
    }

    /**
     * Selects the correct layout content by the registered resource id of the menu item in
     * the navigation drawer.
     *
     * @param activity   The activity containing the wrapper layout in which to inflate the new content
     * @param resourceId The resource id of the menu item, registered with the layout content
     */
    public static void changeToId(Activity activity, int resourceId) {
        for (PageList content : PageList.values()) {
            if (content.menuItemResourceId == resourceId) {
                content.show(activity);
                break;
            }
        }
    }

    /**
     * Dispatches the onCreate event to all contents
     */
    public static void dispatchOnCreate(SharedPageData sharedPageData, Activity activity) {
        for (PageList layout : PageList.values()) {
            layout.pageHandler.dispatchOnCreate(sharedPageData, activity);
        }
    }

    /**
     * Dispatches the onAfterShow event to the currently selected content
     */
    public static void dispatchOnResume(Activity activity) {
        PageList.values()[selectedContent].pageHandler.dispatchOnBeforeShow(true);
        LinearLayout contentWrapper = activity.findViewById(R.id.exampleapp_content_wrapper);
        if (contentWrapper != null) {
            activity.getLayoutInflater().inflate(PageList.values()[selectedContent].pageHandler.dispatchGetLayoutResourceId(), contentWrapper);
        } else Logger.warning("PageList", "Page wrapper not found, could not refresh UI onAfterShow!");
        //Resume even if there was an error in refreshing the UI (failsafe, will trigger only a visual bug)
        PageList.values()[selectedContent].pageHandler.dispatchOnAfterShow();
    }

    /**
     * Dispatches the onBeforeHide event to the currently selected content
     */
    public static void dispatchOnPause(Activity activity) {
        PageList.values()[selectedContent].pageHandler.dispatchOnBeforeHide(true);
        LinearLayout contentWrapper = activity.findViewById(R.id.exampleapp_content_wrapper);
        if (contentWrapper != null) {
            contentWrapper.removeAllViews();
        } else Logger.warning("PageList", "Page wrapper not found, could not refresh UI onAfterShow!");
        PageList.values()[selectedContent].pageHandler.dispatchOnAfterHide();
    }

    /**
     * Dispatches the onDestroy event to all contents
     */
    public static void dispatchOnDestroy() {
        for (PageList layout : PageList.values()) {
            layout.pageHandler.dispatchOnDestroy();
        }
    }

    /**
     * Dispatches back button presses to the currently shown content
     *
     * @return True if the app should be closed in response to the back button press, False otherwise.
     */
    public static boolean dispatchOnBackPressed() {
        return PageList.values()[selectedContent].pageHandler.onBackPressed();
    }

    /**
     * Dispatches clicks to the currently shown content
     *
     * @param view The clicked view
     */
    public static void dispatchOnClick(View view) {
        PageList.values()[selectedContent].pageHandler.onClick(view);
    }

    /**
     * Dispatches request results of permissions to the currently shown content
     *
     * @param requestCode  The request code supplied at the request of the permissions.
     * @param permissions  The permissions requested.
     * @param grantResults The results of the request per permission.
     */
    public static void dispatchOnRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        PageList.values()[selectedContent].pageHandler.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    /**
     * Dispatches received intents to the currently shown content
     *
     * @param context The context
     * @param intent  The received intent
     */
    public static void dispatchOnReceive(Context context, Intent intent) {
        PageList.values()[selectedContent].pageHandler.onReceive(context, intent);
    }

    /**
     * Shows this content layout.
     *
     * @param activity The activity containing the wrapper layout in which to inflate the new content
     */
    public void show(Activity activity) {
        LinearLayout contentWrapper = activity.findViewById(R.id.exampleapp_content_wrapper);
        if (contentWrapper != null) {
            if (PageList.values()[selectedContent].pageHandler.dispatchOnBeforeHide(false)) {
                contentWrapper.removeAllViews();
                PageList.values()[selectedContent].pageHandler.dispatchOnAfterHide();
                if (this.pageHandler.dispatchOnBeforeShow(false)) {
                    activity.getLayoutInflater().inflate(this.pageHandler.dispatchGetLayoutResourceId(), contentWrapper);
                    selectedContent = this.ordinal();
                    this.pageHandler.dispatchOnAfterShow();
                    mainActivity.refreshBroadcastReceiverFilter(this.pageHandler.dispatchGetIntentFilterActions());
                    Logger.info("PageList", "Page fully changed to " + this.toString() + ".");
                } else {
                    PageList.values()[selectedContent].pageHandler.dispatchOnBeforeShow(true);
                    activity.getLayoutInflater().inflate(PageList.values()[selectedContent].pageHandler.dispatchGetLayoutResourceId(), contentWrapper);
                    PageList.values()[selectedContent].pageHandler.dispatchOnAfterShow();
                    mainActivity.refreshBroadcastReceiverFilter(PageList.values()[selectedContent].pageHandler.dispatchGetIntentFilterActions());
                }
            }
            NavigationView navigationView = activity.findViewById(R.id.exampleapp_nav_view);
            if (navigationView != null) {
                navigationView.setCheckedItem(PageList.values()[selectedContent].menuItemResourceId);
            } else
                Logger.error("PageList", "Navigation drawer view could not be found. Can not set selected item!");
        } else Logger.error("PageList", "Page wrapper layout could not be found!");
    }
}
