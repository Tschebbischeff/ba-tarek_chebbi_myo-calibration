package com.tschebbischeff.exampleapp.lazyui.pages.live.imu.myolist;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.tschebbischeff.exampleapp.R;

public class PageLiveRotGraphMyoListViewHolder extends RecyclerView.ViewHolder {

    /**
     * The root card view.
     */
    public CardView cardView;
    /**
     * The text view containing the devices name.
     */
    public TextView deviceName;
    /**
     * The text view containing the devices id.
     */
    public TextView deviceId;
    /**
     * The text view showing the acceleration on the x-axis.
     */
    public TextView accelerometerX;
    /**
     * The text view showing the acceleration on the y-axis.
     */
    public TextView accelerometerY;
    /**
     * The text view showing the acceleration on the z-axis.
     */
    public TextView accelerometerZ;
    /**
     * The text view showing the rotation around the x-axis.
     */
    public TextView gyroscopeX;
    /**
     * The text view showing the rotation around the y-axis.
     */
    public TextView gyroscopeY;
    /**
     * The text view showing the rotation around the z-axis.
     */
    public TextView gyroscopeZ;

    /**
     * Constructor. Binds the layouts views to the public attributes.
     *
     * @param rootView The inflated view at the root of the layout.
     */
    public PageLiveRotGraphMyoListViewHolder(LinearLayout rootView) {
        super(rootView);
        cardView = rootView.findViewById(R.id.exampleapp_lazyui_pages_liverot_cardsmyo_cardview);
        deviceName = rootView.findViewById(R.id.exampleapp_lazyui_pages_liverot_cardsmyo_devicename);
        deviceId = rootView.findViewById(R.id.exampleapp_lazyui_pages_liverot_cardsmyo_deviceid);
        accelerometerX = rootView.findViewById(R.id.exampleapp_lazyui_pages_liverot_cardsmyo_accelerometerx);
        accelerometerY = rootView.findViewById(R.id.exampleapp_lazyui_pages_liverot_cardsmyo_accelerometery);
        accelerometerZ = rootView.findViewById(R.id.exampleapp_lazyui_pages_liverot_cardsmyo_accelerometerz);
        gyroscopeX = rootView.findViewById(R.id.exampleapp_lazyui_pages_liverot_cardsmyo_gyroscopex);
        gyroscopeY = rootView.findViewById(R.id.exampleapp_lazyui_pages_liverot_cardsmyo_gyroscopey);
        gyroscopeZ = rootView.findViewById(R.id.exampleapp_lazyui_pages_liverot_cardsmyo_gyroscopez);
    }
}