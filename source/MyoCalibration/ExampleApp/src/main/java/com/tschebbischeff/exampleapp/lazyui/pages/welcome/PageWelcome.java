package com.tschebbischeff.exampleapp.lazyui.pages.welcome;

import android.view.View;
import com.tschebbischeff.exampleapp.R;
import com.tschebbischeff.exampleapp.lazyui.pages.Page;

/**
 * Handles callbacks for the welcome page
 */
public class PageWelcome extends Page {

    @Override
    protected int getLayoutResourceId() {
        return R.layout.exampleapp_lazyui_pages_welcome_main;
    }

    @Override
    protected String[] getIntentFilterActions() {
        return new String[]{};
    }

    @Override
    protected void onCreate() {
    }

    @Override
    protected boolean onBeforeShow(boolean forced) {
        return true;
    }

    @Override
    protected void onAfterShow() {
    }

    @Override
    protected boolean onBeforeHide(boolean forced) {
        return true;
    }

    @Override
    protected void onAfterHide() {
    }

    @Override
    protected void onDestroy() {
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.exampleapp_lazyui_pages_welcome_main_showcalibrationactivity) {
            this.getSharedPageData().getMyoCalibrationService().startCalibrationActivity(this.getActivity());
        }
    }
}
