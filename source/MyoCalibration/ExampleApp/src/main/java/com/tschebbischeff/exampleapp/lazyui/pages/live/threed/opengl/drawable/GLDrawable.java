package com.tschebbischeff.exampleapp.lazyui.pages.live.threed.opengl.drawable;

import android.opengl.GLES20;

/**
 * Definition of any drawable object
 */
public abstract class GLDrawable {

    private int shaderProgram;

    public GLDrawable(int shaderProgram) {
        this.shaderProgram = shaderProgram;
    }

    public void requestDraw() {
        GLES20.glUseProgram(this.shaderProgram);
        this.draw();
    }

    protected int getShaderProgram() {
        return this.shaderProgram;
    }

    protected abstract void draw();
}
