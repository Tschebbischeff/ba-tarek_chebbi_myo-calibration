package com.tschebbischeff.exampleapp.lazyui;

import android.util.Log;

import com.tschebbischeff.myocalib.api.IMyoStateListener;
import com.tschebbischeff.myocalib.api.MyoCaLib;
import eu.darken.myolib.Myo;

import java.util.ArrayList;

/**
 * Contains setter and getter to share data between different content pages
 */
public class SharedPageData implements IMyoStateListener {

    private static final String TAG = SharedPageData.class.getName()
            ;
    private MyoCaLib myoCalibrationService;
    private ArrayList<Myo> readyMyos;

    public SharedPageData(MyoCaLib myoCalibrationService) {
        this.myoCalibrationService = myoCalibrationService;
        this.readyMyos = new ArrayList<>();
    }

    public MyoCaLib getMyoCalibrationService() {
        return myoCalibrationService;
    }

    public ArrayList<Myo> getReadyMyos() {
        return readyMyos;
    }

    @Override
    public void onMyoConnectedForUsage(Myo myo) {
        Log.e(TAG, "onMyoConnectedForUsage");
    }

    @Override
    public void onMyoDisconnectedAbnormally(Myo myo) {
        Log.e(TAG, "onMyoDisconnectedAbnormally");
    }

    @Override
    public void onMyoConnectedAfterDisconnect(Myo myo) {
        Log.e(TAG, "onMyoConnectedAfterDisconnect");
    }

    @Override
    public void onMyoDisconnected(Myo myo) {
        Log.e(TAG, "onMyoDisconnected");
    }

    @Override
    public void onMyoDisconnectedAtShutdown(Myo myo) {
        Log.e(TAG, "onMyoDisconnectedAtShutdown");
    }

    @Override
    public void onMyoCalibrated(Myo myo) {
        this.readyMyos.add(myo);
        Log.e(TAG, "onMyoCalibrated");
    }

    @Override
    public void onMyoUncalibrated(Myo myo) {
        this.readyMyos.remove(myo);
        Log.e(TAG, "onMyoUncalibrated");
    }
}
