package com.tschebbischeff.exampleapp.lazyui.pages.live.emg.myolist;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.tschebbischeff.exampleapp.R;
import com.tschebbischeff.exampleapp.lazyui.pages.live.emg.PageLiveEmgGraph;
import eu.darken.myolib.Myo;

import java.util.Locale;


/**
 * Adapter for the recyclerview showing the myo cards, which also contain the EMG data for the myo.
 */
public class PageLiveEmgGraphMyoListAdapter extends RecyclerView.Adapter<PageLiveEmgGraphMyoListViewHolder> {

    /**
     * The originating page, for obtaining stored data.
     */
    private PageLiveEmgGraph source;

    /**
     * Constructor.
     *
     * @param source The source page, for obtaining stored data.
     */
    public PageLiveEmgGraphMyoListAdapter(PageLiveEmgGraph source) {
        this.source = source;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PageLiveEmgGraphMyoListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LinearLayout v = (LinearLayout) LayoutInflater.from(parent.getContext()).inflate(R.layout.exampleapp_lazyui_pages_liveemg_cardsmyo, parent, false);
        return new PageLiveEmgGraphMyoListViewHolder(v);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onBindViewHolder(PageLiveEmgGraphMyoListViewHolder holder, int position) {
        Myo myo = this.source.getSharedPageData().getReadyMyos().get(position);
        holder.deviceName.setText(myo.getDeviceName());
        holder.deviceId.setText(myo.getDeviceAddress());
        Double[] data = this.source.getEmgDataByMyo(myo);
        if (data == null) {
            data = new Double[]{0d, 0d, 0d, 0d, 0d, 0d, 0d, 0d};
        }
        holder.pod1.setText(format(data[0]));
        holder.pod2.setText(format(data[1]));
        holder.pod3.setText(format(data[2]));
        holder.pod4.setText(format(data[3]));
        holder.pod5.setText(format(data[4]));
        holder.pod6.setText(format(data[5]));
        holder.pod7.setText(format(data[6]));
        holder.pod8.setText(format(data[7]));
    }

    /**
     * Formats the EMG number to a string with 2 digits after the comma.
     *
     * @param d The EMG value.
     * @return The formatted EMG value as a string.
     */
    private String format(double d) {
        return String.format(Locale.getDefault(), "%+.2f", d);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getItemCount() {
        return this.source.getSharedPageData().getReadyMyos().size();
    }

}
