package com.tschebbischeff.exampleapp;

import android.app.Activity;
import android.content.*;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import com.tschebbischeff.common.android.logger.Logger;
import com.tschebbischeff.exampleapp.lazyui.PageList;
import com.tschebbischeff.exampleapp.lazyui.SharedPageData;
import com.tschebbischeff.myocalib.api.MyoCaLib;

/**
 * Implementation of the main activity.
 * The activity only changes the enclosed content with variable layouts.
 * These content layouts are defined in {@link PageList}.
 * <p>
 * The activity furthermore includes a navigation drawer as a means to navigate through the different content
 * in the app. The navigation drawer is always available, no matter which content is currently shown.
 */
public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, ServiceConnection {

    private final BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            PageList.dispatchOnReceive(context, intent);
        }
    };

    private MyoCaLib apiService;
    private SharedPageData sharedPageData;
    private boolean activityInitialized = false;
    private boolean activityResumed = false;

    @Override
    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        this.apiService = ((MyoCaLib.LocalBinder) iBinder).getService();
        if (!this.activityInitialized) {
            this.initializeCalibration();
            setContentView(R.layout.exampleapp_main_activity);
            Toolbar toolbar = (Toolbar) findViewById(R.id.exampleapp_toolbar);
            setSupportActionBar(toolbar);

            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.exampleapp_drawer_layout);
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                    this, drawer, toolbar, R.string.exampleapp_navigationdrawer_open, R.string.exampleapp_navigationdrawer_close);
            drawer.addDrawerListener(toggle);
            toggle.syncState();

            NavigationView navigationView = (NavigationView) findViewById(R.id.exampleapp_nav_view);
            navigationView.setNavigationItemSelectedListener(this);

            registerReceiver(broadcastReceiver, new IntentFilter());

            PageList.dispatchOnCreate(this.sharedPageData, this);
            if (this.activityResumed) {
                PageList.dispatchOnResume(this);
            }
            PageList.initialize(this);
        }
    }

    private void initializeCalibration() {
        this.apiService.getCalibrationSettings()
                .imu.enableAccelerometerBiasCalibration()
                .done();
        this.sharedPageData = new SharedPageData(this.apiService);
        this.apiService.addMyoStateListener(this.sharedPageData);
    }

    @Override
    public void onServiceDisconnected(ComponentName componentName) {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = new Intent(this, MyoCaLib.class);

        //Continue only when service is bound
        if (!bindService(intent, this, Context.BIND_AUTO_CREATE)) {
            unbindService(this);
            Logger.error("MainActivity", "Could not bind to service.");
            this.finish();
        }
    }

    /**
     * Replaces the intent filter of the broadcast receiver with the current contents filter
     * Called automatically when the content changes.
     */
    public void refreshBroadcastReceiverFilter(String[] filterActions) {
        IntentFilter filter = new IntentFilter();
        for (String action : filterActions) {
            filter.addAction(action);
        }
        unregisterReceiver(broadcastReceiver);
        registerReceiver(broadcastReceiver, filter);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onResume() {
        super.onResume();
        this.activityResumed = true;
        if (this.activityInitialized) {
            PageList.dispatchOnResume(this);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onPause() {
        super.onPause();
        this.activityResumed = false;
        if (this.activityInitialized) {
            PageList.dispatchOnPause(this);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        PageList.dispatchOnDestroy();
        unbindService(this);
        unregisterReceiver(broadcastReceiver);
        this.activityInitialized = false;
    }

    /**
     * Overrides default back-button behaviour with one that closes the navigation drawer if it is open
     * and delegates the back button press to the shown content when it is not open.
     * The content decides, whether to close the app or not.
     */
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.exampleapp_drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (PageList.dispatchOnBackPressed()) {
                super.onBackPressed();
            }
        }
    }

    /**
     * Delegates the changing of the layout content to the content class: {@link PageList}.
     * And closes the navigation drawer afterwards.
     *
     * @param item The selected item, supplied by listener interface
     * @return false, the selected item is changed by the {@link PageList} class and should not be marked selected by the drawer.
     * @see PageList#changeToId(Activity, int)
     */
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        PageList.changeToId(this, item.getItemId());
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.exampleapp_drawer_layout);
        if (drawer != null) drawer.closeDrawer(GravityCompat.START);
        return false;
    }

    /**
     * Handles and dispatches button clicks
     *
     * @param view The clicked view
     */
    public void onClick(View view) {
        PageList.dispatchOnClick(view);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        PageList.dispatchOnRequestPermissionsResult(requestCode, permissions, grantResults);
    }
}
