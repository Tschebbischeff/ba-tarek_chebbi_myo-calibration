package com.tschebbischeff.exampleapp.lazyui.pages.live.emg;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import com.tschebbischeff.exampleapp.R;
import com.tschebbischeff.exampleapp.lazyui.pages.Page;
import com.tschebbischeff.exampleapp.lazyui.pages.live.emg.myolist.PageLiveEmgGraphMyoListAdapter;
import com.tschebbischeff.myocalib.api.IDataOutputListener;
import com.tschebbischeff.myocalib.api.InterpolatedEmgSignal;
import com.tschebbischeff.myocalib.internal.backend.calibration.FullWaveRectificationFilter;
import com.tschebbischeff.myocalib.internal.backend.calibration.MovingAverageFilter;
import com.tschebbischeff.myocalib.internal.backend.tools.StaticLib;
import eu.darken.myolib.Myo;

import java.util.HashMap;

/**
 * Handles callbacks for the live emg graph page
 */
public class PageLiveEmgGraph extends Page implements IDataOutputListener {

    /**
     * The adapter for the recyclerview with the myos and the emg data.
     */
    private PageLiveEmgGraphMyoListAdapter myoListAdapter = null;
    /**
     * Buffers emg data, such that it can be queried by the corresponding card showing the data.
     */
    private HashMap<Myo, Double[]> emgData;
    /**
     * Eigth moving average filters for each Myo.
     */
    private HashMap<Myo, MovingAverageFilter[]> mvgAvgFilter;

    /**
     * {@inheritDoc}
     */
    @Override
    protected int getLayoutResourceId() {
        return R.layout.exampleapp_lazyui_pages_liveemg;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String[] getIntentFilterActions() {
        return new String[0];
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onCreate() {
        this.emgData = new HashMap<>();
        this.mvgAvgFilter = new HashMap<>();
        this.myoListAdapter = new PageLiveEmgGraphMyoListAdapter(this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean onBeforeShow(boolean forced) {
        if (this.getSharedPageData().getReadyMyos().size() == 0) {
            this.setError(this.getActivity().getString(R.string.exampleapp_errors_no_calibrated_myos_title), this.getActivity().getString(R.string.exampleapp_errors_no_calibrated_myos_message));
        }
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onAfterShow() {
        RecyclerView myoList = this.getActivity().findViewById(R.id.exampleapp_lazyui_pages_liveemg_myolist);
        myoList.setHasFixedSize(true);
        myoList.setLayoutManager(new LinearLayoutManager(this.getActivity()));
        myoList.setAdapter(this.myoListAdapter);
        for (Myo myo : this.getSharedPageData().getReadyMyos()) {
            this.getSharedPageData().getMyoCalibrationService().getMyoCalibrationProfile(myo).addEmgListener(this);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean onBeforeHide(boolean forced) {
        for (Myo myo : this.getSharedPageData().getReadyMyos()) {
            this.getSharedPageData().getMyoCalibrationService().getMyoCalibrationProfile(myo).removeEmgListener(this);
        }
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onAfterHide() {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onDestroy() {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onEmgData(Myo myo, long timestamp, InterpolatedEmgSignal emgSignal) {
        if (this.isActive()) {
            if (!this.mvgAvgFilter.containsKey(myo)) {
                MovingAverageFilter[] filters = {
                        new MovingAverageFilter(250),
                        new MovingAverageFilter(250),
                        new MovingAverageFilter(250),
                        new MovingAverageFilter(250),
                        new MovingAverageFilter(250),
                        new MovingAverageFilter(250),
                        new MovingAverageFilter(250),
                        new MovingAverageFilter(250),
                };
                this.mvgAvgFilter.put(myo, filters);
            }
            Double[] averagedEmg = StaticLib.boxDoubleArray(emgSignal.getValues());
            for (int i = 0; i < averagedEmg.length; i++) { //Apply channel-wise moving average
                averagedEmg[i] = this.mvgAvgFilter.get(myo)[i].apply(timestamp, averagedEmg[i]).second;
            }
            this.emgData.put(myo, averagedEmg);
            final int myoIndex = this.getSharedPageData().getReadyMyos().indexOf(myo);
            this.getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        myoListAdapter.notifyItemChanged(myoIndex);
                    } catch (IllegalStateException ex) {
                        //Logger.warning("PageLiveRotGraph", "Update of recycler view failed due to IllegalStateException");
                    }
                }
            });
        } else {
            this.getSharedPageData().getMyoCalibrationService().getMyoCalibrationProfile(myo).removeEmgListener(this);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onImuData(Myo myo, long timestamp, double[] accelerometerData, double[] gyroscopeData, double[] orientationData) {

    }

    /**
     * Gets the data buffered for the given Myo
     *
     * @param myo The Myo to get the data for.
     * @return The buffered data for the Myo, or null, if the Myo has no buffered data.
     */
    public Double[] getEmgDataByMyo(Myo myo) {
        return this.emgData.get(myo);
    }
}
