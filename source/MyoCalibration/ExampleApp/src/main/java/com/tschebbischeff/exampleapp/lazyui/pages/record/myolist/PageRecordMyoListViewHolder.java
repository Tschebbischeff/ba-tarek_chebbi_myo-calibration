package com.tschebbischeff.exampleapp.lazyui.pages.record.myolist;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.tschebbischeff.exampleapp.R;
import com.tschebbischeff.exampleapp.lazyui.pages.record.PageRecord;
import eu.darken.myolib.Myo;

import java.util.ArrayList;

public class PageRecordMyoListViewHolder extends RecyclerView.ViewHolder {

    public CardView cardView;
    public TextView deviceName;
    public TextView deviceId;
    public CheckBox checkBox;

    public PageRecordMyoListViewHolder(LinearLayout rootView) {
        super(rootView);
        cardView = rootView.findViewById(R.id.exampleapp_lazyui_pages_record_cardsmyo_cardview);
        deviceName = rootView.findViewById(R.id.exampleapp_lazyui_pages_record_cardsmyo_devicename);
        deviceId = rootView.findViewById(R.id.exampleapp_lazyui_pages_record_cardsmyo_deviceid);
        checkBox = rootView.findViewById(R.id.exampleapp_lazyui_pages_record_cardsmyo_checkbox);
    }
}