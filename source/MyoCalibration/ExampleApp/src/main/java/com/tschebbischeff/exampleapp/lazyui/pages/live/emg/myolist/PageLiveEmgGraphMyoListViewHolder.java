package com.tschebbischeff.exampleapp.lazyui.pages.live.emg.myolist;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.tschebbischeff.exampleapp.R;

/**
 * Encapsulates information on the card that contains all data and the emg values of a single Myo.
 */
public class PageLiveEmgGraphMyoListViewHolder extends RecyclerView.ViewHolder {

    /**
     * The root card view.
     */
    public CardView cardView;
    /**
     * The text view containing the devices name.
     */
    public TextView deviceName;
    /**
     * The text view containing the devices id.
     */
    public TextView deviceId;
    /**
     * The first EMG value.
     */
    public TextView pod1;
    /**
     * The second EMG value.
     */
    public TextView pod2;
    /**
     * The third EMG value.
     */
    public TextView pod3;
    /**
     * The fourth EMG value.
     */
    public TextView pod4;
    /**
     * The fifth EMG value.
     */
    public TextView pod5;
    /**
     * The sixth EMG value.
     */
    public TextView pod6;
    /**
     * The seventh EMG value.
     */
    public TextView pod7;
    /**
     * The eighth EMG value.
     */
    public TextView pod8;

    /**
     * Constructor. Binds the layouts views to the public attributes.
     *
     * @param rootView The inflated view at the root of the layout.
     */
    public PageLiveEmgGraphMyoListViewHolder(LinearLayout rootView) {
        super(rootView);
        cardView = rootView.findViewById(R.id.exampleapp_lazyui_pages_liveemg_cardsmyo_cardview);
        deviceName = rootView.findViewById(R.id.exampleapp_lazyui_pages_liveemg_cardsmyo_devicename);
        deviceId = rootView.findViewById(R.id.exampleapp_lazyui_pages_liveemg_cardsmyo_deviceid);
        pod1 = rootView.findViewById(R.id.exampleapp_lazyui_pages_liveemg_cardsmyo_pod1);
        pod2 = rootView.findViewById(R.id.exampleapp_lazyui_pages_liveemg_cardsmyo_pod2);
        pod3 = rootView.findViewById(R.id.exampleapp_lazyui_pages_liveemg_cardsmyo_pod3);
        pod4 = rootView.findViewById(R.id.exampleapp_lazyui_pages_liveemg_cardsmyo_pod4);
        pod5 = rootView.findViewById(R.id.exampleapp_lazyui_pages_liveemg_cardsmyo_pod5);
        pod6 = rootView.findViewById(R.id.exampleapp_lazyui_pages_liveemg_cardsmyo_pod6);
        pod7 = rootView.findViewById(R.id.exampleapp_lazyui_pages_liveemg_cardsmyo_pod7);
        pod8 = rootView.findViewById(R.id.exampleapp_lazyui_pages_liveemg_cardsmyo_pod8);
    }
}