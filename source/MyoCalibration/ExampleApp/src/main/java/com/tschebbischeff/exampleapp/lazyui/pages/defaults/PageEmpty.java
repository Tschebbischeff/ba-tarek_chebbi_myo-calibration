package com.tschebbischeff.exampleapp.lazyui.pages.defaults;

import com.tschebbischeff.exampleapp.R;
import com.tschebbischeff.exampleapp.lazyui.pages.Page;

/**
 * An empty handler for the default empty content page. Does nothing.
 */
public class PageEmpty extends Page {

    /**
     * {@inheritDoc}
     */
    @Override
    protected int getLayoutResourceId() {
        return R.layout.exampleapp_lazyui_defaults_empty;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onCreate() {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean onBeforeShow(boolean forced) {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onAfterShow() {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean onBeforeHide(boolean forced) {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onAfterHide() {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onDestroy() {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String[] getIntentFilterActions() {
        return new String[0];
    }

}
