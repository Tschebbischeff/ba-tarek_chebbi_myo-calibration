package com.tschebbischeff.exampleapp.lazyui.pages.live.imu;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import com.tschebbischeff.exampleapp.R;
import com.tschebbischeff.exampleapp.lazyui.pages.Page;
import com.tschebbischeff.exampleapp.lazyui.pages.live.imu.myolist.PageLiveRotGraphMyoListAdapter;
import com.tschebbischeff.myocalib.api.IDataOutputListener;
import com.tschebbischeff.myocalib.api.InterpolatedEmgSignal;
import com.tschebbischeff.myocalib.internal.backend.calibration.MovingAverageFilter;
import eu.darken.myolib.Myo;

import java.util.HashMap;

/**
 * Handles callbacks for the live rotation graph page
 */
public class PageLiveRotGraph extends Page implements IDataOutputListener {

    /**
     * The adapter for the recyclerview with the myos and the imu data.
     */
    private PageLiveRotGraphMyoListAdapter myoListAdapter = null;
    /**
     * Three moving average filters for each Myo's accelerometer values.
     */
    private HashMap<Myo, MovingAverageFilter[]> mvgAvgFilterAccl;
    /**
     * Three moving average filters for each Myo's gyroscope values.
     */
    private HashMap<Myo, MovingAverageFilter[]> mvgAvgFilterGyro;
    /**
     * Buffered data from the accelerometer.
     */
    private HashMap<Myo, Double[]> accelerometerData;
    /**
     * Buffered data from the gyroscope.
     */
    private HashMap<Myo, Double[]> gyroscopeData;

    /**
     * {@inheritDoc}
     */
    @Override
    protected int getLayoutResourceId() {
        return R.layout.exampleapp_lazyui_pages_liverot;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String[] getIntentFilterActions() {
        return new String[0];
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onCreate() {
        this.mvgAvgFilterAccl = new HashMap<>();
        this.mvgAvgFilterGyro = new HashMap<>();
        this.accelerometerData = new HashMap<>();
        this.gyroscopeData = new HashMap<>();
        this.myoListAdapter = new PageLiveRotGraphMyoListAdapter(this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean onBeforeShow(boolean forced) {
        if (this.getSharedPageData().getReadyMyos().size() == 0) {
            this.setError(this.getActivity().getString(R.string.exampleapp_errors_no_calibrated_myos_title), this.getActivity().getString(R.string.exampleapp_errors_no_calibrated_myos_message));
        }
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onAfterShow() {
        RecyclerView myoList = this.getActivity().findViewById(R.id.exampleapp_lazyui_pages_liverot_myolist);
        myoList.setHasFixedSize(true);
        myoList.setLayoutManager(new LinearLayoutManager(this.getActivity()));
        myoList.setAdapter(this.myoListAdapter);
        for (Myo myo : this.getSharedPageData().getReadyMyos()) {
            this.getSharedPageData().getMyoCalibrationService().getMyoCalibrationProfile(myo).addImuListener(this);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean onBeforeHide(boolean forced) {
        for (Myo myo : this.getSharedPageData().getReadyMyos()) {
            this.getSharedPageData().getMyoCalibrationService().getMyoCalibrationProfile(myo).removeImuListener(this);
        }
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onAfterHide() {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onDestroy() {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onEmgData(Myo myo, long timestamp, InterpolatedEmgSignal emgSignal) {
        //No thanks
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onImuData(Myo myo, long timestamp, double[] accelerometerData, double[] gyroscopeData, double[] orientationData) {
        if (this.isActive()) {
            if (!this.mvgAvgFilterAccl.containsKey(myo)) {
                MovingAverageFilter[] filters = {
                        new MovingAverageFilter(50),
                        new MovingAverageFilter(50),
                        new MovingAverageFilter(50),
                };
                this.mvgAvgFilterAccl.put(myo, filters);
            }
            if (!this.mvgAvgFilterGyro.containsKey(myo)) {
                MovingAverageFilter[] filters = {
                        new MovingAverageFilter(50),
                        new MovingAverageFilter(50),
                        new MovingAverageFilter(50),
                };
                this.mvgAvgFilterGyro.put(myo, filters);
            }
            Double[] averagedAcclData = new Double[accelerometerData.length];
            for (int i = 0; i < accelerometerData.length; i++) {
                averagedAcclData[i] = this.mvgAvgFilterAccl.get(myo)[i].apply(timestamp, accelerometerData[i]).second;
            }
            Double[] averagedGyroData = new Double[gyroscopeData.length];
            for (int i = 0; i < gyroscopeData.length; i++) {
                averagedGyroData[i] = this.mvgAvgFilterGyro.get(myo)[i].apply(timestamp, gyroscopeData[i]).second;
            }
            this.accelerometerData.put(myo, averagedAcclData);
            this.gyroscopeData.put(myo, averagedGyroData);
            final int myoIndex = this.getSharedPageData().getReadyMyos().indexOf(myo);
            this.getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        myoListAdapter.notifyItemChanged(myoIndex);
                    } catch (IllegalStateException ex) {
                        //Logger.warning("PageLiveRotGraph", "Update of recycler view failed due to IllegalStateException");
                    }
                }
            });
        } else {
            this.getSharedPageData().getMyoCalibrationService().getMyoCalibrationProfile(myo).removeImuListener(this);
        }
    }

    /**
     * Gets the accelerometer data buffered for the given Myo
     *
     * @param myo The Myo to get the data for.
     * @return The buffered accelerometer data for the Myo, or null, if the Myo has no buffered data.
     */
    public Double[] getAccelerometerDataByMyo(Myo myo) {
        return this.accelerometerData.get(myo);
    }

    /**
     * Gets the gyroscope data buffered for the given Myo
     *
     * @param myo The Myo to get the data for.
     * @return The buffered gyroscope data for the Myo, or null, if the Myo has no buffered data.
     */
    public Double[] getGyroscopeDataByMyo(Myo myo) {
        return this.gyroscopeData.get(myo);
    }
}
