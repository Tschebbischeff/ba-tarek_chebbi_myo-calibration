package com.tschebbischeff.exampleapp.lazyui.pages.live.threed.opengl;

import android.annotation.SuppressLint;
import android.content.Context;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import com.tschebbischeff.exampleapp.R;
import com.tschebbischeff.exampleapp.StaticLib;
import com.tschebbischeff.exampleapp.lazyui.pages.live.threed.opengl.drawable.GLDrawable;
import com.tschebbischeff.exampleapp.lazyui.pages.live.threed.opengl.drawable.GLDrawableArm;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * The renderer for the OpenGL pipeline
 */
public class PageLive3dRenderer implements GLSurfaceView.Renderer {

    private static final String defaultShaderKey = "default";
    private final float[] matrixMVP = new float[16];
    private final float[] matrixProjection = new float[16];
    private final float[] matrixView = new float[16];
    private Context context;
    private HashMap<String, Integer> shaderList;
    private ArrayList<GLDrawable> drawableList;


    @SuppressLint("UseSparseArrays")
    public PageLive3dRenderer(Context context) {
        this.context = context;
        this.shaderList = new HashMap<>();
        this.drawableList = new ArrayList<>();
    }

    private static int loadShader(int type, String shaderCode) {
        int shader = GLES20.glCreateShader(type);
        GLES20.glShaderSource(shader, shaderCode);
        GLES20.glCompileShader(shader);
        return shader;
    }

    @Override
    public void onSurfaceCreated(GL10 unused, EGLConfig config) {
        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        this.compileAndAddShaderSet(defaultShaderKey,
                StaticLib.readRawTextFile(this.context, R.raw.exampleapp_shaders_vertex_default),
                StaticLib.readRawTextFile(this.context, R.raw.exampleapp_shaders_fragment_default));
        this.addDrawable(new GLDrawableArm(this.getDefaultShaderProgram()));
    }

    @Override
    public void onSurfaceChanged(GL10 unused, int width, int height) {
        GLES20.glViewport(0, 0, width, height);
    }

    @Override
    public void onDrawFrame(GL10 unused) {
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);
        for (GLDrawable drawable : this.drawableList) {
            drawable.requestDraw();
        }
    }

    public void addDrawable(GLDrawable drawable) {
        this.drawableList.add(drawable);
    }

    public int getDefaultShaderProgram() {
        return getShaderProgram(defaultShaderKey);
    }

    public int getShaderProgram(String key) {
        return this.shaderList.get(key) == null ? 0 : this.shaderList.get(key);
    }

    private int compileAndAddShaderSet(String key, String vertexShaderCode, String fragmentShaderCode) {
        if (vertexShaderCode != null && fragmentShaderCode != null) {
            int vertexShader = loadShader(GLES20.GL_VERTEX_SHADER, vertexShaderCode);
            int fragmentShader = loadShader(GLES20.GL_FRAGMENT_SHADER, fragmentShaderCode);

            int shaderProgram = GLES20.glCreateProgram();
            GLES20.glAttachShader(shaderProgram, vertexShader);
            GLES20.glAttachShader(shaderProgram, fragmentShader);
            GLES20.glLinkProgram(shaderProgram);

            this.shaderList.put(key, shaderProgram);
            return shaderProgram;
        }
        this.shaderList.put(key, 0);
        return 0;
    }
}
