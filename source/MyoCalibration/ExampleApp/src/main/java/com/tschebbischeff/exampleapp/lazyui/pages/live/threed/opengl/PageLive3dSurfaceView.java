package com.tschebbischeff.exampleapp.lazyui.pages.live.threed.opengl;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.util.AttributeSet;

/**
 * The surface view for the OpenGL context.
 */
public class PageLive3dSurfaceView extends GLSurfaceView {

    private PageLive3dRenderer renderer;

    private boolean initialized = false;

    public PageLive3dSurfaceView(Context context) {
        super(context);
        this.initialize(context);
    }

    public PageLive3dSurfaceView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.initialize(context);
    }

    private void initialize(Context context) {
        if (!this.initialized) {
            this.initialized = true;
            setEGLContextClientVersion(2);
            this.renderer = new PageLive3dRenderer(context);
            this.setRenderer(this.renderer);
        }
    }

    /*@Override
    public boolean onTouchEvent(MotionEvent event) {
        return super.onTouchEvent(event);
    }*/
}
