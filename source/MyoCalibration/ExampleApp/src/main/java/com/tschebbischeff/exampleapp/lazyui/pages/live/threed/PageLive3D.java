package com.tschebbischeff.exampleapp.lazyui.pages.live.threed;

import com.tschebbischeff.exampleapp.R;
import com.tschebbischeff.exampleapp.lazyui.pages.Page;

/**
 * Handles callbacks for the live 3D view page
 */
public class PageLive3D extends Page {

    @Override
    protected int getLayoutResourceId() {
        return R.layout.exampleapp_lazyui_pages_live3d;
    }

    @Override
    protected String[] getIntentFilterActions() {
        return new String[0];
    }

    @Override
    protected void onCreate() {
    }

    @Override
    protected boolean onBeforeShow(boolean forced) {
        if (this.getSharedPageData().getReadyMyos().size() == 0) {
            this.setError(this.getActivity().getString(R.string.exampleapp_errors_no_calibrated_myos_title), this.getActivity().getString(R.string.exampleapp_errors_no_calibrated_myos_message));
        }
        return true;
    }

    @Override
    protected void onAfterShow() {
    }

    @Override
    protected boolean onBeforeHide(boolean forced) {
        return true;
    }

    @Override
    protected void onAfterHide() {
    }

    @Override
    protected void onDestroy() {
    }

}
