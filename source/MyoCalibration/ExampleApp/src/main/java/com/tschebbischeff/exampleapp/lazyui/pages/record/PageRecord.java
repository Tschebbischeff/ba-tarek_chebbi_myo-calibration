package com.tschebbischeff.exampleapp.lazyui.pages.record;

import android.os.Environment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import com.tschebbischeff.common.android.logger.Logger;
import com.tschebbischeff.exampleapp.R;
import com.tschebbischeff.exampleapp.lazyui.pages.Page;
import com.tschebbischeff.exampleapp.lazyui.pages.record.myolist.PageRecordMyoListAdapter;
import com.tschebbischeff.myocalib.api.IDataOutputListener;
import com.tschebbischeff.myocalib.api.IRawDataOutputListener;
import com.tschebbischeff.myocalib.api.InterpolatedEmgSignal;
import eu.darken.myolib.Myo;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Handles callbacks for the record page
 */
public class PageRecord extends Page implements IDataOutputListener, IRawDataOutputListener {

    private PageRecordMyoListAdapter myoListAdapter = null;
    private volatile boolean recording;
    private ArrayList<Myo> checkedMyos;
    private HashMap<Myo, FileOutputStream[]> outputStreams;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.exampleapp_lazyui_pages_record;
    }

    @Override
    protected String[] getIntentFilterActions() {
        return new String[0];
    }

    @Override
    protected void onCreate() {
        this.recording = false;
        this.outputStreams = new HashMap<>();
        this.checkedMyos = new ArrayList<>();
        this.myoListAdapter = new PageRecordMyoListAdapter(this);
    }

    @Override
    protected boolean onBeforeShow(boolean forced) {
        if (this.getSharedPageData().getReadyMyos().size() == 0) {
            this.setError(this.getActivity().getString(R.string.exampleapp_errors_no_calibrated_myos_title), this.getActivity().getString(R.string.exampleapp_errors_no_calibrated_myos_message));
        }
        return true;
    }

    @Override
    protected void onAfterShow() {
        RecyclerView myoList = this.getActivity().findViewById(R.id.exampleapp_lazyui_pages_record_myolist);
        myoList.setHasFixedSize(true);
        myoList.setLayoutManager(new LinearLayoutManager(this.getActivity()));
        myoList.setAdapter(this.myoListAdapter);
    }

    @Override
    protected boolean onBeforeHide(boolean forced) {
        if (forced) {
            this.stopRecording();
        }
        return forced || this.recording;
    }

    @Override
    protected void onAfterHide() {
    }

    @Override
    protected void onDestroy() {
        if (this.recording) {
            this.stopRecording();
        }
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.exampleapp_lazyui_pages_record_button) {
            Button button = this.getActivity().findViewById(R.id.exampleapp_lazyui_pages_record_button);
            if (button.getText() == this.getActivity().getString(R.string.exampleapp_lazyui_pages_record_buttonstart)) {
                this.startRecording();
            } else {
                this.stopRecording();
            }
        }
    }

    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state);
    }

    public File getSavingDirectory(String directory) {
        File file = new File(Environment.getExternalStorageDirectory(), "MyoCaLib Recorder" + File.separatorChar + directory);
        if (!file.mkdirs()) {
            Logger.error("PageRecord", "Directory could not be created.");
            return null;
        }
        return file;
    }

    public ArrayList<Myo> getCheckedMyos() {
        return checkedMyos;
    }

    private void startRecording() {
        if (this.isExternalStorageWritable() && !this.recording) {
            synchronized (this) {
                Logger.debug("Recorder", "Recording started.");
                Button button = this.getActivity().findViewById(R.id.exampleapp_lazyui_pages_record_button);
                EditText folderNameEdit = this.getActivity().findViewById(R.id.exampleapp_lazyui_pages_record_recordingname);
                button.setText(R.string.exampleapp_lazyui_pages_record_buttonend);
                folderNameEdit.setEnabled(false);
                File directory = this.getSavingDirectory(folderNameEdit.getText().toString());
                Logger.debug("Recorder", "Recording directory is: " + directory.toString());
                try {
                    this.myoListAdapter.notifyItemRangeChanged(0, this.myoListAdapter.getItemCount());
                } catch (IllegalStateException ex) {
                    Logger.warning("PageRecord", "Update of recycler view failed due to IllegalStateException");
                }
                for (Myo myo : this.checkedMyos) {
                    if (!this.outputStreams.containsKey(myo)) {
                        this.outputStreams.put(myo, new FileOutputStream[8]);
                        File[] files = new File[8];
                        files[0] = new File(directory, myo.getDeviceAddress().replace(File.separatorChar, '_') + "_CAL_EMG.csv");
                        files[1] = new File(directory, myo.getDeviceAddress().replace(File.separatorChar, '_') + "_CAL_ACCL.csv");
                        files[2] = new File(directory, myo.getDeviceAddress().replace(File.separatorChar, '_') + "_CAL_GYRO.csv");
                        files[3] = new File(directory, myo.getDeviceAddress().replace(File.separatorChar, '_') + "_CAL_ORNT.csv");
                        files[4] = new File(directory, myo.getDeviceAddress().replace(File.separatorChar, '_') + "_RAW_EMG.csv");
                        files[5] = new File(directory, myo.getDeviceAddress().replace(File.separatorChar, '_') + "_RAW_ACCL.csv");
                        files[6] = new File(directory, myo.getDeviceAddress().replace(File.separatorChar, '_') + "_RAW_GYRO.csv");
                        files[7] = new File(directory, myo.getDeviceAddress().replace(File.separatorChar, '_') + "_RAW_ORNT.csv");
                        for (int i = 0; i < this.outputStreams.get(myo).length; i++) {
                            try {
                                this.outputStreams.get(myo)[i] = new FileOutputStream(files[i]);
                                if (i == 0 || i == 4) {
                                    this.outputStreams.get(myo)[i].write(("timestamp, emg0, emg1, emg2, emg3, emg4, emg5, emg6, emg7" + "\r\n").getBytes());
                                } else if (i == 3 || i == 7) {
                                    this.outputStreams.get(myo)[i].write(("timestamp, w, i, j, k" + "\r\n").getBytes());
                                } else {
                                    this.outputStreams.get(myo)[i].write(("timestamp, x, y, z" + "\r\n").getBytes());
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    this.getSharedPageData().getMyoCalibrationService().getMyoCalibrationProfile(myo).addImuEmgListener(this);
                    this.getSharedPageData().getMyoCalibrationService().getMyoCalibrationProfile(myo).addRawImuEmgListener(this);
                }
            }
            this.recording = true;
        }
    }

    private void stopRecording() {
        if (this.recording) {
            this.recording = false;
            synchronized (this) {
                Logger.debug("Recorder", "Recording stopped.");
                Button button = this.getActivity().findViewById(R.id.exampleapp_lazyui_pages_record_button);
                EditText folderNameEdit = this.getActivity().findViewById(R.id.exampleapp_lazyui_pages_record_recordingname);
                button.setText(R.string.exampleapp_lazyui_pages_record_buttonstart);
                folderNameEdit.setEnabled(true);
                try {
                    this.myoListAdapter.notifyItemRangeChanged(0, this.myoListAdapter.getItemCount());
                } catch (IllegalStateException ex) {
                    Logger.warning("PageRecord", "Update of recycler view failed due to IllegalStateException");
                }
                for (Myo myo : this.checkedMyos) {
                    this.getSharedPageData().getMyoCalibrationService().getMyoCalibrationProfile(myo).removeImuEmgListener(this);
                    this.getSharedPageData().getMyoCalibrationService().getMyoCalibrationProfile(myo).removeRawImuEmgListener(this);
                    for (int i = 0; i < this.outputStreams.get(myo).length; i++) {
                        try {
                            this.outputStreams.get(myo)[i].flush();
                            this.outputStreams.get(myo)[i].close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    this.outputStreams.remove(myo);
                }
            }
        }
    }

    public boolean isRecording() {
        return this.recording;
    }

    @Override
    public void onEmgData(Myo myo, long timestamp, InterpolatedEmgSignal emgSignal) {
        if (this.recording) {
            synchronized (this) {
                try {
                    double[] samples = emgSignal.getValues();
                    this.outputStreams.get(myo)[0].write(Double.toString(timestamp).getBytes());
                    for (int i = 0; i < samples.length; i++) {
                        this.outputStreams.get(myo)[0].write((", " + Double.toString(samples[i])).getBytes());
                        if (i == samples.length - 1) {
                            this.outputStreams.get(myo)[0].write(("\r\n").getBytes());
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onImuData(Myo myo, long timestamp, double[] accelerometerData, double[] gyroscopeData, double[] orientationData) {
        if (this.recording) {
            synchronized (this) {
                try {
                    this.outputStreams.get(myo)[1].write(Double.toString(timestamp).getBytes());
                    for (int i = 0; i < accelerometerData.length; i++) {
                        this.outputStreams.get(myo)[1].write((", " + Double.toString(accelerometerData[i])).getBytes());
                        if (i == accelerometerData.length - 1) {
                            this.outputStreams.get(myo)[1].write(("\r\n").getBytes());
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    this.outputStreams.get(myo)[2].write(Double.toString(timestamp).getBytes());
                    for (int i = 0; i < gyroscopeData.length; i++) {
                        this.outputStreams.get(myo)[2].write((", " + Double.toString(gyroscopeData[i])).getBytes());
                        if (i == gyroscopeData.length - 1) {
                            this.outputStreams.get(myo)[2].write(("\r\n").getBytes());
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    this.outputStreams.get(myo)[3].write(Double.toString(timestamp).getBytes());
                    for (int i = 0; i < orientationData.length; i++) {
                        this.outputStreams.get(myo)[3].write((", " + Double.toString(orientationData[i])).getBytes());
                        if (i == orientationData.length - 1) {
                            this.outputStreams.get(myo)[3].write(("\r\n").getBytes());
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onRawEmgData(Myo myo, long timestamp, double[] data) {
        if (this.recording) {
            synchronized (this) {
                try {
                    this.outputStreams.get(myo)[4].write(Double.toString(timestamp).getBytes());
                    for (int i = 0; i < data.length; i++) {
                        this.outputStreams.get(myo)[4].write((", " + Double.toString(data[i])).getBytes());
                        if (i == data.length - 1) {
                            this.outputStreams.get(myo)[4].write(("\r\n").getBytes());
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onRawImuData(Myo myo, long timestamp, double[] accelerometerData, double[] gyroscopeData, double[] orientationData) {
        if (this.recording) {
            synchronized (this) {
                try {
                    this.outputStreams.get(myo)[5].write(Double.toString(timestamp).getBytes());
                    for (int i = 0; i < accelerometerData.length; i++) {
                        this.outputStreams.get(myo)[5].write((", " + Double.toString(accelerometerData[i])).getBytes());
                        if (i == accelerometerData.length - 1) {
                            this.outputStreams.get(myo)[5].write(("\r\n").getBytes());
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    this.outputStreams.get(myo)[6].write(Double.toString(timestamp).getBytes());
                    for (int i = 0; i < gyroscopeData.length; i++) {
                        this.outputStreams.get(myo)[6].write((", " + Double.toString(gyroscopeData[i])).getBytes());
                        if (i == gyroscopeData.length - 1) {
                            this.outputStreams.get(myo)[6].write(("\r\n").getBytes());
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    this.outputStreams.get(myo)[7].write(Double.toString(timestamp).getBytes());
                    for (int i = 0; i < orientationData.length; i++) {
                        this.outputStreams.get(myo)[7].write((", " + Double.toString(orientationData[i])).getBytes());
                        if (i == orientationData.length - 1) {
                            this.outputStreams.get(myo)[7].write(("\r\n").getBytes());
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
