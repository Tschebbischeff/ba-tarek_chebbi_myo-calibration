package com.tschebbischeff.exampleapp.lazyui.pages.live.imu.myolist;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.tschebbischeff.exampleapp.R;
import com.tschebbischeff.exampleapp.lazyui.pages.live.imu.PageLiveRotGraph;
import eu.darken.myolib.Myo;

import java.util.Locale;


/**
 * Adapter for the recyclerview showing the myo cards, which also contain the IMU data for the myo.
 */
public class PageLiveRotGraphMyoListAdapter extends RecyclerView.Adapter<PageLiveRotGraphMyoListViewHolder> {

    /**
     * The originating page, for obtaining stored data.
     */
    private PageLiveRotGraph source;

    /**
     * Constructor.
     *
     * @param source The source page, for obtaining stored data.
     */
    public PageLiveRotGraphMyoListAdapter(PageLiveRotGraph source) {
        this.source = source;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PageLiveRotGraphMyoListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LinearLayout v = (LinearLayout) LayoutInflater.from(parent.getContext()).inflate(R.layout.exampleapp_lazyui_pages_liverot_cardsmyo, parent, false);
        return new PageLiveRotGraphMyoListViewHolder(v);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onBindViewHolder(PageLiveRotGraphMyoListViewHolder holder, int position) {
        Myo myo = this.source.getSharedPageData().getReadyMyos().get(position);
        holder.deviceName.setText(myo.getDeviceName());
        holder.deviceId.setText(myo.getDeviceAddress());
        Double[] accelerometerData = this.source.getAccelerometerDataByMyo(myo);
        Double[] gyroscopeData = this.source.getGyroscopeDataByMyo(myo);
        if (accelerometerData == null) {
            accelerometerData = new Double[]{0d, 0d, 0d};
        }
        if (gyroscopeData == null) {
            gyroscopeData = new Double[]{0d, 0d, 0d};
        }
        holder.accelerometerX.setText(format(accelerometerData[0]));
        holder.accelerometerY.setText(format(accelerometerData[1]));
        holder.accelerometerZ.setText(format(accelerometerData[2]));
        holder.gyroscopeX.setText(format(gyroscopeData[0]));
        holder.gyroscopeY.setText(format(gyroscopeData[1]));
        holder.gyroscopeZ.setText(format(gyroscopeData[2]));
    }

    /**
     * Formats the IMU value to a string with 3 digits after the comma.
     *
     * @param d The IMU value.
     * @return The formatted EMG value as a string.
     */
    private String format(double d) {
        return String.format(Locale.getDefault(), "%+.3f", d);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getItemCount() {
        return this.source.getSharedPageData().getReadyMyos().size();
    }

}
